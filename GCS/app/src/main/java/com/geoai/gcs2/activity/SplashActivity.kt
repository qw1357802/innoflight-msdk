package com.geoai.gcs2.activity

import android.app.Activity
import android.os.Bundle
import android.util.Log
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import com.geoai.basiclib.engine.permission.requestPermission
import com.geoai.basiclib.ext.gotoActivity
import com.geoai.gcs2.R
import java.util.concurrent.atomic.AtomicBoolean

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-15 17:15.
 * @Description :
 */
class SplashActivity: Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        installSplashScreen().apply {

            setKeepOnScreenCondition { false }

            setOnExitAnimationListener {

                requestPermission(*resources.getStringArray(R.array.application_permission_array)) { isAll ->
                    if (!isAll) {
                        finish()
                        return@requestPermission
                    }
                    finish()
                    gotoActivity<MainActivity>()
                }
            }
        }
    }
}