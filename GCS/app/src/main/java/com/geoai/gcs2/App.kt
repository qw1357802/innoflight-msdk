package com.geoai.gcs2

import android.content.Context
import androidx.multidex.MultiDex
import com.geoai.basiclib.base.BaseApplication
import com.zkyt.lib_msdk_ext.MSDKManager
import dagger.hilt.android.HiltAndroidApp
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-02 16:50.
 * @Description :
 */
@HiltAndroidApp
class App : BaseApplication(){
    override fun onCreate() {
        super.onCreate()

        MSDKManager.init(this)
    }
}