package com.geoai.gcs2.activity.mvi.vm

import android.util.Log
import com.geoai.basiclib.base.mvi.BaseEISViewModel
import com.geoai.basiclib.ext.SP
import com.geoai.basiclib.ext.putString
import com.geoai.basiclib.utils.CommUtils.getString
import com.geoai.gcs2.R
import com.geoai.gcs2.activity.mvi.eis.MainActivityEffect
import com.geoai.gcs2.activity.mvi.eis.MainActivityIntent
import com.geoai.gcs2.activity.mvi.eis.MainActivityState
import com.geoai.modservice.di.ARouterPath
import com.geoai.modservice.di.sp.BaseConstants

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-08 09:44.
 * @Description :
 */
class MainActivityViewModel: BaseEISViewModel<MainActivityEffect, MainActivityIntent, MainActivityState>() {

    private var lastToggleMenuTs = -1L

    override fun initUiState(): MainActivityState {
        return MainActivityState.BigMap_MiniFpv
    }

    override fun handleIntent(intent: MainActivityIntent) {
        when (intent) {
            MainActivityIntent.ShowControlMenuItemSelectorDialog -> showControlMenuItemSelectorDialog(getControlMenuItems())
            MainActivityIntent.ShowStatusMenuItemSelectorDialog -> showStatusMenuItemSelectorDialog(getStatusMenuItems())
            MainActivityIntent.BindMenuByLastLaunch -> buildLastLaunchRlMenu()
            MainActivityIntent.BindStatusByLastLaunch -> buildLastLaunchStatusMenu()
            is MainActivityIntent.SaveRlMenuItemSelected -> SP().putString(BaseConstants.RF_MENU_SELECTED_INDEX, intent.keyword)
            is MainActivityIntent.SaveLbMenuItemSelected -> SP().putString(BaseConstants.LS_MENU_SELECTED_INDEX, intent.keyword)
            is MainActivityIntent.ToggleMapAndFpvView -> toggleMapAndFpvView(intent.toggleTs)
        }
    }

    private fun showControlMenuItemSelectorDialog(controlMenuItems: MutableList<Pair<String, String>>) {
        launchOnUI { sendEffect(MainActivityEffect.DisplayRLControlMenuSelectorDialog(controlMenuItems)) }
    }

    private fun showStatusMenuItemSelectorDialog(controlMenuItems: MutableList<Pair<String, String>>) {
        launchOnUI { sendEffect(MainActivityEffect.DisplayLbStatusMenuSelectorDialog(controlMenuItems)) }
    }

    private fun buildLastLaunchRlMenu() {
        when (val items = SP().getString(BaseConstants.RF_MENU_SELECTED_INDEX, ARouterPath.PATH_STATUS_EMPTY)) {
            ARouterPath.PATH_STATUS_EMPTY,
            ARouterPath.PATH_CONTROL_FC_CONTROL,
            ARouterPath.PATH_CONTROL_GIMBAL_CONTROL,
            ARouterPath.PATH_CONTROL_MISSION_CONTROL,
            ARouterPath.PATH_CONTROL_CAMERA_CONTROL,
            ARouterPath.PATH_CONTROL_CAMERA_NV2_CONTROL,
            ARouterPath.PATH_CONTROL_CAMERA_NV3_CONTROL,
            ARouterPath.PATH_CONTROL_CELLULAR_CONTROL,
            ARouterPath.PATH_CONTROL_COMPONENT_MAP_CONTROL -> {
                launchOnUI { sendEffect(MainActivityEffect.DisplayRlControlMenuByLastLaunch(items)) }
            }
        }
    }

    private fun buildLastLaunchStatusMenu() {
        when (val items = SP().getString(BaseConstants.LS_MENU_SELECTED_INDEX, ARouterPath.PATH_STATUS_EMPTY)) {
            ARouterPath.PATH_STATUS_EMPTY,
            ARouterPath.PATH_STATUS_FC,
            ARouterPath.PATH_STATUS_FC_PARACHUTE,
            ARouterPath.PATH_STATUS_MISSION,
            ARouterPath.PATH_STATUS_GIMBAL,
            ARouterPath.PATH_STATUS_CAMERA_MODE,
            ARouterPath.PATH_STATUS_CAMERA_PARAM,
            ARouterPath.PATH_STATUS_CELLULAR -> {
                launchOnUI { sendEffect(MainActivityEffect.DisplayLbStatusMenuByLastLaunch(items)) }
            }
        }
    }

    private fun getStatusMenuItems(): MutableList<Pair<String, String>> {
        return mutableListOf(
            Pair(getString(R.string.main_lb_menu_none), ""),
            Pair(getString(R.string.main_lb_menu_flight_status), ARouterPath.PATH_STATUS_FC),
            Pair(getString(R.string.main_lb_menu_flight_parachute), ARouterPath.PATH_STATUS_FC_PARACHUTE),
            Pair(getString(R.string.main_lb_menu_mission_status), ARouterPath.PATH_STATUS_MISSION),
            Pair(getString(R.string.main_lb_menu_gimbal_status), ARouterPath.PATH_STATUS_GIMBAL),
            Pair(getString(R.string.main_lb_menu_cellular_status), ARouterPath.PATH_STATUS_CELLULAR),
            Pair(getString(R.string.main_lb_menu_camera_mdoe_status), ARouterPath.PATH_STATUS_CAMERA_MODE),
            Pair(getString(R.string.main_lb_menu_camera_param_status), ARouterPath.PATH_STATUS_CAMERA_PARAM)
        )
    }

    private fun getControlMenuItems(): MutableList<Pair<String, String>> {
        return mutableListOf(
            Pair(getString(R.string.main_lb_menu_none), ""),
            Pair(getString(R.string.main_rl_menu_map_control), ARouterPath.PATH_CONTROL_COMPONENT_MAP_CONTROL),
            Pair(getString(R.string.main_rl_menu_fc_control), ARouterPath.PATH_CONTROL_FC_CONTROL),
            Pair(getString(R.string.main_rl_menu_gimbal_control), ARouterPath.PATH_CONTROL_GIMBAL_CONTROL),
            Pair(getString(R.string.main_rl_menu_camera_control), ARouterPath.PATH_CONTROL_CAMERA_CONTROL),
            Pair(getString(R.string.main_rl_menu_camera_nv2_control), ARouterPath.PATH_CONTROL_CAMERA_NV2_CONTROL),
            Pair(getString(R.string.main_rl_menu_camera_nv3_control), ARouterPath.PATH_CONTROL_CAMERA_NV3_CONTROL),
            Pair(getString(R.string.main_rl_menu_mission_control), ARouterPath.PATH_CONTROL_MISSION_CONTROL),
            Pair(getString(R.string.main_rl_menu_cellular_control), ARouterPath.PATH_CONTROL_CELLULAR_CONTROL)
        )
    }

    private fun toggleMapAndFpvView(toggleTs: Long) {
        //limit toggle speed 500ms.
        if (toggleTs - lastToggleMenuTs > 500) {
            when (uiStateFlow.value) {
                MainActivityState.BigFpv_IconMap -> launchOnUI { sendUiState(MainActivityState.BigFpv_MiniMap) }
                MainActivityState.BigFpv_MiniMap -> launchOnUI { sendUiState(MainActivityState.BigMap_MiniFpv) }
                MainActivityState.BigMap_MiniFpv -> launchOnUI { sendUiState(MainActivityState.BigFpv_IconMap) }
            }
            lastToggleMenuTs = toggleTs
        }
    }
}