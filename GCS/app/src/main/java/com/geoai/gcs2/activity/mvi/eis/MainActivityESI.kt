package com.geoai.gcs2.activity.mvi.eis

import com.geoai.basiclib.base.mvi.IUIEffect
import com.geoai.basiclib.base.mvi.IUiIntent
import com.geoai.basiclib.base.mvi.IUiState

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-07 14:09.
 * @Description :
 */
sealed class MainActivityEffect: IUIEffect {
    data class DisplayRLControlMenuSelectorDialog(val controlMenuItems: MutableList<Pair<String, String>>): MainActivityEffect()
    data class DisplayLbStatusMenuSelectorDialog(val controlMenuItems: MutableList<Pair<String, String>>): MainActivityEffect()
    data class DisplayRlControlMenuByLastLaunch(val aRouterPath: String): MainActivityEffect()
    data class DisplayLbStatusMenuByLastLaunch(val aRouterPath: String): MainActivityEffect()
}

sealed class MainActivityIntent: IUiIntent {
    object ShowControlMenuItemSelectorDialog: MainActivityIntent()
    object ShowStatusMenuItemSelectorDialog: MainActivityIntent()
    object BindMenuByLastLaunch: MainActivityIntent()
    object BindStatusByLastLaunch: MainActivityIntent()
    data class ToggleMapAndFpvView(val toggleTs: Long): MainActivityIntent()
    data class SaveRlMenuItemSelected(val keyword: String): MainActivityIntent()
    data class SaveLbMenuItemSelected(val keyword: String): MainActivityIntent()
}

sealed class MainActivityState: IUiState {
    object BigMap_MiniFpv: MainActivityState()
    object BigFpv_IconMap: MainActivityState()
    object BigFpv_MiniMap: MainActivityState()
}