plugins {
    id("com.android.application")
}

apply<DefaultGradlePlugin>()

android {
    namespace = "com.geoai.gcs2"
    defaultConfig {
        applicationId = ProjectConfig.applicationId
    }
}

dependencies {

    api(project(":lib-base"))
    api(project(":lib-service"))
    implementation(project(":lib-ui-map"))
    implementation(project(":lib-msdk-ext"))

    implementation(project(":feat-map-control"))
    implementation(project(":feat-fc-control"))
    implementation(project(":feat-cam-control"))
    implementation(project(":feat-gimbal-control"))
    implementation(project(":feat-mission-control"))
    implementation(project(":feat-crest-control"))
    implementation(project(":feat-status-bar"))
    implementation(project(":feat-geoai-video"))
//    implementation(project(":feat-egg-dropper"))

    implementation ("androidx.core:core-splashscreen:1.0.0")
}