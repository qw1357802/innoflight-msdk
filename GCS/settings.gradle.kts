pluginManagement {
    repositories {
        google()
        mavenCentral()
        gradlePluginPortal()
    }
}

dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        maven("https://maven.aliyun.com/nexus/content/groups/public/")
        maven("https://maven.aliyun.com/nexus/content/repositories/jcenter")
        maven("https://maven.aliyun.com/nexus/content/repositories/google")
        maven("https://maven.aliyun.com/nexus/content/repositories/gradle-plugin")
        maven("https://jitpack.io")
        maven("https://api.mapbox.com/downloads/v2/releases/maven") {
            authentication.create<BasicAuthentication>("basic")
            credentials {
                this.username = "mapbox"
                this.password = "sk.eyJ1IjoiaHVhbmdqaW5xaW5nIiwiYSI6ImNrczV4MXE2ZTA1eWkyb29jeWJ6N25xZzcifQ.Ha_rC9xAf49LrVUqXZj99g"
            }
        }
        maven("http://121.37.203.145:8081/repository/geoai-sonatype/") {
            isAllowInsecureProtocol = true
        }
        google()
        mavenCentral()
        jcenter()
    }
}

rootProject.name = "GCS"
include(":app")
include(":lib-base")
include(":lib-ui-map")
include(":lib-service")
include(":lib-ui-core")
include(":lib-msdk-ext")
include(":feat-map-control")
include(":feat-status-bar")
include(":feat-fc-control")
include(":feat-gimbal-control")
include(":feat-mission-control")
include(":feat-cam-control")
include(":feat-geoai-video")
//include(":feat-egg-dropper")
include(":feat-crest-control")
