plugins {
    id("com.android.library")
}

// 使用自定义插件
apply<UIModuleGradlePlugin>()

android {
    namespace = "com.geoai.featstatusbar"
}

dependencies {

    api(project(":lib-base"))
    api(project(":lib-ui-core"))

    implementation(project(":lib-msdk-ext"))
}