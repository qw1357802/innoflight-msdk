package com.geoai.featstatusbar.mvi.eis

import com.geoai.basiclib.base.mvi.IUIEffect
import com.geoai.basiclib.base.mvi.IUiIntent
import com.geoai.basiclib.base.mvi.IUiState

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-07 09:41.
 * @Description :
 */
sealed class BatteryStatusEffect: IUIEffect {

}

sealed class BatteryStatusIntent: IUiIntent {

}

sealed class BatteryStatusState: IUiState {
    object INIT : BatteryStatusState()
}