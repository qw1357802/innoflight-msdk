package com.geoai.featstatusbar.flightcheck.view

import android.content.Context
import android.os.BatteryManager
import android.os.Bundle
import androidx.core.content.ContextCompat
import com.geoai.basiclib.base.fragment.BaseVVDFragment
import com.geoai.featstatusbar.R
import com.geoai.featstatusbar.databinding.FcheckLyItemDeviceStatusBinding
import com.geoai.featstatusbar.flightcheck.vm.ItemDeviceStatusVM
import io.reactivex.rxkotlin.addTo

/**
@author : XPY on 2024/3/7 13:54
@description:
 */
class ItemDeviceStatusCellView: BaseVVDFragment<ItemDeviceStatusVM, FcheckLyItemDeviceStatusBinding>() {

    private val DEFAULT_VAL = mContext.getString(R.string.fcheck_topbar_val_not_conn)
    private val TXT_COLOR_BLUE = mContext.getColor(R.color.base_blue)
    private val TXT_COLOR_DARK_BLUE = mContext.getColor(R.color.base_dark_blue)
    private val TXT_COLOR_YELLOW = mContext.getColor(R.color.base_yellow)
    private val TXT_COLOR_RED = mContext.getColor(R.color.base_red)


    override fun init(savedInstanceState: Bundle?) {
        mViewModel.getGoHomePointSetStateObservable().subscribe {
            if (it){
                mBinding.tvFCheckSetHome.text = mContext.getString(R.string.fcheck_item_home_have_set)
                mBinding.tvFCheckSetHome.setTextColor(TXT_COLOR_BLUE)
            }else{
                mBinding.tvFCheckSetHome.text = mContext.getString(R.string.fcheck_item_home_not_set)
                mBinding.tvFCheckSetHome.setTextColor(TXT_COLOR_YELLOW)
            }
            val leftDrawable = ContextCompat.getDrawable(mContext, if (it) R.mipmap.fcheck_item_have_set_home else R.mipmap.fcheck_item_not_set_home)
            leftDrawable?.setBounds(
                0,
                0,
                leftDrawable.minimumWidth,
                leftDrawable.minimumHeight
            )
            mBinding.tvFCheckSetHome.setCompoundDrawables(leftDrawable,null,null,null)
        }.addTo(mViewModel.compositeDisposable)

        mViewModel.getRtkEnableStateObservable().subscribe {
            if (it){
                mBinding.tvFCheckOpenRTK.text = mContext.getString(R.string.fcheck_item_rtk_have_start)
                mBinding.tvFCheckOpenRTK.setTextColor(TXT_COLOR_BLUE)
            }else{
                mBinding.tvFCheckOpenRTK.text = mContext.getString(R.string.fcheck_item_rtk_not_start)
                mBinding.tvFCheckOpenRTK.setTextColor(TXT_COLOR_YELLOW)
            }
            val leftDrawable = ContextCompat.getDrawable(mContext, if (it) R.mipmap.fcheck_item_open_rtk else R.mipmap.fcheck_item_not_open_rtk)
            leftDrawable?.setBounds(
                0,
                0,
                leftDrawable.minimumWidth,
                leftDrawable.minimumHeight
            )
            mBinding.tvFCheckOpenRTK.setCompoundDrawables(leftDrawable,null,null,null)
        }.addTo(mViewModel.compositeDisposable)

        mViewModel.getRcBatteryPercentObservable(mContext.getSystemService(Context.BATTERY_SERVICE) as BatteryManager).subscribe {
            mBinding.tvFCheckRcBattery.text = "$it%"
        }.addTo(mViewModel.compositeDisposable)

        mViewModel.getAirBatteryPercentObservable().subscribe {
            if(it.isPresent){
                mBinding.tvFCheckAirBatteryPercent.text = "${it.get()}%"
                mBinding.tvFCheckAirBatteryPercent.setTextColor(TXT_COLOR_DARK_BLUE)
            }else{
                mBinding.tvFCheckAirBatteryPercent.text = DEFAULT_VAL
                mBinding.tvFCheckAirBatteryPercent.setTextColor(TXT_COLOR_RED)
            }
        }.addTo(mViewModel.compositeDisposable)

        mViewModel.getAirBatteryVoltageObservable().subscribe {
            if(it.isPresent){
                mBinding.tvFCheckAirBatteryVoltage.text = "${it.get()}V"
                mBinding.tvFCheckAirBatteryVoltage.setTextColor(TXT_COLOR_DARK_BLUE)
            }else{
                mBinding.tvFCheckAirBatteryVoltage.text = DEFAULT_VAL
                mBinding.tvFCheckAirBatteryVoltage.setTextColor(TXT_COLOR_RED)
            }
        }.addTo(mViewModel.compositeDisposable)

        mViewModel.getCameraStorageAvailableCapacityObservable().subscribe {
            if(it.isPresent){
                mBinding.tvFCheckSDMemory.text = "${it.get()}G"
                mBinding.tvFCheckSDMemory.setTextColor(TXT_COLOR_DARK_BLUE)
            }else{
                mBinding.tvFCheckSDMemory.text = DEFAULT_VAL
                mBinding.tvFCheckSDMemory.setTextColor(TXT_COLOR_RED)
            }
        }.addTo(mViewModel.compositeDisposable)
    }

    companion object {
        private const val TAG = "ItemDeviceStatusCellView"
    }
}