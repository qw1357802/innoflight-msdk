package com.geoai.featstatusbar.view

import android.os.Bundle
import com.geoai.basiclib.base.fragment.BaseVVDDialogFragment
import com.geoai.featstatusbar.databinding.FragmentBatteryStatusBinding
import com.geoai.featstatusbar.mvi.vm.BatteryStatusViewModel

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-07 10:32.
 * @Description :
 */
class BatteryStatusDialogFragment: BaseVVDDialogFragment<BatteryStatusViewModel, FragmentBatteryStatusBinding>() {

    override fun init(savedInstanceState: Bundle?) {
    }
}