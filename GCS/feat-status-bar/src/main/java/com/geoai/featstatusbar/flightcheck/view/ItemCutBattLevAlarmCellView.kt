package com.geoai.featstatusbar.flightcheck.view

import android.os.Bundle
import com.geoai.basiclib.base.fragment.BaseVVDFragment
import com.geoai.featstatusbar.databinding.FcheckLyItemCustomBattLevAlarmBinding
import com.geoai.featstatusbar.flightcheck.custom.DoubleSeekBar
import com.geoai.featstatusbar.flightcheck.vm.ItemCutBattLevAlarmVM
import io.reactivex.rxkotlin.addTo

/**
@author : XPY on 2024/3/7 13:54
@description:
 */
class ItemCutBattLevAlarmCellView: BaseVVDFragment<ItemCutBattLevAlarmVM, FcheckLyItemCustomBattLevAlarmBinding>() {

    private var lastLow: Int? = null
    private var lastSevereLowLow: Int? = null

    override fun init(savedInstanceState: Bundle?) {
        mBinding.dsbFCheckBatteryAlarmSet.setOnRangeListener(object : DoubleSeekBar.OnRangeListener{
            override fun onRange(low: Int, big: Int, isActionFinish: Boolean) {
                mBinding.tvFCheckBatterySevereLowValue.text = "$low%"
                mBinding.tvFCheckBatteryLowValue.text = "$big%"

                if (!isActionFinish) return

                if (big != lastLow)
                    mViewModel.setBatteryCriticalThreshold(big.toFloat()).subscribe({
                        lastLow = big
                    }, {
                        lastLow?.let {
                            mBinding.tvFCheckBatteryLowValue.text = "$it%"
                            mBinding.dsbFCheckBatteryAlarmSet.setRightCursorValue(it)
                        }
                    }).addTo(mViewModel.compositeDisposable)

                if (low != lastSevereLowLow)
                    mViewModel.setBatteryEmergencyThreshold(low.toFloat()).subscribe({
                        lastSevereLowLow = low
                    }, {
                        lastSevereLowLow?.let {
                            mBinding.tvFCheckBatterySevereLowValue.text = "$it%"
                            mBinding.dsbFCheckBatteryAlarmSet.setLeftCursorValue(it)
                        }
                    }).addTo(mViewModel.compositeDisposable)
            }
        })

        mViewModel.getBatteryCriticalThresholdObservable().subscribe(
            {
                if(it != lastLow){
                    mBinding.tvFCheckBatteryLowValue.text = "$it%"
                    mBinding.dsbFCheckBatteryAlarmSet.setRightCursorValue(it.toInt())

                    lastLow = it
                }
            },{}).addTo(mViewModel.compositeDisposable)

        mViewModel.getBatteryEmergencyThresholdObservable().subscribe(
            {
                if(it != lastSevereLowLow){
                    mBinding.tvFCheckBatterySevereLowValue.text = "$it%"
                    mBinding.dsbFCheckBatteryAlarmSet.setLeftCursorValue(it.toInt())

                    lastSevereLowLow = it
                }
            },{
            }).addTo(mViewModel.compositeDisposable)
    }

    companion object {
        private const val TAG = "ItemCutBattLevAlarmCellView"
    }
}