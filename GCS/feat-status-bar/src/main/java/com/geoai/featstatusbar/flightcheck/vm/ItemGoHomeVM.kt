package com.geoai.featstatusbar.flightcheck.vm

import android.view.View
import com.geoai.basiclib.base.vm.BaseViewModel
import com.zkyt.lib_msdk_ext.component.FlyControllerManagerExt
import com.zkyt.lib_msdk_ext.utils.RxExt.distinctToUI
import com.zkyt.lib_msdk_ext.utils.RxExt.filterGetOptional
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
@author : XPY on 2024/3/7 11:50
@description:
 */
class ItemGoHomeVM(view: View): BaseViewModel() {

    fun getGoHomeAltitudeObservable(): Observable<Int> {
        return FlyControllerManagerExt.getGoHomeAltitudeObservable().filterGetOptional().
                distinctToUI()
    }

    fun setGoHomeAltitudeRx(alt: Int): Completable {
        return FlyControllerManagerExt.setGoHomeAltitudeRx(alt)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
    }
}