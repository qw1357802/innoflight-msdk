package com.geoai.featstatusbar.mvi.eis

import com.geoai.basiclib.base.mvi.IUIEffect
import com.geoai.basiclib.base.mvi.IUiIntent
import com.geoai.basiclib.base.mvi.IUiState

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-07 11:12.
 * @Description :
 */
sealed class WirelessStatusEffect: IUIEffect {

}

sealed class WirelessStatusIntent: IUiIntent {

}

sealed class WirelessStatusState: IUiState {
    object INIT : WirelessStatusState()
}