package com.geoai.featstatusbar.flightcheck.custom

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.widget.TextView

/**
 * Created by chenyu on 2024/1/25
 * Description: 点击变灰的TextView 有点击反馈会更舒适
 */
@SuppressLint("AppCompatCustomView")
class PressGrayTextView(context: Context, attrs: AttributeSet): TextView(context, attrs) {
    override fun setPressed(pressed: Boolean) {
        super.setPressed(pressed)
        if(isPressed) {
            alpha = 0.7f
        } else {
            alpha = 1f
        }
    }
}