package com.geoai.featstatusbar.flightcheck.view

import android.os.Bundle
import com.geoai.basiclib.base.fragment.BaseVVDFragment
import com.geoai.featstatusbar.databinding.FcheckLyItemHeightLimitBinding
import com.geoai.featstatusbar.flightcheck.custom.SelectEditView
import com.geoai.featstatusbar.flightcheck.vm.ItemHeightLimitVM
import io.reactivex.rxkotlin.addTo

/**
@author : XPY on 2024/3/7 13:54
@description:
 */
class ItemHeightLimitCellView: BaseVVDFragment<ItemHeightLimitVM, FcheckLyItemHeightLimitBinding>() {

    private val ALLOW_MIN = 20
    private val ALLOW_MAX = 2000

    override fun init(savedInstanceState: Bundle?) {
        mBinding.fcheckLyHeightLimitEdit.configRangeValue(ALLOW_MIN,ALLOW_MAX)
        mBinding.fcheckLyHeightLimitEdit.setOnSelectEditActionCallback(object :
            SelectEditView.OnSelectEditActionCallback {
            override fun onValue(value: Int) {
                mViewModel.setFcAltitudeLimitRx(value).subscribe().addTo(mViewModel.compositeDisposable)
            }
        })

        mViewModel.getFcAltitudeLimitObservable().subscribe({
            mBinding.fcheckLyHeightLimitEdit.setValue(it)
        }, {
        }).addTo(mViewModel.compositeDisposable)
    }

    companion object {
        private const val TAG = "ItemHeightLimitCellView"
    }
}