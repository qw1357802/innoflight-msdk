package com.geoai.featstatusbar.mvi.vm

import com.geoai.basiclib.base.mvi.BaseEISViewModel
import com.geoai.featstatusbar.mvi.eis.BatteryStatusEffect
import com.geoai.featstatusbar.mvi.eis.BatteryStatusIntent
import com.geoai.featstatusbar.mvi.eis.BatteryStatusState
import com.geoai.featstatusbar.mvi.eis.VersionInfoEffect
import com.geoai.featstatusbar.mvi.eis.VersionInfoIntent
import com.geoai.featstatusbar.mvi.eis.VersionInfoState
import javax.inject.Inject

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-07 09:42.
 * @Description :
 */
class BatteryStatusViewModel @Inject constructor(): BaseEISViewModel<BatteryStatusEffect, BatteryStatusIntent, BatteryStatusState>() {

    override fun initUiState(): BatteryStatusState {
        return BatteryStatusState.INIT
    }

    override fun handleIntent(intent: BatteryStatusIntent) {
    }
}