package com.geoai.featstatusbar.flightcheck.view

import android.os.Bundle
import android.view.View
import com.geoai.basiclib.base.fragment.BaseVVDFragment
import com.geoai.featstatusbar.R
import com.geoai.featstatusbar.databinding.FcheckLyItemDisBehaviorBinding
import com.geoai.featstatusbar.flightcheck.custom.SpinnerPopWindow
import com.geoai.featstatusbar.flightcheck.vm.ItemDisBehaviorVM
import com.geoai.mavlink.geoainet.flycontroller.enums.AircraftFailSafeBehaviorMode
import io.reactivex.rxkotlin.addTo

/**
@author : XPY on 2024/3/7 13:54
@description:
 */
class ItemDisBehaviorCellView: BaseVVDFragment<ItemDisBehaviorVM, FcheckLyItemDisBehaviorBinding>() {

    private var spinnerPopWindow: SpinnerPopWindow? = null
    private val behaviorArray: Array<String> = mContext.resources.getStringArray(R.array.fcheck_item_array_dis_behavior)

    override fun init(savedInstanceState: Bundle?) {
        val layoutParams = mBinding.tvFCheckDisBehValue.layoutParams
        val i = mContext.resources.getDimensionPixelOffset(R.dimen.fcheck_select_edit_middle_block_width)
        val i1 = mContext.resources.getDimensionPixelOffset(R.dimen.fcheck_select_edit_block_width) * 4
        val i2 = mContext.resources.getDimensionPixelOffset(R.dimen.base_dimen_margin_1) * 4
        val i3 = i + i1 + i2
        layoutParams.width = i3
        mBinding.tvFCheckDisBehValue.layoutParams = layoutParams

        mBinding.tvFCheckDisBehValue.setOnClickListener {
            if (spinnerPopWindow == null) {
                spinnerPopWindow = SpinnerPopWindow(mContext,behaviorArray,it.width)
                spinnerPopWindow?.setOnItemListener(object :SpinnerPopWindow.OnSpinnerItemListener{
                    override fun onValue(value: String, position: Int) {
                        mBinding.tvFCheckDisBehValue.text = value
                        mViewModel.setConnectionFailSafeBehaviorRx(AircraftFailSafeBehaviorMode.valueOf(position)).subscribe().addTo(mViewModel.compositeDisposable)

                        spinnerPopWindow?.dismiss()
                    }
                })
            }
            spinnerPopWindow?.showAsDropDown(it)
        }

        mViewModel.getConnectionFailSafeBehaviorObservable().subscribe({
            if(it.value() < behaviorArray.size){
                mBinding.tvFCheckDisBehValue.text = behaviorArray[it.value()]
            }
        }, {
        }).addTo(mViewModel.compositeDisposable)
    }

    companion object {
        private const val TAG = "ItemDisBehaviorCellView"
    }
}