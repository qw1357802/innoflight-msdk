package com.geoai.featstatusbar.flightcheck.view

import android.os.Bundle
import com.geoai.basiclib.base.fragment.BaseVVDFragment
import com.geoai.featstatusbar.R
import com.geoai.featstatusbar.databinding.FcheckLyItemLastAlterBinding
import com.geoai.featstatusbar.flightcheck.vm.ItemLastAlterVM
import io.reactivex.rxkotlin.addTo

/**
@author : XPY on 2024/3/7 13:54
@description:
 */
class ItemLastAlterCellView: BaseVVDFragment<ItemLastAlterVM, FcheckLyItemLastAlterBinding>() {

    private val NORMAL_TXT = mContext.resources.getString(R.string.fcheck_item_last_alter_normal)
    private val TAKE_CARE_TXT = mContext.resources.getString(R.string.fcheck_item_last_alter_take_care)
    private val CRITICAL_TXT = mContext.resources.getString(R.string.fcheck_item_last_alter_critical)
    private val UAV_NOT_CONN_TXT = mContext.resources.getString(R.string.fcheck_uav_not_conn)

    override fun init(savedInstanceState: Bundle?) {
        mViewModel.getLatestAlarmObservable().subscribe {
            when(it.latestAlarmLevel){
                ItemLastAlterVM.LatestAlarmLevel.NOT_CONNECT ->{
                    mBinding.tvFCheckAlarmState.text = UAV_NOT_CONN_TXT
                    mBinding.tvFCheckAlarmState.setBackgroundResource(R.drawable.fcheck_last_alarm_critical_bg)
                }
                ItemLastAlterVM.LatestAlarmLevel.TAKE_CARE ->{
                    mBinding.tvFCheckAlarmState.text = TAKE_CARE_TXT + ", " + it.alarmContent
                    mBinding.tvFCheckAlarmState.setBackgroundResource(R.drawable.fcheck_last_alarm_take_care_bg)
                }
                ItemLastAlterVM.LatestAlarmLevel.NORMAL ->{
                    mBinding.tvFCheckAlarmState.text = NORMAL_TXT
                    mBinding.tvFCheckAlarmState.setBackgroundResource(R.drawable.fcheck_last_alarm_normal_bg)
                }
                else -> {
                    mBinding.tvFCheckAlarmState.text = CRITICAL_TXT + ", " + it.alarmContent
                    mBinding.tvFCheckAlarmState.setBackgroundResource(R.drawable.fcheck_last_alarm_critical_bg)
                }
            }
        }.addTo(mViewModel.compositeDisposable)
    }

}