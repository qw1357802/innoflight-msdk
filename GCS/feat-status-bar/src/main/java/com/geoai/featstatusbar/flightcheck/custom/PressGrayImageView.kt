package com.zkyt.lib_common.widget

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.widget.ImageView

/**
 * Created by chenyu on 2024/1/25
 * Description: 点击变灰的ImageView 有点击反馈会更舒适
 */
@SuppressLint("AppCompatCustomView")
class PressGrayImageView(context: Context, attrs: AttributeSet): ImageView(context, attrs) {
    override fun setPressed(pressed: Boolean) {
        super.setPressed(pressed)
        if(isPressed) {
            alpha = 0.7f
        } else {
            alpha = 1f
        }
    }

    override fun setEnabled(enabled: Boolean) {
        super.setEnabled(enabled)
        if(!enabled) {
            alpha = 0.7f
        } else {
            alpha = 1f
        }
    }

}