package com.geoai.featstatusbar.mvi.vm

import com.geoai.basiclib.base.mvi.BaseEISViewModel
import com.geoai.featstatusbar.mvi.eis.WirelessStatusEffect
import com.geoai.featstatusbar.mvi.eis.WirelessStatusIntent
import com.geoai.featstatusbar.mvi.eis.WirelessStatusState
import javax.inject.Inject

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-07 11:12.
 * @Description :
 */
class WirelessStatusViewModel @Inject constructor(): BaseEISViewModel<WirelessStatusEffect, WirelessStatusIntent, WirelessStatusState>() {

    override fun initUiState(): WirelessStatusState {
        return WirelessStatusState.INIT
    }

    override fun handleIntent(intent: WirelessStatusIntent) {
    }
}