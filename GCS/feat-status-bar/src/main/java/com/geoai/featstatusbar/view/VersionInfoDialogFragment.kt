package com.geoai.featstatusbar.view

import android.os.Bundle
import com.geoai.basiclib.base.fragment.BaseVVDDialogFragment
import com.geoai.featstatusbar.databinding.FragmentVersionInfoBinding
import com.geoai.featstatusbar.mvi.vm.VersionInfoViewModel
import com.geoai.mavlink.geoainet.base.constant.GEOAIConstant
import com.zkyt.lib_msdk_ext.component.BatteryManagerExt
import com.zkyt.lib_msdk_ext.component.CrestManagerExt
import com.zkyt.lib_msdk_ext.component.FlyControllerManagerExt
import com.zkyt.lib_msdk_ext.component.SystemManagerExt
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-07 09:41.
 * @Description :
 */
class VersionInfoDialogFragment: BaseVVDDialogFragment<VersionInfoViewModel, FragmentVersionInfoBinding>() {

    override fun init(savedInstanceState: Bundle?) {
        initVersionInfo()
        initSnInfo()
    }

    private fun initSnInfo() {
        FlyControllerManagerExt.getSnInfo()
            .subscribeOn(AndroidSchedulers.mainThread())
            .subscribe({
                mBinding.tabVersionFcSn.setContent(it)
            }){

            }.addTo(mViewModel.compositeDisposable)

        BatteryManagerExt.getSnInfo()
            .subscribeOn(AndroidSchedulers.mainThread())
            .subscribe({
                mBinding.tabVersionFcSn.setContent(it)
            }){

            }.addTo(mViewModel.compositeDisposable)

        CrestManagerExt.getSnInfo()
            .subscribeOn(AndroidSchedulers.mainThread())
            .subscribe({
                mBinding.tabVersionCrestSn.setContent(it)
            }){

            }.addTo(mViewModel.compositeDisposable)
    }

    private fun initVersionInfo() {
        SystemManagerExt.getOriginManager()?.componentVersionInfo?.let {
            it.forEach { (componentType, versionInfo) ->
                when (componentType.toInt()) {
                    GEOAIConstant.MAVLINK_SYS_COMP_TYPE_FC -> {
                        mBinding.tabVersionFc.setContent(
                            String.format(
                                "%s / %s",
                                analyzeSoftWareVersionInfo(versionInfo.swVersionCode),
                                versionInfo.hwVersionCode
                            )
                        )
                    }

                    GEOAIConstant.MAVLINK_SYS_COMP_TYPE_CREST -> {
                        mBinding.tabVersionCrest.setContent(
                            String.format(
                                "%s / %s",
                                analyzeSoftWareVersionInfo(versionInfo.swVersionCode),
                                versionInfo.hwVersionCode
                            )
                        )
                    }

                    GEOAIConstant.MAVLINK_SYS_HEARTBEAT_TYPE_GIMBAL -> {
                        mBinding.tabVersionGimbal.setContent(
                            String.format(
                                "%s / %s",
                                analyzeSoftWareVersionInfo(versionInfo.swVersionCode),
                                versionInfo.hwVersionCode
                            )
                        )
                    }

                    GEOAIConstant.MAVLINK_SYS_HEARTBEAT_TYPE_CAMERA -> {
                        mBinding.tabVersionCamera.setContent(
                            String.format(
                                "%s / %s",
                                analyzeSoftWareVersionInfo(versionInfo.swVersionCode),
                                versionInfo.hwVersionCode
                            )
                        )
                    }

                    GEOAIConstant.MAVLINK_SYS_HEARTBEAT_TYPE_BATTERY -> {
                        mBinding.tabVersionBattery.setContent(
                            String.format(
                                "%s / %s",
                                analyzeSoftWareVersionInfo(versionInfo.swVersionCode),
                                versionInfo.hwVersionCode
                            )
                        )
                    }

                    GEOAIConstant.MAVLINK_SYS_HEARTBEAT_TYPE_RTK -> {
                        mBinding.tabVersionGps.setContent(
                            String.format(
                                "%s / %s",
                                analyzeSoftWareVersionInfo(versionInfo.swVersionCode),
                                versionInfo.hwVersionCode
                            )
                        )
                    }

                    GEOAIConstant.MAVLINK_SYS_HEARTBEAT_TYPE_ECS1 -> {
                        mBinding.tabVersionEsc1.setContent(
                            String.format(
                                "%s / %s",
                                analyzeSoftWareVersionInfo(versionInfo.swVersionCode),
                                versionInfo.hwVersionCode
                            )
                        )
                    }

                    GEOAIConstant.MAVLINK_SYS_HEARTBEAT_TYPE_ECS2 -> {
                        mBinding.tabVersionEsc2.setContent(
                            String.format(
                                "%s / %s",
                                analyzeSoftWareVersionInfo(versionInfo.swVersionCode),
                                versionInfo.hwVersionCode
                            )
                        )
                    }

                    GEOAIConstant.MAVLINK_SYS_HEARTBEAT_TYPE_ECS3 -> {
                        mBinding.tabVersionEsc3.setContent(
                            String.format(
                                "%s / %s",
                                analyzeSoftWareVersionInfo(versionInfo.swVersionCode),
                                versionInfo.hwVersionCode
                            )
                        )
                    }

                    GEOAIConstant.MAVLINK_SYS_HEARTBEAT_TYPE_ECS4 -> {
                        mBinding.tabVersionEsc4.setContent(
                            String.format(
                                "%s / %s",
                                analyzeSoftWareVersionInfo(versionInfo.swVersionCode),
                                versionInfo.hwVersionCode
                            )
                        )
                    }

                    GEOAIConstant.MAVLINK_SYS_HEARTBEAT_TYPE_OBSTACLE -> {
                        mBinding.tabVersionObstacle.setContent(
                            String.format(
                                "%s / %s",
                                analyzeSoftWareVersionInfo(versionInfo.swVersionCode),
                                versionInfo.hwVersionCode
                            )
                        )
                    }

                    else -> {}
                }
            }
        }
    }

    private fun analyzeSoftWareVersionInfo(s: Long): String {
        val major = (s shr 24).toInt()
        val minor = (s shr 16 and 0xff).toInt()
        val patch = (s shr 8 and 0xff).toInt()
        val type = (s and 0xff).toInt()
        return "$major.$minor.$patch.$type"
    }
}