package com.geoai.featstatusbar.flightcheck.view

import android.os.Bundle
import com.geoai.basiclib.base.fragment.BaseVVDFragment
import com.geoai.featstatusbar.databinding.FcheckLyItemDistanceLimitBinding
import com.geoai.featstatusbar.flightcheck.custom.SelectEditView
import com.geoai.featstatusbar.flightcheck.vm.ItemDistanceLimitVM
import io.reactivex.rxkotlin.addTo

/**
@author : XPY on 2024/3/7 13:54
@description:
 */
class ItemDistanceLimitCellView: BaseVVDFragment<ItemDistanceLimitVM, FcheckLyItemDistanceLimitBinding>() {

    private val ALLOW_MIN = 15
    private val ALLOW_MAX = 8000

    override fun init(savedInstanceState: Bundle?) {
        mBinding.fcheckLyDistanceLimitEdit.configRangeValue(ALLOW_MIN,ALLOW_MAX)
        mBinding.fcheckLyDistanceLimitEdit.setOnSelectEditActionCallback(object : SelectEditView.OnSelectEditActionCallback {
            override fun onValue(value: Int) {
                mViewModel.setFcDistanceLimitRx(value).subscribe().addTo(mViewModel.compositeDisposable)
            }
        })
        mViewModel.getFcDistanceLimitObservable().subscribe({
            mBinding.fcheckLyDistanceLimitEdit.setValue(it)
        }, {
        }).addTo(mViewModel.compositeDisposable)
    }

    companion object {
        private const val TAG = "ItemDistanceLimitCellView"
    }
}