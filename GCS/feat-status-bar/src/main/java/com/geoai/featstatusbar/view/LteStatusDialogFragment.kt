package com.geoai.featstatusbar.view

import android.os.Bundle
import com.geoai.basiclib.base.fragment.BaseVVDDialogFragment
import com.geoai.featstatusbar.databinding.FragmentLteStatusBinding
import com.geoai.featstatusbar.mvi.vm.LteStatusViewModel

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-07 11:29.
 * @Description :
 */
class LteStatusDialogFragment: BaseVVDDialogFragment<LteStatusViewModel, FragmentLteStatusBinding>() {

    override fun init(savedInstanceState: Bundle?) {
    }
}