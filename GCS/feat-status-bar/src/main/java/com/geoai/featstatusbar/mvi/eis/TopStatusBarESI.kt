package com.geoai.featstatusbar.mvi.eis

import androidx.annotation.IntRange
import com.geoai.basiclib.base.mvi.IUIEffect
import com.geoai.basiclib.base.mvi.IUiIntent
import com.geoai.basiclib.base.mvi.IUiState
import com.geoai.mavlink.geoainet.airlink.info.CellularServerConnectionInfo
import com.geoai.mavlink.geoainet.flycontroller.info.AircraftDiagnosticsStateInfo
import com.geoai.mavlink.geoainet.rtk.enums.GPSPosType

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-03 19:32.
 * @Description :
 */
sealed class TopStatusBarEffect: IUIEffect {

}

sealed class TopStatusBarIntent: IUiIntent {
    data class ProductConnected(val isConnected: Boolean) : TopStatusBarIntent()
}

sealed class TopStatusBarState: IUiState {
    data class DroneConnected(val isConnected: Boolean) : TopStatusBarState()
    data class DroneFlyMode(val flyMode: String) : TopStatusBarState()
    data class DroneWirelessSignal(@IntRange(from = -1, to = 5) val uplinkLevel: Int, @IntRange(from = -1, to = 5) val downlinkLevel: Int) : TopStatusBarState()
    data class DroneSatelliteCount(val count: Int, val fixType: String) : TopStatusBarState()
    data class DroneBatteryPercent(val percent: Int) : TopStatusBarState()
    data class DroneCellularStatus(val cellularStatus: CellularServerConnectionInfo?): TopStatusBarState()
    data class DroneHmsStatus(val infos: List<AircraftDiagnosticsStateInfo>): TopStatusBarState()
}