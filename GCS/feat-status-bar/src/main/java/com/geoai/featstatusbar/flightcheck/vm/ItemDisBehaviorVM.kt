package com.geoai.featstatusbar.flightcheck.vm

import android.view.View
import com.geoai.basiclib.base.vm.BaseViewModel
import com.geoai.mavlink.geoainet.flycontroller.enums.AircraftFailSafeBehaviorMode
import com.zkyt.lib_msdk_ext.component.FlyControllerManagerExt
import com.zkyt.lib_msdk_ext.utils.RxExt.distinctToUI
import com.zkyt.lib_msdk_ext.utils.RxExt.filterGetOptional
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
@author : XPY on 2024/3/7 11:50
@description:
 */
class ItemDisBehaviorVM(view: View): BaseViewModel() {

    fun getConnectionFailSafeBehaviorObservable(): Observable<AircraftFailSafeBehaviorMode> {
        return FlyControllerManagerExt.getConnectionFailSafeBehaviorObservable().filterGetOptional().distinctToUI()
    }

    fun setConnectionFailSafeBehaviorRx(behavior: AircraftFailSafeBehaviorMode): Completable {
        return FlyControllerManagerExt.setConnectionFailSafeBehaviorRx(behavior)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
    }
}