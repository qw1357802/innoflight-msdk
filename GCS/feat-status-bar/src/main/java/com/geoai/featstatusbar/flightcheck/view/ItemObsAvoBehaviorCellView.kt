package com.geoai.featstatusbar.flightcheck.view

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.RadioGroup.OnCheckedChangeListener
import com.geoai.basiclib.base.fragment.BaseVVDFragment
import com.geoai.featstatusbar.R
import com.geoai.featstatusbar.databinding.FcheckLyItemDistanceLimitBinding
import com.geoai.featstatusbar.databinding.FcheckLyItemObsAvoBehaviorBinding
import com.geoai.featstatusbar.flightcheck.vm.ItemDistanceLimitVM
import com.geoai.featstatusbar.flightcheck.vm.ItemObsAvoBehaviorVM
import io.reactivex.rxkotlin.addTo

/**
@author : XPY on 2024/3/18 15:15
@description:
 */
class ItemObsAvoBehaviorCellView: BaseVVDFragment<ItemObsAvoBehaviorVM, FcheckLyItemObsAvoBehaviorBinding>() {

    private lateinit var onCheckedChangeListener: OnCheckedChangeListener

    override fun init(savedInstanceState: Bundle?) {
        onCheckedChangeListener =
            OnCheckedChangeListener { group, checkedId ->
                mViewModel.setObstacleAvoidanceEnableRx(checkedId == R.id.rbFCheckBehaviorStop)
                    .subscribe({

                    }, {
                        mBinding.rgFCheckObsAvoBehavior.setOnCheckedChangeListener(null)
                        mBinding.rgFCheckObsAvoBehavior.check(if (checkedId == R.id.rbFCheckBehaviorStop) R.id.rbFCheckBehaviorClose else R.id.rbFCheckBehaviorStop)
                        mBinding.rgFCheckObsAvoBehavior.setOnCheckedChangeListener(
                            onCheckedChangeListener
                        )

                    }).addTo(mViewModel.compositeDisposable)
            }
        mBinding.rgFCheckObsAvoBehavior.setOnCheckedChangeListener(onCheckedChangeListener)

        mViewModel.getObstacleAvoidanceEnableObservable().subscribe({
            mBinding.rgFCheckObsAvoBehavior.setOnCheckedChangeListener(null)
            mBinding.rgFCheckObsAvoBehavior.check(if (it) R.id.rbFCheckBehaviorStop else R.id.rbFCheckBehaviorClose)
            mBinding.rgFCheckObsAvoBehavior.setOnCheckedChangeListener(onCheckedChangeListener)
        },{
        }).addTo(mViewModel.compositeDisposable)
    }

    companion object {
        private const val TAG = "ItemObsAvoBehaviorCellView"
    }

}