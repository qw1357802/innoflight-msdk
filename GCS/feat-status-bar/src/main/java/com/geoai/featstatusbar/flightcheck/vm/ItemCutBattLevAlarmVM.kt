package com.geoai.featstatusbar.flightcheck.vm

import android.view.View
import com.geoai.basiclib.base.vm.BaseViewModel
import com.zkyt.lib_msdk_ext.component.BatteryManagerExt
import com.zkyt.lib_msdk_ext.utils.RxExt.distinctToUI
import com.zkyt.lib_msdk_ext.utils.RxExt.filterGetOptional
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

/**
@author : XPY on 2024/3/7 11:50
@description:
 */
class ItemCutBattLevAlarmVM(view: View): BaseViewModel() {
    fun setBatteryCriticalThreshold(level: Float): Completable {
        return BatteryManagerExt.setBatteryCriticalThresholdRx(level)
    }

    fun setBatteryEmergencyThreshold(level: Float): Completable {
        return BatteryManagerExt.setBatteryEmergencyThresholdRx(level)
    }

    fun getBatteryCriticalThresholdObservable(): Observable<Int> {
        return BatteryManagerExt.getBatteryCriticalThresholdObservable().filterGetOptional().distinctToUI().map {
            return@map it.toInt()
        }
    }

    fun getBatteryEmergencyThresholdObservable(): Observable<Int> {
        return BatteryManagerExt.getBatteryEmergencyThresholdObservable().filterGetOptional().distinctToUI().map {
            return@map it.toInt()
        }
    }
}