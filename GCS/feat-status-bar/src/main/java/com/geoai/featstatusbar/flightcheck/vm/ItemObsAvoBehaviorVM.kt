package com.geoai.featstatusbar.flightcheck.vm

import android.view.View
import com.geoai.basiclib.base.vm.BaseViewModel
import com.zkyt.lib_msdk_ext.component.ObstacleManagerExt
import com.zkyt.lib_msdk_ext.utils.RxExt.distinctToUI
import com.zkyt.lib_msdk_ext.utils.RxExt.filterGetOptional
import io.reactivex.Completable
import io.reactivex.Observable

/**
@author : XPY on 2024/3/7 11:50
@description:
 */
class ItemObsAvoBehaviorVM(view: View) : BaseViewModel() {

    fun setObstacleAvoidanceEnableRx(enable: Boolean): Completable {
        return ObstacleManagerExt.setObstacleAvoidanceEnableRx(enable)
    }

    fun getObstacleAvoidanceEnableObservable(): Observable<Boolean> {
        return ObstacleManagerExt.getObstacleAvoidanceEnableObservable().filterGetOptional().distinctToUI()
    }
}