package com.geoai.featstatusbar.flightcheck.view

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.View
import android.widget.SeekBar
import android.widget.SeekBar.OnSeekBarChangeListener
import androidx.appcompat.content.res.AppCompatResources
import com.geoai.basiclib.base.fragment.BaseVVDFragment
import com.geoai.featstatusbar.R
import com.geoai.featstatusbar.databinding.FcheckLyItemFourWayObsAvoBinding
import com.geoai.featstatusbar.flightcheck.vm.ItemFourWayObsAvoVM
import io.reactivex.rxkotlin.addTo

/**
@author : XPY on 2024/3/18 15:15
@description:
 */
class ItemFourWayObsAvoCellView: BaseVVDFragment<ItemFourWayObsAvoVM, FcheckLyItemFourWayObsAvoBinding>(){

    override fun init(savedInstanceState: Bundle?) {
        val thumbDrw: Drawable = AppCompatResources.getDrawable(
            mContext,
            R.mipmap.fcheck_item_battery_severe_low
        )!!
        mBinding.sbFCheckObsAvoSet.setPadding(thumbDrw.minimumWidth / 2 ,0 , thumbDrw.minimumWidth / 2 ,0)

        mBinding.sbFCheckObsAvoSet.setOnSeekBarChangeListener(object : OnSeekBarChangeListener{
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                mBinding.tvFCheckBrakeStopValue.text = "${(progress / 10f)}m"
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {
                mViewModel.setObstacleAvoidanceBrakingDistanceRx(seekBar.progress / 10f).subscribe().addTo(mViewModel.compositeDisposable)
            }
        })

        mViewModel.getObstacleAvoidanceBrakingDistanceObservable().subscribe({
            mBinding.tvFCheckBrakeStopValue.text = "${it}m"
            mBinding.sbFCheckObsAvoSet.progress = (it * 10).toInt()
        },{
        }).addTo(mViewModel.compositeDisposable)

        mViewModel.getObstacleAvoidanceEnableObservable().subscribe {
            mBinding.ivFCheckObsOpenTag.setImageResource(if(it) R.mipmap.fcheck_item_obs_avo else R.mipmap.fcheck_item_close_obs_avo)
            mBinding.ivFCheckObsOpenTag.setBackgroundResource(if(it) R.drawable.fcheck_four_way_obs else R.drawable.fcheck_close_four_way_obs)
        }.addTo(mViewModel.compositeDisposable)
    }

    companion object {
        private const val TAG = "ItemFourWayObsAvoCellView"
    }
}