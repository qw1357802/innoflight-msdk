package com.geoai.featstatusbar.mvi.vm

import com.geoai.basiclib.base.mvi.BaseEISViewModel
import com.geoai.featstatusbar.mvi.eis.RtkSatelliteEffect
import com.geoai.featstatusbar.mvi.eis.RtkSatelliteIntent
import com.geoai.featstatusbar.mvi.eis.RtkSatelliteState
import javax.inject.Inject

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-07 10:48.
 * @Description :
 */
class RtkSatelliteViewModel @Inject constructor(): BaseEISViewModel<RtkSatelliteEffect, RtkSatelliteIntent, RtkSatelliteState>() {
    override fun initUiState(): RtkSatelliteState {
        return RtkSatelliteState.INIT
    }

    override fun handleIntent(intent: RtkSatelliteIntent) {
    }
}