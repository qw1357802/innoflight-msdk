package com.geoai.featstatusbar.flightcheck.vm

import android.view.View
import com.geoai.basiclib.base.vm.BaseViewModel
import com.zkyt.lib_msdk_ext.component.FlyControllerManagerExt
import com.zkyt.lib_msdk_ext.utils.RxExt.distinctToUI
import com.zkyt.lib_msdk_ext.utils.RxExt.filterGetOptional
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
@author : XPY on 2024/3/7 11:50
@description:
 */
class ItemDistanceLimitVM(view: View): BaseViewModel() {

    fun setFcDistanceLimitRx(limit: Int): Completable {
        return FlyControllerManagerExt.setFcDistanceLimitRx(limit).observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
    }

    fun getFcDistanceLimitObservable(): Observable<Int> {
        return FlyControllerManagerExt.getFcDistanceLimitObservable().filterGetOptional().distinctToUI()
    }
}