package com.geoai.featstatusbar.view;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.geoai.featstatusbar.R;
import com.geoai.mavlink.geoainet.flycontroller.info.AircraftDiagnosticsStateInfo;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class HmsTipsPopWindow extends FrameLayout {

    public HmsTipsPopWindow(@NonNull Context context) {
        super(context);
        init(context);
    }

    public HmsTipsPopWindow(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public HmsTipsPopWindow(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private final List<AircraftDiagnosticsStateInfo> mAircraftDiagnosticsStateInfo = new CopyOnWriteArrayList<>();

    private RecyclerView mRcyHms;

    private RecycleAdapter mRecycleAdapter;

    private void init(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.view_hms_tips_popwindow, this);

        mRcyHms = view.findViewById(R.id.rcy_hms);

        initRecycle();
    }

    private void initRecycle() {
        mRecycleAdapter = new RecycleAdapter(getContext(), mAircraftDiagnosticsStateInfo);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        mRcyHms.setLayoutManager(layoutManager);
        mRcyHms.setAdapter(mRecycleAdapter);
    }

    public void updateData(List<AircraftDiagnosticsStateInfo> list) {
        mAircraftDiagnosticsStateInfo.clear();
        mAircraftDiagnosticsStateInfo.addAll(list);
        mRecycleAdapter.notifyDataSetChanged();
    }

    private static class RecycleAdapter extends RecyclerView.Adapter<VH> {

        private final Context mContext;

        private final List<AircraftDiagnosticsStateInfo> mAircraftDiagnosticsStateInfos;

        public RecycleAdapter(Context context, List<AircraftDiagnosticsStateInfo> aircraftDiagnosticsStateInfos) {
            this.mContext = context;
            this.mAircraftDiagnosticsStateInfos = aircraftDiagnosticsStateInfos;
        }

        @NonNull
        @Override
        public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View inflate = LayoutInflater.from(mContext).inflate(R.layout.item_hms_child, parent, false);
            return new VH(inflate);
        }

        @Override
        public void onBindViewHolder(@NonNull VH holder, int position) {
            AircraftDiagnosticsStateInfo info = mAircraftDiagnosticsStateInfos.get(position);
            holder.mTvErrorCode.setText(String.format("CODE: 0x%s", info.getAlertCode()));
            holder.mTvErrorMsg.setText(String.valueOf(info.getMessage()));
            if (!TextUtils.isEmpty(info.getDescription())) {
                holder.mTvErrorDesc.setVisibility(View.VISIBLE);
                holder.mTvErrorDesc.setText(String.valueOf(info.getDescription()));
            } else {
                holder.mTvErrorDesc.setVisibility(View.GONE);
            }
            switch (mAircraftDiagnosticsStateInfos.get(position).getAlertLevel()) {
                case MAV_SEVERITY_EMERGENCY:
                case MAV_SEVERITY_CRITICAL:
                case MAV_SEVERITY_ERROR:
                    holder.mRlMenu.setBackgroundColor(Color.parseColor("#808B0000"));
                    holder.mImgErrorType.setBackgroundResource(R.mipmap.icon_error);
                    break;
                case MAV_SEVERITY_WARNING:
                case MAV_SEVERITY_ALERT:
                    holder.mRlMenu.setBackgroundColor(Color.parseColor("#80F08080"));
                    holder.mImgErrorType.setBackgroundResource(R.mipmap.icon_warn);
                    break;
                case MAV_SEVERITY_INFO:
                case MAV_SEVERITY_NOTICE:
                    holder.mRlMenu.setBackgroundColor(Color.parseColor("#80008B8B"));
                    holder.mImgErrorType.setBackgroundResource(R.mipmap.icon_tips);
                    break;
                case MAV_SEVERITY_DEBUG:
                default:
                    holder.mRlMenu.setBackgroundColor(Color.parseColor("#80D2B48C"));
                    holder.mImgErrorType.setBackgroundResource(R.mipmap.icon_debug);
                    break;
            }
        }

        @Override
        public int getItemCount() {
            return mAircraftDiagnosticsStateInfos.size();
        }
    }

    private static class VH extends RecyclerView.ViewHolder {

        private final TextView mTvErrorCode;

        private final TextView mTvErrorMsg;

        private final TextView mTvErrorDesc;

        private final ImageView mImgErrorType;

        private final RelativeLayout mRlMenu;

        public VH(@NonNull View itemView) {
            super(itemView);
            mTvErrorCode = itemView.findViewById(R.id.tv_hms_err_code);
            mImgErrorType = itemView.findViewById(R.id.img_hms_type);
            mTvErrorMsg = itemView.findViewById(R.id.tv_hms_err_msg);
            mTvErrorDesc = itemView.findViewById(R.id.tv_hms_err_desc);
            mRlMenu = itemView.findViewById(R.id.rl_hms);
        }
    }
}
