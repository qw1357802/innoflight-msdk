package com.geoai.featstatusbar.flightcheck.view

import android.os.Bundle
import com.geoai.basiclib.base.fragment.BaseVVDFragment
import com.geoai.basiclib.engine.toast.toast
import com.geoai.featstatusbar.R
import com.geoai.featstatusbar.databinding.FcheckLyItemReturnPointBinding
import com.geoai.featstatusbar.flightcheck.vm.ItemReturnPointVM
import io.reactivex.rxkotlin.addTo

/**
@author : XPY on 2024/3/7 13:54
@description:
 */
class ItemReturnPointCellView: BaseVVDFragment<ItemReturnPointVM, FcheckLyItemReturnPointBinding>() {

    override fun init(savedInstanceState: Bundle?) {
        mBinding.btnFCheckRcPoint.setOnClickListener {
            mViewModel.setReturnHomePointMode(false).subscribe({
                mBinding.btnFCheckRcPoint.setImageResource(R.mipmap.fcheck_return_select_rc_point)
                mBinding.btnFCheckRcPoint.setBackgroundResource(R.drawable.common_blue_radius_bg)

                mBinding.btnFCheckUavPoint.setImageResource(R.mipmap.fcheck_return_uav_point)
                mBinding.btnFCheckUavPoint.setBackgroundResource(R.drawable.fcheck_return_point_frame)
            },{
                it.message?.let { msg -> toast(msg) }
            }).addTo(mViewModel.compositeDisposable)
        }
        mBinding.btnFCheckUavPoint.setOnClickListener {
            mViewModel.setReturnHomePointMode(true).subscribe({
                mBinding.btnFCheckRcPoint.setImageResource(R.mipmap.fcheck_return_rc_point)
                mBinding.btnFCheckRcPoint.setBackgroundResource(R.drawable.fcheck_return_point_frame)

                mBinding.btnFCheckUavPoint.setImageResource(R.mipmap.fcheck_return_select_uav_point)
                mBinding.btnFCheckUavPoint.setBackgroundResource(R.drawable.common_blue_radius_bg)
            },{
                it.message?.let { msg -> toast(msg) }
            }).addTo(mViewModel.compositeDisposable)
        }
        
    }

    companion object {
        private const val TAG = "ItemReturnPointCellView"
    }
}