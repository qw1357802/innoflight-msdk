package com.geoai.featstatusbar.flightcheck.vm

import android.view.View
import com.geoai.basiclib.base.vm.BaseViewModel
import com.zkyt.lib_msdk_ext.component.FlyControllerManagerExt
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
@author : XPY on 2024/3/7 11:50
@description:
 */
class ItemReturnPointVM(view: View): BaseViewModel() {

    fun setReturnHomePointMode(isAircraftReturnPoint: Boolean): Completable {
        return if(isAircraftReturnPoint){
            FlyControllerManagerExt.setHomeLocationUsingAircraftCurrentLocationRx()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
        } else {
            FlyControllerManagerExt.setHomeLocationUsingAircraftCurrentLocationRx()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
        }
//        else{
//            val currentLocation = GALocationManager.getCurrentLocation()
//            FlyControllerManagerExt.setHomeLocationRx(
//                currentLocation.latitude,
//                currentLocation.longitude
//            )
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribeOn(Schedulers.io())
//        }
    }
}