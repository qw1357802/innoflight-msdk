package com.geoai.featstatusbar.flightcheck.vm

import android.os.BatteryManager
import android.view.View
import com.geoai.basiclib.base.vm.BaseViewModel
import com.google.gson.Gson
import com.zkyt.lib_msdk_ext.component.BatteryManagerExt
import com.zkyt.lib_msdk_ext.component.FlyControllerManagerExt
import com.zkyt.lib_msdk_ext.component.RtkManagerExt
import com.zkyt.lib_msdk_ext.component.camera.BaseCameraExt
import com.zkyt.lib_msdk_ext.utils.RxExt.distinctToUI
import com.zkyt.lib_msdk_ext.utils.RxExt.filterGetOptional
import com.zkyt.lib_msdk_ext.utils.RxExt.throttleDistinctToUI
import io.reactivex.Observable
import java.text.DecimalFormat
import java.util.*

/**
@author : XPY on 2024/3/7 11:50
@description:
 */
class ItemDeviceStatusVM(view: View): BaseViewModel() {

    fun getGoHomePointSetStateObservable(): Observable<Boolean>{
        return FlyControllerManagerExt.getGoHomeLocationObservable()
            .filterGetOptional()
            .distinctToUI()
            .map {
                return@map it.size == 2
            }
    }

    fun getRtkEnableStateObservable(): Observable<Boolean>{
        return RtkManagerExt.getRtkEnableObservable()
            .filterGetOptional()
            .distinctToUI()
            .map {
                return@map it
            }
    }

    fun getRcBatteryPercentObservable(manager: BatteryManager): Observable<Int> {
        return BatteryManagerExt.getRcBatteryPercentObservable(manager).filterGetOptional()
            .distinctToUI()
            .map {
                return@map it
            }
    }

    fun getAirBatteryPercentObservable(): Observable<Optional<Int>> {
        return BatteryManagerExt.getBatteryStateInfoObservable().map {
            if(!it.isPresent || it.get().isEmpty()){
                return@map Optional.empty<Int>()
            }
            return@map Optional.of(it.get()[0].batteryRemainingPercentage)
        }.distinctToUI()
    }

    fun getAirBatteryVoltageObservable(): Observable<Optional<Float>> {
        return BatteryManagerExt.getBatteryStateInfoObservable().map {
            if(!it.isPresent || it.get().isEmpty()) {
                return@map Optional.empty<Float>()
            }
            return@map Optional.of(it.get()[0].totalVoltage / 100)
        }.distinctToUI()
    }

    fun getCameraStorageAvailableCapacityObservable(): Observable<Optional<Float>> {
        return BaseCameraExt.getCameraStorageInfoObservable().map {
            if(!it.isPresent) return@map  Optional.empty<Float>()
            val format = DecimalFormat("0.00").format(it.get().availableCapacity / 1024f)
            return@map Optional.of(format.toFloat())
        }.throttleDistinctToUI(2000)
    }
}