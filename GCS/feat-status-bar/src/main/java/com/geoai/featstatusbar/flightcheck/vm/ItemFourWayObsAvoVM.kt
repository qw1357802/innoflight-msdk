package com.geoai.featstatusbar.flightcheck.vm

import android.view.View
import com.geoai.basiclib.base.vm.BaseViewModel
import com.zkyt.lib_msdk_ext.component.ObstacleManagerExt
import com.zkyt.lib_msdk_ext.utils.RxExt.distinctToUI
import com.zkyt.lib_msdk_ext.utils.RxExt.filterGetOptional
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

/**
@author : XPY on 2024/3/7 11:50
@description:
 */
class ItemFourWayObsAvoVM(view: View): BaseViewModel() {

    fun setObstacleAvoidanceBrakingDistanceRx(distance: Float): Completable {
        return ObstacleManagerExt.setObstacleAvoidanceBrakingDistanceRx(distance)
    }

    fun getObstacleAvoidanceBrakingDistanceObservable(): Observable<Float> {
        return ObstacleManagerExt.getObstacleAvoidanceBrakingDistanceObservable().filterGetOptional().doOnNext {
        }.distinctToUI()
    }

    fun getObstacleAvoidanceEnableObservable(): Observable<Boolean> {
        return ObstacleManagerExt.getObstacleAvoidanceEnableObservable().filterGetOptional().distinctToUI()
    }
}