package com.geoai.featstatusbar.view

import android.os.Bundle
import android.util.Log
import com.geoai.basiclib.base.fragment.BaseVVDDialogFragment
import com.geoai.basiclib.engine.toast.toastSuccess
import com.geoai.featstatusbar.databinding.FragmentWirelessStatusBinding
import com.geoai.featstatusbar.mvi.vm.WirelessStatusViewModel
import com.geoai.mavlink.geoainet.airlink.AirlinkManager
import com.zkyt.lib_msdk_ext.component.AirLinkManagerExt
import com.zkyt.lib_msdk_ext.component.FlyControllerManagerExt
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-07 11:15.
 * @Description :
 */
class WirelessStatusDialogFragment: BaseVVDDialogFragment<WirelessStatusViewModel, FragmentWirelessStatusBinding>() {

    override fun init(savedInstanceState: Bundle?) {
        mBinding.tabWirelessPair.setOnClickListener {
            AirlinkManager.getInstance().dsp?.startPairing {
                toastSuccess("Pairing success")
            }
        }
    }
}