package com.geoai.featstatusbar.flightcheck.custom

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.FrameLayout
import android.widget.Toast
import com.geoai.featstatusbar.R
import com.geoai.featstatusbar.databinding.FcheckLySelectEditBinding

/**
@author : XPY on 2024/3/21 17:24
@description:
 */
class SelectEditView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : FrameLayout(
    context, attrs, defStyleAttr
), View.OnClickListener{
    private val viewBinding: FcheckLySelectEditBinding
    private var onSelectEditActionCallback: OnSelectEditActionCallback? = null

    private val NULL_VALUE_SHOW_TEXT = "-"

    private var lastNormalValue: Int? = null

    private var min = 20
    private var max = 2000

    init {
        viewBinding = FcheckLySelectEditBinding.inflate(LayoutInflater.from(context),this,true)

        viewBinding.fcheckSelEditHun.setOnClickListener(this)
        viewBinding.fcheckSelEditTen.setOnClickListener(this)
        viewBinding.fcheckSelEditNegHun.setOnClickListener(this)
        viewBinding.fcheckSelEditNegTen.setOnClickListener(this)

//        viewBinding.fcheckEditValue.setOnFocusChangeListener { v, hasFocus ->
//            if(hasFocus){
//                onSelectEditActionCallback?.onValue(viewBinding.fcheckEditValue.text.toString().toInt())
//            }
//        }

        viewBinding.fcheckEditValue.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_NEXT) {
                onSelectEditActionCallback?.onValue(viewBinding.fcheckEditValue.text.toString().toInt())
            }
            return@setOnEditorActionListener false
        }

        viewBinding.fcheckEditValue.hint = NULL_VALUE_SHOW_TEXT
    }

    override fun onClick(v: View?) {
        if(lastNormalValue == null){
            return
        }

        var value = viewBinding.fcheckEditValue.text.toString().toInt()
        when(v){
            viewBinding.fcheckSelEditTen-> {
                value += 10
            }
            viewBinding.fcheckSelEditHun -> {
                value += 100
            }
            viewBinding.fcheckSelEditNegHun -> {
                value -= 100
            }
            viewBinding.fcheckSelEditNegTen -> {
                value -= 10
            }
        }

        if(!checkValueLegitimate(value)){
            return
        }

        viewBinding.fcheckEditValue.setText(value.toString())
        onSelectEditActionCallback?.onValue(value)
    }

    interface OnSelectEditActionCallback{
        fun onValue(value: Int)
    }

    fun setOnSelectEditActionCallback(callback: OnSelectEditActionCallback){
        onSelectEditActionCallback = callback
    }

    fun setValue(value: Int){
//        if(!checkValueLegitimate(value)){
//            return
//        }

        lastNormalValue = value
        viewBinding.fcheckEditValue.setText(value.toString())
    }

    fun configRangeValue(min: Int,max: Int){
        this.min = min
        this.max = max
    }

    fun checkValueLegitimate(value: Int): Boolean{
        if(value < min || value > max){
            Toast.makeText(context,context.resources.getString(R.string.fcheck_edit_out_of_range),Toast.LENGTH_SHORT).show()
            if(lastNormalValue == null){
                viewBinding.fcheckEditValue.setText("")
                viewBinding.fcheckEditValue.hint = NULL_VALUE_SHOW_TEXT
            }else{
                viewBinding.fcheckEditValue.setText(lastNormalValue.toString())
                viewBinding.fcheckEditValue.hint = ""
            }
            return false
        }
        return true
    }

}