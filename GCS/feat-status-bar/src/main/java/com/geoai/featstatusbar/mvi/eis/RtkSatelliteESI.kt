package com.geoai.featstatusbar.mvi.eis

import com.geoai.basiclib.base.mvi.IUIEffect
import com.geoai.basiclib.base.mvi.IUiIntent
import com.geoai.basiclib.base.mvi.IUiState

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-07 10:48.
 * @Description :
 */
sealed class RtkSatelliteEffect: IUIEffect {

}

sealed class RtkSatelliteIntent: IUiIntent {

}

sealed class RtkSatelliteState: IUiState {
    object INIT : RtkSatelliteState()
}