package com.geoai.featstatusbar

import android.os.Bundle
import com.alibaba.android.arouter.facade.annotation.Route
import com.geoai.basiclib.base.fragment.BaseVVDFragment
import com.geoai.basiclib.base.vm.EmptyViewModel
import com.geoai.basiclib.ext.runOnUIThread
import com.geoai.featstatusbar.databinding.FragmentHmsFlexViewBinding
import com.geoai.mavlink.geoainet.flycontroller.info.AircraftDiagnosticsStateInfo
import com.geoai.modservice.di.ARouterPath
import com.zkyt.lib_msdk_ext.component.FlyControllerManagerExt
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import java.util.concurrent.TimeUnit

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-11 17:52.
 * @Description :
 */
@Route(path = ARouterPath.PATH_TOP_STATUS_HMS)
class HmsFlexFragment: BaseVVDFragment<EmptyViewModel, FragmentHmsFlexViewBinding>() {

    override fun init(savedInstanceState: Bundle?) {
        registerDroneHmsStatus {
            runOnUIThread { mBinding.hmsComponent.updateData(it) }
        }
    }

    private fun registerDroneHmsStatus(successBlock: (info: List<AircraftDiagnosticsStateInfo>) -> Unit) {
        FlyControllerManagerExt.getDiagnosticsStateObservable()
            .throttleLast(3, TimeUnit.SECONDS)
            .filter { it.isPresent }
            .map { it.get() }
            .filter {it.isNotEmpty()}
            .subscribe {
                successBlock.invoke(it)
            }.addTo(mViewModel.compositeDisposable)
    }
}