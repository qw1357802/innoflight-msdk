package com.geoai.featstatusbar.mvi.vm

import com.geoai.basiclib.base.mvi.BaseEISViewModel
import com.geoai.featstatusbar.mvi.eis.LteStatusEffect
import com.geoai.featstatusbar.mvi.eis.LteStatusIntent
import com.geoai.featstatusbar.mvi.eis.LteStatusState
import javax.inject.Inject

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-07 11:29.
 * @Description :
 */
class LteStatusViewModel @Inject constructor(): BaseEISViewModel<LteStatusEffect, LteStatusIntent, LteStatusState>() {
    override fun initUiState(): LteStatusState {
        return LteStatusState.INIT
    }

    override fun handleIntent(intent: LteStatusIntent) {
    }
}