package com.geoai.featstatusbar.mvi.eis

import com.geoai.basiclib.base.mvi.IUIEffect
import com.geoai.basiclib.base.mvi.IUiIntent
import com.geoai.basiclib.base.mvi.IUiState

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-07 09:41.
 * @Description :
 */
sealed class VersionInfoEffect: IUIEffect {

}

sealed class VersionInfoIntent: IUiIntent {

}

sealed class VersionInfoState: IUiState {
    object INIT : VersionInfoState()
}