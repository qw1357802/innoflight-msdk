package com.geoai.featstatusbar

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.lifecycleScope
import com.alibaba.android.arouter.facade.annotation.Route
import com.geoai.basiclib.base.fragment.BaseVVDFragment
import com.geoai.basiclib.ext.commContext
import com.geoai.basiclib.ext.drawable
import com.geoai.basiclib.ext.gone
import com.geoai.basiclib.ext.visible
import com.geoai.featstatusbar.databinding.FragmentTopStatusBarBinding
import com.geoai.featstatusbar.mvi.eis.TopStatusBarState
import com.geoai.featstatusbar.mvi.vm.TopStatusBarViewModel
import com.geoai.featstatusbar.view.BatteryStatusDialogFragment
import com.geoai.featstatusbar.view.LteStatusDialogFragment
import com.geoai.featstatusbar.view.RtkSatelliteDialogFragment
import com.geoai.featstatusbar.view.VersionInfoDialogFragment
import com.geoai.featstatusbar.view.WirelessStatusDialogFragment
import com.geoai.mavlink.geoainet.flycontroller.enums.HmsSeverityLevel
import com.geoai.modservice.di.ARouterPath
import com.geoai.modservice.di.eventbus.OnHmsFlexVisibility
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-03 19:31.
 * @Description :
 */
@Route(path = ARouterPath.PATH_TOP_STATUS_BAR)
class TopStatusBarFragment: BaseVVDFragment<TopStatusBarViewModel, FragmentTopStatusBarBinding>() {

    override fun init(savedInstanceState: Bundle?) {
        mBinding.imgSetting.setOnClickListener {
            //跳转ver-setting
            VersionInfoDialogFragment().also {
                it.show(mActivity.supportFragmentManager, it.javaClass.name)
            }
        }
        mBinding.uxBat.setOnClickListener {
            //跳转bat-setting
            BatteryStatusDialogFragment().also {
                it.show(mActivity.supportFragmentManager, it.javaClass.name)
            }
        }
        mBinding.uxRc.setOnClickListener {
            //跳转wireless-setting
            WirelessStatusDialogFragment().also {
                it.show(mActivity.supportFragmentManager, it.javaClass.name)
            }
        }
        mBinding.uxSatellite.setOnClickListener {
            //跳转rtk-setting
            RtkSatelliteDialogFragment().also {
                it.show(mActivity.supportFragmentManager, it.javaClass.name)
            }
        }
        mBinding.uxLte.setOnClickListener {
            //跳转lte-setting
            LteStatusDialogFragment().also {
                it.show(mActivity.supportFragmentManager, it.javaClass.name)
            }
        }

        mBinding.llHmsMenu.setOnClickListener {
            if (mViewModel.isHmsFlexViewVisibility.value!!) {
                EventBus.getDefault().post(OnHmsFlexVisibility(View.VISIBLE))
                mViewModel.isHmsFlexViewVisibility.value = false
            } else {
                EventBus.getDefault().post(OnHmsFlexVisibility(View.GONE))
                mViewModel.isHmsFlexViewVisibility.value = true
            }
        }
    }

    override fun startObserve() {
        super.startObserve()

        lifecycleScope.launch(Dispatchers.Main) {
            mViewModel.uiStateFlow.collect { state ->
                when (state) {
                    is TopStatusBarState.DroneConnected -> if (state.isConnected.not()) mBinding.tvFlyMode.text = getString(R.string.not_connected)
                    is TopStatusBarState.DroneFlyMode -> mBinding.tvFlyMode.text = state.flyMode
                    is TopStatusBarState.DroneSatelliteCount -> mBinding.uxSatellite.setSatelliteCount(state.fixType, state.count)
                    is TopStatusBarState.DroneBatteryPercent -> mBinding.uxBat.updateBatteryInfo(state.percent)
                    is TopStatusBarState.DroneWirelessSignal -> mBinding.uxRc.let {
                        it.setRCSignal(state.uplinkLevel)
                        it.setVideoSignal(state.downlinkLevel)
                    }
                    is TopStatusBarState.DroneCellularStatus -> mBinding.uxLte.let { lteView->
                        state.cellularStatus?.let { lteView.visible() } ?: lteView.gone()
                    }
                    is TopStatusBarState.DroneHmsStatus -> {
                        val sortList = state.infos.sortedByDescending { diagnostics -> diagnostics.alertLevel.value() }
                        if (sortList.isEmpty()) {
                            mBinding.llHmsMenu.gone()
                        } else {
                            mBinding.llHmsMenu.visible()
                        }
                        val type = HmsSeverityLevel.valueOf(sortList[0].alertLevel.value())
                        val subType = type.toString().substring(type.toString().lastIndexOf("_") + 1)
                        mBinding.tvHmsIndex.text = state.infos.size.toString()
                        mBinding.tvHmsTips.text = "$subType · ${sortList[0].message}"
                        val bg = when (type) {
                            HmsSeverityLevel.MAV_SEVERITY_EMERGENCY, HmsSeverityLevel.MAV_SEVERITY_CRITICAL, HmsSeverityLevel.MAV_SEVERITY_ERROR -> commContext().drawable(R.drawable.bg_hms_error_title_content)
                            HmsSeverityLevel.MAV_SEVERITY_WARNING -> commContext().drawable(R.drawable.bg_hms_warn_title_content)
                            else -> commContext().drawable(R.drawable.bg_hms_default_title_content)
                        }
                        mBinding.tvHmsIndex.background = bg
                        mBinding.tvHmsTips.background = bg
                    }
                }
            }
        }
    }
}