package com.geoai.featstatusbar.flightcheck.view

import android.os.Bundle
import android.view.View
import com.geoai.basiclib.base.fragment.BaseVVDFragment
import com.geoai.featstatusbar.databinding.FcheckLyItemGoHomeBinding
import com.geoai.featstatusbar.flightcheck.custom.SelectEditView
import com.geoai.featstatusbar.flightcheck.vm.ItemGoHomeVM
import io.reactivex.rxkotlin.addTo

/**
@author : XPY on 2024/3/7 13:54
@description:
 */
class ItemGoHomeCellView: BaseVVDFragment<ItemGoHomeVM, FcheckLyItemGoHomeBinding>() {

    private val ALLOW_MIN = 20
    private val ALLOW_MAX = 2000

    override fun init(savedInstanceState: Bundle?) {
        mBinding.fcheckLyGoHomeEdit.configRangeValue(ALLOW_MIN, ALLOW_MAX)

        mBinding.fcheckLyGoHomeEdit.setOnSelectEditActionCallback(object :
            SelectEditView.OnSelectEditActionCallback {
            override fun onValue(value: Int) {
                mViewModel.setGoHomeAltitudeRx(value).subscribe().addTo(mViewModel.compositeDisposable)
            }
        })

        mViewModel.getGoHomeAltitudeObservable().subscribe({
            mBinding.fcheckLyGoHomeEdit.setValue(it)
        }, {
        }).addTo(mViewModel.compositeDisposable)
    }

    companion object {
        private const val TAG = "ItemGoHomeCellView"
    }
}