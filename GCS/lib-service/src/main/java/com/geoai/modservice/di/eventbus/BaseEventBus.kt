package com.geoai.modservice.di.eventbus

import android.view.View

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-08 12:54.
 * @Description :
 */

data class OnMapClickEvent(val latitude: Double = 0.0, val longitude: Double = 0.0) {}

data class OnMapTouchEvent(val x: Float = 0f, val y: Float = 0f) {}

data class OnHmsFlexVisibility(val isVisibility: Int) {}

data class OnStatusMenuClickEvent(val ts: Long) {}