package com.geoai.modservice.di.sp

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-03 18:33.
 * @Description :
 */
class MapControlConstants {
    companion object {
        const val IS_MAP_LOCKED_BY_DRONE = "IS_MAP_LOCKED_BY_DRONE"

        const val MAP_SOURCE_KEY = "MAP_SOURCE_KEY"
    }
}