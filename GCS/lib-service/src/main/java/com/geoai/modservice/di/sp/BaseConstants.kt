package com.geoai.modservice.di.sp

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-07 11:48.
 * @Description :
 */
class BaseConstants {
    companion object {
        //右侧栏目初始化的序列
        const val RF_MENU_SELECTED_INDEX = "RF_MENU_SELECTED_INDEX"
        //左侧状态初始化的序列
        const val LS_MENU_SELECTED_INDEX = "LS_MENU_SELECTED_INDEX"
    }
}