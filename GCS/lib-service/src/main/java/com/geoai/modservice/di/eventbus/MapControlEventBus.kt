package com.geoai.modservice.di.eventbus

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-03 18:01.
 * @Description :
 */
data class CenterToLastLocation(val hashCode: Long?) {}

data class CenterToDroneLocation(val hashCode: Long?) {}

data class LockCameraByDroneLocation(val isLock: Boolean) {}

data class ChangeMapSource(val index: Int){}

data class ToggleMapSize(val type: ToggleSize){}//0: 最大；1：mini；2：icon

data class ToggleFpvSize(val type: ToggleSize) {}

data class DisplayCameraLinesStyle(val style: CameraLinesStyle) {}

enum class ToggleSize {
    BIG, MINI, ICON
}

enum class CameraLinesStyle {
    NONE, A, B, C
}