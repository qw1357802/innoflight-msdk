package com.geoai.modservice.di.sp

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-10 16:14.
 * @Description :
 */
class FpvControlConstants {
    companion object {
        const val FPV_SOURCE_TYPE = "FPV_SOURCE_TYPE"

        const val CAMERA_LINE_STYLE = "CAMERA_LINE_STYLE"
    }
}