package com.geoai.modservice.di

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-02 16:30.
 * @Description :
 */
object ARouterPath {
    const val PATH_PAGE_MAPVIEW = "/ui_map/frame/map"

    const val PATH_TOP_STATUS_BAR = "/ui_top/status/bar"

    const val PATH_TOP_STATUS_HMS = "/ui_top/status/hms"

    const val PATH_CONTROL_COMPONENT_MAP_CONTROL = "/feat_map_control/component/map_control"

    const val PATH_CONTROL_FC_CONTROL = "/feat_fc_control/component/fc_control"

    const val PATH_CONTROL_FLIGHT_STATUS = "/feat_fc_control/component/flight_status"

    const val PATH_CONTROL_FLIGHT_OBSTACLE = "/feat_fc_control/component/flight_obstacle"

    const val PATH_CONTROL_GIMBAL_CONTROL = "/feat_gimbal_control/component/gimbal_control"

    const val PATH_CONTROL_MISSION_CONTROL = "/feat_mission_control/component/mission_control"

    const val PATH_CONTROL_CAMERA_CONTROL = "/feat_camera_control/component/camera_control"

    const val PATH_CONTROL_CAMERA_NV2_CONTROL = "/feat_camera_control/component/camera_control_nv2"

    const val PATH_CONTROL_CAMERA_NV3_CONTROL = "/feat_camera_control/component/camera_control_nv3"

    const val PATH_CONTROL_CELLULAR_CONTROL = "/feat_crest_control/component/cellular_control"

    const val PATH_UI_CAMERA_VIDEO = "/feat_geoai_video/component/camera"

//    const val PATH_THIRD_EGG_DROPPER = "/feat_egg_dropper/component/drop"
//    ---------------------状态扩展---------------------
    const val PATH_STATUS_EMPTY = ""
    const val PATH_STATUS_FC = "/feat_fc_control/component/fc_status"
    const val PATH_STATUS_FC_PARACHUTE = "/feat_fc_control/component/fc_parachute"
    const val PATH_STATUS_MISSION = "/feat_mission_control/component/mission"
    const val PATH_STATUS_GIMBAL = "/feat_gimbal_control/component/gimbal"
    const val PATH_STATUS_CELLULAR = "/feat_crest_control/component/cellular"
    const val PATH_STATUS_CAMERA_MODE = "/feat_camera_control/component/camera_mode"
    const val PATH_STATUS_CAMERA_PARAM = "/feat_camera_control/component/camera_param"
}