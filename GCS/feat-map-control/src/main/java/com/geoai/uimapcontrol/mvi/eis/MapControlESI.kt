package com.geoai.uimapcontrol.mvi.eis

import com.geoai.basiclib.base.mvi.IUIEffect
import com.geoai.basiclib.base.mvi.IUiIntent
import com.geoai.basiclib.base.mvi.IUiState

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-03 14:47.
 * @Description :
 */
sealed class MapControlEffect: IUIEffect {

}

sealed class MapControlIntent: IUiIntent {

}

sealed class MapControlState: IUiState {
    object INIT : MapControlState()
}