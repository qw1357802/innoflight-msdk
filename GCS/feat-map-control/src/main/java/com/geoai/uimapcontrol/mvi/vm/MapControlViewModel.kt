package com.geoai.uimapcontrol.mvi.vm

import com.alibaba.android.arouter.facade.annotation.Route
import com.geoai.basiclib.base.mvi.BaseEISViewModel
import com.geoai.uimapcontrol.mvi.eis.MapControlEffect
import com.geoai.uimapcontrol.mvi.eis.MapControlIntent
import com.geoai.uimapcontrol.mvi.eis.MapControlState
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-03 14:47.
 * @Description :
 */
@HiltViewModel
class MapControlViewModel @Inject constructor(): BaseEISViewModel<MapControlEffect, MapControlIntent, MapControlState>(){
    override fun initUiState(): MapControlState {
        return MapControlState.INIT
    }

    override fun handleIntent(intent: MapControlIntent) {
    }
}