package com.geoai.uimapcontrol

import android.os.Bundle
import android.util.Log
import com.alibaba.android.arouter.facade.annotation.Route
import com.geoai.basiclib.base.fragment.BaseVVDFragment
import com.geoai.basiclib.engine.dialog.OnDialogSelectListener
import com.geoai.basiclib.engine.dialog.showCenterSelectDialog
import com.geoai.basiclib.ext.SP
import com.geoai.basiclib.ext.putBoolean
import com.geoai.basiclib.ext.putInt
import com.geoai.modservice.di.ARouterPath
import com.geoai.modservice.di.eventbus.CenterToDroneLocation
import com.geoai.modservice.di.eventbus.CenterToLastLocation
import com.geoai.modservice.di.eventbus.ChangeMapSource
import com.geoai.modservice.di.eventbus.LockCameraByDroneLocation
import com.geoai.modservice.di.sp.MapControlConstants.Companion.IS_MAP_LOCKED_BY_DRONE
import com.geoai.modservice.di.sp.MapControlConstants.Companion.MAP_SOURCE_KEY
import com.geoai.uicore.ControllerTextView
import com.geoai.uimapcontrol.mvi.vm.MapControlViewModel
import com.geoai.uimapcoontrol.R
import com.geoai.uimapcoontrol.databinding.FragmentMapControlBinding
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-03 14:46.
 * @Description :
 */
@Route(path = ARouterPath.PATH_CONTROL_COMPONENT_MAP_CONTROL)
class MapControlFragment : BaseVVDFragment<MapControlViewModel, FragmentMapControlBinding>(){

    override fun init(savedInstanceState: Bundle?) {

        mBinding.btnMoveByDroneLocation.setClickLock(SP().getBoolean(IS_MAP_LOCKED_BY_DRONE, false))

        mBinding.btnCenterAtDroneLocation.setOnClickListener { EventBus.getDefault().post(CenterToDroneLocation(null)) }

        mBinding.btnCenterAtCurrentLocation.setOnClickListener { EventBus.getDefault().post(CenterToLastLocation(null)) }

        mBinding.btnMoveByDroneLocation.setOnClickListener {it as ControllerTextView
            it.getLocked().let { currentState ->
                EventBus.getDefault().post(LockCameraByDroneLocation(currentState.not()))
                SP().putBoolean(IS_MAP_LOCKED_BY_DRONE, currentState.not())
                it.setClickLock(currentState.not())
            }
        }

        mBinding.btnChangeMapSource.setOnClickListener {
            showCenterSelectDialog(requireActivity(), object : OnDialogSelectListener {
                    override fun onResult(index: Int, keyword: String) {
                        SP().putInt(MAP_SOURCE_KEY, index + 1)
                        EventBus.getDefault().post(ChangeMapSource(index + 1))
                    }
                }, getString(R.string.select_map_source),
                *resources.getStringArray(R.array.map_source_array)
            )
        }
    }
}