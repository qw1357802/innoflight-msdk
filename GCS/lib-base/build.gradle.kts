plugins {
    id("com.android.library")
}

// 使用自定义插件
apply<DefaultGradlePlugin>()

android {
    namespace = "com.geoai.basiclib"
}


dependencies {
    retrofit()
    dataStore()
    imageSelector()
    xpopup()
    glide()
    implementation(VersionThirdPart.toaster)
    implementation(VersionThirdPart.xxPermission)

    eventBus()

    refresh()
    work()
}