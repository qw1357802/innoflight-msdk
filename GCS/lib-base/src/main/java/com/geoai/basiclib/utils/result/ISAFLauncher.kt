package com.geoai.basiclib.utils.result

import androidx.activity.result.ActivityResultCaller


typealias saf = ISAFLauncher

/**
 * 定义是否需要SAFLauncher
 */
interface ISAFLauncher {

    fun <T : ActivityResultCaller> T.initLauncher()

    fun getLauncher(): GetSAFLauncher?

}