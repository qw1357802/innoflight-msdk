package com.geoai.basiclib.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.geoai.basiclib.utils.log.MyLogUtils

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-02 11:10.
 * @Description :
 */
class MyPager2Adapter(
    fm: FragmentManager,
    lifecycle: Lifecycle,
    private val fragments: List<Fragment>
) : FragmentStateAdapter(fm, lifecycle) {

    override fun getItemCount(): Int = fragments.size

    override fun createFragment(position: Int): Fragment {
        return fragments[position]
    }

    override fun getItemId(position: Int): Long {
        val name = fragments[position].javaClass.simpleName+ position
        val toLong = name.hashCode().toLong()
        MyLogUtils.w("getItemId:$toLong")
        return toLong
    }
}