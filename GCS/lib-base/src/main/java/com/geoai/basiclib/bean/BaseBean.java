package com.geoai.basiclib.bean;

import com.geoai.basiclib.bean.Optional;
import com.google.gson.annotations.SerializedName;


import java.io.Serializable;

/**
 * 基类数据结构
 */
public class BaseBean<T> implements Serializable {
    @SerializedName(value = "code", alternate = "errorCode")
    private int code;
    @SerializedName("data")
    private T data;
    @SerializedName(value = "msg", alternate = "errorMsg")
    private String message;

    public void setData(T data) {
        this.data = data;
    }

    public T getData() {
        return data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * 自定义可为空的字段
     */
    public Optional<T> transform() {
        return new Optional<>(data);
    }
}
