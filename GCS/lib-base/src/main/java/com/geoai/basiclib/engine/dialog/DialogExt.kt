package com.geoai.basiclib.engine.dialog

import android.content.Context
import com.geoai.basiclib.ext.commContext
import com.lxj.xpopup.XPopup


fun Any.showCenterSelectDialog(context: Context, listener: OnDialogSelectListener, title: String, vararg items: String) {
    XPopup.Builder(context) //.maxWidth(600)
        .asCenterList(title, items) { position, text ->
            listener.onResult(position, text)
        }.show()
}

fun <T> Any.showCenterSelectDialogWithType(context: Context, listener: OnDialogSelectWithTypeListener<T>, title: String, items: MutableList<String>, retType: MutableList<T>) {
    if (items.size != retType.size) return

    XPopup.Builder(context) //.maxWidth(600)
        .asCenterList(title, items.toTypedArray()) { position, text ->
            listener.onResult(position, text, retType[position])
        }.show()
}

interface OnDialogSelectListener {
    fun onResult(index: Int, keyword: String)
}

interface OnDialogSelectWithTypeListener<T> {
    fun onResult(index: Int, keyword: String, type: T)
}