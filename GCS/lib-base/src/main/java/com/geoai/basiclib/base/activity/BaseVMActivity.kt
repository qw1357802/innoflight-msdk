package com.geoai.basiclib.base.activity

import androidx.activity.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.geoai.basiclib.utils.NetWorkUtil
import com.geoai.basiclib.view.dialog.LoadingDialogManager
import com.geoai.basiclib.base.vm.BaseViewModel
import com.geoai.basiclib.bean.LoadAction
import com.geoai.basiclib.ext.getVMCls
import org.greenrobot.eventbus.EventBus

/**
 * 加入ViewModel与LoadState
 * 默认为Loading弹窗的加载方式
 */
abstract class BaseVMActivity<VM : BaseViewModel> : AbsActivity() {

    protected lateinit var mViewModel: VM

    abstract fun hasRegisterEventBus(): Boolean

    override fun onStart() {
        super.onStart()
        if (hasRegisterEventBus()) {
            EventBus.getDefault().register(this)
        }
    }

    override fun onStop() {
        super.onStop()
        if (hasRegisterEventBus()) {
            EventBus.getDefault().unregister(this)
        }
    }

    //使用这个方法简化ViewModel的获取
    protected inline fun <reified VM : BaseViewModel> getViewModel(): VM {
        val viewModel: VM by viewModels()
        return viewModel
    }

    //反射自动获取ViewModel实例
    protected open fun createViewModel(): VM {
        return ViewModelProvider(this).get(getVMCls(this))
    }

    override fun initViewModel() {
        mViewModel = createViewModel()
    }

    override fun startObserve() {
        //观察网络数据状态
        mViewModel.getActionLiveData().observe(this, stateObserver)
    }

    override fun setContentView() {
        setContentView(getLayoutIdRes())
    }

    abstract fun getLayoutIdRes(): Int

    override fun onNetworkConnectionChanged(isConnected: Boolean, networkType: NetWorkUtil.NetworkType?) {
    }

    // ================== 网络状态的监听 ======================

    private var stateObserver: Observer<LoadAction> = Observer { loadAction ->
        when (loadAction.action) {
            LoadAction.STATE_NORMAL -> showStateNormal()
            LoadAction.STATE_ERROR -> showStateError(loadAction.message)
            LoadAction.STATE_SUCCESS -> showStateSuccess()
            LoadAction.STATE_LOADING -> showStateLoading()
            LoadAction.STATE_NO_DATA -> showStateNoData()
            LoadAction.STATE_PROGRESS -> showStateProgress()
            LoadAction.STATE_HIDE_PROGRESS -> hideStateProgress()
        }
    }

    protected open fun showStateNormal() {}

    protected open fun showStateError(message: String?) {
        LoadingDialogManager.get().dismissLoading()
    }

    protected open fun showStateSuccess() {
        LoadingDialogManager.get().dismissLoading()
    }

    protected open fun showStateLoading() {
        LoadingDialogManager.get().showLoading(this)
    }

    protected open fun showStateNoData() {
        LoadingDialogManager.get().dismissLoading()
    }

    protected fun showStateProgress() {
        LoadingDialogManager.get().showLoading(mActivity)
    }

    protected fun hideStateProgress() {
        LoadingDialogManager.get().dismissLoading()
    }

}