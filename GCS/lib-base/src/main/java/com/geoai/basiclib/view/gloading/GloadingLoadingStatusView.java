package com.geoai.basiclib.view.gloading;


import static com.geoai.basiclib.view.gloading.Gloading.STATUS_EMPTY_DATA;
import static com.geoai.basiclib.view.gloading.Gloading.STATUS_LOADING;
import static com.geoai.basiclib.view.gloading.Gloading.STATUS_LOAD_FAILED;
import static com.geoai.basiclib.view.gloading.Gloading.STATUS_LOAD_SUCCESS;
import static com.geoai.basiclib.view.gloading.Gloading.STATUS_NORMAL;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.geoai.basiclib.R;
import com.geoai.basiclib.view.titlebar.EasyTitleBar;


/**
 * 这是菊花Loading
 */
@SuppressLint("ViewConstructor")
public class GloadingLoadingStatusView extends LinearLayout implements View.OnClickListener {

    private final TextView mTextView;
    private Runnable mRetryTask;
    private final ImageView mImageView;
    private final EasyTitleBar mTitle;

    public GloadingLoadingStatusView(Context context, Runnable retryTask) {
        super(context);
        setOrientation(VERTICAL);
        setGravity(Gravity.CENTER_HORIZONTAL);
        LayoutInflater.from(context).inflate(R.layout.view_gloading_loading_status, this, true);
        mImageView = findViewById(R.id.image);
        mTextView = findViewById(R.id.text);
        mTitle = findViewById(R.id.title);

        if (retryTask != null) {
            this.mRetryTask = retryTask;
        }
    }

    //是否显示文本
    public void setMsgViewVisibility(boolean visible) {
        mTextView.setVisibility(visible ? VISIBLE : GONE);
    }

    //是否显示Title
    public void setTitleBarVisibility(boolean visible) {
        mTitle.setVisibility(visible ? VISIBLE : GONE);
    }

    //设置是否沉浸式
    public void setImmersive(boolean isImmersive) {
        mTitle.setHasStatusPadding(isImmersive);
    }

    public void setStatus(int status, String msg) {
        boolean show = true;  //是否展示这个布局

        OnClickListener onClickListener = null;
        int image = R.drawable.anim_gloading;
        mImageView.setVisibility(VISIBLE);
        String str = "加载中...";

        switch (status) {
            case STATUS_LOAD_SUCCESS:
                mImageView.setVisibility(GONE);
                show = false;
                break;

            case STATUS_LOADING:
            case STATUS_NORMAL:
                mImageView.setVisibility(VISIBLE);
                str = "加载中...";
                break;

            case STATUS_LOAD_FAILED:
                mImageView.setVisibility(VISIBLE);
                //是否需要加网络状态判断
                str = TextUtils.isEmpty(msg) ? "加载错位" : msg;
                image = R.mipmap.loading_error;
                onClickListener = this;
                break;

            case STATUS_EMPTY_DATA:
                mImageView.setVisibility(VISIBLE);
                str = "没有数据";
                image = R.mipmap.loading_error;
                break;

            default:
                break;
        }

        mImageView.setImageResource(image);
        setOnClickListener(onClickListener);

        mTextView.setText(str);
        setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onClick(View v) {
        if (mRetryTask != null) {
            mRetryTask.run();
        }
    }

    public void setRetryTask(Runnable retryTask) {
        if (retryTask != null) {
            mRetryTask = retryTask;
        }

    }

}
