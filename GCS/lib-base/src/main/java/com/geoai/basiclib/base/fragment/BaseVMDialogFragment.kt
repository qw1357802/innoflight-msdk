package com.geoai.basiclib.base.fragment

import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.geoai.basiclib.utils.NetWorkUtil
import com.geoai.basiclib.view.dialog.LoadingDialogManager
import com.geoai.basiclib.base.vm.BaseViewModel
import com.geoai.basiclib.bean.LoadAction
import com.geoai.basiclib.ext.getVMCls
import org.greenrobot.eventbus.EventBus

/**
 * 加入ViewModel与LoadState
 * 默认为Loading的加载
 */
abstract class BaseVMDialogFragment<VM : BaseViewModel> : AbsDialogFragment() {

    protected lateinit var mViewModel: VM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViewModel()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        startObserve()

        init(savedInstanceState)

        if (hasRegisterEventBus()) {
            EventBus.getDefault().register(this)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        if (hasRegisterEventBus()) {
            EventBus.getDefault().unregister(this)
        }
    }

    protected open fun initViewModel() {
        mViewModel = createViewModel()
    }

    protected open fun startObserve() {
        //观察网络数据状态
        mViewModel.getActionLiveData().observe(viewLifecycleOwner, stateObserver)
    }

    //使用这个方法简化ViewModel的初始化
    protected inline fun <reified VM : BaseViewModel> getViewModel(): VM {
        val viewModel: VM by viewModels()
        return viewModel
    }

    //反射获取ViewModel实例
    protected open fun createViewModel(): VM {
        return ViewModelProvider(this).get(getVMCls(this))
    }

    override fun setContentView(container: ViewGroup?): View {
        return layoutInflater.inflate(getLayoutIdRes(), container, false)
    }

    abstract fun hasRegisterEventBus(): Boolean

    /**
     * 获取layout的id，具体由子类实现
     */
    abstract fun getLayoutIdRes(): Int

    abstract fun init(savedInstanceState: Bundle?)

    override fun onNetworkConnectionChanged(isConnected: Boolean, networkType: NetWorkUtil.NetworkType?) {
    }

    // ================== 网络状态的监听 ======================

    private var stateObserver: Observer<LoadAction> = Observer { loadAction ->
        when (loadAction.action) {
            LoadAction.STATE_NORMAL -> showStateNormal()
            LoadAction.STATE_ERROR -> showStateError(loadAction.message)
            LoadAction.STATE_SUCCESS -> showStateSuccess()
            LoadAction.STATE_LOADING -> showStateLoading()
            LoadAction.STATE_NO_DATA -> showStateNoData()
            LoadAction.STATE_PROGRESS -> showStateProgress()
            LoadAction.STATE_HIDE_PROGRESS -> hideStateProgress()
        }
    }

    protected fun showStateNormal() {}

    protected fun showStateError(message: String?) {
        LoadingDialogManager.get().dismissLoading()
    }

    protected fun showStateSuccess() {
        LoadingDialogManager.get().dismissLoading()
    }

    protected fun showStateLoading() {
        LoadingDialogManager.get().showLoading(mActivity)
    }

    protected fun showStateNoData() {
        LoadingDialogManager.get().dismissLoading()
    }

    protected fun showStateProgress() {
        LoadingDialogManager.get().showLoading(mActivity)
    }

    protected fun hideStateProgress() {
        LoadingDialogManager.get().dismissLoading()
    }
}