package com.geoai.basiclib.engine.permission

import android.app.Activity
import android.net.Uri
import android.os.Build
import android.util.Log
import androidx.fragment.app.Fragment
import com.geoai.basiclib.ext.commContext
import com.hjq.permissions.OnPermissionCallback
import com.hjq.permissions.XXPermissions
import java.io.File

/**
 * 申请权限的引擎类
 * Activity的申请权限
 */
fun Activity.requestPermission(
    vararg permissions: String,
    block: () -> Unit
) {

    XXPermissions.with(this)
        .permission(permissions)
        .interceptor(PermissionInterceptor())
        .request { _, all ->
            if (all) {
                block()
            }
        }
}

/**
 * 申请权限的引擎类
 * Activity的申请权限
 */
fun Activity.requestPermission(
    vararg permissions: String,
    block: (isAll: Boolean) -> Unit
) {

    XXPermissions.with(this)
        .permission(permissions)
        .interceptor(PermissionInterceptor())
        .request(object : OnPermissionCallback{
            override fun onGranted(p0: MutableList<String>, p1: Boolean) {
                block(true)
            }

            override fun onDenied(permissions: MutableList<String>, doNotAskAgain: Boolean) {
                super.onDenied(permissions, doNotAskAgain)
                block(false)
            }
        })
}

/**
 * 申请权限的引擎
 * Fragment的申请权限
 */
fun Fragment.requestPermission(
    vararg permissions: String,
    block: () -> Unit
) {
    activity?.requestPermission(permissions = permissions, block = block)
}


fun Any.getFileUri(file: File): Uri {

    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        PermissionFileProvider.getUriForFile(commContext(), commContext().packageName + ".file.path.share", file)
    } else Uri.fromFile(file)

}