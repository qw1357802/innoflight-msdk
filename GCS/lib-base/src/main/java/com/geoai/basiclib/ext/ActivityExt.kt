package com.geoai.basiclib.ext

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.geoai.basiclib.utils.result.ghost.Ghost

/**
 * Activity相关
 */

//内联函数+标注泛型 = 泛型实例化
inline fun <reified T> Fragment.gotoActivity(
    flag: Int = -1,
    bundle: Array<out Pair<String, Any?>>? = null
) {
    activity?.gotoActivity<T>(flag, bundle)
}

inline fun <reified T> View.gotoActivity(
    flag: Int = -1,
    bundle: Array<out Pair<String, Any?>>? = null
) {
    context.gotoActivity<T>(flag, bundle)
}

inline fun <reified T> Context.gotoActivity(
    flag: Int = -1,
    bundle: Array<out Pair<String, Any?>>? = null
) {

    val intent = Intent(this, T::class.java).apply {
        if (flag != -1) {
            this.addFlags(flag)
        }
        if (this@gotoActivity !is Activity) {
            this.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        }
        if (bundle != null) {
            putExtras(bundle.toBundle()!!)
        }
    }
    startActivity(intent)
}

//真正执行ActivityForResult的方法，使用Ghost的方式执行
inline fun <reified T> Fragment.gotoSAFByGhost(
    flag: Int = -1,
    bundle: Array<out Pair<String, Any?>>? = null,
    crossinline callback: ((result: Intent?) -> Unit)
) {
    activity?.gotoSAFByGhost<T>(flag, bundle, callback)
}

inline fun <reified T> View.gotoSAFByGhost(
    flag: Int = -1,
    bundle: Array<out Pair<String, Any?>>? = null,
    crossinline callback: ((result: Intent?) -> Unit)
) {
    (context as FragmentActivity).gotoSAFByGhost<T>(flag, bundle, callback)
}


inline fun <reified T> FragmentActivity.gotoSAFByGhost(
    flag: Int = -1,
    bundle: Array<out Pair<String, Any?>>? = null,
    crossinline callback: ((result: Intent?) -> Unit)
) {
    val intent = Intent(this, T::class.java).apply {
        if (flag != -1) {
            this.addFlags(flag)
        }
        if (bundle != null) {
            //调用自己的扩展方法-数组转Bundle
            putExtras(bundle.toBundle()!!)
        }
    }
    Ghost.launchActivityForResult(this, intent, callback)
}
