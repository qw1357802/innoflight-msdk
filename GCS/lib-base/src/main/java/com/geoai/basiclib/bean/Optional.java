package com.geoai.basiclib.bean;


/**
 * 处理data为null的情况
 */
public class Optional<M> {

    private final M data; // 接收到的返回结果

    public Optional(M data) {
        this.data = data;
    }

    // 判断返回结果是否为null
    public boolean isEmpty() {
        return this.data == null;
    }

    // 获取可以为null的返回结果
    public M getData() {
        return data;
    }

}
