package com.geoai.basiclib.ext

import com.alibaba.android.arouter.launcher.ARouter

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-02 19:54.
 * @Description :
 */
inline fun <reified T> Any.aRouterInvokeService(key: String): T {
    return (ARouter.getInstance().build(key).navigation() as T)
}