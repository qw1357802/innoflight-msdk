package com.geoai.feat_crest_control

import android.os.Bundle
import com.alibaba.android.arouter.facade.annotation.Route
import com.geoai.basiclib.base.fragment.BaseVVDFragment
import com.geoai.basiclib.base.vm.EmptyViewModel
import com.geoai.basiclib.engine.toast.toastError
import com.geoai.basiclib.engine.toast.toastSuccess
import com.geoai.feat_crest_control.databinding.FragmentCellularControlBinding
import com.geoai.mavlink.geoainet.airlink.enums.CellularEnhancedTransmissionType
import com.geoai.mavlink.geoainet.flycontroller.enums.AircraftFailSafeBehaviorMode
import com.geoai.modservice.di.ARouterPath
import com.geoai.uicore.ext.showItemSelectedDialog
import com.zkyt.lib_msdk_ext.component.AirLinkManagerExt
import com.zkyt.lib_msdk_ext.component.CrestManagerExt
import com.zkyt.lib_msdk_ext.component.FlyControllerManagerExt
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-30 11:18.
 * @Description :
 */
@Route(path = ARouterPath.PATH_CONTROL_CELLULAR_CONTROL)
class CellularControlFragment: BaseVVDFragment<EmptyViewModel, FragmentCellularControlBinding>() {

    override fun init(savedInstanceState: Bundle?) {
        mBinding.btnCellularLicenseRegister.setOnClickListener {
            if (getCellularRegisterStatus()) {
                toastSuccess("Cellular License Register: ${getCellularRegisterStatus()}")
            } else {
                toastError("Cellular License Register: ${getCellularRegisterStatus()}")
            }
        }
        mBinding.btnCellularServerConnectStatus.setOnClickListener { getCellularServerConnectStatus() }
        mBinding.btnCellularEnhancedMode.setOnClickListener { showCellularEnhancedModeDialog() }
    }

    private fun getCellularRegisterStatus(): Boolean {
        return CrestManagerExt.getOriginManager()?.isDroneLTERegister?:false
    }

    private fun getCellularServerConnectStatus() {
        AirLinkManagerExt.getCellularStatusRx().subscribe({
            val ret = "连接状态：${it.connectState.name}\r\n 服务器状态：${it.cellularServerConnectionChangedState.name}"
            toastSuccess(ret)
        }, {
            toastError("状态获取失败：" + it.stackTraceToString())
        }).addTo(mViewModel.compositeDisposable)
    }

    private fun showCellularEnhancedModeDialog() {
        showStateLoading()
        getCellularEnhancedModeRx({mode ->
            showStateSuccess()
            showItemSelectedDialog(
                getString(R.string.cellular_setting_cellular_enhanced_mode_title),
                mode.name,
                CellularEnhancedTransmissionType.values().map { it.name }.toTypedArray(),
                parentFragmentManager
            ) { text, pos ->
                showStateLoading()
                AirLinkManagerExt.setCellularModeRx(CellularEnhancedTransmissionType.valueOf(text!!)).subscribe({
                    showStateSuccess()
                }, {
                    showStateError(it.message!!)
                }).addTo(mViewModel.compositeDisposable)
            }
        }, {
            showStateError(it)
        })
    }

    private fun getCellularEnhancedModeRx(success: (CellularEnhancedTransmissionType) -> Unit, error: (String) -> Unit) {
        AirLinkManagerExt.getCellularModeRx()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({success.invoke(it)}) {
                error.invoke(it.message!!)
            }.addTo(mViewModel.compositeDisposable)
    }
}