package com.geoai.feat_crest_control

import android.os.Bundle
import com.alibaba.android.arouter.facade.annotation.Route
import com.geoai.basiclib.base.fragment.BaseVVDFragment
import com.geoai.basiclib.base.vm.EmptyViewModel
import com.geoai.feat_crest_control.databinding.FragmentCellularStatusBinding
import com.geoai.mavlink.geoainet.airlink.info.CellularQualityInfo
import com.geoai.mavlink.geoainet.airlink.info.CellularSignalInfo
import com.geoai.mavlink.geoainet.crest.enums.SimExist
import com.geoai.mavlink.geoainet.crest.info.LteStatus
import com.geoai.modservice.di.ARouterPath
import com.geoai.modservice.di.eventbus.OnStatusMenuClickEvent
import com.zkyt.lib_msdk_ext.component.AirLinkManagerExt
import com.zkyt.lib_msdk_ext.component.CrestManagerExt
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import org.greenrobot.eventbus.EventBus

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-30 09:47.
 * @Description :
 */
@Route(path = ARouterPath.PATH_STATUS_CELLULAR)
class CellularStatusFragment: BaseVVDFragment<EmptyViewModel, FragmentCellularStatusBinding>() {

    override fun init(savedInstanceState: Bundle?) {
        mBinding.cellularStatus.setOnClickListener {
            EventBus.getDefault().post(OnStatusMenuClickEvent(System.currentTimeMillis()))
        }

        getCellularVideoStatusObservable {
            val ret = "packetLoss: ${it.packetLoss}, rBit: ${it.receivedBitrate}, frame: ${it.outputFrameRate}"
            mBinding.tvCellularStatus.text = ret
        }

        getCellularSignalStatusObservable {
            val ret = "gnd: ${it.groundSignal.name}, air: ${it.airlinkSignal.name}"
            mBinding.tvSignalStatus.text = ret
        }

        getCrestLteStatusObservable { lteStatus ->
            if (lteStatus.simExist == SimExist.SIM_EXIST) {
                val ret = "carrier: ${lteStatus.lteWirelessCarrier.name}, level: ${lteStatus.signalLevel.name}, rsrp: ${lteStatus.rsrp}, snr: ${lteStatus.snr}"
                mBinding.tvSimStatus.text = ret
            } else {
                val ret = "no sim card detected."
                mBinding.tvSimStatus.text = ret
            }
        }
    }

    private fun getCellularVideoStatusObservable(block: (cellularVideoStatus: CellularQualityInfo) -> Unit) {
        AirLinkManagerExt.getCellularVideoQualityObservable()
            .observeOn(AndroidSchedulers.mainThread())
            .filter { it.isPresent }
            .subscribe {
                block.invoke(it.get())
            }.addTo(mViewModel.compositeDisposable)
    }

    private fun getCellularSignalStatusObservable(block: (cellularStatus: CellularSignalInfo) -> Unit) {
        AirLinkManagerExt.getCellularSignalObservable()
            .observeOn(AndroidSchedulers.mainThread())
            .filter { it.isPresent }
            .subscribe {
                block.invoke(it.get())
            }.addTo(mViewModel.compositeDisposable)
    }

    private fun getCrestLteStatusObservable(block: (crestLteStatus: LteStatus) -> Unit) {
        CrestManagerExt.getCrestLteStatusObservable()
            .observeOn(AndroidSchedulers.mainThread())
            .filter { it.isPresent }
            .subscribe {
                block.invoke(it.get())
            }.addTo(mViewModel.compositeDisposable)
    }
}