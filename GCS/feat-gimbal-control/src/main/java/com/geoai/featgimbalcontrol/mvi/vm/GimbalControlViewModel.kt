package com.geoai.featgimbalcontrol.mvi.vm

import android.util.Log
import com.geoai.basiclib.base.mvi.BaseEISViewModel
import com.geoai.featgimbalcontrol.mvi.eis.GimbalControlEffect
import com.geoai.featgimbalcontrol.mvi.eis.GimbalControlIntent
import com.geoai.featgimbalcontrol.mvi.eis.GimbalControlState
import com.zkyt.lib_msdk_ext.component.GimbalManagerExt
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-07 19:02.
 * @Description :
 */
class GimbalControlViewModel :
    BaseEISViewModel<GimbalControlEffect, GimbalControlIntent, GimbalControlState>() {
    override fun initUiState(): GimbalControlState {
        return GimbalControlState.INIT
    }

    override fun handleIntent(intent: GimbalControlIntent) {
    }

    fun getGimbalPitchAngleRx(successBlock: (Float) -> Unit, errorBlock: (Throwable) -> Unit) {
        Single.create<Float> {
            it.onSuccess(GimbalManagerExt.getGimbalStateObservable().value?.get()?.pitchAngle?:0f)
        }.observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                successBlock.invoke(it)
            }, {
                errorBlock.invoke(it)
            }).addTo(compositeDisposable)
    }

    fun getGimbalYawAngleRx(successBlock: (Float) -> Unit, errorBlock: (Throwable) -> Unit) {
        Single.create<Float> {
            it.onSuccess(GimbalManagerExt.getGimbalStateObservable().value?.get()?.yawAngle?:0f)
        }.observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                successBlock.invoke(it)
            }, {
                errorBlock.invoke(it)
            }).addTo(compositeDisposable)
    }
}