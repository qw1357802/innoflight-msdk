package com.geoai.featgimbalcontrol.mvi.eis

import com.geoai.basiclib.base.mvi.IUIEffect
import com.geoai.basiclib.base.mvi.IUiIntent
import com.geoai.basiclib.base.mvi.IUiState

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-07 19:03.
 * @Description :
 */
sealed class GimbalControlEffect: IUIEffect {
}

sealed class GimbalControlIntent: IUiIntent {
}

sealed class GimbalControlState: IUiState {
    object INIT: GimbalControlState()
}
