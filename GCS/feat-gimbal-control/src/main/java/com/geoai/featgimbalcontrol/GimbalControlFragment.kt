package com.geoai.featgimbalcontrol

import android.os.Bundle
import android.util.Log
import com.alibaba.android.arouter.facade.annotation.Route
import com.geoai.basiclib.base.fragment.BaseVVDFragment
import com.geoai.basiclib.ext.commContext
import com.geoai.featgimbalcontrol.mvi.eis.GimbalControlIntent
import com.geoai.featgimbalcontrol.mvi.eis.GimbalControlState
import com.geoai.featgimbalcontrol.mvi.vm.GimbalControlViewModel
import com.geoai.modservice.di.ARouterPath
import com.geoai.uicore.ext.showMessageConfirmDialog
import com.geoai.uicore.ext.showSeekbarDialog
import com.geoai.uigimbalcontrol.R
import com.geoai.uigimbalcontrol.databinding.FragmentGimbalBinding
import com.zkyt.lib_msdk_ext.component.GimbalManagerExt
import io.reactivex.rxkotlin.addTo
import kotlinx.coroutines.flow.flow

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-07 19:02.
 * @Description :
 */
@Route(path = ARouterPath.PATH_CONTROL_GIMBAL_CONTROL)
class GimbalControlFragment: BaseVVDFragment<GimbalControlViewModel, FragmentGimbalBinding>() {

    override fun init(savedInstanceState: Bundle?) {
        mBinding.btnGimbalPitchAngle.setOnClickListener { showGimbalPitchAngleDialog()}
        mBinding.btnGimbalYawAngle.setOnClickListener { showGimbalYawAngleDialog()}
        mBinding.btnGimbalReset.setOnClickListener { showGimbalResetDialog()}
        mBinding.btnGimbalResetAndDown.setOnClickListener { showGimbalResetAndDownDialog()}
    }

    private fun showGimbalPitchAngleDialog() {
        showStateLoading()
        mViewModel.getGimbalPitchAngleRx({
            showStateSuccess()
            showSeekbarDialog(
                getString(R.string.gimbal_pitch_angle),
                getString(R.string.gimbal_pitch_angle_limit),
                30f,
                -90f,
                1f,
                getString(R.string.degree),
                it.toDouble(),
                parentFragmentManager
            ) {
                showStateLoading()
                GimbalManagerExt.setGimbalPitchAngleRx(it.toFloat()).subscribe({
                    showStateSuccess()
                }, {err->
                    showStateError(err.message)
                }).addTo(mViewModel.compositeDisposable)
            }
        }, {
            showStateError(it.message)
        })
    }

    private fun showGimbalYawAngleDialog() {
        showStateLoading()
        mViewModel.getGimbalYawAngleRx({
            showStateSuccess()
            showSeekbarDialog(
                getString(R.string.gimbal_yaw_angle),
                getString(R.string.gimbal_yaw_angle_limit),
                180f,
                -180f,
                1f,
                getString(R.string.degree),
                it.toDouble(),
                parentFragmentManager
            ) {
                showStateLoading()
                GimbalManagerExt.setGimbalYawAngleRx(it.toFloat()).subscribe({
                    showStateSuccess()
                }, {err->
                    showStateError(err.message)
                }).addTo(mViewModel.compositeDisposable)
            }
        }, {
            showStateError(it.message)
        })
    }

    private fun showGimbalResetDialog() {
        showMessageConfirmDialog(getString(R.string.gimbal_control_reset), parentFragmentManager, {
            GimbalManagerExt.resetGimbalRx().subscribe({
                showStateSuccess()
            }, {err->
                showStateError(err.message)
            }).addTo(mViewModel.compositeDisposable)
        })
    }

    private fun showGimbalResetAndDownDialog() {
        showMessageConfirmDialog(getString(R.string.gimbal_control_reset_and_down), parentFragmentManager, {
            GimbalManagerExt.resetGimbalAndDownRx().subscribe({
                showStateSuccess()
            }, {err->
                showStateError(err.message)
            }).addTo(mViewModel.compositeDisposable)
        })
    }
}