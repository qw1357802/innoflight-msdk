package com.geoai.featgimbalcontrol

import android.os.Bundle
import com.alibaba.android.arouter.facade.annotation.Route
import com.geoai.basiclib.base.fragment.BaseVVDFragment
import com.geoai.basiclib.base.vm.EmptyViewModel
import com.geoai.mavlink.geoainet.gimbal.info.GimbalStateInfo
import com.geoai.modservice.di.ARouterPath
import com.geoai.modservice.di.eventbus.OnStatusMenuClickEvent
import com.geoai.uigimbalcontrol.databinding.FragmentGimbalStatusBinding
import com.zkyt.lib_msdk_ext.component.GimbalManagerExt
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import org.greenrobot.eventbus.EventBus

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-26 18:39.
 * @Description :
 */
@Route(path = ARouterPath.PATH_STATUS_GIMBAL)
class GimbalStatusFragment : BaseVVDFragment<EmptyViewModel, FragmentGimbalStatusBinding>() {

    override fun init(savedInstanceState: Bundle?) {
        mBinding.gimbalStatus.setOnClickListener {
            EventBus.getDefault().post(OnStatusMenuClickEvent(System.currentTimeMillis()))
        }
        getGimbalStatusObservable {
            val ret = "pitch: ${String.format("%.2f",it.pitchAngle.toDouble())} \r\n" +
                    "yaw:  ${String.format("%.2f",it.yawAngle.toDouble())} \r\n" +
                    "deviceID: ${it.deviceID} \r\n"
            mBinding.tvGimbalStatus.text = ret
        }
    }

    private fun getGimbalStatusObservable(block: (gimbalStatus: GimbalStateInfo) -> Unit) {
        GimbalManagerExt.getGimbalStateObservable()
            .observeOn(AndroidSchedulers.mainThread())
            .filter { it.isPresent }
            .subscribe {
                block.invoke(it.get())
            }.addTo(mViewModel.compositeDisposable)
    }
}