package com.geoai.feat_fc_control.mvi.vm

import com.geoai.basiclib.base.mvi.BaseEISViewModel
import com.geoai.basiclib.utils.CommUtils.getString
import com.geoai.feat_fc_control.mvi.eis.FcControlEffect
import com.geoai.feat_fc_control.mvi.eis.FcControlIntent
import com.geoai.feat_fc_control.mvi.eis.FcControlState
import com.geoai.mavlink.geoainet.flycontroller.enums.AircraftFailSafeBehaviorMode
import com.geoai.mavlink.geoainet.flycontroller.enums.AircraftPrecisionLandMode
import com.geoai.uifccontrol.R
import com.zkyt.lib_msdk_ext.component.FlyControllerManagerExt
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import javax.inject.Inject

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-07 14:09.
 * @Description :
 */
class FcControlViewModel @Inject constructor(): BaseEISViewModel<FcControlEffect, FcControlIntent, FcControlState>() {

    fun getAltitudeLimitRx(success: (Int) -> Unit, error: (String) -> Unit) {
        FlyControllerManagerExt.getFcAltitudeLimitRx()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({success.invoke(it)}) {
                error.invoke(it.message!!)
            }.addTo(compositeDisposable)
    }

    fun getDistanceLimitRx(success: (Int) -> Unit, error: (String) -> Unit) {
        FlyControllerManagerExt.getFcDistanceLimitRx()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({success.invoke(it)}) {
                error.invoke(it.message!!)
            }.addTo(compositeDisposable)
    }

    fun getHorizontalSpeedLimitRx(success: (Float) -> Unit, error: (String) -> Unit) {
        FlyControllerManagerExt.getHorizonSpeedLimitObservable()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({success.invoke(it.get())}) {
                error.invoke(it.message!!)
            }.addTo(compositeDisposable)
    }

    fun getGoHomeAltitudeLimitRx(success: (Int) -> Unit, error: (String) -> Unit) {
        FlyControllerManagerExt.getGoHomeAltitudeRx()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({success.invoke(it)}) {
                error.invoke(it.message!!)
            }.addTo(compositeDisposable)
    }

    fun getRcLostBehaviorRx(success: (AircraftFailSafeBehaviorMode) -> Unit, error: (String) -> Unit) {
        FlyControllerManagerExt.getConnectionFailSafeBehaviorRx()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({success.invoke(it)}) {
                error.invoke(it.message!!)
            }.addTo(compositeDisposable)
    }

    fun getLedRx(success: (Boolean) -> Unit, error: (String) -> Unit) {
        FlyControllerManagerExt.getLedEnableRx()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({success.invoke(it)}) {
                error.invoke(it.message!!)
            }.addTo(compositeDisposable)
    }

    fun getPrecisionLandRx(success: (AircraftPrecisionLandMode) -> Unit, error: (String) -> Unit) {
        FlyControllerManagerExt.getPrecisionLandModeRx()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({success.invoke(it)}) {
                error.invoke(it.message!!)
            }.addTo(compositeDisposable)
    }

    fun getSimulatorModeRx(success: (Boolean) -> Unit, error: (String) -> Unit) {
        FlyControllerManagerExt.getSimulateModeEnableRx()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({success.invoke(it)}) {
                error.invoke(it.message!!)
            }.addTo(compositeDisposable)
    }

    override fun initUiState(): FcControlState {
        return FcControlState.INIT
    }

    override fun handleIntent(intent: FcControlIntent) {
    }
}