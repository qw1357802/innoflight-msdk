package com.geoai.feat_fc_control

import android.os.Bundle
import com.alibaba.android.arouter.facade.annotation.Route
import com.geoai.basiclib.base.fragment.BaseVVDFragment
import com.geoai.basiclib.base.vm.EmptyViewModel
import com.geoai.mavlink.geoainet.obstacle.info.ObstacleStateInfo
import com.geoai.modservice.di.ARouterPath
import com.geoai.uifccontrol.databinding.FragmentObstacleViewBinding
import com.zkyt.lib_msdk_ext.component.ObstacleManagerExt
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-12 11:10.
 * @Description :
 */
@Route(path = ARouterPath.PATH_CONTROL_FLIGHT_OBSTACLE)
class ObstacleFragment: BaseVVDFragment<EmptyViewModel, FragmentObstacleViewBinding>() {

    override fun init(savedInstanceState: Bundle?) {
        getObstacleStateObservable {
            mBinding.radarWidget.setDataSource(it)
        }
    }

    private fun getObstacleStateObservable(block: (obstacleState: ObstacleStateInfo) -> Unit) {
        ObstacleManagerExt.getObstacleStateInfoObservable()
            .observeOn(AndroidSchedulers.mainThread())
            .filter{ it.isPresent }
            .subscribe {
                block.invoke(it.get())
            }.addTo(mViewModel.compositeDisposable)
    }

}