package com.geoai.feat_fc_control.mvi.eis

import com.geoai.basiclib.base.mvi.IUIEffect
import com.geoai.basiclib.base.mvi.IUiIntent
import com.geoai.basiclib.base.mvi.IUiState

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-07 14:09.
 * @Description :
 */
sealed class FcControlEffect: IUIEffect {
}

sealed class FcControlIntent: IUiIntent {
}

sealed class FcControlState: IUiState {
    object INIT : FcControlState()
}