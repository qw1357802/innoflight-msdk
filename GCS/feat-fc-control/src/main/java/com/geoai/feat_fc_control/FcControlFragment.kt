package com.geoai.feat_fc_control

import android.os.Bundle
import com.alibaba.android.arouter.facade.annotation.Route
import com.geoai.basiclib.base.fragment.BaseVVDFragment
import com.geoai.feat_fc_control.mvi.vm.FcControlViewModel
import com.geoai.feat_fc_control.view.ObstacleSettingFragmentDialog
import com.geoai.mavlink.geoainet.flycontroller.enums.AircraftFailSafeBehaviorMode
import com.geoai.mavlink.geoainet.flycontroller.enums.AircraftPrecisionLandMode
import com.geoai.modservice.di.ARouterPath
import com.geoai.uicore.custom.ui.GEOUIMessageDialog
import com.geoai.uicore.ext.showItemSelectedDialog
import com.geoai.uicore.ext.showMessageConfirmDialog
import com.geoai.uicore.ext.showSeekbarDialog
import com.geoai.uifccontrol.R
import com.geoai.uifccontrol.databinding.FragmentFcControlMenuBinding
import com.zkyt.lib_msdk_ext.component.FlyControllerManagerExt
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-07 14:08.
 * @Description :
 */
@Route(path = ARouterPath.PATH_CONTROL_FC_CONTROL)
class FcControlFragment: BaseVVDFragment<FcControlViewModel, FragmentFcControlMenuBinding>() {

    override fun init(savedInstanceState: Bundle?) {
        mBinding.btnObstacle.setOnClickListener { showObstacleSettingDialog() }
        mBinding.btnAltitudeLimit.setOnClickListener { showAltitudeLimitDialog() }
        mBinding.btnDistanceLimit.setOnClickListener { showDistanceLimitDialog() }
        mBinding.btnSpeedLimit.setOnClickListener { showHorizontalSpeedLimitDialog() }
        mBinding.btnRthAltitude.setOnClickListener { showGoHomeAltitudeDialog() }
        mBinding.btnLED.setOnClickListener { showLedCommandDialog() }
        mBinding.btnRTHPosition.setOnClickListener { showRthPositionDialog() }
        mBinding.btnLost.setOnClickListener { showLostSignalBehaviorDialog() }
        mBinding.btnPrecLand.setOnClickListener { showPrecisionLandingDialog() }
        mBinding.btnSimulate.setOnClickListener { showSimulateDialog() }
        mBinding.btnReboot.setOnClickListener { showRebootDroneDialog() }
    }

    private fun showObstacleSettingDialog() {
        ObstacleSettingFragmentDialog().let {
            it.show(parentFragmentManager, it.javaClass.name)
        }
    }

    private fun showAltitudeLimitDialog() {
        showStateLoading()
        mViewModel.getAltitudeLimitRx({
            showStateSuccess()
            showSeekbarDialog(
                getString(R.string.fc_setting_fc_altitude_limit_title),
                getString(R.string.fc_setting_fc_altitude_limit_tips),
                500.0f,
                30.0f,
                1.0f,
                "m",
                it.toDouble(),
                parentFragmentManager
            ) {ret ->
                showStateLoading()
                FlyControllerManagerExt.setFcAltitudeLimitRx(ret.toInt()).subscribe(
                    { showStateSuccess() }) { error ->
                    showStateError(error.message)
                }.addTo(mViewModel.compositeDisposable)
            }
        }, {
            showStateError(it)
        })
    }

    private fun showDistanceLimitDialog() {
        showStateLoading()
        mViewModel.getDistanceLimitRx({
            showStateSuccess()
            showSeekbarDialog(
                getString(R.string.fc_setting_fc_distance_limit_title),
                getString(R.string.fc_setting_fc_distance_limit_tips),
                8000.0f,
                30.0f,
                10.0f,
                "m",
                it.toDouble(),
                parentFragmentManager
            ) {ret ->
                showStateLoading()
                FlyControllerManagerExt.setFcDistanceLimitRx(ret.toInt()).subscribe(
                    { showStateSuccess() }) { error ->
                    showStateError(error.message)
                }.addTo(mViewModel.compositeDisposable)
            }
        }, {
            showStateError(it)
        })
    }

    private fun showHorizontalSpeedLimitDialog() {
        showStateLoading()
        mViewModel.getHorizontalSpeedLimitRx({
            showStateSuccess()
            showSeekbarDialog(
                getString(R.string.fc_setting_fc_horizon_speed_limit_title),
                getString(R.string.fc_setting_fc_horizon_speed_limit_tips),
                26.0f,
                1.0f,
                0.5f,
                "m/s",
                it.toDouble(),
                parentFragmentManager
            ) {ret ->
                showStateLoading()
                FlyControllerManagerExt.setHorizonSpeedLimitRx(ret.toInt()).subscribe(
                    { showStateSuccess() }) { error ->
                    showStateError(error.message)
                }.addTo(mViewModel.compositeDisposable)
            }
        }, {
            showStateError(it)
        })
    }

    private fun showGoHomeAltitudeDialog() {
        showStateLoading()
        mViewModel.getGoHomeAltitudeLimitRx({
            showStateSuccess()
            showSeekbarDialog(
                getString(R.string.fc_setting_fc_rth_title),
                getString(R.string.fc_setting_fc_rth_tips),
                500.0f,
                10.0f,
                1.0f,
                "m",
                it.toDouble(),
                parentFragmentManager
            ) {ret ->
                showStateLoading()
                FlyControllerManagerExt.setGoHomeAltitudeRx(ret.toInt()).subscribe(
                    { showStateSuccess() }) { error ->
                    showStateError(error.message)
                }.addTo(mViewModel.compositeDisposable)
            }
        }, {
            showStateError(it)
        })
    }

    private fun showLedCommandDialog() {
        showStateLoading()
        mViewModel.getLedRx({
            showStateSuccess()
            showItemSelectedDialog(
                getString(R.string.fc_setting_led_control_title),
                if (it) 1 else 2,
                arrayOf(getString(R.string.fc_setting_led_control_arm_enable), getString(R.string.fc_setting_led_control_arm_disable)),
                parentFragmentManager
            ) { _, pos ->
                showStateLoading()
                FlyControllerManagerExt.setLedEnableRx(pos == 0).subscribe({
                    showStateSuccess()
                }, {error->
                    showStateError(error.message)
                }).addTo(mViewModel.compositeDisposable)
            }
        }, {
            showStateError(it)
        })
    }

    private fun showRthPositionDialog() {
    }

    private fun showLostSignalBehaviorDialog() {
        showStateLoading()
        mViewModel.getRcLostBehaviorRx({aircraftFailSafeBehaviorMode ->
            showStateSuccess()
            showItemSelectedDialog(
                getString(R.string.fc_setting_lost_behavior_title),
                aircraftFailSafeBehaviorMode.name,
                AircraftFailSafeBehaviorMode.values().map { it.name }.toTypedArray(),
                parentFragmentManager
            ) { text, pos ->
                showStateLoading()
                FlyControllerManagerExt.setConnectionFailSafeBehaviorRx(AircraftFailSafeBehaviorMode.valueOf(text!!)).subscribe({
                    showStateSuccess()
                }, {error->
                    showStateError(error.message)
                }).addTo(mViewModel.compositeDisposable)
            }
        }, {
            showStateError(it)
        })
    }

    private fun showPrecisionLandingDialog() {
        showStateLoading()
        mViewModel.getPrecisionLandRx({aircraftPrecisionLandMode ->
            showStateSuccess()
            showItemSelectedDialog(
                getString(R.string.fc_setting_precision_landing_title),
                aircraftPrecisionLandMode.name,
                AircraftPrecisionLandMode.values().map { it.name }.toTypedArray(),
                parentFragmentManager
            ) { text, pos ->
                showStateLoading()
                FlyControllerManagerExt.setPrecisionLandModeRx(AircraftPrecisionLandMode.valueOf(text!!)).subscribe({
                    showStateSuccess()
                }, {error->
                    showStateError(error.message)
                }).addTo(mViewModel.compositeDisposable)
            }
        }, {
            showStateError(it)
        })
    }

    private fun showSimulateDialog() {
        showStateLoading()
        mViewModel.getSimulatorModeRx({isEnable ->
            showStateSuccess()
            showItemSelectedDialog(
                getString(R.string.fc_setting_simulate_setting_title),
                if (isEnable) 1 else 2,
                arrayOf(getString(R.string.fc_setting_simulate_setting_enable), getString(R.string.fc_setting_simulate_setting_disable)),
                parentFragmentManager
            ) { _, pos ->
                showStateLoading()
                FlyControllerManagerExt.setSimulateModeRx(pos == 0).subscribe({
                    showStateSuccess()
                }, {error->
                    showStateError(error.message)
                }).addTo(mViewModel.compositeDisposable)
            }
        }, {
            showStateError(it)
        })
    }

    private fun showRebootDroneDialog() {
        showMessageConfirmDialog(getString(R.string.fc_setting_reboot_title), parentFragmentManager, {
            showStateLoading()
            FlyControllerManagerExt.rebootFCRx()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    showStateSuccess()
                }){error ->
                    showStateError(error.message)
                }.addTo(mViewModel.compositeDisposable)
        })
    }
}