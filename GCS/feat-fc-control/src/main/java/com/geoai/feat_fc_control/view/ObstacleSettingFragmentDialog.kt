package com.geoai.feat_fc_control.view

import android.os.Bundle
import android.util.Log
import com.geoai.basiclib.base.fragment.BaseVVDDialogFragment
import com.geoai.basiclib.base.vm.EmptyViewModel
import com.geoai.uicore.custom.tab.TabItemSwitchInput
import com.geoai.uicore.custom.tab.TabItemTextInput
import com.geoai.uicore.ext.showSeekbarDialog
import com.geoai.uifccontrol.R
import com.geoai.uifccontrol.databinding.FragmentObstacleSettingBinding
import com.zkyt.lib_msdk_ext.component.FlyControllerManagerExt
import com.zkyt.lib_msdk_ext.component.ObstacleManagerExt
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import java.util.concurrent.TimeUnit

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-07 14:59.
 * @Description :
 */
class ObstacleSettingFragmentDialog: BaseVVDDialogFragment<EmptyViewModel, FragmentObstacleSettingBinding>() {

    override fun init(savedInstanceState: Bundle?) {

        mBinding.tabObstacleEnable.setOnSwitchListener(object : TabItemSwitchInput.OnSwitchListener{
            override fun onSwitch(isCheck: Boolean) {
                showStateLoading()
                ObstacleManagerExt.setObstacleAvoidanceEnableRx(isCheck)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        showStateSuccess()
                    }){
                        showStateError(it.message)
                    }.addTo(mViewModel.compositeDisposable)
            }
        })

        mBinding.tabObstacleDistance.setOnClickDelegate(object : TabItemTextInput.OnClickDelegate{
            override fun onClick(result: TabItemTextInput.OnResult) {
                showObstacleDistanceDialog()
            }
        })

        updateObstacleDistance()

        ObstacleManagerExt.getObstacleAvoidanceEnableRx()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                mBinding.tabObstacleEnable.setClick(it)
            }){
                showStateError(it.message)
            }.addTo(mViewModel.compositeDisposable)
    }

    private fun updateObstacleDistance() {
        ObstacleManagerExt.getObstacleAvoidanceBrakingDistanceRx()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                mBinding.tabObstacleDistance.setContent(it.toString())
            }){
                showStateError(it.message)
            }.addTo(mViewModel.compositeDisposable)
    }

    private fun showObstacleDistanceDialog() {
        showStateLoading()
        showSeekbarDialog(
            getString(R.string.obstacle_setting_obstacle_distance),
            getString(R.string.obstacle_setting_obstacle_distance),
            30.0f,
            1.0f,
            1.0f,
            "m",
            mBinding.tabObstacleDistance.getContent().toDouble(),
            parentFragmentManager
        ) { ret ->
            ObstacleManagerExt.setObstacleAvoidanceBrakingDistanceRx(ret.toFloat())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    showStateSuccess()
                    updateObstacleDistance()
                }) {
                    showStateError(it.message)
                }.addTo(mViewModel.compositeDisposable)
        }
    }
}