package com.geoai.feat_fc_control.view

import android.content.Context
import android.media.Image
import android.util.AttributeSet
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.geoai.mavlink.geoainet.obstacle.info.ObstacleStateInfo
import com.geoai.uifccontrol.R
import kotlin.math.ceil
import kotlin.math.floor

class RadarWidget @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private val view = inflate(getContext(), R.layout.uxsdk_widget_radar, this)

    private val noseRange = 50  //前视视场角50°
    private val tailRange = 50  //后视视场角50°
    private val leftRange = 50  //左方视场角50°
    private val rightRange = 50  //右视视场角50°

    private val rideWarningDistance = 300 //旁侧告警距离

    //间隔角度；现在每个间隔平均分配
    private val intervalAngle = (360 - noseRange - tailRange - leftRange - rightRange) / 4

    private var resolutionRadio: Int? = null
    private var minObstacleDistance: Int = 0
    private var maxObstacleDistance: Int = 3000

    private val rightObstacleDataSet = mutableListOf<Int>()
    private val tailObstacleDataSet = mutableListOf<Int>()
    private val leftObstacleDataSet = mutableListOf<Int>()
    private val noseObstacleDataSet = mutableListOf<Int>()

    private val imageview_radar_left: ImageView = view.findViewById(R.id.imageview_radar_left)
    private val imageview_radar_right: ImageView = view.findViewById(R.id.imageview_radar_right)

    private val imageview_forward_arrow: ImageView = view.findViewById(R.id.imageview_forward_arrow)
    private val imageview_backward_arrow: ImageView = view.findViewById(R.id.imageview_backward_arrow)
    private val imageview_left_arrow: ImageView = view.findViewById(R.id.imageview_left_arrow)
    private val imageview_right_arrow: ImageView = view.findViewById(R.id.imageview_right_arrow)

    private val textview_forward_distance: TextView = view.findViewById(R.id.textview_forward_distance)
    private val textview_backward_distance: TextView = view.findViewById(R.id.textview_backward_distance)
    private val textview_left_distance: TextView = view.findViewById(R.id.textview_left_distance)
    private val textview_right_distance: TextView = view.findViewById(R.id.textview_right_distance)
    private val imageview_radar_forward_0: ImageView = view.findViewById(R.id.imageview_radar_forward_0)
    private val imageview_radar_forward_1: ImageView = view.findViewById(R.id.imageview_radar_forward_1)
    private val imageview_radar_forward_2: ImageView = view.findViewById(R.id.imageview_radar_forward_2)
    private val imageview_radar_forward_3: ImageView = view.findViewById(R.id.imageview_radar_forward_3)

    private val imageview_radar_backward_0: ImageView = view.findViewById(R.id.imageview_radar_backward_0)
    private val imageview_radar_backward_1: ImageView = view.findViewById(R.id.imageview_radar_backward_1)
    private val imageview_radar_backward_2: ImageView = view.findViewById(R.id.imageview_radar_backward_2)
    private val imageview_radar_backward_3: ImageView = view.findViewById(R.id.imageview_radar_backward_3)

    fun setDataSource(data: ObstacleStateInfo) {
        rightObstacleDataSet.clear()
        tailObstacleDataSet.clear()
        leftObstacleDataSet.clear()
        noseObstacleDataSet.clear()

        if (data.horizontalAngleInterval < 360 / data.horizontalObstacleDistance.size) return

        resolutionRadio = data.horizontalAngleInterval
        minObstacleDistance = data.minObstacleDistance
        maxObstacleDistance = data.maxObstacleDistance

        //连接处死区数量
        val emptyBlockSize = intervalAngle / data.horizontalAngleInterval

        //前向右侧死区
        val noseEndIndex = ceil(noseRange / data.horizontalAngleInterval / 2.0).toInt()

        //初始化避障方位模型
        //右侧
        for (index in 0 until (rightRange / data.horizontalAngleInterval)) {
            val index = index + noseEndIndex + emptyBlockSize
            rightObstacleDataSet.add(data.horizontalObstacleDistance[index])
        }
        //后侧
        for (index in 0 until (tailRange / data.horizontalAngleInterval)) {
            val index = index + noseEndIndex + rightObstacleDataSet.size + (emptyBlockSize * 2)
            tailObstacleDataSet.add(data.horizontalObstacleDistance[index])
        }
        //左侧
        for (index in 0 until (leftRange / data.horizontalAngleInterval)) {
            val index = index + noseEndIndex + rightObstacleDataSet.size + tailObstacleDataSet.size + (emptyBlockSize * 3)
            leftObstacleDataSet.add(data.horizontalObstacleDistance[index])
        }
        //前方左侧
        for (index in 0 until floor(noseRange / data.horizontalAngleInterval / 2.0).toInt()) {
            val index = index + noseEndIndex + rightObstacleDataSet.size + tailObstacleDataSet.size + leftObstacleDataSet.size + (emptyBlockSize * 4)
            noseObstacleDataSet.add(data.horizontalObstacleDistance[index])
        }
        //前方右侧
        for (index in 0 until ceil(noseRange / data.horizontalAngleInterval / 2.0).toInt()) {
            noseObstacleDataSet.add(data.horizontalObstacleDistance[index])
        }

        updateSideObstacle(leftObstacleDataSet, rightObstacleDataSet)
        updateFrontBackObstacle(noseObstacleDataSet, tailObstacleDataSet)
    }

    private fun updateFrontBackObstacle(
        noseObstacleDataSet: MutableList<Int>,
        tailObstacleDataSet: MutableList<Int>
    ) {
        //目前只处理C60搭载模块的避障数据（5组）
        if (noseObstacleDataSet.size != 5 && tailObstacleDataSet.size != 5) return

        noseObstacleDataSet.minOrNull()?.let {
            if (it in minObstacleDistance until maxObstacleDistance) {
                textview_forward_distance.text = "${it / 100.0} m"
                setObstacleVisibility(ObstaclePosition.NOSE, VISIBLE)
            } else {
                setObstacleVisibility(ObstaclePosition.NOSE, GONE)
            }
        }
        noseObstacleDataSet.let {
            imageview_radar_forward_0.setImageLevel(4 - floor(it[0] / 300.0).toInt())
            imageview_radar_forward_3.setImageLevel(4 - floor(it[4] / 300.0).toInt())

            if (it[2] < it[1] && it[2] < it[3]) {
                imageview_radar_forward_1.setImageLevel(4 - floor(it[2] / 300.0).toInt())
                imageview_radar_forward_2.setImageLevel(4 - floor(it[2] / 300.0).toInt())
            } else {
                imageview_radar_forward_1.setImageLevel(4 - floor(it[1] / 300.0).toInt())
                imageview_radar_forward_2.setImageLevel(4 - floor(it[3] / 300.0).toInt())
            }
        }
        tailObstacleDataSet.minOrNull()?.let {
            if (it in minObstacleDistance until maxObstacleDistance) {
                textview_backward_distance.text = "${it / 100.0} m"
                setObstacleVisibility(ObstaclePosition.TAIL, VISIBLE)
            } else {
                setObstacleVisibility(ObstaclePosition.TAIL, GONE)
            }
        }
        tailObstacleDataSet.let {
            imageview_radar_backward_0.setImageLevel(ceil(it[0] / 300.0).toInt())
            imageview_radar_backward_3.setImageLevel(ceil(it[4] / 300.0).toInt())

            if (it[2] < it[1] && it[2] < it[3]) {
                imageview_radar_backward_1.setImageLevel(ceil(it[2] / 300.0).toInt())
                imageview_radar_backward_2.setImageLevel(ceil(it[2] / 300.0).toInt())
            } else {
                imageview_radar_backward_1.setImageLevel(ceil(it[1] / 300.0).toInt())
                imageview_radar_backward_2.setImageLevel(ceil(it[3] / 300.0).toInt())
            }
        }
    }

    private fun updateSideObstacle(
        leftObstacleDataSet: MutableList<Int>,
        rightObstacleDataSet: MutableList<Int>
    ) {
        leftObstacleDataSet.sort()
        leftObstacleDataSet.first().let {
            if (it in minObstacleDistance until maxObstacleDistance) {
                //可展示
                setObstacleVisibility(ObstaclePosition.LEFT, View.VISIBLE)
                textview_left_distance.text = "${it / 100.0} m"
                if (it < rideWarningDistance) {
                    imageview_radar_left.setImageLevel(1)
                } else {
                    imageview_radar_left.setImageLevel(0)
                }
            } else {
                setObstacleVisibility(ObstaclePosition.LEFT, View.GONE)
            }
        }

        rightObstacleDataSet.sort()
        rightObstacleDataSet.first().let {
            if (it in minObstacleDistance until maxObstacleDistance) {
                //可展示
                setObstacleVisibility(ObstaclePosition.RIGHT, View.VISIBLE)
                textview_right_distance.text = "${it / 100.0} m"
                if (it < rideWarningDistance) {
                    imageview_radar_right.setImageLevel(1)
                } else {
                    imageview_radar_right.setImageLevel(0)
                }
            } else {
                setObstacleVisibility(ObstaclePosition.RIGHT, View.GONE)
            }
        }
    }

    private fun setObstacleVisibility(obstaclePosition: ObstaclePosition, visibility: Int) {
        when (obstaclePosition) {
            ObstaclePosition.NOSE -> {
                imageview_radar_forward_0.visibility = visibility
                imageview_radar_forward_1.visibility = visibility
                imageview_radar_forward_2.visibility = visibility
                imageview_radar_forward_3.visibility = visibility
                imageview_forward_arrow.visibility = visibility
                textview_forward_distance.visibility = visibility
            }
            ObstaclePosition.TAIL -> {
                imageview_radar_backward_0.visibility = visibility
                imageview_radar_backward_1.visibility = visibility
                imageview_radar_backward_2.visibility = visibility
                imageview_radar_backward_3.visibility = visibility
                imageview_backward_arrow.visibility = visibility
                textview_backward_distance.visibility = visibility
            }
            ObstaclePosition.LEFT -> {
                imageview_radar_left.visibility = visibility
                imageview_left_arrow.visibility = visibility
                textview_left_distance.visibility = visibility
            }
            ObstaclePosition.RIGHT -> {
                imageview_radar_right.visibility = visibility
                imageview_right_arrow.visibility = visibility
                textview_right_distance.visibility = visibility
            }
        }
    }

    enum class ObstaclePosition {
        NOSE,
        RIGHT,
        TAIL,
        LEFT
    }
}