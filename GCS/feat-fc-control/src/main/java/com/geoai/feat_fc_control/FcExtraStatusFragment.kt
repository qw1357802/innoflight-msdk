package com.geoai.feat_fc_control

import android.os.Bundle
import com.alibaba.android.arouter.facade.annotation.Route
import com.geoai.basiclib.base.fragment.BaseVVDFragment
import com.geoai.basiclib.base.vm.EmptyViewModel
import com.geoai.mavlink.geoainet.flycontroller.info.FlyControllerStateInfo
import com.geoai.modservice.di.ARouterPath
import com.geoai.modservice.di.eventbus.OnStatusMenuClickEvent
import com.geoai.uifccontrol.databinding.FragmentFcExtraStatusMenuBinding
import com.zkyt.lib_msdk_ext.component.FlyControllerManagerExt
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import org.greenrobot.eventbus.EventBus

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-26 18:39.
 * @Description :
 */
@Route(path = ARouterPath.PATH_STATUS_FC)
class FcExtraStatusFragment: BaseVVDFragment<EmptyViewModel, FragmentFcExtraStatusMenuBinding>() {

    override fun init(savedInstanceState: Bundle?) {
        mBinding.fcExtraStatusMenu.setOnClickListener {
            EventBus.getDefault().post(OnStatusMenuClickEvent(System.currentTimeMillis()))
        }
        getFlyControllerStateObservable {
            val location = "lat: ${it.aircraftLatitude}, lon: ${it.aircraftLongitude}"
            val extra = "fly time: ${it.flyTime}, mileage: ${it.flightMileage}"
            mBinding.tvLocation.text = location
            mBinding.tvFlyExtra.text = extra
        }
    }

    private fun getFlyControllerStateObservable(block: (flyControllerState: FlyControllerStateInfo) -> Unit) {
        FlyControllerManagerExt.getFlyControllerStateObservable()
            .observeOn(AndroidSchedulers.mainThread())
            .filter{ it.isPresent }
            .subscribe {
                block.invoke(it.get())
            }.addTo(mViewModel.compositeDisposable)
    }
}