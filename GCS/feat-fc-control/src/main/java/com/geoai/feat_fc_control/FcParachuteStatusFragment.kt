package com.geoai.feat_fc_control

import android.os.Bundle
import com.alibaba.android.arouter.facade.annotation.Route
import com.geoai.basiclib.base.fragment.BaseVVDFragment
import com.geoai.basiclib.base.vm.EmptyViewModel
import com.geoai.mavlink.geoainet.flycontroller.info.FlyControllerStateInfo
import com.geoai.mavlink.geoainet.flycontroller.info.ParachuteStateInfo
import com.geoai.modservice.di.ARouterPath
import com.geoai.modservice.di.eventbus.OnStatusMenuClickEvent
import com.geoai.uifccontrol.databinding.FragmentFcExtraStatusMenuBinding
import com.geoai.uifccontrol.databinding.FragmentFcParachuteStatusMenuBinding
import com.zkyt.lib_msdk_ext.component.FlyControllerManagerExt
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import org.greenrobot.eventbus.EventBus

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-26 18:39.
 * @Description :
 */
@Route(path = ARouterPath.PATH_STATUS_FC_PARACHUTE)
class FcParachuteStatusFragment :
    BaseVVDFragment<EmptyViewModel, FragmentFcParachuteStatusMenuBinding>() {

    override fun init(savedInstanceState: Bundle?) {
        mBinding.fcParachuteStatus.setOnClickListener {
            EventBus.getDefault().post(OnStatusMenuClickEvent(System.currentTimeMillis()))
        }
        getFlyControllerParachuteStatusObservable {
            val ret = "connected: ${it.isParachuteConnected} \r\n" +
                    "enable: ${it.isParachuteEnable} \r\n" +
                    "status: ${it.parachuteLaunchStatus.name.replace("PARACHUTE_FIRE_STATUS_", "")} \r\n" +
                    "error: ${it.parachuteErrorConde} \r\n"
            mBinding.tvParachuteStatus.text = ret
        }
    }

    private fun getFlyControllerParachuteStatusObservable(block: (parachuteStateInfo: ParachuteStateInfo) -> Unit) {
        FlyControllerManagerExt.getFlyControllerParachuteStateObservable()
            .observeOn(AndroidSchedulers.mainThread())
            .filter { it.isPresent }
            .subscribe {
                block.invoke(it.get())
            }.addTo(mViewModel.compositeDisposable)
    }
}