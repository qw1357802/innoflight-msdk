package com.geoai.featcamcontrol.mvi.vm

import com.geoai.basiclib.base.mvi.BaseEISViewModel
import com.geoai.featcamcontrol.mvi.eis.CameraControlEffect
import com.geoai.featcamcontrol.mvi.eis.CameraControlIntent
import com.geoai.featcamcontrol.mvi.eis.CameraControlState
import com.geoai.modservice.di.eventbus.CameraLinesStyle
import com.geoai.modservice.di.eventbus.DisplayCameraLinesStyle
import org.greenrobot.eventbus.EventBus

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-07 19:31.
 * @Description :
 */
class CameraControlViewControl: BaseEISViewModel<CameraControlEffect, CameraControlIntent, CameraControlState>() {
    override fun initUiState(): CameraControlState {
        return CameraControlState.INIT
    }

    override fun handleIntent(intent: CameraControlIntent) {
        when (intent) {
            is CameraControlIntent.SelectCameraLineStyle -> EventBus.getDefault().post(DisplayCameraLinesStyle(intent.style))
        }
    }
}