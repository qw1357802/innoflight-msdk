package com.geoai.featcamcontrol

import android.os.Bundle
import android.util.Log
import com.alibaba.android.arouter.facade.annotation.Route
import com.geoai.basiclib.base.fragment.BaseVVDFragment
import com.geoai.basiclib.base.vm.EmptyViewModel
import com.geoai.mavlink.geoainet.payload.enums.CameraNV2StreamSource
import com.geoai.mavlink.geoainet.payload.enums.CameraZoomRatio
import com.geoai.modservice.di.ARouterPath
import com.geoai.uicamcontrol.R
import com.geoai.uicamcontrol.databinding.FragmentCameraNv2Binding
import com.geoai.uicore.ext.showItemSelectedDialog
import com.zkyt.lib_msdk_ext.component.camera.BaseCameraExt
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-17 11:50.
 * @Description :
 */
@Route(path = ARouterPath.PATH_CONTROL_CAMERA_NV2_CONTROL)
class CameraNV2Fragment : BaseVVDFragment<EmptyViewModel, FragmentCameraNv2Binding>() {

    override fun init(savedInstanceState: Bundle?) {
        mBinding.btnVideoSource.setOnClickListener { showVideoSourceDialog() }
        mBinding.btnCameraZoom.setOnClickListener { showCameraZoomDialog() }
        mBinding.btnCameraSingleShoot.setOnClickListener { showSingleShootByLensDialog() }
    }

    private fun showVideoSourceDialog() {
        showStateLoading()
        BaseCameraExt.getNV2Camera()?.let {
            it.getCameraSourceRx()
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe({source ->
                    showStateSuccess()
                    val sourceList = CameraNV2StreamSource.values().map { it.name }.toTypedArray()
                    showItemSelectedDialog(getString(R.string.camera_video_source), source.name, sourceList, parentFragmentManager) {actionName, pos ->
                        showStateLoading()
                        it.setCameraSourceRx(CameraNV2StreamSource.valueOf(actionName!!))
                            .subscribeOn(AndroidSchedulers.mainThread())
                            .subscribe({
                                showStateSuccess()
                            }){ error ->
                                showStateError(error.message)
                            }.addTo(mViewModel.compositeDisposable)
                    }
                }){ error ->
                    showStateError(error.message)
                }.addTo(mViewModel.compositeDisposable)
        }?: showStateError("Camera is null")
    }

    private fun showCameraZoomDialog() {
        showStateLoading()
        BaseCameraExt.getNV2Camera()?.let {
            it.getCameraZoomRatioRx()
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe({source ->
                    showStateSuccess()
                    val sourceList = CameraZoomRatio.values().map { it.name }.toTypedArray()
                    showItemSelectedDialog(getString(R.string.camera_camera_zoom), source.name, sourceList, parentFragmentManager) {actionName, pos ->
                        showStateLoading()
                        it.setCameraZoomRatioRx(CameraZoomRatio.valueOf(actionName!!))
                            .subscribeOn(AndroidSchedulers.mainThread())
                            .subscribe({
                                showStateSuccess()
                            }){ error ->
                                showStateError(error.message)
                            }.addTo(mViewModel.compositeDisposable)
                    }
                }){ error ->
                    showStateError(error.message)
                }.addTo(mViewModel.compositeDisposable)
        }?: showStateError("Camera is null")
    }

    private fun showSingleShootByLensDialog() {
        BaseCameraExt.getNV2Camera()?.let {
            val items = CameraNV2StreamSource.values().map { it.name }.toTypedArray()
            showItemSelectedDialog(getString(R.string.camera_control_single_shoot), -1, items, parentFragmentManager) {actionName, pos ->
                showStateLoading()
                it.singleShootRx(CameraNV2StreamSource.valueOf(actionName!!))
                    .subscribeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        showStateSuccess()
                    }){ error ->
                        showStateError(error.message)
                    }.addTo(mViewModel.compositeDisposable)
            }
        }?: showStateError("Camera is null")
    }
}