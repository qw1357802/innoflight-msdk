package com.geoai.featcamcontrol

import android.os.Bundle
import com.alibaba.android.arouter.facade.annotation.Route
import com.geoai.basiclib.base.fragment.BaseVVDFragment
import com.geoai.basiclib.engine.dialog.showCenterSelectDialogWithType
import com.geoai.basiclib.ext.commContext
import com.geoai.featcamcontrol.mvi.eis.CameraControlIntent
import com.geoai.featcamcontrol.mvi.vm.CameraControlViewControl
import com.geoai.mavlink.geoainet.payload.enums.CameraMode
import com.geoai.mavlink.geoainet.payload.enums.CameraNV3StreamSource
import com.geoai.mavlink.geoainet.payload.enums.StorageFormatType
import com.geoai.modservice.di.ARouterPath
import com.geoai.modservice.di.eventbus.CameraLinesStyle
import com.geoai.uicamcontrol.R
import com.geoai.uicamcontrol.databinding.FragmentCameraBinding
import com.geoai.uicore.custom.ui.GEOUIMessageDialog
import com.geoai.uicore.custom.ui.GEOUISelectorDialog
import com.geoai.uicore.ext.showItemSelectedDialog
import com.geoai.uicore.ext.showMessageConfirmDialog
import com.geoai.uicore.ext.showSeekbarDialog
import com.zkyt.lib_msdk_ext.component.camera.BaseCameraExt
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-07 19:30.
 * @Description :
 */
@Route(path = ARouterPath.PATH_CONTROL_CAMERA_CONTROL)
class CameraControlFragment: BaseVVDFragment<CameraControlViewControl, FragmentCameraBinding>() {

    override fun init(savedInstanceState: Bundle?) {
        mBinding.btnCameraMode.setOnClickListener { showCameraModeDialog()}
        mBinding.btnCameraSingleShoot.setOnClickListener { showSingleShootDialog()}
        mBinding.btnCameraMultShoot.setOnClickListener { showMultShootDialog() }
        mBinding.btnCameraStartIntervalCapture.setOnClickListener { showIntervalStartShootDialog() }
        mBinding.btnCameraStopIntervalCapture.setOnClickListener { showIntervalStopShootDialog() }
        mBinding.btnStartRecord.setOnClickListener { showStartRecordDialog() }
        mBinding.btnStopRecord.setOnClickListener { showStopRecordDialog() }
        mBinding.btnFormatStorage.setOnClickListener { showFormatStorageDialog() }
        mBinding.btnCameraLineStyle.setOnClickListener { showCameraLineStyleDisplayDialog() }
    }

    private fun showCameraLineStyleDisplayDialog () {
        GEOUISelectorDialog()
            .setTitle(getString(R.string.camera_control_line_style))
            .setDataSource(CameraLinesStyle.values().map { it.name }.toTypedArray())
            .setConfirmButton { actionName, _ ->
                mViewModel.sendUiIntent(CameraControlIntent.SelectCameraLineStyle(CameraLinesStyle.valueOf(actionName!!)))
            }.show(parentFragmentManager)
    }

    private fun showCameraModeDialog() {
        val array = CameraMode.values().map { it.name }.toTypedArray()
        showStateLoading()
        BaseCameraExt.getCameraModeRx()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                showStateSuccess()
                showItemSelectedDialog(getString(R.string.camera_control_mode), it.name, array, parentFragmentManager) { actionName, _ ->
                    showStateLoading()
                    BaseCameraExt.setCameraModeRx(CameraMode.valueOf(actionName!!))
                        .subscribeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            showStateSuccess()
                        }) { error ->
                            showStateError(error.message)
                        }.addTo(mViewModel.compositeDisposable)
                }
            }) {error->
                showStateError(error.message)
            }.addTo(mViewModel.compositeDisposable)
    }

    private fun showSingleShootDialog() {
        showMessageConfirmDialog(getString(R.string.camera_control_single_shoot), parentFragmentManager, {
            showStateLoading()
            BaseCameraExt.startSingleCaptureRx().subscribe({
                showStateSuccess()
            }){
                showStateError(it.message)
            }.addTo(mViewModel.compositeDisposable)
        })
    }

    private fun showMultShootDialog() {
        showSeekbarDialog(getString(R.string.camera_control_mult_shoot), "", 10.0f, 2.0f, 1.0f, "pcs", 2.0, parentFragmentManager) {
            showStateLoading()
            BaseCameraExt.startComboCaptureRx(it.toInt()).subscribe({
                showStateSuccess()
            }){ error ->
                showStateError(error.message)
            }.addTo(mViewModel.compositeDisposable)
        }
    }

    private fun showIntervalStartShootDialog() {
        showSeekbarDialog(getString(R.string.camera_control_interval_capture), "", 20.0f, 2.0f, 1.0f, "s", 2.0, parentFragmentManager) {
            showStateLoading()
            BaseCameraExt.startIntervalCaptureRx(it.toInt()).subscribe({
                showStateSuccess()
            }){ error ->
                showStateError(error.message)
            }.addTo(mViewModel.compositeDisposable)
        }
    }

    private fun showIntervalStopShootDialog() {
        showMessageConfirmDialog(getString(R.string.camera_control_stop_interval_capture), parentFragmentManager, {
            showStateLoading()
            BaseCameraExt.stopIntervalCaptureRx().subscribe({
                showStateSuccess()
            }){
                showStateError(it.message)
            }.addTo(mViewModel.compositeDisposable)
        })
    }

    private fun showStartRecordDialog() {
        showMessageConfirmDialog(getString(R.string.camera_control_start_record), parentFragmentManager, {
            showStateLoading()
            BaseCameraExt.startRecordRx().subscribe({
                showStateSuccess()
            }){
                showStateError(it.message)
            }.addTo(mViewModel.compositeDisposable)
        })
    }

    private fun showStopRecordDialog() {
        showMessageConfirmDialog(getString(R.string.camera_control_stop_record), parentFragmentManager, {
            showStateLoading()
            BaseCameraExt.stopRecordRx().subscribe({
                showStateSuccess()
            }){
                showStateError(it.message)
            }.addTo(mViewModel.compositeDisposable)
        })
    }

    private fun showFormatStorageDialog() {
        val array = StorageFormatType.values().map { it.name }.toTypedArray()
        showItemSelectedDialog(getString(R.string.camera_control_format_storage), -1, array, parentFragmentManager) { actionName, _ ->
            showStateLoading()
            BaseCameraExt.formatStorageRx(StorageFormatType.valueOf(actionName!!)).subscribe({
                showStateSuccess()
            }){
                showStateError(it.message)
            }.addTo(mViewModel.compositeDisposable)
        }
    }
}