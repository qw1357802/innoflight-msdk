package com.geoai.featcamcontrol.mvi.eis

import com.geoai.basiclib.base.mvi.IUIEffect
import com.geoai.basiclib.base.mvi.IUiIntent
import com.geoai.basiclib.base.mvi.IUiState
import com.geoai.modservice.di.eventbus.CameraLinesStyle

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-07 14:09.
 * @Description :
 */
sealed class CameraControlEffect: IUIEffect {
}

sealed class CameraControlIntent: IUiIntent {
    data class SelectCameraLineStyle(val style: CameraLinesStyle): CameraControlIntent()
}

sealed class CameraControlState: IUiState {
    object INIT : CameraControlState()
}