package com.geoai.featcamcontrol

import android.os.Bundle
import com.alibaba.android.arouter.facade.annotation.Route
import com.geoai.basiclib.base.fragment.BaseVVDFragment
import com.geoai.basiclib.base.vm.EmptyViewModel
import com.geoai.mavlink.geoainet.payload.enums.CameraNV3StreamSource
import com.geoai.mavlink.geoainet.payload.enums.CameraThermalPalette
import com.geoai.mavlink.geoainet.payload.enums.CameraZoomRatio
import com.geoai.mavlink.geoainet.payload.enums.Thermal2DNoiseMode
import com.geoai.mavlink.geoainet.payload.enums.ThermalFFCMode
import com.geoai.mavlink.geoainet.payload.enums.ThermalImageModel
import com.geoai.mavlink.geoainet.payload.enums.ThermalImageTone
import com.geoai.modservice.di.ARouterPath
import com.geoai.uicamcontrol.R
import com.geoai.uicamcontrol.databinding.FragmentCameraNv3Binding
import com.geoai.uicore.ext.showItemSelectedDialog
import com.geoai.uicore.ext.showSeekbarDialog
import com.zkyt.lib_msdk_ext.component.camera.BaseCameraExt
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-17 11:50.
 * @Description :
 */
@Route(path = ARouterPath.PATH_CONTROL_CAMERA_NV3_CONTROL)
class CameraNV3Fragment : BaseVVDFragment<EmptyViewModel, FragmentCameraNv3Binding>() {

    override fun init(savedInstanceState: Bundle?) {
        mBinding.btnVideoSource.setOnClickListener { showVideoSourceDialog() }
        mBinding.btnCameraZoom.setOnClickListener { showCameraZoomDialog() }
        mBinding.btnSingleShootBySource.setOnClickListener { showSingleShootByLensDialog() }
        mBinding.btnCameraThermalPalette.setOnClickListener { showCameraThermalPaletteDialog() }
        mBinding.btnCameraThermalGain.setOnClickListener { showCameraThermalGainDialog() }
        mBinding.btnCameraThermalFcc.setOnClickListener { showThermalFccDialog() }
        mBinding.btnCameraNoiseFilter.setOnClickListener { showThermalNoiseFilterDialog() }
        mBinding.btnCameraDebunchingEnable.setOnClickListener { showThermalDebunchingDialog() }
        mBinding.btnCameraImageModel.setOnClickListener { showThermalImageModelDialog() }
        mBinding.btnCameraImageTone.setOnClickListener { showThermalImageToneDialog() }
        mBinding.btnCameraThermalDistance.setOnClickListener { showCameraThermalDistanceDialog() }
        mBinding.btnCameraThermalEmissivity.setOnClickListener { showCameraThermalEmissivityDialog() }
    }

    private fun showVideoSourceDialog() {
        showStateLoading()
        BaseCameraExt.getNV3Camera()?.let {
            it.getCameraSourceRx()
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe({source ->
                    showStateSuccess()
                    val sourceList = CameraNV3StreamSource.values().map { it.name }.toTypedArray()
                    showItemSelectedDialog(getString(R.string.camera_video_source), source.name, sourceList, parentFragmentManager) {actionName, pos ->
                        showStateLoading()
                        it.setCameraSourceRx(CameraNV3StreamSource.valueOf(actionName!!))
                            .subscribeOn(AndroidSchedulers.mainThread())
                            .subscribe({
                                showStateSuccess()
                            }){ error ->
                                showStateError(error.message)
                            }.addTo(mViewModel.compositeDisposable)
                    }
                }){ error ->
                    showStateError(error.message)
                }.addTo(mViewModel.compositeDisposable)
        }?: showStateError("Camera is null")
    }

    private fun showCameraZoomDialog() {
        showStateLoading()
        BaseCameraExt.getNV3Camera()?.let {
            it.getCameraZoomRatioRx()
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe({source ->
                    showStateSuccess()
                    val sourceList = CameraZoomRatio.values().map { it.name }.toTypedArray()
                    showItemSelectedDialog(getString(R.string.camera_camera_zoom), source.name, sourceList, parentFragmentManager) {actionName, pos ->
                        showStateLoading()
                        it.setCameraZoomRatioRx(CameraZoomRatio.valueOf(actionName!!))
                            .subscribeOn(AndroidSchedulers.mainThread())
                            .subscribe({
                                showStateSuccess()
                            }){ error ->
                                showStateError(error.message)
                            }.addTo(mViewModel.compositeDisposable)
                    }
                }){ error ->
                    showStateError(error.message)
                }.addTo(mViewModel.compositeDisposable)
        }?: showStateError("Camera is null")
    }

    private fun showSingleShootByLensDialog() {
        BaseCameraExt.getNV3Camera()?.let {
            val items = CameraNV3StreamSource.values().map { it.name }.toTypedArray()
            showItemSelectedDialog(getString(R.string.camera_control_single_shoot), -1, items, parentFragmentManager) {actionName, pos ->
                showStateLoading()
                it.singleShootRx(CameraNV3StreamSource.valueOf(actionName!!))
                    .subscribeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        showStateSuccess()
                    }){ error ->
                        showStateError(error.message)
                    }.addTo(mViewModel.compositeDisposable)
            }
        }?: showStateError("Camera is null")
    }

    private fun showCameraThermalPaletteDialog() {
        showStateLoading()
        BaseCameraExt.getNV3Camera()?.let {
            it.getThermalPaletteRx()
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe({ palette ->
                    showStateSuccess()
                    val items = CameraThermalPalette.values().map { it.name }.toTypedArray()
                    showItemSelectedDialog(getString(R.string.camera_thermal_palette), palette.name, items, parentFragmentManager) {actionName, pos ->
                        showStateLoading()
                        it.setThermalPaletteRx(CameraThermalPalette.valueOf(actionName!!))
                            .subscribeOn(AndroidSchedulers.mainThread())
                            .subscribe({
                                showStateSuccess()
                            }){ error ->
                                showStateError(error.message)
                            }.addTo(mViewModel.compositeDisposable)
                    }
                }){ error ->
                    showStateError(error.message)
                }.addTo(mViewModel.compositeDisposable)
        }?: showStateError("Camera is null")
    }

    private fun showCameraThermalGainDialog() {
        showStateLoading()
        BaseCameraExt.getNV3Camera()?.let {
            it.getThermalGainRx()
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe({source ->
                    showStateSuccess()
                    showSeekbarDialog(getString(R.string.camera_thermal_gain), "", 255.0f, 0.0f, 1.0f, "", source.toDouble(), parentFragmentManager) { value ->
                        showStateLoading()
                        it.setThermalGainRx(value.toInt())
                            .subscribeOn(AndroidSchedulers.mainThread())
                            .subscribe({
                                showStateSuccess()
                            }){ error ->
                                showStateError(error.message)
                            }.addTo(mViewModel.compositeDisposable)
                    }
                }){ error ->
                    showStateError(error.message)
                }.addTo(mViewModel.compositeDisposable)
        }?: showStateError("Camera is null")
    }

    private fun showThermalFccDialog() {
        showStateLoading()
        BaseCameraExt.getNV3Camera()?.let {
            it.getThermalFFCModeRx()
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe({source ->
                    showStateSuccess()
                    val items = ThermalFFCMode.values().map { it.name }.toTypedArray()
                    showItemSelectedDialog(getString(R.string.camera_thermal_fcc), source.name, items, parentFragmentManager) {actionName, pos ->
                        showStateLoading()
                        it.setThermalFFCModeRx(ThermalFFCMode.valueOf(actionName!!))
                            .subscribeOn(AndroidSchedulers.mainThread())
                            .subscribe({
                                showStateSuccess()
                            }){ error ->
                                showStateError(error.message)
                            }.addTo(mViewModel.compositeDisposable)
                    }
                }){ error ->
                    showStateError(error.message)
                }.addTo(mViewModel.compositeDisposable)
        }?: showStateError("Camera is null")
    }

    private fun showThermalNoiseFilterDialog() {
        showStateLoading()
        BaseCameraExt.getNV3Camera()?.let {
            it.getThermalNoiseFilterRx()
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe({source ->
                    showStateSuccess()
                    val items = Thermal2DNoiseMode.values().map { it.name }.toTypedArray()
                    showItemSelectedDialog(getString(R.string.camera_thermal_noise_filter), source.name, items, parentFragmentManager) {actionName, pos ->
                        showStateLoading()
                        it.setThermalNoiseFilterRx(Thermal2DNoiseMode.valueOf(actionName!!))
                            .subscribeOn(AndroidSchedulers.mainThread())
                            .subscribe({
                                showStateSuccess()
                            }){ error ->
                                showStateError(error.message)
                            }.addTo(mViewModel.compositeDisposable)
                    }
                }){ error ->
                    showStateError(error.message)
                }.addTo(mViewModel.compositeDisposable)
        }?: showStateError("Camera is null")
    }

    private fun showThermalDebunchingDialog() {
        showStateLoading()
        BaseCameraExt.getNV3Camera()?.let {
            it.getThermalDebunchingEnableRx()
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe({source ->
                    showStateSuccess()
                    val items = mutableListOf("true", "false").toTypedArray()
                    showItemSelectedDialog(getString(R.string.camera_thermal_noise_filter), source.toString(), items, parentFragmentManager) {actionName, pos ->
                        showStateLoading()
                        it.setThermalDebunchingEnableRx(actionName == "true")
                            .subscribeOn(AndroidSchedulers.mainThread())
                            .subscribe({
                                showStateSuccess()
                            }){ error ->
                                showStateError(error.message)
                            }.addTo(mViewModel.compositeDisposable)
                    }
                }){ error ->
                    showStateError(error.message)
                }.addTo(mViewModel.compositeDisposable)
        }?: showStateError("Camera is null")
    }

    private fun showThermalImageModelDialog() {
        showStateLoading()
        BaseCameraExt.getNV3Camera()?.let {
            it.getThermalImageModelRx()
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe({source ->
                    showStateSuccess()
                    val items = ThermalImageModel.values().map { it.name }.toTypedArray()
                    showItemSelectedDialog(getString(R.string.camera_thermal_image_model), source.name, items, parentFragmentManager) {actionName, pos ->
                        showStateLoading()
                        it.setThermalImageModelRx(ThermalImageModel.valueOf(actionName!!))
                            .subscribeOn(AndroidSchedulers.mainThread())
                            .subscribe({
                                showStateSuccess()
                            }){ error ->
                                showStateError(error.message)
                            }.addTo(mViewModel.compositeDisposable)
                    }
                }){ error ->
                    showStateError(error.message)
                }.addTo(mViewModel.compositeDisposable)
        }?: showStateError("Camera is null")
    }

    private fun showThermalImageToneDialog() {
        showStateLoading()
        BaseCameraExt.getNV3Camera()?.let {
            it.getThermalImageToneRx()
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe({source ->
                    showStateSuccess()
                    val items = ThermalImageTone.values().map { it.name }.toTypedArray()
                    showItemSelectedDialog(getString(R.string.camera_thermal_image_model), source.name, items, parentFragmentManager) {actionName, pos ->
                        showStateLoading()
                        it.setThermalImageToneRx(ThermalImageTone.valueOf(actionName!!))
                            .subscribeOn(AndroidSchedulers.mainThread())
                            .subscribe({
                                showStateSuccess()
                            }){ error ->
                                showStateError(error.message)
                            }.addTo(mViewModel.compositeDisposable)
                    }
                }){ error ->
                    showStateError(error.message)
                }.addTo(mViewModel.compositeDisposable)
        }?: showStateError("Camera is null")
    }

    private fun showCameraThermalDistanceDialog() {
        showStateLoading()
        BaseCameraExt.getNV3Camera()?.let {
            it.getThermalDistanceRx()
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe({source ->
                    showStateSuccess()
                    showSeekbarDialog(getString(R.string.camera_thermal_distance), "", 100.0f, 0.0f, 1.0f, "m", source.toDouble(), parentFragmentManager) { value ->
                        showStateLoading()
                        it.setThermalDistanceRx(value.toInt())
                            .subscribeOn(AndroidSchedulers.mainThread())
                            .subscribe({
                                showStateSuccess()
                            }){ error ->
                                showStateError(error.message)
                            }.addTo(mViewModel.compositeDisposable)
                    }
                }){ error ->
                    showStateError(error.message)
                }.addTo(mViewModel.compositeDisposable)
        }?: showStateError("Camera is null")
    }

    private fun showCameraThermalEmissivityDialog() {
        showStateLoading()
        BaseCameraExt.getNV3Camera()?.let {
            it.getThermalEmissivityRx()
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe({source ->
                    showStateSuccess()
                    showSeekbarDialog(getString(R.string.camera_thermal_emissivity), "", 100.0f, 0.0f, 1.0f, "m", source.toDouble(), parentFragmentManager) { value ->
                        showStateLoading()
                        it.setThermalEmissivityRx(value.toInt())
                            .subscribeOn(AndroidSchedulers.mainThread())
                            .subscribe({
                                showStateSuccess()
                            }){ error ->
                                showStateError(error.message)
                            }.addTo(mViewModel.compositeDisposable)
                    }
                }){ error ->
                    showStateError(error.message)
                }.addTo(mViewModel.compositeDisposable)
        }?: showStateError("Camera is null")
    }
}