package com.geoai.featcamcontrol

import android.os.Bundle
import com.alibaba.android.arouter.facade.annotation.Route
import com.geoai.basiclib.base.fragment.BaseVVDFragment
import com.geoai.basiclib.base.vm.EmptyViewModel
import com.geoai.mavlink.geoainet.payload.info.CameraCaptureStatusInfo
import com.geoai.modservice.di.ARouterPath
import com.geoai.modservice.di.eventbus.OnStatusMenuClickEvent
import com.geoai.uicamcontrol.databinding.FragmentCameraModeStatusBinding
import com.zkyt.lib_msdk_ext.component.camera.BaseCameraExt
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import org.greenrobot.eventbus.EventBus

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-30 10:41.
 * @Description :
 */
@Route(path = ARouterPath.PATH_STATUS_CAMERA_MODE)
class CameraModeStatusFragment: BaseVVDFragment<EmptyViewModel, FragmentCameraModeStatusBinding>() {

    override fun init(savedInstanceState: Bundle?) {
        mBinding.cameraModeStatus.setOnClickListener {
            EventBus.getDefault().post(OnStatusMenuClickEvent(System.currentTimeMillis()))
        }
        getCameraStatusObservable {
            val ret = "Mode: ${it.cameraMode} \r\n" +
                    "Image: ${it.imageStatus.name} \r\n" +
                    "Video: ${it.videoStatus.name} \r\n" +
                    "StorageBusy: ${it.isFileCopyingToStorage} \r\n"
            mBinding.tvCameraModeStatus.text = ret
        }
    }

    private fun getCameraStatusObservable(block: (cameraCaptureStatusInfo: CameraCaptureStatusInfo) -> Unit) {
        BaseCameraExt.getCaptureInfoObservable()
            .observeOn(AndroidSchedulers.mainThread())
            .filter { it.isPresent }
            .subscribe {
                block.invoke(it.get().second)
            }.addTo(mViewModel.compositeDisposable)
    }
}