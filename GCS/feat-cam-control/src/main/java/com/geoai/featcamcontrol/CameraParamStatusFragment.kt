package com.geoai.featcamcontrol

import android.os.Bundle
import com.alibaba.android.arouter.facade.annotation.Route
import com.geoai.basiclib.base.fragment.BaseVVDFragment
import com.geoai.basiclib.base.vm.EmptyViewModel
import com.geoai.mavlink.geoainet.payload.info.CameraCaptureStatusInfo
import com.geoai.modservice.di.ARouterPath
import com.geoai.modservice.di.eventbus.OnStatusMenuClickEvent
import com.geoai.uicamcontrol.databinding.FragmentCameraParamStatusBinding
import com.zkyt.lib_msdk_ext.component.camera.BaseCameraExt
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import org.greenrobot.eventbus.EventBus

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-30 10:41.
 * @Description :
 */
@Route(path = ARouterPath.PATH_STATUS_CAMERA_PARAM)
class CameraParamStatusFragment: BaseVVDFragment<EmptyViewModel, FragmentCameraParamStatusBinding>() {

    override fun init(savedInstanceState: Bundle?) {
        mBinding.cameraParamStatus.setOnClickListener {
            EventBus.getDefault().post(OnStatusMenuClickEvent(System.currentTimeMillis()))
        }
        getCameraStatusObservable {
            val ret = "recordingTimeMs: ${it.recordingTimeMs} \r\n" +
                    "availableCapacity: ${it.availableCapacity} \r\n" +
                    "availableSnapCount: ${it.availableSnapCount} \r\n" +
                    "availableRecordMin: ${it.availableRecordMin} \r\n" +
                    "imageIntervalUnit: ${it.imageIntervalUnit} \r\n"
            mBinding.tvCameraParamStatus.text = ret
        }
    }

    private fun getCameraStatusObservable(block: (cameraCaptureStatusInfo: CameraCaptureStatusInfo) -> Unit) {
        BaseCameraExt.getCaptureInfoObservable()
            .observeOn(AndroidSchedulers.mainThread())
            .filter { it.isPresent }
            .subscribe {
                block.invoke(it.get().second)
            }.addTo(mViewModel.compositeDisposable)
    }
}