package com.geoai.featmissioncontrol

import android.os.Bundle
import com.alibaba.android.arouter.facade.annotation.Route
import com.geoai.basiclib.base.fragment.BaseVVDFragment
import com.geoai.basiclib.base.vm.EmptyViewModel
import com.geoai.mavlink.geoainet.mission.info.MissionStateInfo
import com.geoai.modservice.di.ARouterPath
import com.geoai.modservice.di.eventbus.OnStatusMenuClickEvent
import com.geoai.uimissioncontrol.databinding.FragmentMissionStatusBinding
import com.zkyt.lib_msdk_ext.component.MissionManagerExt
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import org.greenrobot.eventbus.EventBus

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-26 18:39.
 * @Description :
 */
@Route(path = ARouterPath.PATH_STATUS_MISSION)
class MissionStatusFragment : BaseVVDFragment<EmptyViewModel, FragmentMissionStatusBinding>() {

    override fun init(savedInstanceState: Bundle?) {
        mBinding.missionStatus.setOnClickListener {
            EventBus.getDefault().post(OnStatusMenuClickEvent(System.currentTimeMillis()))
        }
        getMissionStatusObservable {
            val ret = "mission status: ${it.missionState.name} \r\n" +
                    "mission upload: ${it.missionUploadState.name} \r\n" +
                    "total: ${it.totalWaypoint} \r\n" +
                    "current: ${it.currentReachWaypointIndex} \r\n" +
                    "reached: ${it.isReachedWaypoint} \r\n"
            mBinding.tvMissionStatus.text = ret
        }
    }

    private fun getMissionStatusObservable(block: (missionStatusInfo: MissionStateInfo) -> Unit) {
        MissionManagerExt.getMissionStateObservable()
            .observeOn(AndroidSchedulers.mainThread())
            .filter { it.isPresent }
            .subscribe {
                block.invoke(it.get())
            }.addTo(mViewModel.compositeDisposable)
    }

}