package com.geoai.featmissioncontrol

import android.os.Bundle
import com.alibaba.android.arouter.facade.annotation.Route
import com.geoai.basiclib.base.fragment.BaseVVDFragment
import com.geoai.featmissioncontrol.mvi.vm.MissionControlViewModel
import com.geoai.mavlink.geoainet.mission.enums.MissionSignalLostMode
import com.geoai.modservice.di.ARouterPath
import com.geoai.uicore.ext.showItemSelectedDialog
import com.geoai.uicore.ext.showMessageConfirmDialog
import com.geoai.uimissioncontrol.R
import com.geoai.uimissioncontrol.databinding.FragmentMissionBinding
import com.zkyt.lib_msdk_ext.component.MissionManagerExt
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-07 19:16.
 * @Description :
 */
@Route(path = ARouterPath.PATH_CONTROL_MISSION_CONTROL)
class MissionControlFragment: BaseVVDFragment<MissionControlViewModel, FragmentMissionBinding>() {

    override fun init(savedInstanceState: Bundle?) {
        mBinding.btnSelectMission.setOnClickListener {

        }
        mBinding.btnUploadMission.setOnClickListener {

        }
        mBinding.btnStartMission.setOnClickListener {
            showMessageConfirmDialog(getString(R.string.mission_control_start), parentFragmentManager, {
                showStateLoading()
                MissionManagerExt.startMissionRx()
                    .subscribeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        showStateSuccess()
                    }){ error ->
                        showStateError(error.message)
                    }.addTo(mViewModel.compositeDisposable)
            })
        }
        mBinding.btnPauseMission.setOnClickListener {
            showMessageConfirmDialog(getString(R.string.mission_control_pause), parentFragmentManager, {
                showStateLoading()
                MissionManagerExt.pauseMissionRx()
                    .subscribeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        showStateSuccess()
                    }){ error ->
                        showStateError(error.message)
                    }.addTo(mViewModel.compositeDisposable)
            })
        }
        mBinding.btnContinueMission.setOnClickListener {
            showMessageConfirmDialog(getString(R.string.mission_control_continue), parentFragmentManager, {
                showStateLoading()
                MissionManagerExt.continueMissionRx()
                    .subscribeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        showStateSuccess()
                    }){ error ->
                        showStateError(error.message)
                    }.addTo(mViewModel.compositeDisposable)
            })
        }
        mBinding.btnStopMission.setOnClickListener {
            showMessageConfirmDialog(getString(R.string.mission_control_stop), parentFragmentManager, {
                showStateLoading()
                MissionManagerExt.stopMissionRx()
                    .subscribeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        showStateSuccess()
                    }){ error ->
                        showStateError(error.message)
                    }.addTo(mViewModel.compositeDisposable)
            })
        }
        mBinding.btnMissionLostBehavior.setOnClickListener { showMissionLostBehaviorDialog() }
    }

    private fun showMissionLostBehaviorDialog() {
        showStateLoading()
        MissionManagerExt.getMissionLostBehaviorRx()
            .subscribeOn(AndroidSchedulers.mainThread())
            .subscribe({
                showStateSuccess()
                val items = MissionSignalLostMode.values().map { it.name }.toTypedArray()
                showItemSelectedDialog(getString(R.string.mission_control_lost_behavior), it.name, items, parentFragmentManager) { actionName, pos ->
                    showStateLoading()
                    MissionManagerExt.setMissionLostBehaviorRx(MissionSignalLostMode.valueOf(actionName!!))
                        .subscribeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            showStateSuccess()
                        }){ error ->
                            showStateError(error.message)
                        }.addTo(mViewModel.compositeDisposable)
                }
            }){ error ->
                showStateError(error.message)
            }.addTo(mViewModel.compositeDisposable)
    }
}