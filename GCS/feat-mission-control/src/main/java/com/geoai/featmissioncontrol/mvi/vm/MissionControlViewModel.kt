package com.geoai.featmissioncontrol.mvi.vm

import com.geoai.basiclib.base.mvi.BaseEISViewModel
import com.geoai.featmissioncontrol.mvi.eis.MissionControlEffect
import com.geoai.featmissioncontrol.mvi.eis.MissionControlIntent
import com.geoai.featmissioncontrol.mvi.eis.MissionControlState

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-07 19:17.
 * @Description :
 */
class MissionControlViewModel: BaseEISViewModel<MissionControlEffect, MissionControlIntent, MissionControlState>() {

    override fun initUiState(): MissionControlState {
        return MissionControlState.INIT
    }

    override fun handleIntent(intent: MissionControlIntent) {
    }

}