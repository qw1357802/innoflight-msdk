/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-02 10:40.
 * @Description :
 */
object VersionKotlin {
    private var version = "1.8.22"

    var stdlib = "org.jetbrains.kotlin:kotlin-stdlib:$version"
    var reflect = "org.jetbrains.kotlin:kotlin-reflect:$version"
    val stdlibJdk7 = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:$version"
    val stdlibJdk8 = "org.jetbrains.kotlin:kotlin-stdlib-jdk8:$version"

    //协程
    object Coroutines {
        private const val version = "1.7.1"
        const val core = "org.jetbrains.kotlinx:kotlinx-coroutines-core:$version"
        const val android = "org.jetbrains.kotlinx:kotlinx-coroutines-android:$version"
    }
}