/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-02 10:40.
 * @Description :
 */
object ProjectConfig {
    const val minSdk = 24
    const val compileSdk = 34
    const val targetSdk = 31

    const val versionCode = 103
    const val versionName = "0.0.3-dropper"
    const val applicationId = "com.geoai.gcs2"
    const val testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
}

//签名文件信息配置
object SigningConfigs {
    //密钥文件路径
    const val store_file = "key.jks"

    //密钥密码
    const val store_password = "123456"

    //密钥别名
    const val key_alias = "geoai"

    //别名密码
    const val key_password = "123456"
}