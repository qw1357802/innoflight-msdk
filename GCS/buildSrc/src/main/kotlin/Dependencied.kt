import org.gradle.api.artifacts.dsl.DependencyHandler

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-02 10:38.
 * @Description :
 */

fun DependencyHandler.appcompat() {
    api(VersionAndroidX.appcompat)
    api(VersionAndroidX.supportV4)
    api(VersionAndroidX.coreKtx)
    api(VersionAndroidX.activityKtx)
    api(VersionAndroidX.fragmentKtx)
    api(VersionAndroidX.multidex)
    api(VersionAndroidX.documentFile)
}

fun DependencyHandler.rxJava() {
    api(VersionThirdPart.RxJava.rxJava)
    api(VersionThirdPart.RxJava.rxAndroid)
    api(VersionThirdPart.RxJava.rxKotlin)
}

//生命周期监听
fun DependencyHandler.lifecycle() {
    api(VersionAndroidX.Lifecycle.livedata)
    api(VersionAndroidX.Lifecycle.liveDataKtx)
    api(VersionAndroidX.Lifecycle.runtime)
    api(VersionAndroidX.Lifecycle.runtimeKtx)

    api(VersionAndroidX.Lifecycle.viewModel)
    api(VersionAndroidX.Lifecycle.viewModelKtx)
    api(VersionAndroidX.Lifecycle.viewModelSavedState)

    kapt(VersionAndroidX.Lifecycle.compiler)
}

//Kotlin与协程
fun DependencyHandler.kotlin() {
    api(VersionKotlin.stdlib)
    api(VersionKotlin.reflect)
    api(VersionKotlin.stdlibJdk7)
    api(VersionKotlin.stdlibJdk8)

    api(VersionKotlin.Coroutines.android)
    api(VersionKotlin.Coroutines.core)
}

//依赖注入
fun DependencyHandler.hilt() {
    implementation(VersionAndroidX.Hilt.hiltAndroid)
    implementation(VersionAndroidX.Hilt.javapoet)
    implementation(VersionAndroidX.Hilt.javawriter)
    kapt(VersionAndroidX.Hilt.hiltCompiler)
}

//测试Test依赖
fun DependencyHandler.test() {
    testImplementation(VersionTesting.junit)
    androidTestImplementation(VersionTesting.androidJunit)
    androidTestImplementation(VersionTesting.espresso)
}

//常用的布局控件
fun DependencyHandler.widgetLayout() {
    api(VersionAndroidX.constraintlayout)
    api(VersionAndroidX.cardView)
    api(VersionAndroidX.recyclerView)
    api(VersionThirdPart.baseRecycleViewHelper)
    api(VersionAndroidX.material)
    api(VersionAndroidX.ViewPager.viewpager)
    api(VersionAndroidX.ViewPager.viewpager2)
}

//路由
fun DependencyHandler.router() {
    implementation(VersionThirdPart.ARouter.core)
    kapt(VersionThirdPart.ARouter.compiler)
}

//Work任务
fun DependencyHandler.work() {
    api(VersionAndroidX.Work.runtime)
    api(VersionAndroidX.Work.runtime_ktx)
}

fun DependencyHandler.eventBus() {
    api(VersionThirdPart.EventBus.core)
}

//KV存储
fun DependencyHandler.dataStore() {
    implementation(VersionAndroidX.DataStore.preferences)
    implementation(VersionAndroidX.DataStore.core)
}

//网络请求
fun DependencyHandler.retrofit() {
    api(VersionThirdPart.Retrofit.core)
    api(VersionThirdPart.Retrofit.okio)
    implementation(VersionThirdPart.Retrofit.convertGson)
    api(VersionThirdPart.Retrofit.gson)
//    api(VersionThirdPart.gsonFactory)
}

//图片加载
fun DependencyHandler.glide() {
    implementation(VersionThirdPart.Glide.core)
    implementation(VersionThirdPart.Glide.annotation)
    implementation(VersionThirdPart.Glide.integration)
    kapt(VersionThirdPart.Glide.compiler)
}

//多媒体相机相册
fun DependencyHandler.imageSelector() {
//    implementation(VersionThirdPart.ImageSelector.core)
//    implementation(VersionThirdPart.ImageSelector.compress)
//    implementation(VersionThirdPart.ImageSelector.ucrop)
}

//弹窗
fun DependencyHandler.xpopup() {
    implementation(VersionThirdPart.XPopup.core)
    implementation(VersionThirdPart.XPopup.picker)
    implementation(VersionThirdPart.XPopup.easyAdapter)
}

fun DependencyHandler.aMap() {
    api(VersionThirdPart.aMap.amap3d)
    api(VersionThirdPart.aMap.amapLocation)
    api(VersionThirdPart.aMap.amapSearch)
}

fun DependencyHandler.osmMap() {
    api(VersionThirdPart.osmMap.osmdroid)
}

fun DependencyHandler.mapbox() {
    api(VersionThirdPart.mapboxMap.mapbox)
    api(VersionThirdPart.mapboxMap.mapboxPlugin)
}

//下拉刷新
fun DependencyHandler.refresh() {
//    api(VersionThirdPart.SmartRefresh.core)
//    api(VersionThirdPart.SmartRefresh.classicsHeader)
}