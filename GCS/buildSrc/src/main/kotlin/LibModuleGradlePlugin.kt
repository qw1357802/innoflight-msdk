/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-02 10:40.
 * @Description :
 */
class LibModuleGradlePlugin : DefaultGradlePlugin(){
    override fun isLibraryNeedService(): Boolean {
        return true
    }
}