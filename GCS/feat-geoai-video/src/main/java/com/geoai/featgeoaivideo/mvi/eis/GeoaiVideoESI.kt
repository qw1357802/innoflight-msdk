package com.geoai.featgeoaivideo.mvi.eis

import com.geoai.basiclib.base.mvi.IUIEffect
import com.geoai.basiclib.base.mvi.IUiIntent
import com.geoai.basiclib.base.mvi.IUiState
import com.geoai.modservice.di.eventbus.CameraLinesStyle
import com.geoai.video.YFV5Params

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-11 11:14.
 * @Description :
 */

sealed class GeoaiVideoEffect: IUIEffect {
    data class StartPlay(val source: YFV5Params): GeoaiVideoEffect()
    data class UpdateFpvSource(val source: String): GeoaiVideoEffect()
    data class UpdateCameraLineStyle(val style: CameraLinesStyle): GeoaiVideoEffect()
}

sealed class GeoaiVideoIntent: IUiIntent {
    data class ObtainPlayParam(val autoPlay: Boolean): GeoaiVideoIntent()
    object OnFpvTouchEvent: GeoaiVideoIntent()
    object ToggleFpvSource: GeoaiVideoIntent()
    data class SaveCameraLineStyle(val style: CameraLinesStyle): GeoaiVideoIntent()
}

sealed class GeoaiVideoState: IUiState {
    object MiniFpv: GeoaiVideoState()
    object BigFpv: GeoaiVideoState()
}