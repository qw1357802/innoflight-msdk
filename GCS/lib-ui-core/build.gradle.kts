plugins {
    id("com.android.library")
}

// 使用自定义插件
apply<UIModuleGradlePlugin>()

android {
    namespace = "com.geoai.uicore"
}

dependencies {

    api(project(":lib-base"))
    implementation("com.blankj:utilcodex:1.29.0")
}