package com.geoai.uicore.custom.ui

import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.geoai.basiclib.ext.getDimension
import com.geoai.uicore.R
import com.geoai.uicore.custom.dialog.GEOUIBaseBottomDialog
import kotlin.math.roundToInt

class GEOUISelectorDialog : GEOUIBaseBottomDialog(), SelectorDialogBuilder {

    private var title: String? = null
    private var dataSource: MutableList<SelectorItem> = mutableListOf()
    private val mItemAdapter by lazy { SelectorItemAdapter(requireContext(), dataSource) }
    private var mInputSelector = -1
    private var mCurrentSelectIndex = -1;

    private var mClickListener: ((actionName: String?, actionPosition: Int?) -> Unit)? = null

    override fun getLayoutId(): Int = R.layout.view_dialog_bottom_selector

    override fun onStart() {
        setFixHeight(requireView().getDimension(R.dimen.base_bottom_dialog_height).roundToInt())
        setFixWidth(requireView().getDimension(R.dimen.base_bottom_dialog_width).roundToInt())
        requireView().findViewById<GEOUIDialogTopBar>(R.id.topbar).setCancelAction { dismiss() }
        requireView().findViewById<GEOUIDialogTopBar>(R.id.topbar).setOkAction {
            if (mCurrentSelectIndex != -1) {
                mClickListener?.invoke(dataSource[mCurrentSelectIndex].title, mCurrentSelectIndex)
            }
            dismiss()
        }
        super.onStart()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        title?.let { requireView().findViewById<GEOUIDialogTopBar>(R.id.topbar).title = it }
        //initListener
        mItemAdapter.setClickListener { _, _, pos ->
            mCurrentSelectIndex = pos
            mItemAdapter.notifyDataSetChanged()
        }
        //initRecycleView
        dataSource.isNotEmpty().let {
            requireView().findViewById<RecyclerView>(R.id.recycle).adapter = mItemAdapter
            requireView().findViewById<RecyclerView>(R.id.recycle).layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        }
    }

    override fun setTitle(title: String?): GEOUISelectorDialog {
        this.title = title
        return this
    }

    override fun setConfirmButton(action: ((actionName: String?, actionPosition: Int?) -> Unit)?): GEOUISelectorDialog {
        this.mClickListener = action
        return this
    }

    override fun setDataSource(data: MutableList<String>): GEOUISelectorDialog {
        data.forEachIndexed { index, s ->
            if (index == mInputSelector) {
                this.dataSource.add(SelectorItem(s, true))
            } else {
                this.dataSource.add(SelectorItem(s, false))
            }
        }
        return this
    }

    override fun setDataSource(data: Array<String>): GEOUISelectorDialog {
        data.forEachIndexed { index, s ->
            if (index == mInputSelector) {
                this.dataSource.add(SelectorItem(s, true))
            } else {
                this.dataSource.add(SelectorItem(s, false))
            }
        }
        return this
    }

    override fun setCurrentSelectIndex(pos: Int): GEOUISelectorDialog {
        // begin from 1
        this.mInputSelector = pos - 1
        this.mCurrentSelectIndex = pos - 1
        return this
    }
}

interface SelectorDialogBuilder {
    fun setTitle(title: String?): GEOUISelectorDialog

    fun setConfirmButton(
        action: ((actionName: String?,
                  actionPosition: Int?) -> Unit)?
    ): GEOUISelectorDialog

    fun setDataSource(data: MutableList<String>): GEOUISelectorDialog

    fun setDataSource(stringArray: Array<String>): GEOUISelectorDialog
    fun setCurrentSelectIndex(pos: Int): GEOUISelectorDialog
}

class SelectorItemAdapter(private val context: Context,
                          private val dataSource: MutableList<SelectorItem>
): RecyclerView.Adapter<SelectorItemAdapter.VH>() {

    private var onClickListener: ((View, String, Int) -> Unit)? = null

    fun setClickListener(onClickListener: ((View, String, Int) -> Unit)?) {
        this.onClickListener = onClickListener
    }

    class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val rb: RadioButton = itemView.findViewById(R.id.rb)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(View.inflate(context, R.layout.item_dialog_bottom_selector, null))
    }

    override fun getItemCount(): Int = dataSource.size

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.rb.setOnClickListener { view ->
            dataSource.map { it.selector = false }
            dataSource[position].selector = true
            onClickListener?.invoke(view, holder.rb.text.toString(), position)
        }
        holder.rb.text = dataSource[position].title
        holder.rb.isChecked = dataSource[position].selector
    }
}

data class SelectorItem(val title: String, var selector: Boolean)