package com.geoai.uicore.custom.ui

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.geoai.uicore.R
import com.geoai.uicore.custom.dialog.GEOUIBaseCenterDialog

class GEOUISeekbarDialog : GEOUIBaseCenterDialog(), SeekbarDialogBuilder {

    private var primaryButtonName: String? = null
    private var secondaryButtonName: String? = null
    private var title: String? = null
    private var topText: String? = null
    private var min: Double = 1.0
    private var max: Double = 100.0
    private var value: Double = 50.0
    private var step: Double = 1.0
    private var unitText: String = ""

    private var seekBarValue: Double? = null

    private var primaryAction: ((GEOUISeekbarDialog) -> Unit)? = null
    private var secondaryAction: ((GEOUISeekbarDialog) -> Unit)? = null

    override fun getLayoutId(): Int = R.layout.view_dialog_center_seekbar

    fun getSeekbarValue(): Double = seekBarValue?:value

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (title.isNullOrEmpty()) {
            view.findViewById<TextView>(R.id.tv_title_device).visibility = View.GONE
        } else {
            view.findViewById<TextView>(R.id.tv_title_device).text = title
        }

        view.findViewById<GEOUISeekbar>(R.id.uiseekbar).apply {
            topText?.let { this.setItemName(it) }
            setMin(min)
            setMax(max)
            setStep(step)
            setValue(value)
            setTopText(value.toString())
            setUnitText(unitText)
        }.setListener(object : GEOUISeekbar.OnSeekBarAdvancedListener {
            override fun onValueChanging(value: Double) {

            }

            override fun onValueChanged(value: Double) {
                seekBarValue = value
            }

            override fun onTextFormat(value: Double): String {
                return String.format("%.1f", value)
            }
        })

        view.findViewById<Button>(R.id.btn_secondary_action).let {
            if (secondaryButtonName.isNullOrEmpty()) {
                it.visibility = View.GONE
            } else {
                it.text = secondaryButtonName
                it.setOnClickListener {
                    secondaryAction?.invoke(this)
                    dismiss()
                }
            }
        }

        view.findViewById<Button>(R.id.btn_primary_action).let {
            if (primaryButtonName.isNullOrEmpty()) {
                it.visibility = View.GONE
            } else {
                it.text = primaryButtonName
                it.setOnClickListener {
                    primaryAction?.invoke(this)
                    dismiss()
                }
            }
        }
    }
    override fun setTitle(title: String?): GEOUISeekbarDialog {
        this.title = title
        return this
    }

    override fun setSubTitle(topText: String?): GEOUISeekbarDialog {
        this.topText = topText
        return this
    }

    override fun setMin(value: Double): GEOUISeekbarDialog {
        this.min = value
        return this
    }

    override fun setMax(value: Double): GEOUISeekbarDialog {
        this.max = value
        return this
    }

    override fun setValue(value: Double): GEOUISeekbarDialog {
        this.value = value
        return this
    }

    override fun setStep(value: Double): GEOUISeekbarDialog {
        this.step = value
        return this
    }

    override fun setUnitText(unit: String): GEOUISeekbarDialog {
        this.unitText = unit
        return this
    }

    override fun setPrimaryButton(
        actionName: String,
        action: ((GEOUISeekbarDialog) -> Unit)?
    ): GEOUISeekbarDialog {
        primaryButtonName = actionName
        primaryAction = action
        return this
    }

    override fun setSecondaryButton(
        actionName: String,
        action: ((GEOUISeekbarDialog) -> Unit)?
    ): GEOUISeekbarDialog {
        secondaryButtonName = actionName
        secondaryAction = action
        return this
    }
}

interface SeekbarDialogBuilder {
    fun setTitle(title: String?): GEOUISeekbarDialog

    fun setSubTitle(title: String?): GEOUISeekbarDialog

    fun setMin(value: Double): GEOUISeekbarDialog

    fun setMax(value: Double): GEOUISeekbarDialog

    fun setValue(value: Double): GEOUISeekbarDialog

    fun setStep(value: Double): GEOUISeekbarDialog

    fun setUnitText(unit: String): GEOUISeekbarDialog

    fun setPrimaryButton(actionName: String, action: ((GEOUISeekbarDialog) -> Unit)?): GEOUISeekbarDialog

    fun setSecondaryButton(actionName: String, action: ((GEOUISeekbarDialog) -> Unit)?): GEOUISeekbarDialog
}