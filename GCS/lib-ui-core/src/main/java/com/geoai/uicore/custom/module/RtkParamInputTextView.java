package com.geoai.uicore.custom.module;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.geoai.uicore.R;

public class RtkParamInputTextView extends FrameLayout {

    public RtkParamInputTextView(@NonNull Context context) {
        super(context);
    }

    public RtkParamInputTextView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public RtkParamInputTextView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    private TextView mTvTitle;
    private EditText mEdtInput;

    private void init(Context context, AttributeSet attrs, int defStyleAttr) {
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.RtkComponentView, defStyleAttr, 0);

        String title = array.getString(R.styleable.RtkComponentView_rtk_title);
        int inputType = array.getInteger(R.styleable.RtkComponentView_android_inputType, 0);

        View view = View.inflate(context, R.layout.view_rtk_param_input_text_view, this);
        mTvTitle = view.findViewById(R.id.tv_title);
        mEdtInput = view.findViewById(R.id.edt_input);

        if (!TextUtils.isEmpty(title)) {
            mTvTitle.setText(title);
        }
        mEdtInput.setInputType(inputType);
    }

    public void setParam(String param) {
        mEdtInput.setText(param);
    }

    public String getParam() {
        return mEdtInput.getText().toString();
    }

}
