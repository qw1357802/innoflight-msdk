package com.geoai.uicore.custom.tab

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.widget.SwitchCompat
import com.geoai.uicore.R

class TabItemSwitchInput(context: Context, attr: AttributeSet?) : LinearLayout(context, attr) {

    private val mView = inflate(context, R.layout.tab_item_switch_input, this)

    private var mListener: OnSwitchListener? = null

    init {
        val obtainStyledAttributes = context.obtainStyledAttributes(attr, R.styleable.TabItemSwitchInput)

        mView.findViewById<TextView>(R.id.tv_tab_item_input_title).text = obtainStyledAttributes.getText(R.styleable.TabItemSwitchInput_view_switch_title).toString()
        mView.findViewById<SwitchCompat>(R.id.sw_tab_item_input).isChecked = obtainStyledAttributes.getBoolean(R.styleable.TabItemSwitchInput_view_switch_default, false)

        obtainStyledAttributes.recycle()

        mView.findViewById<SwitchCompat>(R.id.sw_tab_item_input).setOnClickListener {
            this.mListener?.onSwitch(mView.findViewById<SwitchCompat>(R.id.sw_tab_item_input).isChecked)
        }
    }

    fun setClick(isClick: Boolean) {
        mView.findViewById<SwitchCompat>(R.id.sw_tab_item_input).isChecked = isClick
    }

    fun setOnSwitchListener(listener: OnSwitchListener) {
        this.mListener = listener;
    }

    interface OnSwitchListener {
        fun onSwitch(isCheck: Boolean)
    }
}