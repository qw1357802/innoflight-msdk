package com.geoai.uicore;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.geoai.uicore.model.SignalLevel;

public class RemoteControlSignalView extends FrameLayout {
    public RemoteControlSignalView(@NonNull Context context) {
        super(context);
        init(context);
    }

    public RemoteControlSignalView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public RemoteControlSignalView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

//    private SignalView rcSignal;
    private SignalView rcSignalUplink;
    private SignalView rcSignalDownlink;


    private void init(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.view_rc_signal, this);
        rcSignalUplink = view.findViewById(R.id.rc_signal_uplink);
        rcSignalDownlink = view.findViewById(R.id.rc_signal_downlink);
    }

    /**
     * 设置遥控器信号量
     *
     * @param level
     */
    public void setRCSignal(int level) {
        if (level == 0) {
            rcSignalUplink.setWeakIndex(0);
        } else if (level < SignalLevel.LEVEL_1) {
            rcSignalUplink.setWeakIndex(1);
        } else if (level < SignalLevel.LEVEL_2) {
            rcSignalUplink.setWeakIndex(2);
        } else if (level < SignalLevel.LEVEL_3) {
            rcSignalUplink.setNormalIndex(3);
        } else if (level < SignalLevel.LEVEL_4) {
            rcSignalUplink.setNormalIndex(4);
        } else if (level <= SignalLevel.LEVEL_5) {
            rcSignalUplink.setNormalIndex(5);
        }
        rcSignalUplink.refresh();
    }

    /**
     * 设置遥控器信号量
     *
     * @param level
     */
    public void setVideoSignal(int level) {
        if (level == 0) {
            rcSignalDownlink.setWeakIndex(0);
        } else if (level < SignalLevel.LEVEL_1) {
            rcSignalDownlink.setWeakIndex(1);
        } else if (level < SignalLevel.LEVEL_2) {
            rcSignalDownlink.setWeakIndex(2);
        } else if (level < SignalLevel.LEVEL_3) {
            rcSignalDownlink.setNormalIndex(3);
        } else if (level < SignalLevel.LEVEL_4) {
            rcSignalDownlink.setNormalIndex(4);
        } else if (level <= SignalLevel.LEVEL_5) {
            rcSignalDownlink.setNormalIndex(5);
        }
        rcSignalDownlink.refresh();
    }
}
