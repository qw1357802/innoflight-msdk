package com.geoai.uicore.model;

/**
 * 信号值
 */
public class SignalLevel {
    /**
     * 信号 1: 值为 20 以下的信号量
     */
    public static final int LEVEL_1 = 20;
    /**
     * 信号 2: 值为 20 ~ 40 的信号量
     */
    public static final int LEVEL_2 = 40;
    /**
     * 信号 3: 值为 40 ~ 60 的信号量
     */
    public static final int LEVEL_3 = 60;
    /**
     * 信号 4: 值为 60 ~ 80 的信号量
     */
    public static final int LEVEL_4 = 80;
    /**
     * 信号 5: 值为 80 ~ 100 以下的信号量
     */
    public static final int LEVEL_5 = 100;
}
