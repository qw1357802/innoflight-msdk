package com.geoai.uicore.custom.ui

import android.animation.ObjectAnimator
import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.animation.LinearInterpolator
import android.widget.Button
import android.widget.FrameLayout
import android.widget.TextView
import androidx.core.view.isVisible
import com.geoai.uicore.R

/**
 *  通用itemView
 */
class GEOUIDialogTopBar @JvmOverloads constructor(context: Context,
                                                  attrs: AttributeSet? = null,
                                                  defStyleAttr: Int = 0) : FrameLayout(context, attrs, defStyleAttr) {

    // 加载动画
    private var animator: ObjectAnimator? = null

    private var mView: View? = null

    var title: CharSequence?
        get() = mView!!.findViewById<TextView>(R.id.tv_title_device).text
        set(value) {
            mView!!.findViewById<TextView>(R.id.tv_title_device).text = value
        }


    init {
        mView = LayoutInflater.from(context).inflate(R.layout.widget_dialog_top_bar, this)
        val ta = context.obtainStyledAttributes(attrs, R.styleable.GEOUIDialogTopBar)
        val title = ta.getString(R.styleable.GEOUIDialogTopBar_dialog_title)
        ta.recycle()
        this.title = title
    }

    fun setOkAction(action: ((GEOUIDialogTopBar) -> Unit)) {
        mView!!.findViewById<TextView>(R.id.btn_ok).setOnClickListener {
            action.invoke(this@GEOUIDialogTopBar)
        }
    }

    fun setCancelAction(action: ((GEOUIDialogTopBar) -> Unit)) {
        mView!!.findViewById<TextView>(R.id.btn_cancel).setOnClickListener {
            action.invoke(this@GEOUIDialogTopBar)
        }
    }

    fun setBtnOkEnable(enable: Boolean) {
        mView!!.findViewById<TextView>(R.id.btn_ok).isEnabled = enable
    }

    fun setBtnCancelVisibility(visibility: Int) {
        mView!!.findViewById<TextView>(R.id.btn_cancel).visibility = visibility
    }

    fun setBtnOkTitle(title: String, boldFlag: Boolean = false) {
        mView!!.findViewById<TextView>(R.id.btn_ok)?.let {
            if (!title.isNullOrEmpty()) {
                it.text = title
            }
            if (boldFlag) {
                it.setTypeface(null, Typeface.BOLD)
            }
        }
    }

    fun setIvLoadingVisible(visible: Boolean) {
        val ivLoading = mView!!.findViewById<Button>(R.id.iv_loading)
        if (visible && animator == null) {
            // 初始话动画
            animator = ObjectAnimator.ofFloat(ivLoading, "rotation", 0f, 360f)
            animator?.repeatCount = ObjectAnimator.INFINITE
            animator?.repeatMode = ObjectAnimator.RESTART
            animator?.interpolator = LinearInterpolator()
            animator?.duration = 800
        }
        if (visible) {
            animator?.start()
        } else {
            animator?.pause()
        }
        mView!!.findViewById<TextView>(R.id.btn_ok).isVisible = !visible
        ivLoading.isVisible = visible
    }
}