package com.geoai.uicore.custom.dialog

interface DismissListener {
    fun dismiss()
}