package com.geoai.uicore;

import static com.geoai.basiclib.ext.CommonExtKt.dp2px;
import static com.geoai.basiclib.utils.CommUtils.getColor;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import androidx.annotation.Nullable;

public class SignalView extends View {

    public SignalView(Context context) {
        super(context);
    }

    public SignalView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public SignalView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /**
     * 信号格数量
     */
    private static final int SIGNAL_NUM = 5;
    private static final int NO_INDEX = -1;
    /**
     * 差值距离、信号宽度
     */
    private int signalWidth = dp2px(2);

    private int width, height;

    /**
     * 满信号，即白色的条子
     */
    private Paint signalPaint;

    private int normalColor;
    private int noneColor;
    private int weakColor;


    private int margin;
    private int leftMargin;
    private int rightMargin;


    private int weakIndex = NO_INDEX;
    private int normalIndex = NO_INDEX;

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        this.width = w;
        this.height = h;

        leftMargin = rightMargin = signalWidth / 2;
        // margin = (总宽度 - 所有信号量占据的宽度 - 左右俩边界 margin) / 间隔
        margin = (width - SIGNAL_NUM * signalWidth - leftMargin - rightMargin) / (SIGNAL_NUM - 1);

        normalColor = getColor(R.color.signal_normal);
        noneColor = getColor(R.color.signal_none);
        weakColor = getColor(R.color.signal_weak);

        signalPaint = new Paint();
        signalPaint.setStyle(Paint.Style.FILL);
        signalPaint.setStrokeWidth(signalWidth);
        signalPaint.setColor(getColor(R.color.signal_normal));
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int startX = leftMargin + signalWidth;
        int startY;
        int stopY = height;

        for (int i = 0; i < SIGNAL_NUM; i++) {

            if (weakIndex > i) {
                signalPaint.setColor(weakColor);
            } else if (normalIndex > i) {
                signalPaint.setColor(normalColor);
            } else {
                signalPaint.setColor(noneColor);
            }

            startY = (SIGNAL_NUM - i - 1) * signalWidth;
            canvas.drawLine(startX, startY, startX, stopY, signalPaint);
            startX += margin + signalWidth;
        }
    }

    /**
     * 设置信号弱 index，重置信号正常 index
     *
     * @param weakIndex
     */
    public void setWeakIndex(int weakIndex) {
        this.weakIndex = weakIndex;
        this.normalIndex = NO_INDEX;
    }

    /**
     * 设置信号正常 index，重置信号弱 index
     *
     * @param normalIndex
     */
    public void setNormalIndex(int normalIndex) {
        this.normalIndex = normalIndex;
        this.weakIndex = NO_INDEX;
    }

    /**
     * 刷新页面
     */
    public void refresh(){
        invalidate();
    }
}
