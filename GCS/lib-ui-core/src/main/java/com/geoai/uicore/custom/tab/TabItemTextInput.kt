package com.geoai.uicore.custom.tab

import android.content.Context
import android.util.AttributeSet
import android.widget.RelativeLayout
import android.widget.TextView
import com.geoai.basiclib.ext.textColor
import com.geoai.basiclib.utils.CommUtils.getColor
import com.geoai.uicore.R
import com.geoai.uicore.custom.MarqueeTextView

class TabItemTextInput(context: Context, attr: AttributeSet?) : RelativeLayout(context, attr) {

    private val view = inflate(getContext(), R.layout.tab_item_text_input, this)

    private var mTitle: String = ""

    private var mContent: String = ""

    private val isBorder: Boolean

    private var mDelegate: OnClickDelegate? = null

    init {
        val obtainStyledAttributes = context.obtainStyledAttributes(attr, R.styleable.TabItemTextInput)
        mTitle = obtainStyledAttributes.getString(R.styleable.TabItemTextInput_view_text_title).toString()
        mContent = obtainStyledAttributes.getString(R.styleable.TabItemTextInput_view_text_content).toString()
        isBorder = obtainStyledAttributes.getBoolean(R.styleable.TabItemTextInput_view_text_border, false)
        obtainStyledAttributes.recycle()

        view.findViewById<TextView>(R.id.tv_tab_item_input_title).text = mTitle
        view.findViewById<TextView>(R.id.tv_tab_item_input_content).text = mContent

        if (isBorder) {
            view.findViewById<MarqueeTextView>(R.id.tv_tab_item_input_content).setBackgroundResource(R.drawable.bg_textview_border)
            view.findViewById<MarqueeTextView>(R.id.tv_tab_item_input_content).textColor = getColor(R.color.btn_confirm_dark)
        }

        view.findViewById<TextView>(R.id.tv_tab_item_input_content).setOnClickListener {
            mDelegate?.onClick(object : OnResult {
                override fun result(str: String) {
                    if (it is TextView) {
                        it.text = str
                    }
                }
            })
        }
    }

    companion object {
        @JvmStatic
        fun setParam(view: TabItemTextInput, param:String) {
            view.setContent(param)
        }
    }

    fun setTitle(s: String) {
        view.findViewById<TextView>(R.id.tv_tab_item_input_title).text = s
    }

    fun getTitle(): String {
        return view.findViewById<TextView>(R.id.tv_tab_item_input_title).text.toString()
    }

    fun setContent(s: String) {
        view.findViewById<TextView>(R.id.tv_tab_item_input_content).text = s
    }

    fun getContent(): String {
        return view.findViewById<TextView>(R.id.tv_tab_item_input_content).text.toString()
    }

    fun setOnClickDelegate(delegate: OnClickDelegate) {
        mDelegate = delegate;
    }

    interface OnClickDelegate {
        fun onClick(result: OnResult)
    }

    interface OnResult {
        fun result(str: String)
    }
}