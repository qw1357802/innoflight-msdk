package com.geoai.uicore

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-03 15:12.
 * @Description :
 */
class ControllerTextView: androidx.appcompat.widget.AppCompatTextView {

    private var isClickLock = false

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }

    @SuppressLint("ClickableViewAccessibility")
    fun init() {
        // 设置自动换行
        isSingleLine = false
        // 设置圆角背景
        setBackgroundResource(R.drawable.textview_rounded_background_default)

        setOnTouchListener { _, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    // 在这里设置你的背景资源
                    this.setBackgroundResource(R.drawable.textview_rounded_background_clicked)
                }
                MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL -> {
                    if (isClickLock) {
                        return@setOnTouchListener false
                    }
                    // 这里将背景清除，或者设置为默认背景
                    this.setBackgroundResource(R.drawable.textview_rounded_background_default)
                }
            }
            false
        }
    }

    fun setClickLock(isLock: Boolean) {
        isClickLock = isLock
        if (isLock) {
            this.setBackgroundResource(R.drawable.textview_rounded_background_clicked)
        } else {
            this.setBackgroundResource(R.drawable.textview_rounded_background_default)
        }
    }

    fun getLocked(): Boolean {
        return isClickLock
    }
}