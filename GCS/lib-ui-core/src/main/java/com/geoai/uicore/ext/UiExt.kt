package com.geoai.uicore.ext

import com.geoai.basiclib.utils.CommUtils.getString
import com.geoai.uicore.R
import com.geoai.uicore.custom.ui.GEOUIMessageDialog
import com.geoai.uicore.custom.ui.GEOUISeekbarDialog
import com.geoai.uicore.custom.ui.GEOUISelectorDialog

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-17 10:07.
 * @Description :
 */

fun Any.showSeekbarDialog(
    title: String,
    subTitle: String,
    maxValue: Float,
    minValue: Float,
    stepValue: Float,
    unitText: String,
    currentValue: Double,
    parentFragmentManager: androidx.fragment.app.FragmentManager,
    block: (value: Double) -> Unit) {
    GEOUISeekbarDialog()
        .setTitle(title)
        .setSubTitle(subTitle)
        .setMax(maxValue.toDouble())
        .setMin(minValue.toDouble())
        .setStep(stepValue.toDouble())
        .setUnitText(unitText)
        .setValue(currentValue)
        .setPrimaryButton(getString(R.string.confirm)) { block.invoke(it.getSeekbarValue()) }
        .show(parentFragmentManager)
}

fun Any.showItemSelectedDialog(
    title: String,
    value: String,
    data: Array<String>,
    parentFragmentManager: androidx.fragment.app.FragmentManager,
    confirmBlock: (actionName: String?, pos: Int?) -> Unit
) {
    val index = data.indexOf(value) + 1
    GEOUISelectorDialog()
        .setTitle(title)
        .setCurrentSelectIndex(index)
        .setDataSource(data)
        .setConfirmButton { actionName, actionPosition ->
            confirmBlock.invoke(actionName, actionPosition)
        }.show(parentFragmentManager)
}

fun Any.showItemSelectedDialog(
    title: String,
    currentSelectIndex: Int,
    data: Array<String>,
    parentFragmentManager: androidx.fragment.app.FragmentManager,
    confirmBlock: (actionName: String?, pos: Int?) -> Unit
) {
    GEOUISelectorDialog()
        .setTitle(title)
        .setCurrentSelectIndex(currentSelectIndex)
        .setDataSource(data)
        .setConfirmButton { actionName, actionPosition ->
            confirmBlock.invoke(actionName, actionPosition)
        }.show(parentFragmentManager)
}

fun Any.showMessageConfirmDialog(title: String,
                                 parentFragmentManager: androidx.fragment.app.FragmentManager,
                                 confirmBlock: () -> Unit,
                                 cancelBlock: (() -> Unit)? = null) {
    GEOUIMessageDialog()
        .setTitle(title)
        .setSecondaryButton(getString(R.string.cancel)) { cancelBlock?.invoke() }
        .setPrimaryButton(getString(R.string.confirm)) { confirmBlock.invoke() }
        .show(parentFragmentManager)
}