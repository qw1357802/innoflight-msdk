package com.geoai.uicore.custom.ui

import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.geoai.basiclib.ext.dp2px
import com.geoai.uicore.R
import com.geoai.uicore.custom.dialog.GEOUIBaseCenterDialog

class GEOUIMessageDialog : GEOUIBaseCenterDialog(), MessageDialogBuilder {
    private var title: String? = null
    private var message: String? = null
    private var subMessage: String? = null
    private var primaryButtonName: String? = null
    private var secondaryButtonName: String? = null
    private var icon: Drawable? = null
    private var primaryAction: ((GEOUIMessageDialog) -> Unit)? = null
    private var secondaryAction: ((GEOUIMessageDialog) -> Unit)? = null
    private var mPrimaryColor: String? = null
    private var mSecondaryColor: String? = null
    private var mLeftFlag: Boolean = false
    private var mSubLeftFlag: Boolean = false
    private var mPrimaryBoldFlag: Boolean = false
    private var mSecondaryBoldFlag: Boolean = false
    private var mMessageSize: Int = 0
    private var mBoldFlag: Boolean = false
    private var mSubMessageSize: Int = 0
    private var mSubBoldFlag: Boolean = false
    private var mTopPadding: Int = 0
    private var mTopMargin: Int = 0
    private var mTitleBoldFlag: Boolean = false

    override fun getLayoutId(): Int {
        return R.layout.view_dialog_center_message
    }

    override fun setTitle(title: String?): GEOUIMessageDialog {
        this.title = title
        return this
    }

    override fun setTitleBold(boldFlag: Boolean): GEOUIMessageDialog {
        this.mTitleBoldFlag = boldFlag
        return this
    }

    override fun setIcon(icon: Drawable?): GEOUIMessageDialog {
        this.icon = icon
        return this
    }

    override fun setMessage(message: String?): GEOUIMessageDialog {
        this.message = message
        return this
    }

    override fun setSubMessage(message: String?): GEOUIMessageDialog {
        this.subMessage = message
        return this
    }

    override fun setPrimaryButton(
        actionName: String,
        action: ((GEOUIMessageDialog) -> Unit)?
    ): GEOUIMessageDialog {
        primaryButtonName = actionName
        primaryAction = action
        return this
    }

    override fun setSecondaryButton(
        actionName: String,
        action: ((GEOUIMessageDialog) -> Unit)?
    ): GEOUIMessageDialog {
        secondaryButtonName = actionName
        secondaryAction = action
        return this
    }

    override fun setMessageTopPadding(topPadding: Int): GEOUIMessageDialog {
        mTopPadding = dp2px(topPadding.toFloat())
        return this
    }

    override fun setMessageTopMargin(topMargin: Int): GEOUIMessageDialog {
        mTopMargin = dp2px(topMargin.toFloat())
        return this
    }

    override fun setMessageBold(boldFlag: Boolean, textSize: Int): GEOUIMessageDialog {
        mBoldFlag = boldFlag
        mMessageSize = textSize
        return this
    }

    override fun setMessageLeft(leftFlag: Boolean): GEOUIMessageDialog {
        mLeftFlag = leftFlag
        return this
    }

    override fun setSubMessageLeft(subLeftFlag: Boolean): GEOUIMessageDialog {
        mSubLeftFlag = subLeftFlag
        return this
    }

    override fun setSubMessageBold(boldFlag: Boolean, textSize: Int): GEOUIMessageDialog {
        mSubBoldFlag = boldFlag
        mSubMessageSize = textSize
        return this
    }

    override fun setPrimaryButtonTextColor(color: String): GEOUIMessageDialog {
        mPrimaryColor = color
        return this
    }

    override fun setPrimaryButtonTextBold(boldFlag: Boolean): GEOUIMessageDialog {
        mPrimaryBoldFlag = boldFlag
        return this
    }

    override fun setSecondaryButtonTextColor(color: String): GEOUIMessageDialog {
        mSecondaryColor = color
        return this
    }

    override fun setSecondaryButtonBold(boldFlag: Boolean): GEOUIMessageDialog {
        mSecondaryBoldFlag = boldFlag
        return this
    }

    fun setDialogCancelable(outCancel: Boolean): GEOUIMessageDialog {
        setOutCancel(outCancel)
        return this
    }

    override fun updateMessage(message: String?) {
        requireView().findViewById<TextView>(R.id.tv_message).let {
            if (message.isNullOrEmpty()) {
                it.visibility = View.GONE
            } else {
                if (it != null) it.text = message
                if (mLeftFlag) {
                    it.gravity = Gravity.START
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requireView().findViewById<TextView>(R.id.tv_title_device).let {
            if (title.isNullOrEmpty()) {
                it.visibility = View.GONE
            } else {
                it.text = title
                if (mTitleBoldFlag) {
                    it.setTypeface(null, Typeface.BOLD)
                }
            }
        }

        requireView().findViewById<TextView>(R.id.tv_sub_message).let {
            if (subMessage.isNullOrEmpty()) {
                it.visibility = View.GONE
            } else {
                it.text = subMessage
                if (mSubLeftFlag) {
                    it.gravity = Gravity.START
                }

                if (mSubBoldFlag) {
                    it.setTypeface(null, Typeface.BOLD)
                }
                if (mSubMessageSize > 0) {
                    it.setTextSize(TypedValue.COMPLEX_UNIT_SP, mSubMessageSize.toFloat())
                }
            }
        }

        requireView().findViewById<TextView>(R.id.tv_message).let {
            if (message.isNullOrEmpty()) {
                it.visibility = View.GONE
            } else {
                it.text = message
                if (mLeftFlag) {
                    it.gravity = Gravity.START
                }

                if (mBoldFlag) {
                    it.setTypeface(null, Typeface.BOLD)
                }

                if (mTopPadding > 0) {
                    it.setPadding(
                        it.paddingLeft,
                        mTopPadding,
                        it.paddingRight,
                        it.paddingBottom
                    )
                }

                if (mTopMargin > 0) {
                    val layoutP = it.layoutParams as ConstraintLayout.LayoutParams
                    layoutP.topMargin = mTopMargin
                    it.layoutParams = layoutP
                }

                if (mMessageSize > 0) {
                    it.setTextSize(TypedValue.COMPLEX_UNIT_SP, mMessageSize.toFloat())
                }
            }
        }

        requireView().findViewById<ImageView>(R.id.iv_connect_icon).let {
            if (icon == null) {
                it.visibility = View.GONE
            } else {
                it.setImageDrawable(icon)
            }
        }

        view.findViewById<Button>(R.id.btn_secondary_action).let {
            if (secondaryButtonName.isNullOrEmpty()) {
                it.visibility = View.GONE
            } else {
                it.text = secondaryButtonName
                it.setOnClickListener {
                    secondaryAction?.invoke(this)
                    dismiss()
                }
                if (mSecondaryBoldFlag) {
                    it.setTypeface(null, Typeface.BOLD)
                }

                mSecondaryColor?.let { key ->
                    it.setTextColor(Color.parseColor(mSecondaryColor))
                }
            }
        }

        view.findViewById<Button>(R.id.btn_primary_action).let {
            if (primaryButtonName.isNullOrEmpty()) {
                it.visibility = View.GONE
            } else {
                it.text = primaryButtonName
                it.setOnClickListener {
                    primaryAction?.invoke(this)
                    dismiss()
                }

                if (mPrimaryBoldFlag) {
                    it.setTypeface(null, Typeface.BOLD)
                }

                mPrimaryColor?.let {key ->
                    it.setTextColor(Color.parseColor(mPrimaryColor))
                }
            }
        }
    }
}

interface MessageDialogBuilder {
    fun setTitle(title: String?): GEOUIMessageDialog
    fun setTitleBold(boldFlag: Boolean): GEOUIMessageDialog
    fun setIcon(icon: Drawable?): GEOUIMessageDialog
    fun setMessage(message: String?): GEOUIMessageDialog
    fun setSubMessage(message: String?): GEOUIMessageDialog
    fun setPrimaryButton(
        actionName: String,
        action: ((GEOUIMessageDialog) -> Unit)?
    ): GEOUIMessageDialog

    fun setSecondaryButton(
        actionName: String,
        action: ((GEOUIMessageDialog) -> Unit)?
    ): GEOUIMessageDialog

    fun setMessageTopPadding(topPadding: Int): GEOUIMessageDialog
    fun setMessageTopMargin(topMargin: Int): GEOUIMessageDialog
    fun setMessageBold(boldFlag: Boolean, textSize: Int): GEOUIMessageDialog
    fun setMessageLeft(leftFlag: Boolean): GEOUIMessageDialog
    fun setSubMessageLeft(leftFlag: Boolean): GEOUIMessageDialog
    fun setSubMessageBold(boldFlag: Boolean, textSize: Int): GEOUIMessageDialog
    fun setPrimaryButtonTextBold(boldFlag: Boolean): GEOUIMessageDialog
    fun setPrimaryButtonTextColor(color: String): GEOUIMessageDialog
    fun setSecondaryButtonTextColor(color: String): GEOUIMessageDialog
    fun setSecondaryButtonBold(boldFlag: Boolean): GEOUIMessageDialog
    fun updateMessage(message: String?)
}