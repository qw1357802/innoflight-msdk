package com.geoai.uicore.custom.tab

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.geoai.basiclib.ext.getColor
import com.geoai.basiclib.ext.textColor
import com.geoai.uicore.R

class TabItemMultButtonInput(context: Context, attr: AttributeSet?) :
    LinearLayout(context, attr) {

    private val mView = inflate(context, R.layout.tab_item_mult_button_input, this)

    private var mTitle: String = ""

    private var mValueCount: Int = 0

    private var mListener: OnItemSelectListener? = null

    init {
        val obtainStyledAttributes = context.obtainStyledAttributes(attr, R.styleable.TabItemMultButtonInput)
        mValueCount = obtainStyledAttributes.getInt(R.styleable.TabItemMultButtonInput_view_mult_button_num, 0)

        mView.findViewById<TextView>(R.id.tv_tab_item_input_title).text = obtainStyledAttributes.getString(R.styleable.TabItemMultButtonInput_view_mult_button_title).toString()

        if (obtainStyledAttributes.getBoolean(R.styleable.TabItemMultButtonInput_view_mult_need_border, false)) {
            mView.findViewById<TextView>(R.id.tv_tab_item_input_1).setBackgroundResource(R.drawable.bg_textview_border)
            mView.findViewById<TextView>(R.id.tv_tab_item_input_2).setBackgroundResource(R.drawable.bg_textview_border)
            mView.findViewById<TextView>(R.id.tv_tab_item_input_3).setBackgroundResource(R.drawable.bg_textview_border)
        }

        when (mValueCount) {
            1 -> {
                mView.findViewById<TextView>(R.id.tv_tab_item_input_1).visibility = View.VISIBLE
                mView.findViewById<TextView>(R.id.tv_tab_item_input_1).text = obtainStyledAttributes.getString(R.styleable.TabItemMultButtonInput_view_mult_button1)
            }
            2 -> {
                mView.findViewById<TextView>(R.id.tv_tab_item_input_1).visibility = View.VISIBLE
                mView.findViewById<TextView>(R.id.tv_tab_item_input_1).text = obtainStyledAttributes.getString(R.styleable.TabItemMultButtonInput_view_mult_button1)
                mView.findViewById<TextView>(R.id.tv_tab_item_input_2).visibility = View.VISIBLE
                mView.findViewById<TextView>(R.id.tv_tab_item_input_2).text = obtainStyledAttributes.getString(R.styleable.TabItemMultButtonInput_view_mult_button2)
            }
            3 -> {
                mView.findViewById<TextView>(R.id.tv_tab_item_input_1).visibility = View.VISIBLE
                mView.findViewById<TextView>(R.id.tv_tab_item_input_1).text = obtainStyledAttributes.getString(R.styleable.TabItemMultButtonInput_view_mult_button1)
                mView.findViewById<TextView>(R.id.tv_tab_item_input_2).visibility = View.VISIBLE
                mView.findViewById<TextView>(R.id.tv_tab_item_input_2).text = obtainStyledAttributes.getString(R.styleable.TabItemMultButtonInput_view_mult_button2)
                mView.findViewById<TextView>(R.id.tv_tab_item_input_3).visibility = View.VISIBLE
                mView.findViewById<TextView>(R.id.tv_tab_item_input_3).text = obtainStyledAttributes.getString(R.styleable.TabItemMultButtonInput_view_mult_button3)
            }
            else -> {}
        }
        obtainStyledAttributes.recycle()

        mView.findViewById<TextView>(R.id.tv_tab_item_input_1).setOnClickListener {
            mListener?.onSelect(0)
        }
        mView.findViewById<TextView>(R.id.tv_tab_item_input_2).setOnClickListener {
            mListener?.onSelect(1)
        }
        mView.findViewById<TextView>(R.id.tv_tab_item_input_3).setOnClickListener {
            mListener?.onSelect(2)
        }
    }

    fun setCurrentSelect(index: Int) {
        if (mValueCount == 0) return
        mView.findViewById<TextView>(R.id.tv_tab_item_input_1).textColor = Color.WHITE
        mView.findViewById<TextView>(R.id.tv_tab_item_input_2).textColor = Color.WHITE
        mView.findViewById<TextView>(R.id.tv_tab_item_input_3).textColor = Color.WHITE
        when (index) {
            1 -> {
                mView.findViewById<TextView>(R.id.tv_tab_item_input_1).textColor = getColor(R.color.btn_confirm_dark)
            }
            2 -> {
                mView.findViewById<TextView>(R.id.tv_tab_item_input_2).textColor = getColor(R.color.btn_confirm_dark)
            }
            3 -> {
                mView.findViewById<TextView>(R.id.tv_tab_item_input_3).textColor = getColor(R.color.btn_confirm_dark)
            }
            else -> {}
        }
    }

    companion object {
        @JvmStatic
        fun setParam(view: TabItemMultButtonInput, select: Int) {
            view.setCurrentSelect(select)
        }
    }

    fun setSelectListener(listener: OnItemSelectListener) {
        this.mListener = listener
    }

    interface OnItemSelectListener {
        fun onSelect(int: Int)
    }
}