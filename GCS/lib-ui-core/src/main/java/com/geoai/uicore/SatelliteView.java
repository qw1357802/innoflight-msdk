package com.geoai.uicore;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

public class SatelliteView extends FrameLayout {
    public SatelliteView(Context context) {
        super(context);
        init(context);
    }

    public SatelliteView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public SatelliteView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private TextView mTvSatelliteCount;

    private void init(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.view_satellite, this);
        mTvSatelliteCount = view.findViewById(R.id.tv_satellite_count);
    }

    public void setSatelliteCount(String posType, int count) {
        mTvSatelliteCount.setText(String.format("%s%d", posType, count));
    }

    public void disconnect() {
        setSatelliteCount("N", 0);
    }
}
