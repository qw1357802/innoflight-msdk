package com.geoai.uicore.custom.dialog

import android.content.DialogInterface
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.geoai.basiclib.ext.getDimension
import com.geoai.uicore.R
import kotlin.math.roundToInt

open class GEOUIBaseCenterDialog : DialogFragment() {
    private var mDimAmount = 0.5f //背景昏暗度
    private var mOutCancel = true //点击外部取消
    private var mOnDismissListener: DismissListener? = null

    open fun getLayoutId(): Int = -1
    open fun initView() {}
    open fun initListener() {}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.BaseDialogFragment)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(getLayoutId(), container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    override fun onStart() {
        // 不显示状态栏
        dialog?.window?.setFlags(
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
        )
        super.onStart()
        initParams()
    }

    private fun initParams() {
        val window = dialog?.window
        if (window != null) {
            setSystemUiFlag(window)
            window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE)
            val params = window.attributes
            params.dimAmount = mDimAmount

            //设置dialog显示位置
            params.gravity = Gravity.CENTER
            params.width = view?.getDimension(R.dimen.base_center_dialog_width)!!.roundToInt()
            params.height = WindowManager.LayoutParams.WRAP_CONTENT
            window.attributes = params
        }
        isCancelable = mOutCancel
    }

    fun show(manager: FragmentManager): GEOUIBaseCenterDialog {
        super.show(manager, System.currentTimeMillis().toString())
        return this
    }

    /**
     * 设置背景昏暗度
     *
     * @param dimAmount
     * @return
     */
    fun setDimAmout(dimAmount: Float): GEOUIBaseCenterDialog {
        mDimAmount = dimAmount
        return this
    }

    /**
     * 设置是否点击外部取消
     *
     * @param outCancel
     * @return
     */
    fun setOutCancel(outCancel: Boolean): GEOUIBaseCenterDialog {
        mOutCancel = outCancel
        return this
    }


    override fun onResume() {
        super.onResume()
        initListener()
    }

    /** 配合flag隐藏导航栏和状态栏 */
    private fun setSystemUiFlag(window: Window) {
        window.decorView.systemUiVisibility = (
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                        View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
                        View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or
                        View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or
                        View.SYSTEM_UI_FLAG_FULLSCREEN or
                        View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY)

    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        mOnDismissListener?.dismiss()
    }


    fun setOnDismissListener(onDismissListener: DismissListener?) {
        mOnDismissListener = onDismissListener
    }
}