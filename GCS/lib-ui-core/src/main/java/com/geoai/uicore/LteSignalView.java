package com.geoai.uicore;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.geoai.uicore.model.SignalLevel;

public class LteSignalView extends FrameLayout {
    public LteSignalView(@NonNull Context context) {
        super(context);
        init(context);
    }

    public LteSignalView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public LteSignalView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private SignalView droneSignal;
    private SignalView playerSignal;

    private void init(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.view_lte_signal, this);
        droneSignal = view.findViewById(R.id.drone_signal_uplink);
        playerSignal = view.findViewById(R.id.video_signal_downlink);
    }

    public void setVideoSignal(int level) {
        if (level == 0) {
            droneSignal.setWeakIndex(0);
        } else if (level < SignalLevel.LEVEL_1) {
            droneSignal.setWeakIndex(1);
        } else if (level < SignalLevel.LEVEL_2) {
            droneSignal.setWeakIndex(2);
        } else if (level < SignalLevel.LEVEL_3) {
            droneSignal.setNormalIndex(3);
        } else if (level < SignalLevel.LEVEL_4) {
            droneSignal.setNormalIndex(4);
        } else if (level <= SignalLevel.LEVEL_5) {
            droneSignal.setNormalIndex(5);
        }
        droneSignal.refresh();
    }

    public void setPlayerSignal(int level) {
        if (level == 0) {
            playerSignal.setWeakIndex(0);
        } else if (level < SignalLevel.LEVEL_1) {
            playerSignal.setWeakIndex(1);
        } else if (level < SignalLevel.LEVEL_2) {
            playerSignal.setWeakIndex(2);
        } else if (level < SignalLevel.LEVEL_3) {
            playerSignal.setNormalIndex(3);
        } else if (level < SignalLevel.LEVEL_4) {
            playerSignal.setNormalIndex(4);
        } else if (level <= SignalLevel.LEVEL_5) {
            playerSignal.setNormalIndex(5);
        }
        playerSignal.refresh();
    }

    public void disconnect() {
        droneSignal.setWeakIndex(0);
        droneSignal.refresh();
    }
}