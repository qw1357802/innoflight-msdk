package com.geoai.uicore;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class BatteryView extends FrameLayout {

    public BatteryView(@NonNull Context context) {
        super(context);
        init(context);
    }

    public BatteryView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public BatteryView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private TextView mTvBatteryInfo;
    private ImageView mImgBattery;

    private static final int BATTERY_LOW_PERCENT = 30;

    private void init(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.view_battery, this);
        mTvBatteryInfo = view.findViewById(R.id.tv_battery);
        mImgBattery = view.findViewById(R.id.img_battery);
    }

    public void updateBatteryInfo(int batteryPercent) {
        mImgBattery.setImageDrawable(getBatteryResource(getContext(), batteryPercent));
        mTvBatteryInfo.setText(String.format("%s %%", batteryPercent));
        if (batteryPercent < BATTERY_LOW_PERCENT) {
            mTvBatteryInfo.setTextColor(Color.RED);
        } else {
            mTvBatteryInfo.setTextColor(Color.GREEN);
        }
    }

    private Drawable getBatteryResource(Context context, int batteryPercent) {
        if (batteryPercent < BATTERY_LOW_PERCENT) {
            return context.getDrawable(R.drawable.uxsdk_ic_topbar_battery_single_warning);
        } else {
            return context.getDrawable(R.drawable.uxsdk_ic_topbar_battery_single_nor);
        }
    }
}
