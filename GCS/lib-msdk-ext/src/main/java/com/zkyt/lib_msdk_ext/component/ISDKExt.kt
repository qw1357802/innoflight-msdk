package com.zkyt.lib_msdk_ext.component

import com.geoai.mavlink.geoainet.base.mavlinkcore.interfaces.CompletionCallback
import io.reactivex.Observable

/**
 * Created by chenyu on 2024/1/10
 * Description: SDK各个Component Manager的扩展。
 * 一般为RxJava扩展。一般使用[CompletionCallback]里这些通用回调的，比较适合扩展为RxJava。
 */
interface ISDKExt<T>: DestroyableExt {

    /**
     * 初始化
     *
     * @param originManager Component原始的Manager
     */
    fun init(originManager: T)

    /**
     * 获取最原始的Manager。
     *
     * 当Component的某个接口，不方便扩展为RxJava时，可以通过此方法获取原始的Manager，然后调用原始的接口。
     *
     * 如：[com.geoai.mavlink.geoainet.mission.interfaces.IBaseMissionManager.uploadMission]中的回调，不为通用回调，
     * 则不方便扩展为RxJava
     *
     * @return
     */
    fun getOriginManager(): T?
}