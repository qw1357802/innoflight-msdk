package com.zkyt.lib_msdk_ext.component.camera

import com.geoai.mavlink.geoainet.payload.enums.CameraModuleType
import com.geoai.mavlink.geoainet.payload.interfaces.IBaseCameraListener
import com.geoai.mavlink.geoainet.payload.interfaces.INV2CameraListener
import com.geoai.mavlink.geoainet.payload.interfaces.INV3CameraListener

/**
 * Created by chenyu on 2024/1/19
 * Description:
 */
class RealCameraExtProvider {
    private var realCameraExt: IRealCameraExt? = null

    fun init(cameraType: CameraModuleType, originManager: IBaseCameraListener) {
        if(cameraType == CameraModuleType.NV3) {
            val tmp = NV3CameraExt()
            tmp.init(originManager as INV3CameraListener)
            realCameraExt = tmp
        } else if (cameraType == CameraModuleType.NV2) {
            val tmp = NV2CameraExt()
            tmp.init(originManager as INV2CameraListener)
            realCameraExt = tmp
        }
    }

    fun destroy() {
        realCameraExt?.destroy()
        realCameraExt = null
    }

    fun getRealCameraExt(): IRealCameraExt? {
        return realCameraExt
    }


}