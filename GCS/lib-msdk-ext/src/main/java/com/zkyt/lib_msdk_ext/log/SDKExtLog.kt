package com.zkyt.lib_msdk_ext.log

/**
 * Created by chenyu on 2024/1/10
 * Description:
 */
object SDKExtLog: BaseModuleLog() {

    override fun getMainTag(): String {
        return "MSDK-Ext"
    }
}