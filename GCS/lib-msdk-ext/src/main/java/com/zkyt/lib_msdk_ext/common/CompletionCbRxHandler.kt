package com.zkyt.lib_msdk_ext.common

import com.geoai.mavlink.geoainet.base.mavlinkcore.engine.GEOAIError
import com.geoai.mavlink.geoainet.base.mavlinkcore.interfaces.CompletionCallback
import com.zkyt.lib_msdk_ext.log.SDKExtLog
import io.reactivex.CompletableEmitter

/**
 * Created by chenyu on 2024/1/30
 * Description:
 */
class CompletionCbRxHandler(private val emitter: CompletableEmitter): CompletionCallback.ICompletionCallback {
    private val entryStackTrace = Throwable().stackTraceToString()

    override fun onResult(geoaiError: GEOAIError?) {
        if (geoaiError == null) {
            emitter.onComplete()
        } else {
            SDKExtLog.e("CompletionCbRxHandler", "onResult error, entry stack trace: $entryStackTrace")
            emitter.onError(SDKExceptionWrapper(geoaiError))
        }
    }
}