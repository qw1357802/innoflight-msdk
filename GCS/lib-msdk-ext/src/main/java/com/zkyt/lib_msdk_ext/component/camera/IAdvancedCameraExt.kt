package com.zkyt.lib_msdk_ext.component.camera

import com.zkyt.lib_msdk_ext.component.DestroyableExt

/**
 * Created by chenyu on 2024/1/19
 * Description: 相机高级功能的扩展。
 *
 * 不继承基础功能扩展[BaseCameraExt]，一是为了使用方便，二是明确这些类的定位只是扩展类，没有明显的继承关系。
 */
interface IAdvancedCameraExt: DestroyableExt {

}