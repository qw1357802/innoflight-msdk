package com.zkyt.lib_msdk_ext.common

import com.geoai.mavlink.geoainet.base.mavlinkcore.engine.GEOAIError
import com.geoai.mavlink.geoainet.base.mavlinkcore.interfaces.CompletionCallback
import com.zkyt.lib_msdk_ext.log.SDKExtLog
import io.reactivex.SingleEmitter

/**
 * Created by chenyu on 2024/1/10
 * Description:
 */
class CallbackWithRxHandler<T : Any>(private val emitter: SingleEmitter<T>): CompletionCallback.ICompletionCallbackWith<T> {
    private val entryStackTrace = Throwable().stackTraceToString()

    override fun onFailure(geoaiError: GEOAIError) {
        SDKExtLog.e("CallbackWithRxHandler", "onResult error, entry stack trace: $entryStackTrace")
        emitter.onError(SDKExceptionWrapper(geoaiError))
    }

    override fun onResult(t: T) {
        emitter.onSuccess(t)
    }

}