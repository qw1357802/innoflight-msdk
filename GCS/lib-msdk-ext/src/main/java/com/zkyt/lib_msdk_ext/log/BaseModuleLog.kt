package com.zkyt.lib_msdk_ext.log

import android.util.Log
import com.geoai.basiclib.utils.log.MyLogUtils

/**
 * Created by chenyu on 2023/12/8
 * Description: 各模块的Log工具可以继承此类
 */
abstract class BaseModuleLog: IGALog {

    override fun d(subtag: String, msg: String, saveToFile: Boolean) {
        Log.d("Ronny", msg)
    }

    override fun i(subtag: String, msg: String, saveToFile: Boolean) {
        Log.i("Ronny", msg)
    }

    override fun e(subtag: String, msg: String, saveToFile: Boolean) {
        Log.e("Ronny", msg)
    }

    /**
     * 使用默认的tag打印d级别log
     *
     * @param msg
     * @param saveToFile
     */
    fun d(msg: String, saveToFile: Boolean = true) {
//        MyLogUtils.d(msg)
    }

    /**
     * 使用默认的tag打印i级别log
     *
     * @param msg
     * @param saveToFile
     */
    fun i(msg: String, saveToFile: Boolean = true) {
//        MyLogUtils.i(msg)
    }

    /**
     * 使用默认的tag打印e级别log
     *
     * @param msg
     * @param saveToFile
     */
    fun e(msg: String, saveToFile: Boolean = true) {
//        MyLogUtils.e(msg)
    }

    protected abstract fun getMainTag(): String


}