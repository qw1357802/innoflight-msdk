package com.zkyt.lib_msdk_ext

import android.content.Context
import com.geoai.mavlink.geoainet.base.BaseManager
import com.geoai.mavlink.geoainet.base.mavlinkcore.controller.ProductManager
import com.geoai.mavlink.geoainet.base.mavlinkcore.controller.StarLinkClientManager
import com.geoai.mavlink.geoainet.base.mavlinkcore.controller.interfaces.IProductsConnectionListener
import com.zkyt.lib_msdk_ext.log.SDKExtLog
import java.io.File

/**
 * Created by chenyu on 2024/1/9
 * Description:
 */
object MSDKManager {

    fun init(context: Context) {
        // MSDK初始化
        StarLinkClientManager.getInstance().register(context,
            context.getExternalFilesDir(null)?.absolutePath + File.separator + "/msdk")
        ProductManagerExt.initEarlyConnectModule()

        StarLinkClientManager.getInstance().productConnectionListener = object : IProductsConnectionListener {
            override fun onProductConnected(droneIndex: Int, productManager: ProductManager) {
                ProductManagerExt.handleProductConnected(droneIndex, productManager)
            }

            override fun onProductDisConnected(p0: Int) {
                ProductManagerExt.handleProductDisConnected()
            }

            override fun onComponentConnected(sysID: Int, componentType: Int, p2: BaseManager?) {
                SDKExtLog.i(TAG, "onComponentConnected, sysID: $sysID, componentType: $componentType")
                ProductManagerExt.handleComponentConnected(componentType)
            }

            override fun onComponentDisconnected(sysID: Int, componentType: Int) {
                SDKExtLog.i(TAG, "onComponentDisconnected, sysID: $sysID, componentType: $componentType")
                ProductManagerExt.handleComponentDisconnected(componentType)
            }
        }

        StarLinkClientManager.getInstance().openConnection()

        SDKExtLog.i(TAG, "init finish")
    }

    fun destroy() {
        StarLinkClientManager.getInstance().closeConnection()
        SDKExtLog.i(TAG, "destroy")
    }

    const val TAG = "MSDKManager"

}