package com.zkyt.lib_msdk_ext.component

import com.geoai.mavlink.geoainet.crest.info.LteStatus
import com.geoai.mavlink.geoainet.crest.interfaces.ICrestManager
import com.geoai.mavlink.geoainet.system.interfaces.IBaseSystemManager
import com.zkyt.lib_msdk_ext.common.CallbackWithRxHandler
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.subjects.BehaviorSubject
import java.util.Optional

/**
 * Created by chenyu on 2024/3/1
 * Description:
 */
object CrestManagerExt: AbsSDKExt<ICrestManager>() {
    private var orgManager: ICrestManager? = null
    private val orgCellularSimStatusInfoSubject =
        BehaviorSubject.createDefault(Optional.empty<LteStatus>())

    override fun init(originManager: ICrestManager) {
        this.orgManager = originManager
        originManager.setLteStateListener {
            orgCellularSimStatusInfoSubject.onNext(Optional.of(it))
        }
    }

    override fun destroy() {
        super.destroy()
        orgCellularSimStatusInfoSubject.onNext(Optional.empty())
        orgManager = null
    }

    override fun getOriginManager(): ICrestManager? {
        return orgManager
    }

    fun getCrestLteStatusObservable(): Observable<Optional<LteStatus>> {
        return orgCellularSimStatusInfoSubject.hide()
    }

    fun getSnInfo(): Single<String> {
        return Single.create { emitter ->
            orgManager?.getSn(CallbackWithRxHandler(emitter))
        }
    }
}