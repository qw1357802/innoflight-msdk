package com.zkyt.lib_msdk_ext.component

import androidx.annotation.CallSuper
import com.zkyt.lib_msdk_ext.common.AutoGetProcessor

/**
 * Created by chenyu on 2024/2/3
 * Description:
 */
abstract class AbsSDKExt<T>: ISDKExt<T> {
    protected val autoGetProcessor = AutoGetProcessor()

    @CallSuper
    override fun destroy() {
        autoGetProcessor.clearCache()
    }
}