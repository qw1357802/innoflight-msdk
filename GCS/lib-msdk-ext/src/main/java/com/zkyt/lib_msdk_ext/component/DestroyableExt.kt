package com.zkyt.lib_msdk_ext.component

/**
 * Created by chenyu on 2024/1/19
 * Description:
 */
interface DestroyableExt {
    /**
     * 销毁
     *
     */
    fun destroy()
}