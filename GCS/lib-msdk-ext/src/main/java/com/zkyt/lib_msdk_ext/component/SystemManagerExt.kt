package com.zkyt.lib_msdk_ext.component

import com.geoai.mavlink.geoainet.system.interfaces.IBaseSystemManager

/**
 * Created by chenyu on 2024/3/1
 * Description:
 */
object SystemManagerExt: AbsSDKExt<IBaseSystemManager>() {
    private var orgManager: IBaseSystemManager? = null

    override fun init(originManager: IBaseSystemManager) {
        this.orgManager = originManager
    }

    override fun destroy() {
        super.destroy()
        orgManager = null
    }

    override fun getOriginManager(): IBaseSystemManager? {
        return orgManager
    }
}