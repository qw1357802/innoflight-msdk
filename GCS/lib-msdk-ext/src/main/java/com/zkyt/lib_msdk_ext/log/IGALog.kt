package com.zkyt.lib_msdk_ext.log

/**
 * Created by chenyu on 2023/12/8
 * Description:
 */
interface IGALog {
    /**
     * 打印d级别log，并且保存到文件中
     *
     * @param tag
     * @param msg
     * @param saveToFile 默认true，false则不保存
     */
    fun d(tag: String, msg: String, saveToFile: Boolean = true)

    /**
     * 打印i级别log，并且保存到文件中
     *
     * @param tag
     * @param msg
     * @param saveToFile 默认true，false则不保存
     */
    fun i(tag: String, msg: String, saveToFile: Boolean = true)

    /**
     * 打印e级别log，并且保存到文件中
     *
     * @param tag
     * @param msg
     * @param saveToFile 默认true，false则不保存
     */
    fun e(tag: String, msg: String, saveToFile: Boolean = true)
}