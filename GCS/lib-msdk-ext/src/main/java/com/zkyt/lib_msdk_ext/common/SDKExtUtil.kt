package com.zkyt.lib_msdk_ext.common

import com.geoai.mavlink.geoainet.base.mavlinkcore.engine.GEOAIError
import io.reactivex.CompletableEmitter

/**
 * Created by chenyu on 2024/1/10
 * Description:
 */
object SDKExtUtil {

    /**
     *
     * @param emitter
     * @param geoaiError
     * @deprecated 请使用[CompletionCbRxHandler]
     */
    @Deprecated("请使用[CompletionCbRxHandler]")
    fun handleError(emitter: CompletableEmitter, geoaiError: GEOAIError?) {
        if (geoaiError == null) {
            emitter.onComplete()
        } else {
            emitter.onError(SDKExceptionWrapper(geoaiError))
        }
    }
}