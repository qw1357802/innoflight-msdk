package com.zkyt.lib_msdk_ext.common

import com.geoai.mavlink.geoainet.base.mavlinkcore.engine.GEOAIError

/**
 * Created by chenyu on 2024/1/9
 * Description:
 */
class SDKExceptionWrapper(private val geoaiError: GEOAIError): Throwable(geoaiError.description) {
}