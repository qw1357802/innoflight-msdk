package com.zkyt.lib_msdk_ext.component

import io.reactivex.Observable

/**
 * Created by chenyu on 2024/2/2
 * Description: 可拆卸的Component，生命周期需要自己管理，不跟着ProductManager的生命周期走的Component。
 */
interface IRemovableComp {

    fun isConnected(): Boolean

    fun getConnectedObservable(): Observable<Boolean>
}