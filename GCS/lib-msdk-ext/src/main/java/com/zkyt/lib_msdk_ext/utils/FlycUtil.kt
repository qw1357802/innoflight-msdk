package com.zkyt.lib_msdk_ext.utils

import com.geoai.mavlink.geoainet.flycontroller.enums.HmsSeverityLevel

/**
 * Created by chenyu on 2024/2/20
 * Description:
 */
object FlycUtil {

    fun isCriticalAlert(alertLevel: HmsSeverityLevel): Boolean {
        return alertLevel == HmsSeverityLevel.MAV_SEVERITY_WARNING
                || alertLevel == HmsSeverityLevel.MAV_SEVERITY_CRITICAL
                || alertLevel == HmsSeverityLevel.MAV_SEVERITY_EMERGENCY
    }

    fun isTakeCareAlert(alertLevel: HmsSeverityLevel): Boolean {
        return alertLevel == HmsSeverityLevel.MAV_SEVERITY_ERROR
                || alertLevel == HmsSeverityLevel.MAV_SEVERITY_ALERT
    }
}