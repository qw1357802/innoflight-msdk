#----------------MSDK 混淆文件定义的--------------------------
-keepclassmembers class ** implements java.io.Serializable {
static final long serialVersionUID;
private static final java.io.ObjectStreamField[] serialPersistentFields;
!static !transient <fields>;
private void writeObject(java.io.ObjectOutputStream);
private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}
#parcelable
-keep class * implements android.os.Parcelable {
    public static final android.os.Parcelable$Creator *;
}
-keepnames class * implements android.os.Parcelable
-keepclassmembers class * implements android.os.Parcelable {
    <fields>;
    public void writeToParcel(android.os.Parcel, int);
}
#---------------------------------第三方包-------------------------------
#zerotier
-keep class com.zerotier.**{*;}
#tukaani
-keep class org.tukaani.xz.**{*;}
#jsch
-keep class com.jcraft.jsch.**{*;}
#mqtt
-keep class org.eclipse.paho.client.mqttv3.** { *; }
#---------------------------------反射相关的类和方法-----------------------
# aop注解
-adaptclassstrings
-keepattributes InnerClasses, EnclosingMethod, Signature, *Annotation*
-keepnames @org.aspectj.lang.annotation.Aspect class * {
    ajc* <methods>;
}
-keepclassmembers class ** {
    @com.geoai.mavlink.util.annotation.EALogAnnoClass <methods>;
    @com.geoai.mavlink.util.annotation.EALogAnnoMethod <methods>;
}

#---------------------------------RTK 实体类---------------------------------
-keep class com.geoai.mavlink.geoainet.rtk.entry.RtkServiceCode {*;}
-keep interface com.geoai.mavlink.geoainet.rtk.interfaces.IRtkModuleConnectCallback {*;}
-keep class com.geoai.mavlink.geoainet.rtk.module.cmcc.CMCCModule {
    public <methods>;
}
-keep class com.geoai.mavlink.geoainet.rtk.module.qx.QXModule {
    public <methods>;
}
-keep class com.geoai.mavlink.geoainet.rtk.module.sixevent.SixEventModule {
    public <methods>;
}
-keep class com.geoai.mavlink.geoainet.rtk.module.ntrip.NtripServerResolve {
    public <methods>;
}
#---------------------------------第三方包-------------------------------
-keep class com.qxwz.sdk.** {*;}
-keep class com.sixents.sdk.** {*;}
-keep class com.cmcc.sy.hap.** {*;}
#----------------MSDK 混淆文件定义的--------------------------


-keep class org.kapok.** { *; }

# 需要反射调用BehaviorSubject的方法
-keep class io.reactivex.subjects.BehaviorSubject { *; }

-keep class com.geoai.mavlink.geoainet.flycontroller.utils.EventReportBuilder$* { *; }
-keep class com.geoai.mavlink.geoainet.flycontroller.utils.EventReportBuilder { *; }

-keep class com.geoai.mavlink.geoainet.banfly.info.UnlockZoneInfo { *; }

-keep class com.geoai.mavlink.geoainet.media.info.RemoteFileInfo { *; }
-keep class com.geoai.mavlink.geoainet.media.info.RemoteFileInfo$* { *; }

-keep class com.geoai.mavlink.geoainet.system.info.ParameterEntryInfo { *; }
-keep class com.geoai.mavlink.geoainet.system.info.ParameterEntryInfo$* { *; }

-keep class com.geoai.mavlink.util.MeshHelper$* { *; }

-keep class * extends com.MAVLink.Messages.MAVLinkMessage { *; }
-keepclassmembers class * extends com.MAVLink.Messages.MAVLinkMessage { *; }

