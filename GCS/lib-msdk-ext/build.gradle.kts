plugins {
    id("com.android.library")
}

// 使用自定义插件
apply<DefaultGradlePlugin>()

android {
    namespace = "com.zkyt.lib_msdk_ext"
}

dependencies {

    implementation(project(":lib-base"))

    // 第二方库
//    implementation project(path: ':MSDK')
    // MSDK核心库
    api("com.geoai.mavlink.msdk-provided:mavsdk:v1.4.2-20240430.021746-2")
    // MSDK播放器
    api("com.geoai.mavlink.videoplayer-geoairtsp:mavsdk:v1.3.5")
    // MSDK APM框架
//    implementation("com.geoai.mavlink.apm:mavsdk:v1.1")
    // MSDK视觉校准库
//    api("com.geoai.mavlink.vision-calibration:mavsdk:v1.1")
}