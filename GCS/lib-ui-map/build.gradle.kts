plugins {
    id("com.android.library")
}

// 使用自定义插件
apply<UIModuleGradlePlugin>()

android {
    namespace = "com.geoai.uimap"
}

dependencies {
    aMap()
    osmMap()
    mapbox()
    api(project(":lib-base"))

    implementation(project(":lib-msdk-ext"))
}