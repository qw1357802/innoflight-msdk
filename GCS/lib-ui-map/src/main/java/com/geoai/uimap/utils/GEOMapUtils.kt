package com.geoai.uimap.utils

import android.content.res.Resources
import com.amap.api.maps.AMap
import com.amap.api.maps.TextureMapView
import com.amap.api.maps.model.LatLng
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.geoai.uimap.api.IGEOMap
import com.geoai.uimap.core.amap.GDMap
import com.geoai.uimap.core.mapbox.MapboxGeometryManager
import com.geoai.uimap.core.mapbox.MapboxImpl
import com.geoai.uimap.core.osmdroid.OsmMap
import com.geoai.uimap.model.GEOLatLng
import org.osmdroid.views.MapView

/**
 * 获取高德地图
 */
fun IGEOMap.getAMap(): AMap? {
    if (this is GDMap) return (this.getMapView() as TextureMapView).map
    return null
}

/**
 *获取MapboxMap
 */
fun IGEOMap.getMapboxMap(): MapboxMap? {
    if (this is MapboxImpl) return (this as MapboxImpl).getMapbox()
    return null
}

fun IGEOMap.getMapboxView(): com.mapbox.mapboxsdk.maps.MapView? {
    if (this is MapboxImpl) return (this as MapboxImpl).getMapView() as com.mapbox.mapboxsdk.maps.MapView
    return null
}


fun IGEOMap.getOsmMapView(): MapView? {
    if (this is OsmMap) return (this.getMapView() as MapView)
    return null
}


fun LatLng.toGEOLatLng(): GEOLatLng {
    val gcj02 = CoordinateUtils.gcj02ToWGS84(this.longitude, this.latitude)
    return GEOLatLng(gcj02[1], gcj02[0])
}

fun com.mapbox.mapboxsdk.geometry.LatLng.toGEOLatLng(): GEOLatLng {
    return GEOLatLng(this.latitude, this.longitude)
}

/**
 * 获取MapBox的要素管理器
 */
fun IGEOMap.getMapBoxGeometry(): MapboxGeometryManager? {
    if (this is MapboxImpl) {
        return this.getGeometryManager()
    }
    return null
}

fun dpToPx(dpValue: Float): Float {
    return (dpValue * Resources.getSystem().displayMetrics.density + 0.5f)
}