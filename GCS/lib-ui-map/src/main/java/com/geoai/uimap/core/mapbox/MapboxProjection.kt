package com.geoai.uimap.core.mapbox

import android.graphics.Point
import android.graphics.PointF
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.geoai.uimap.api.IGEOMap
import com.geoai.uimap.api.IGEOMapProjection
import com.geoai.uimap.geo.ILatLng
import com.geoai.uimap.model.GEOLatLng
import kotlin.math.roundToInt

class MapboxProjection(private val map: IGEOMap) : IGEOMapProjection {
    override fun toScreenPoint(geoPoint: ILatLng): Point {
        val pointF = getMapbox().projection.toScreenLocation(LatLng(geoPoint.latitude, geoPoint.longitude))
        return Point(pointF.x.roundToInt(), pointF.y.roundToInt())
    }

    override fun toGeoPoint(point: Point): ILatLng {
        val latLng =
                getMapbox().projection.fromScreenLocation(PointF(point.x.toFloat(), point.y.toFloat()))
        return GEOLatLng(latLng.latitude, latLng.longitude)
    }

    private fun getMapbox(): MapboxMap {
        return (map as MapboxImpl).getMapbox()!!
    }
}