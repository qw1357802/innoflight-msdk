package com.geoai.uimap.geo;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * 经纬度点
 */
public class LatLng implements ILatLng, Parcelable, Serializable, Cloneable {

    private double lat;
    private double lng;

    private LatLng(final Parcel in) {
        this.lat = in.readDouble();
        this.lng = in.readDouble();
    }

    public LatLng() {
        this(0, 0);
    }

    public LatLng(ILatLng latLng) {
        this(latLng.getLatitude(), latLng.getLongitude());
    }

    public LatLng(double lat, double lng) {
        setLatitude(lat);
        setLongitude(lng);
    }

    @Override
    public double getLatitude() {
        return lat;
    }

    @Override
    public void setLatitude(double latitude) {
        this.lat = latitude;
    }

    @Override
    public double getLongitude() {
        return lng;
    }

    @Override
    public void setLongitude(double longitude) {
        this.lng = longitude;
    }

    @Override
    public String toString() {
        return "{" + "lat=" + lat + ", lng=" + lng + '}';
    }

    @Override
    public LatLng clone() {
        try {
            return (LatLng) super.clone();
        } catch (CloneNotSupportedException ignore) {
            return new LatLng(lat, lng);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;

        if (o == null || getClass() != o.getClass())
            return false;

        LatLng latLng = (LatLng) o;

        if (Double.compare(latLng.getLatitude(), lat) != 0) return false;
        if (Double.compare(latLng.getLongitude(), lng) != 0) return false;

        return true;
    }

    @Override
    public final int hashCode() {
        long tmp1 = Double.doubleToLongBits(lat);
        int tmp2 = 31 + (int) (tmp1 ^ tmp1 >>> 32);
        tmp1 = Double.doubleToLongBits(lng);
        return tmp2 * 31 + (int) (tmp1 ^ tmp1 >>> 32);
    }

    // ===========================================================
    // Parcelable
    // ===========================================================

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(lat);
        dest.writeDouble(lng);
    }

    public static final Creator<LatLng> CREATOR = new Creator<LatLng>() {

        @Override
        public LatLng createFromParcel(Parcel source) {
            return new LatLng(source);
        }

        @Override
        public LatLng[] newArray(int size) {
            return new LatLng[size];
        }
    };
}
