package com.geoai.uimap.core.osmdroid

import com.geoai.uimap.api.IGEOMap
import com.geoai.uimap.api.GEOMapPolygon
import com.geoai.uimap.utils.getOsmMapView
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.overlay.Polygon

class OsmMapPolygon(private val map: IGEOMap) : GEOMapPolygon() {
    private var mPolygon: Polygon? = null
    override fun show(): GEOMapPolygon {
        if (mPointList.isNullOrEmpty()) return this
        val mapView = map.getOsmMapView() ?: return this
        val points = mutableListOf<GeoPoint>()
        mPointList?.forEach {
            points.add(GeoPoint(it.latitude, it.longitude))
        }
        mPolygon = Polygon(mapView)
        mPolygon?.points = points
        mPolygon?.outlinePaint?.color = mStrokeColor
        mPolygon?.outlinePaint?.strokeWidth = mStrokeWidth
        mPolygon?.fillPaint?.color = mFillColor
        mapView.overlayManager.add(mPolygon)
        mapView.invalidate()
        return this
    }

    override fun destroy() {
        map.getOsmMapView()?.let {
            it.overlayManager.remove(mPolygon)
            it.invalidate()
        }
    }
}