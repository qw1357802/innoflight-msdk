package com.geoai.uimap.core.amap

import com.amap.api.maps.model.Circle
import com.amap.api.maps.model.CircleOptions
import com.geoai.uimap.api.IGEOMap
import com.geoai.uimap.api.GEOMapCircle
import com.geoai.uimap.model.GEOLatLng
import com.geoai.uimap.utils.dpToPx
import com.geoai.uimap.utils.getAMap

class GDMapCircle(private val map: IGEOMap) : GEOMapCircle() {

    private var mCircle: Circle? = null

    override fun show(): GEOMapCircle {
        centerPoint?.let {
            val circleOptions = CircleOptions()
            circleOptions.center(IGEOMap.getGdLatlng(map, it))
            circleOptions.radius(mRadius)
            circleOptions.fillColor(mColor)
            circleOptions.strokeWidth(dpToPx(mStrokeWidth))
            circleOptions.strokeColor(mStrokeColor)
            mCircle = map.getAMap()?.addCircle(circleOptions)
        }
        return this
    }

    override fun setCircleCenter(point: GEOLatLng): GEOMapCircle {
        centerPoint = point
        mCircle?.center = IGEOMap.getGdLatlng(map, point)
        return this
    }

    override fun destroy() {
        mCircle?.remove()
    }


}