package com.geoai.uimap.core.geomap

import com.amap.api.maps.UiSettings
import com.geoai.uimap.api.IGEOMap
import com.geoai.uimap.api.IGEOMapSettings
import com.geoai.uimap.core.amap.GDMap
import com.geoai.uimap.core.mapbox.MapboxImpl
import com.geoai.uimap.utils.getAMap

class GEOMapSettingsImpl(private val map: IGEOMap) : IGEOMapSettings {
    private var gdMapSettings: UiSettings? = null
    private var mapBoxMapSetting: com.mapbox.mapboxsdk.maps.UiSettings? = null

    init {
        if (map is GDMap) {
            gdMapSettings = map.getAMap()?.uiSettings
        }
        if (map is MapboxImpl) {
            mapBoxMapSetting = map.getMapbox()?.uiSettings
        }

    }

    override fun setRotateGesturesEnabled(enable: Boolean): IGEOMapSettings {
        gdMapSettings?.isRotateGesturesEnabled = enable
        mapBoxMapSetting?.isRotateGesturesEnabled = enable
        return this
    }


    override fun setZoomControlsEnabled(enable: Boolean): IGEOMapSettings {
        gdMapSettings?.isZoomControlsEnabled = enable
        mapBoxMapSetting?.isZoomGesturesEnabled = enable
        return this
    }

    override fun setScaleControlsEnabled(enable: Boolean): IGEOMapSettings {
        gdMapSettings?.isScaleControlsEnabled = enable
        mapBoxMapSetting?.isScrollGesturesEnabled = enable
        return this
    }

    override fun setScrollGesturesEnabled(enable: Boolean): IGEOMapSettings {
        gdMapSettings?.isScrollGesturesEnabled = enable
        mapBoxMapSetting?.isScrollGesturesEnabled = enable
        return this
    }

    override fun setTiltGesturesEnabled(enable: Boolean): IGEOMapSettings {
        gdMapSettings?.isTiltGesturesEnabled = enable
        mapBoxMapSetting?.isTiltGesturesEnabled = enable
        return this
    }

}