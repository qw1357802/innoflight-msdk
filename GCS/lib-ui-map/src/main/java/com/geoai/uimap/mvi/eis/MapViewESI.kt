package com.geoai.uimap.mvi.eis

import com.geoai.basiclib.base.mvi.IUIEffect
import com.geoai.basiclib.base.mvi.IUiIntent
import com.geoai.basiclib.base.mvi.IUiState
import com.geoai.uimap.model.GEOLatLng

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-04-02 18:26.
 * @Description :
 */
sealed class MapViewEffect: IUIEffect {
    data class CenterAtTargetLocation(val location: GEOLatLng): MapViewEffect()

    data class UpdateAircraftMarker(val location: GEOLatLng, val rotate: Float): MapViewEffect()

    data class UpdateAircraftReturnMarker(val location: GEOLatLng): MapViewEffect()

    data class UpdateAircraftTrackPolyline(val points: MutableList<GEOLatLng>): MapViewEffect()
}

sealed class MapViewIntent: IUiIntent {
    object OnMapTouchEvent: MapViewIntent()
    data class OnMapClickEvent(val location: GEOLatLng): MapViewIntent()
}

sealed class MapViewState: IUiState {
    object MapBigView: MapViewState()
    object MapMiniView: MapViewState()
    object MapIconView: MapViewState()
}
