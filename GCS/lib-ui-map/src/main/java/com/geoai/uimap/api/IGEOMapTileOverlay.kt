package com.geoai.uimap.api

interface IGEOMapTileOverlay {
    fun show(): IGEOMapTileOverlay
    fun destroy()
}