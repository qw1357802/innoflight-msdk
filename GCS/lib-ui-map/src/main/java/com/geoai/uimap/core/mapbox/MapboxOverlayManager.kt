package com.geoai.uimap.core.mapbox

import com.geoai.uimap.api.IGEOMap
import com.geoai.uimap.api.IGEOMapOverlay
import com.geoai.uimap.api.IGEOMapOverlayManager

/**
 *
 * @Description:     类作用描述
 * @Author:         hjq
 * @CreateDate:     2021/2/23 10:20
 *
 */
class MapboxOverlayManager(private val map: IGEOMap) : IGEOMapOverlayManager {
    //图层集合
    private val overlayLinkMap = LinkedHashMap<String, IGEOMapOverlay>()
    override fun addOverlay(overlayName: String, overlay: IGEOMapOverlay) {
        overlayLinkMap[overlayName] = overlay
    }

    override fun removeOverlay(overlayName: String) {
        val overlay = getOverlay(overlayName)
        overlay?.destroy()
        overlayLinkMap.remove(overlayName)
    }

    override fun getOverlay(overlayName: String): IGEOMapOverlay? {
        return overlayLinkMap[overlayName]
    }

    override fun getAllOverlay(): LinkedHashMap<String, IGEOMapOverlay> {
        return overlayLinkMap
    }

    override fun onDestroy() {
        for (map in overlayLinkMap) {
            map.value.destroy()
        }
    }
}