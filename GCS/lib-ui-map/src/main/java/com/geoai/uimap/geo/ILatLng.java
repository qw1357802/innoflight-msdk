/*
 *
 *  * Copyright (c) 2015 XAIRCRAFT UAV Solutions.
 *  *
 *  * All rights reserved.
 *
 */

package com.geoai.uimap.geo;

/**
 * 经、纬位置
 */
public interface ILatLng {
    /**
     * @return 纬度
     */
    double getLatitude();

    /**
     * @param latitude 纬度
     */
    void setLatitude(double latitude);

    /**
     * @return 经度
     */
    double getLongitude();

    /**
     * @param longitude 经度
     */
    void setLongitude(double longitude);

}
