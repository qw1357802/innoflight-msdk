package com.geoai.uimap.core.amap

import android.graphics.BitmapFactory
import com.amap.api.maps.AMap
import com.amap.api.maps.CoordinateConverter
import com.amap.api.maps.TextureMapView
import com.amap.api.maps.model.BitmapDescriptor
import com.amap.api.maps.model.BitmapDescriptorFactory
import com.amap.api.maps.model.LatLng
import com.amap.api.maps.model.MarkerOptions
import com.geoai.uimap.api.IGEOMap
import com.geoai.uimap.api.IGEOMapOverlay
import com.geoai.uimap.model.GEOLatLng

abstract class GDMapOverlay(val map: IGEOMap) : IGEOMapOverlay {
    protected fun getAMap(): AMap {
        return (map.getMapView() as TextureMapView).map
    }

    protected fun gpsToAMapLatLng(lat: Double, long: Double): LatLng {
        val converter = CoordinateConverter(map.getContext())
        converter.from(CoordinateConverter.CoordType.GPS)
        converter.coord(LatLng(lat, long))
        return converter.convert()
    }

    /**
     * 创建一个Marker
     */
    fun getMarkerOptions(angle: Float, iconId: Int, position: LatLng): MarkerOptions {
        val markerOptions = MarkerOptions()
        markerOptions.rotateAngle(angle)
        //Marker居中
        markerOptions.anchor(0.5f, 0.5f)
        markerOptions.period(20000)
        markerOptions.icon(
                BitmapDescriptorFactory.fromBitmap(
                        BitmapFactory.decodeResource(
                                map.getContext().resources,
                                iconId
                        )
                )
        )
        markerOptions.position(position)
        return markerOptions
    }


    fun getMarkerConfig(angle: Float, icon: BitmapDescriptor, position: LatLng): MarkerOptions {
        val markerOptions = MarkerOptions()
        markerOptions.rotateAngle(angle)
        //Marker居中
        markerOptions.anchor(0.5f, 0.5f)
        markerOptions.period(20000)
        markerOptions.icon(icon)
        markerOptions.position(position)
        return markerOptions
    }


    override fun onMapClick(GEOLatLng: GEOLatLng): Boolean {
        return false
    }

}