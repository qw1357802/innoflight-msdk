/*
 *
 *  * Copyright (c) 2015 XAIRCRAFT UAV Solutions.
 *  *
 *  * All rights reserved.
 *
 */

package com.geoai.uimap.geo;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class Point implements IPoint, Parcelable, Serializable, Cloneable {

    public static final Creator<Point> CREATOR = new Creator<Point>() {
        public Point createFromParcel(Parcel in) {
            return new Point(in);
        }

        public Point[] newArray(int size) {
            return new Point[size];
        }
    };

    private double x;
    private double y;

    public Point() {
        this(0, 0);
    }

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Point(android.graphics.Point p) {
        this.x = p.x;
        this.y = p.y;
    }

    private Point(Parcel in) {
        x = in.readDouble();
        y = in.readDouble();
    }

    public static double length(double x, double y) {
        return Math.sqrt((x * x + y * y));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;

        if (o == null || getClass() != o.getClass())
            return false;

        Point coord = (Point) o;

        if (Double.compare(coord.x, x) != 0) return false;
        if (Double.compare(coord.y, y) != 0) return false;

        return true;
    }

    @Override
    public String toString() {
        return "{" + "x=" + x + ", y=" + y + '}';
    }

    @Override
    public double getX() {
        return x;
    }

    @Override
    public void setX(double x) {
        this.x = x;
    }

    @Override
    public double getY() {
        return y;
    }

    @Override
    public void setY(double y) {
        this.y = y;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeDouble(x);
        out.writeDouble(y);
    }

    @Override
    public Point clone() {
        try {
            return (Point) super.clone();
        } catch (CloneNotSupportedException ignore) {
            return new Point(x, y);
        }
    }

    @Override
    public int hashCode() {
        long tmp1 = Double.doubleToLongBits(x);
        int  tmp2 = 31 + (int) (tmp1 ^ tmp1 >>> 32);
        tmp1 = Double.doubleToLongBits(y);
        return tmp2 * 31 + (int) (tmp1 ^ tmp1 >>> 32);
    }

    public final double length() {
        return length(x, y);
    }

    public final void negate() {
        x = -x;
        y = -y;
    }

    public final void offset(double dx, double dy) {
        x += dx;
        y += dy;
    }

    public final void set(Point p) {
        this.x = p.x;
        this.y = p.y;
    }

    public final void set(double x, double y) {
        this.x = x;
        this.y = y;
    }
}