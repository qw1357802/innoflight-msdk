package com.geoai.uimap.core.amap

import com.geoai.uimap.api.IGEOMap
import com.geoai.uimap.api.IGEOMapOverlay
import com.geoai.uimap.api.IGEOMapOverlayManager

/**
 *
 * @Description:     高德地图图层管理类
 *
 */
class GDMapOverlayManager(private val map: IGEOMap) : IGEOMapOverlayManager {
    //图层集合
    private val overlayLinkMap = LinkedHashMap<String, IGEOMapOverlay>()

    override fun addOverlay(overlayName: String, overlay: IGEOMapOverlay) {
        overlayLinkMap[overlayName] = overlay
    }

    override fun removeOverlay(overlayName: String) {
        val overlay = getOverlay(overlayName)
        overlay?.destroy()
        overlayLinkMap.remove(overlayName)
    }

    override fun getOverlay(overlayName: String): IGEOMapOverlay? {
        return overlayLinkMap[overlayName]
    }

    override fun getAllOverlay(): LinkedHashMap<String, IGEOMapOverlay> {
        return overlayLinkMap
    }

    override fun onDestroy() {
        for (map in overlayLinkMap) {
            map.value.destroy()
        }
    }


}