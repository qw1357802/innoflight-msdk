package com.geoai.uimap.api

import android.view.MotionEvent
import android.view.View
import com.geoai.uimap.core.geomap.bean.GEOMapCameraPosition
import com.geoai.uimap.model.GEOLatLng

class GEOMapListener {


    interface OnCameraChangeListener {
        fun onCameraChange(var1: GEOMapCameraPosition?)
        fun onCameraChangeFinish(var1: GEOMapCameraPosition?)
    }

    interface OnMarkerClickListener {
        fun onMarkerClick(markerId: String): Boolean
    }

    interface OnMapClickListener {
        fun onMapClick(var1: GEOLatLng)
    }

    interface OnMapLongClickListener {
        fun onMapLongClick(var1: GEOLatLng)
    }

    interface OnMapLoadedListener {
        fun onMapLoaded()
    }

    interface OnCacheRemoveListener {
        fun onRemoveCacheFinish(success: Boolean)
    }

    interface OnMapTouchListener {
        fun onTouch(event: MotionEvent)
    }

    interface InfoWindowAdapter {
        fun getInfoWindow(markerId: String?): View?
        fun getInfoContents(markerId: String?): View?
    }


}