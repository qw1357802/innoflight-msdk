package com.geoai.uimap.utils

import android.content.Context
import android.graphics.Bitmap
import androidx.annotation.DrawableRes
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.toBitmap

object GEOMapCacheUtils {
    private val bitmapMap = mutableMapOf<String, Bitmap>()

    fun getBitmapCache(context: Context, @DrawableRes resId: Int): Bitmap? {
        var bitmap = bitmapMap[resId.toString()]
        if (bitmap == null) {
            bitmap = ResourcesCompat.getDrawable(context.resources, resId, null)?.toBitmap()
        }
        if (bitmap != null) bitmapMap[resId.toString()] = bitmap
        return bitmap
    }

    /**
     * 清除缓存
     */
    fun destroyCache() {
        Thread {
            kotlin.run {
                bitmapMap.forEach {
                    it.value.recycle()
                }
                bitmapMap.clear()
            }
        }.start()

    }

}