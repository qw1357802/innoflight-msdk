package com.geoai.uimap.api

import android.graphics.Point
import com.geoai.uimap.geo.ILatLng

interface IGEOMapProjection {
    fun toScreenPoint(geoPoint: ILatLng): Point
    fun toGeoPoint(point: Point): ILatLng
}