package com.geoai.uimap.core.geomap


class GEOMapCustomMapStyleOptions {
    var mEnable = false
    var styleData: ByteArray? = null
    var styleExtraData: ByteArray? = null

    fun setEnable(enable: Boolean): GEOMapCustomMapStyleOptions {
        mEnable = enable
        return this
    }

    fun setStyleData(var1: ByteArray?): GEOMapCustomMapStyleOptions {
        this.styleData = var1
        return this
    }

    fun setStyleExtraData(var1: ByteArray?): GEOMapCustomMapStyleOptions {
        this.styleExtraData = var1
        return this
    }


}
