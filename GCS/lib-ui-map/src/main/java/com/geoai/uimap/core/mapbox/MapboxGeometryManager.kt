package com.geoai.uimap.core.mapbox

import com.geoai.uimap.api.IGEOMap
import com.geoai.uimap.core.mapbox.markerview.NewMarkerViewManager
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.mapboxsdk.plugins.annotation.FillManager
import com.mapbox.mapboxsdk.plugins.annotation.LineManager
import com.mapbox.mapboxsdk.plugins.annotation.SymbolManager
import com.mapbox.mapboxsdk.style.layers.Property

class MapboxGeometryManager(private val map: IGEOMap, val mapView: MapView, val mapBox: MapboxMap) {

    private var style: Style? = null
    private var fillManager: FillManager? = null
    private var lineManager: LineManager? = null
    private var dottedLineManager: LineManager? = null
    private var symbolManager: SymbolManager? = null
    private var topSymbolManager: SymbolManager? = null
    private var newMarkerViewManager: NewMarkerViewManager? = null

    init {
        mapBox.getStyle {
            style = it
            initManager(it)
        }
    }

    private fun initManager(style: Style) {

        fillManager = FillManager(mapView, mapBox, style)

        dottedLineManager = LineManager(mapView, mapBox, style)

        dottedLineManager?.let {
            it.lineDasharray = arrayOf(1f, 2f)
            it.lineCap = Property.LINE_CAP_ROUND
            it.lineMiterLimit = 1f
            it.lineRoundLimit = 2f
        }

        lineManager = LineManager(mapView, mapBox, style)

        newMarkerViewManager = NewMarkerViewManager(mapView, mapBox)

        symbolManager = SymbolManager(mapView, mapBox, style)
//        symbolManager?.iconAllowOverlap = true
        symbolManager?.iconIgnorePlacement = true
        symbolManager?.iconRotationAlignment = Property.ICON_ROTATION_ALIGNMENT_AUTO


        topSymbolManager = SymbolManager(mapView, mapBox, style)
        topSymbolManager?.iconAllowOverlap = true
        topSymbolManager?.iconIgnorePlacement = true
        topSymbolManager?.iconRotationAlignment = Property.ICON_ROTATION_ALIGNMENT_AUTO

    }

    /**
     * 获取闭合的要素管理
     */
    fun getFillManager(): FillManager? {
        return fillManager
    }

    /**
     * 获取虚线的要素管理
     */
    fun getDottedLineManager(): LineManager? {
        return dottedLineManager
    }

    /**
     * 获取一般线的要素管理
     */
    fun getLineManager(): LineManager? {
        return lineManager
    }


    /**
     * 获取地图的样式
     */
    fun getStyle(): Style? {
        return style
    }

    /**
     * 获取所有的Marker图层
     */
    fun getSymbolManager(): SymbolManager? {
        return symbolManager
    }

    /**
     * 顶部的Marker，当前用于飞行的图标，其他用上面的symbolManager
     */
    fun getTopSymbolManager(): SymbolManager? {
        return topSymbolManager
    }

    /**
     * 获取MarkerView管理器
     */
    fun getMarkerViewManager():NewMarkerViewManager?{
        return newMarkerViewManager
    }

    /**
     * 清理资源
     */
    fun onDestroy(){
        newMarkerViewManager?.onDestroy()
        fillManager?.onDestroy()
        lineManager?.onDestroy()
        symbolManager?.onDestroy()
        dottedLineManager?.onDestroy()
        topSymbolManager?.onDestroy()
    }

    fun updateStyle(style: Style?) {
        style?.let {
            this.style = it
        }
    }

}