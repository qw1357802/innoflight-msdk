package com.geoai.uimap.api

interface IGEOMapOverlayManager {

    fun addOverlay(overlayName: String, overlay: IGEOMapOverlay)

    fun removeOverlay(overlayName: String)

    fun getOverlay(overlayName: String): IGEOMapOverlay?

    fun getAllOverlay(): LinkedHashMap<String, IGEOMapOverlay>

    fun onDestroy()
}