package com.geoai.uimap.core.geomap

import com.geoai.uimap.api.IGEOMap
import com.geoai.uimap.api.GEOMapCircle
import com.geoai.uimap.core.amap.GDMap
import com.geoai.uimap.core.amap.GDMapCircle
import com.geoai.uimap.core.mapbox.MapboxCircle
import com.geoai.uimap.core.mapbox.MapboxImpl
import com.geoai.uimap.core.osmdroid.OsmMap
import com.geoai.uimap.core.osmdroid.OsmMapCircle

class GEOMapCircleImpl(private val map: IGEOMap) : GEOMapCircle() {

    override fun show(): GEOMapCircle {
        if (centerPoint == null) return this
        when (map) {
            is GDMap ->
                return GDMapCircle(map)
                        .setCircleColor(mColor)
                        .setRadius(mRadius)
                        .setCircleStrokeColor(mStrokeColor)
                        .setCircleStrokeWidth(mStrokeWidth)
                        .setCircleCenter(centerPoint!!)
                        .show()
            is OsmMap ->
                return OsmMapCircle(map)
                        .setCircleColor(mColor)
                        .setRadius(mRadius)
                        .setCircleStrokeColor(mStrokeColor)
                        .setCircleStrokeWidth(mStrokeWidth)
                        .setCircleCenter(centerPoint!!)
                        .show()
            is MapboxImpl ->
                return MapboxCircle(map)
                        .setCircleColor(mColor)
                        .setRadius(mRadius)
                        .setCircleStrokeColor(mStrokeColor)
                        .setCircleStrokeWidth(mStrokeWidth)
                        .setCircleCenter(centerPoint!!)
                        .show()
            else -> return this
        }
    }


    override fun destroy() {

    }
}