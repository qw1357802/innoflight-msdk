package com.geoai.uimap.model

import android.content.Context
import com.amap.api.maps.CoordinateConverter
import com.amap.api.maps.model.LatLng
import com.geoai.uimap.geo.ILatLng
import com.geoai.uimap.utils.CoordinateUtils
import java.io.Serializable
import kotlin.math.abs

class GEOLatLng : Cloneable, Serializable, ILatLng {
    var lat = 0.0
    var lng = 0.0

    constructor()

    constructor(lat: Double, lng: Double) {
        this.lat = lat
        this.lng = lng
    }

    public override fun clone(): GEOLatLng {
        return GEOLatLng(lat, lng)
    }

    override fun toString(): String {
        return "GEOLatLng(latitude=$lat, longitude=$lng)"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as GEOLatLng

        if (lat != other.lat) return false
        if (lng != other.lng) return false

        return true
    }

    override fun hashCode(): Int {
        var result = lat.hashCode()
        result = 31 * result + lng.hashCode()
        return result
    }

    override fun getLatitude(): Double {
        return lat
    }

    override fun setLatitude(latitude: Double) {
        lat = latitude
    }

    override fun getLongitude(): Double {
        return lng
    }

    override fun setLongitude(longitude: Double) {
        lng = longitude
    }

}

/**
 * 转换为高德地图坐标
 */
fun ILatLng.toGdPoint(context: Context): LatLng {
    val converter = CoordinateConverter(context)
    converter.from(CoordinateConverter.CoordType.GPS)
    converter.coord(LatLng(this.latitude, this.longitude))
    return converter.convert()
}

fun GEOLatLng.toGcjPointInChina(): GEOLatLng {
    val isInChina = !CoordinateUtils.outOfChina(this.lng, this.lat)
    return if (isInChina) {
        val wgs84ToGcj02 = CoordinateUtils.wgs84ToGcj02(this.lng, this.lat)
        val latitude = wgs84ToGcj02[1]
        val longitude = wgs84ToGcj02[0]
        GEOLatLng(latitude, longitude)
    } else {
        this
    }
}

fun GEOLatLng.coverGcjToWgs84InChina(): GEOLatLng {
    val isInChina = !CoordinateUtils.outOfChina(this.lng, this.lat)
    return if (isInChina) {
        val wgs84ToGcj02 = CoordinateUtils.gcj02ToWGS84(this.lng, this.lat)
        val latitude = wgs84ToGcj02[1]
        val longitude = wgs84ToGcj02[0]
        GEOLatLng(latitude, longitude)
    } else {
        this
    }
}

fun GEOLatLng.isLocationLegal(): Boolean {
    return !(abs(this.lat) > 90 && abs(this.lng) <= 180) && (this.lat != 0.0 && this.lng != 0.0)
}