package com.geoai.uimap.core.osmdroid

import com.geoai.uimap.api.IGEOMap
import com.geoai.uimap.api.GEOMapPolyline
import com.geoai.uimap.utils.getOsmMapView
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.overlay.Polyline

class OsmMapPolyline(private val map: IGEOMap) : GEOMapPolyline() {

    private var mPolyline: Polyline? = null

    override fun show(): GEOMapPolyline {
        if (mPoints.isNullOrEmpty()) return this
        val mapView = map.getOsmMapView() ?: return this
        mPolyline = Polyline(mapView)
        val list = mutableListOf<GeoPoint>()
        mPoints?.forEach {
            list.add(GeoPoint(it.latitude, it.longitude))
        }
        mPolyline?.let {
            it.setPoints(list)
            it.outlinePaint.color = mLineColor
            it.outlinePaint.strokeWidth = mWidth
        }
        mapView.overlayManager.add(mPolyline)
        mapView.invalidate()
        return this
    }

    override fun destroy() {
        map.getOsmMapView()?.let {
            it.overlayManager.remove(mPolyline)
            it.invalidate()
        }
    }


}