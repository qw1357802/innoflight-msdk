package com.geoai.uimap.core.geomap

import com.geoai.uimap.api.IGEOMap
import com.geoai.uimap.api.IGEOMapOverlay
import com.geoai.uimap.core.amap.GDMap
import com.geoai.uimap.core.mapbox.MapboxImpl
import com.geoai.uimap.core.osmdroid.OsmMap
import com.geoai.uimap.model.GEOLatLng

abstract class GEOMapOverlayAbs(val geoMap: IGEOMap) : IGEOMapOverlay {


    override fun onMapClick(GEOLatLng: GEOLatLng): Boolean {
        return false
    }


    /**
     * 当前地图是否是高德地图
     */
    fun isGDMap(): Boolean {
        return geoMap is GDMap
    }

    /**
     * 当前地图是否是Osm地图
     */
    fun isOsmMap(): Boolean {
        return geoMap is OsmMap
    }

    /**
     * 当前地图是否是MapBox
     */
    fun isMapBox(): Boolean {
        return geoMap is MapboxImpl
    }

}
