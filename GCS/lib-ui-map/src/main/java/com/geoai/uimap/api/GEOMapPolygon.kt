package com.geoai.uimap.api

import android.graphics.Color
import com.geoai.uimap.geo.ILatLng

abstract class GEOMapPolygon : IGEOMapGeometry {

    protected var mPointList: List<ILatLng>? = null
    protected var mFillColor: Int = Color.TRANSPARENT
    protected var mStrokeWidth = 1f
    protected var mStrokeColor = Color.TRANSPARENT

    open fun setPositions(positions: List<ILatLng>): GEOMapPolygon {
        mPointList = positions
        return this
    }

    fun setFillColor(color: Int): GEOMapPolygon {
        mFillColor = color
        return this
    }


    fun setStrokeColor(color: Int): GEOMapPolygon {
        mStrokeColor = color
        return this
    }

    fun setStrokeWidth(width: Float): GEOMapPolygon {
        mStrokeWidth = width
        return this
    }

    fun getPositions(): List<ILatLng>? {
        return mPointList
    }

    abstract fun show(): GEOMapPolygon


}