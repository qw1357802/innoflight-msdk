package com.geoai.uimap.core.amap

import com.amap.api.maps.model.LatLng
import com.amap.api.maps.model.Polyline
import com.amap.api.maps.model.PolylineOptions
import com.geoai.uimap.api.IGEOMap
import com.geoai.uimap.api.GEOMapPolyline
import com.geoai.uimap.geo.ILatLng
import com.geoai.uimap.utils.dpToPx
import com.geoai.uimap.utils.getAMap

class GDMapPolyline(private val map: IGEOMap) : GEOMapPolyline() {

    private var mPolyline: Polyline? = null

    override fun show(): GEOMapPolyline {
        if (mPoints.isNullOrEmpty()) return this
        val pointList = mutableListOf<LatLng>()
        mPoints?.forEach {
            pointList.add(IGEOMap.getGdLatlng(map, it))
        }
        val polylineOptions = PolylineOptions()
        polylineOptions.color(mLineColor)
        polylineOptions.addAll(pointList)
        polylineOptions.width(dpToPx(mWidth))
        polylineOptions.isDottedLine = mDotted
        mPolyline = map.getAMap()?.addPolyline(polylineOptions)
        return this
    }

    override fun setPositions(positions: List<ILatLng>): GEOMapPolyline {
        mPolyline?.let {
            val pointList = mutableListOf<LatLng>()
            positions.forEach { p ->
                pointList.add(IGEOMap.getGdLatlng(map, p))
            }
            it.points = pointList
        }
        this.mPoints = positions
        return this
    }

    override fun setDottedLine(dotted: Boolean): GEOMapPolyline {
        mDotted = dotted
        mPolyline?.isDottedLine = dotted
        return this
    }

    override fun destroy() {
        mPolyline?.remove()
        mPoints = null
    }


}