package com.geoai.uimap.model

import com.geoai.uimap.utils.MathUtils

/**
 *    start 起点
 *    end 终点，注意与起点的角度为180度，这样的圆
 *    mAngel 转动角度，绝对值
 *    mClockwise 转动方向，顺时针true，逆时针false
 *
 *
 */
class GEOArcInfo(val start: GEOLatLng, val end: GEOLatLng, val mAngel: Float, val mClockwise: Boolean = false, val circle: MathUtils.Circle, val startAngle:Float)