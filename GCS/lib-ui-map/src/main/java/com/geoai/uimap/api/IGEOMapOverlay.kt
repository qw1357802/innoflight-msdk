package com.geoai.uimap.api

import com.geoai.uimap.model.GEOLatLng

interface IGEOMapOverlay {
    fun show(): IGEOMapOverlay
    fun onMapClick(geoLatLng: GEOLatLng): Boolean
    fun destroy()
}