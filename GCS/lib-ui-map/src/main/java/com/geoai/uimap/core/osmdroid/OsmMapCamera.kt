package com.geoai.uimap.core.osmdroid

import com.geoai.uimap.api.IGEOMapCamera
import com.geoai.uimap.core.geomap.bean.GEOMapCameraPosition
import com.geoai.uimap.geo.ILatLng
import com.geoai.uimap.model.GEOLatLng
import org.osmdroid.util.BoundingBox
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.MapView

class OsmMapCamera(private val map: OsmMap) : IGEOMapCamera {
    override fun getPosition(): GEOLatLng {
        val center = getMapView().mapCenter
        return GEOLatLng(center.latitude, center.longitude)
    }

    override fun getCameraPadding(): DoubleArray? {
        return null
    }

    override fun setPosition(position: ILatLng) {
        getMapView().controller.setCenter(GeoPoint(position.latitude, position.longitude))
    }

    override fun setPosition(position: ILatLng, level: Float, padding: DoubleArray?) {

    }

    override fun changeBearing(bearValue: Float) {

    }

    override fun getCameraPosition(): GEOMapCameraPosition? {
        return null
    }

    override fun getZoomLevel(): Double {
        return getMapView().zoomLevelDouble
    }

    override fun setZoomLevel(level: Double) {
        getMapView().controller.setZoom(level)
    }

    override fun zoomToBounds(bound: List<ILatLng>, padding: Int) {
        val bounding =
                BoundingBox.fromGeoPointsSafe(bound.map { GeoPoint(it.latitude, it.longitude) })
        getMapView().zoomToBoundingBox(bounding, true, padding)
    }

    override fun zoomToBounds(bound: List<ILatLng>, paddingHLeft: Int, paddingVTop: Int, paddingHRight: Int, paddingVBottom: Int) {
        // TODO
    }

    override fun getMaxZoomLevel(): Double {
        return getMapView().maxZoomLevel
    }

    override fun getMinZoomLevel(): Double {
        return getMapView().minZoomLevel
    }

    override fun zoomIn() {
        getMapView().controller.zoomIn()
    }

    override fun zoomOut() {
        getMapView().controller.zoomOut()
    }

    override fun zoomTo(zoom: Double) {
        getMapView().controller.zoomTo(zoom)
    }

    private fun getMapView(): MapView {
        return map.getMapView() as MapView
    }
}