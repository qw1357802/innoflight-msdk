package com.geoai.uimap.core.amap

import android.graphics.Point
import com.amap.api.maps.AMap
import com.amap.api.maps.CoordinateConverter
import com.amap.api.maps.TextureMapView
import com.amap.api.maps.model.LatLng
import com.geoai.uimap.api.IGEOMap
import com.geoai.uimap.api.IGEOMapProjection
import com.geoai.uimap.geo.ILatLng
import com.geoai.uimap.model.GEOLatLng

class GDMapProjection(private val map: IGEOMap) : IGEOMapProjection {
    override fun toScreenPoint(geoPoint: ILatLng): Point {
        return getAMap().projection.toScreenLocation(IGEOMap.getGdLatlng(map, geoPoint))
    }

    override fun toGeoPoint(point: Point): ILatLng {
        val latLng = getAMap().projection.fromScreenLocation(point)
        return IGEOMap.getGeoLatlng(map, latLng)
    }

    private fun getAMap(): AMap {
        return (map.getMapView() as TextureMapView).map
    }

    private fun convert(point: GEOLatLng): LatLng {
        val converter =
            CoordinateConverter(map.getContext()).from(CoordinateConverter.CoordType.GPS)
        val source = LatLng(point.lat, point.lng)
        return converter.coord(source).convert()
    }
}