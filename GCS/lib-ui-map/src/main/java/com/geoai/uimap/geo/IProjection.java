package com.geoai.uimap.geo;


/**
 * 投影
 */
public interface IProjection {

    /**
     * 经纬度转平面坐标
     *
     * @param latLng 经纬度
     * @return 平面坐标点
     */
    IPoint GeoPoint2Point(ILatLng latLng);

    /**
     * 平面坐标转经纬度
     *
     * @param point 平面坐标点
     * @return 经纬度
     */
    ILatLng Point2GeoPoint(IPoint point);

    /**
     * 平面坐标转经纬度
     *
     * @param x 平面坐标x
     * @param y 平面坐标y
     * @return 经纬度
     */
    ILatLng Point2GeoPoint(double x, double y);
}
