package com.geoai.uimap.core.geomap

import com.geoai.uimap.api.IGEOMap
import com.geoai.uimap.api.GEOMapArc
import com.geoai.uimap.core.amap.GDMap
import com.geoai.uimap.core.amap.GDMapArc
import com.geoai.uimap.core.mapbox.MapboxArc
import com.geoai.uimap.core.mapbox.MapboxImpl
import com.geoai.uimap.core.osmdroid.OsmMap
import com.geoai.uimap.core.osmdroid.OsmMapArc

class GEOMapArcImpl(private val map: IGEOMap) : GEOMapArc() {
    override fun show(): GEOMapArc {
        if (mArcInfo == null) return this
        when (map) {
            is GDMap -> {
                return GDMapArc(map)
                        .setLineColor(mLineColor)
                        .setStrokeWidth(mWidth)
                        .setArcInfo(mArcInfo!!)
                        .show()
            }
            is OsmMap -> {
                return OsmMapArc(map)
                        .setLineColor(mLineColor)
                        .setStrokeWidth(mWidth)
                        .setArcInfo(mArcInfo!!)
                        .show()
            }
            is MapboxImpl -> {
                return MapboxArc(map)
                        .setLineColor(mLineColor)
                        .setStrokeWidth(mWidth)
                        .setArcInfo(mArcInfo!!)
                        .show()
            }
            else -> return this
        }
    }

    override fun destroy() {

    }


}