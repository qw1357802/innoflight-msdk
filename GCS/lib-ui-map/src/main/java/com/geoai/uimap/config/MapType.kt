package com.geoai.uimap.config

object MapType {
    const val MAP_TYPE_NORMAL = 1
    const val MAP_TYPE_SATELLITE = 2
    const val MAP_TYPE_SATELLITE_STREETS = 3
}