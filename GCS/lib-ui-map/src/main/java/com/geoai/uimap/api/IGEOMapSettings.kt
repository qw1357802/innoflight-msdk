package com.geoai.uimap.api

interface IGEOMapSettings {
    fun setRotateGesturesEnabled(enable: Boolean): IGEOMapSettings
    fun setZoomControlsEnabled(enable: Boolean): IGEOMapSettings
    fun setScaleControlsEnabled(enable: Boolean): IGEOMapSettings
    fun setTiltGesturesEnabled(enable: Boolean): IGEOMapSettings
    fun setScrollGesturesEnabled(enable: Boolean): IGEOMapSettings
}