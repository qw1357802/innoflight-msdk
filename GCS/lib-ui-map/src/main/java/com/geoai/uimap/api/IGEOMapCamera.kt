package com.geoai.uimap.api

import com.geoai.uimap.core.geomap.bean.GEOMapCameraPosition
import com.geoai.uimap.geo.ILatLng
import com.geoai.uimap.model.GEOLatLng

interface IGEOMapCamera {
    fun getPosition(): GEOLatLng
    fun getCameraPadding(): DoubleArray?
    fun setPosition(position: ILatLng)
    fun setPosition(position: ILatLng, level: Float, padding: DoubleArray? = null)
    fun changeBearing(bearValue: Float)
    fun getCameraPosition(): GEOMapCameraPosition?
    fun getZoomLevel(): Double
    fun setZoomLevel(level: Double)
    fun zoomToBounds(bound: List<ILatLng>, padding: Int)
    fun zoomToBounds(bound: List<ILatLng>, paddingHLeft: Int, paddingVTop: Int, paddingHRight: Int, paddingVBottom: Int)
    fun getMaxZoomLevel(): Double
    fun getMinZoomLevel(): Double
    fun zoomIn()
    fun zoomOut()
    fun zoomTo(zoom: Double)
}