package com.geoai.uimap.core.geomap

import com.geoai.uimap.api.IGEOMap
import com.geoai.uimap.api.GEOMapPolyline
import com.geoai.uimap.core.amap.GDMap
import com.geoai.uimap.core.amap.GDMapPolyline
import com.geoai.uimap.core.mapbox.MapboxImpl
import com.geoai.uimap.core.mapbox.MapboxPolyline
import com.geoai.uimap.core.osmdroid.OsmMap
import com.geoai.uimap.core.osmdroid.OsmMapPolyline

class GEOMapPolylineImpl(private val map: IGEOMap) : GEOMapPolyline() {
    override fun show(): GEOMapPolyline {
        mPoints?.let {
            when (map) {
                is GDMap -> {
                    return GDMapPolyline(map)
                            .setLineColor(mLineColor)
                            .setLineWidth(mWidth)
                            .setPositions(mPoints!!)
                            .setDottedLine(mDotted)
                            .show()
                }
                is OsmMap -> {
                    return OsmMapPolyline(map)
                            .setLineColor(mLineColor)
                            .setLineWidth(mWidth)
                            .setPositions(mPoints!!)
                            .setDottedLine(mDotted)
                            .show()
                }
                is MapboxImpl -> {
                    return MapboxPolyline(map)
                            .setLineColor(mLineColor)
                            .setLineWidth(mWidth)
                            .setPositions(mPoints!!)
                            .setDottedLine(mDotted)
                            .show()
                }
                else -> return this
            }
        }
        return this
    }

    override fun destroy() {

    }
}