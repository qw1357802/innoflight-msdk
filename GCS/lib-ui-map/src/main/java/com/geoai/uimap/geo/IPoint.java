package com.geoai.uimap.geo;

/**
 * 平面坐标点
 */
public interface IPoint {

    /**
     * @return X坐标
     */
    double getX();

    /**
     * @param x X坐标
     */
    void setX(double x);

    /**
     * @return Y坐标
     */
    double getY();

    /**
     * @param y Y坐标
     */
    void setY(double y);
}
