package com.geoai.uimap.core.osmdroid

import android.graphics.Point
import com.geoai.uimap.api.IGEOMap
import com.geoai.uimap.api.IGEOMapProjection
import com.geoai.uimap.geo.ILatLng
import com.geoai.uimap.model.GEOLatLng
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.MapView

class OsmMapProjection(private val map: IGEOMap) : IGEOMapProjection {
    override fun toScreenPoint(geoPoint: ILatLng): Point {
        return getMapView().projection.toPixels(
            GeoPoint(geoPoint.latitude, geoPoint.longitude),
            null
        )
    }

    override fun toGeoPoint(point: Point): GEOLatLng {
        val geoPoint = getMapView().projection.fromPixels(point.x, point.y, null)
        return GEOLatLng(geoPoint.latitude, geoPoint.longitude)
    }

    private fun getMapView(): MapView {
        return map.getMapView() as MapView
    }
}