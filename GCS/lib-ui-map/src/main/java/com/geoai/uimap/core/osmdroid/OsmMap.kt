package com.geoai.uimap.core.osmdroid

import android.content.Context
import android.graphics.Bitmap
import android.os.Bundle
import android.view.View
import com.geoai.uimap.api.GEOMapListener
import com.geoai.uimap.api.IGEOMap
import com.geoai.uimap.api.IGEOMapCamera
import com.geoai.uimap.api.IGEOMapOverlayManager
import com.geoai.uimap.api.IGEOMapProjection
import com.geoai.uimap.api.IGEOMapSettings
import com.geoai.uimap.config.MapType
import com.geoai.uimap.core.geomap.GEOMapCustomMapStyleOptions
import com.geoai.uimap.model.GEOMapException
import org.osmdroid.config.Configuration
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.views.MapView

class OsmMap(private val context: Context) : IGEOMap {

    private var mapView: MapView? = null
    private var mapCamera: IGEOMapCamera? = null
    private var mapProjection: IGEOMapProjection? = null
    private var mapOverlayManager: IGEOMapOverlayManager? = null

    private var mapType: Int = MapType.MAP_TYPE_SATELLITE

    init {
        mapView = MapView(context)
        mapCamera = OsmMapCamera(this)
        mapProjection = OsmMapProjection(this)
        mapOverlayManager = OsmOverlayManager(this)
    }

    override fun onCreate(bundle: Bundle?, maptype: Int) {
//        Configuration.getInstance().isDebugMode = true
        Configuration.getInstance().userAgentValue = "com.geoai.map.demo"
//        BingMapTileSource.setBingKey(BING_MAP_KEY)
//        val bing = BingMapTileSource(null)
//        bing.style = BingMapTileSource.IMAGERYSET_AERIALWITHLABELS
//        mapView?.setTileSource(bing)
        mapView?.setTileSource(TileSourceFactory.MAPNIK)
        mapView?.setMultiTouchControls(true)
    }

    override fun onResume() {
        mapView?.onResume()
    }

    override fun onPause() {
        mapView?.onPause()
    }

    override fun onStart() {

    }

    override fun onStop() {

    }

    override fun onDestroy() {
        mapView?.onDetach()
    }

    override fun getContext(): Context {
        return context
    }

    override fun getMapView(): View {
        return mapView ?: throw GEOMapException(0x2001, "init first")
    }

    override fun getMapCamera(): IGEOMapCamera {
        return mapCamera ?: throw GEOMapException(0x2001, "init first")
    }

    override fun getMapOverlayManager(): IGEOMapOverlayManager {
        return mapOverlayManager!!
    }

    override fun getMapProjection(): IGEOMapProjection {
        return mapProjection ?: throw GEOMapException(0x2001, "init first")
    }

    override fun setMapType(type: Int) {
        mapType = type
    }

    override fun showHdLayer(show: Boolean) {

    }

    override fun getMapType(): Int {
        return mapType
    }

    override fun isLoaded(): Boolean {
        return false
    }

    override fun getMapUiSettings(): IGEOMapSettings? {
        return null
    }

    override fun setCustomMapStyle(options: GEOMapCustomMapStyleOptions) {

    }

    override fun initMapLocation(locationIcon: Int, isMyLocationEnabled: Boolean) {

    }

    override fun moveToLocation() {

    }

    override fun removeCache(listener: GEOMapListener.OnCacheRemoveListener) {

    }

    override fun setOnMapLoadedListener(listener: GEOMapListener.OnMapLoadedListener) {

    }

    override fun setOnMapClickListener(listener: GEOMapListener.OnMapClickListener) {

    }

    override fun setOnMapLongClickListener(listener: GEOMapListener.OnMapLongClickListener) {

    }

    override fun setOnCameraChangeListener(listener: GEOMapListener.OnCameraChangeListener) {

    }

    override fun addOnMapTouchListener(listener: GEOMapListener.OnMapTouchListener) {

    }

    override fun removeOnMapTouchListener(listener: GEOMapListener.OnMapTouchListener) {

    }

    override fun setOnMarkerClickListener(listener: GEOMapListener.OnMarkerClickListener) {

    }


    override fun getMapScreenShot(function: (Bitmap?) -> Unit) {
    }
    override fun setInfoWindowAdapter(listener: GEOMapListener.InfoWindowAdapter) {

    }

    companion object {
        const val BING_MAP_KEY = "AjqrPVCHYjMkRUtSbZphl2gVYYHfjRX72W73KRWzwofcR5P-spJFQRKMlGHVVrsQ"
//        const val BING_MAP_KEY = "Am3AJB0UVy-LM5L4ENBUgorSXQiTfErJzTbHy0SChqnvqDfpaNSvzbRZz-PI2yU-"
    }
}