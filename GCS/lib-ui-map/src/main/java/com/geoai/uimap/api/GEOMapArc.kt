package com.geoai.uimap.api

import android.graphics.Color
import com.geoai.uimap.model.GEOArcInfo

abstract class GEOMapArc : IGEOMapGeometry {
    protected var mWidth = 1f
    protected var mLineColor = Color.WHITE
    protected var mArcInfo: GEOArcInfo? = null

    fun setArcInfo(info: GEOArcInfo): GEOMapArc {
        mArcInfo = info
        return this
    }

    fun setStrokeWidth(width: Float): GEOMapArc {
        mWidth = width
        return this
    }

    fun setLineColor(color: Int): GEOMapArc {
        mLineColor = color
        return this
    }

    abstract fun show(): GEOMapArc


}