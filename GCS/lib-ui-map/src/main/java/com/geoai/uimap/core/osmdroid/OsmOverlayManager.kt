package com.geoai.uimap.core.osmdroid

import com.geoai.uimap.api.IGEOMap
import com.geoai.uimap.api.IGEOMapOverlay
import com.geoai.uimap.api.IGEOMapOverlayManager
import org.osmdroid.views.MapView

/**
 *
 * @Description:     类作用描述
 * @Author:         hjq
 * @CreateDate:     2021/2/23 10:23
 *
 */
class OsmOverlayManager(private val map: IGEOMap) : IGEOMapOverlayManager {
    private fun getMapView(): MapView {
        return map.getMapView() as MapView
    }


    private val overlayLinkMap = LinkedHashMap<String, IGEOMapOverlay>()
    override fun addOverlay(overlayName: String, overlay: IGEOMapOverlay) {

    }

    override fun removeOverlay(overlayName: String) {

    }

    override fun getOverlay(overlayName: String): IGEOMapOverlay? {
        return overlayLinkMap[overlayName]
    }

    override fun getAllOverlay(): LinkedHashMap<String, IGEOMapOverlay> {
        return overlayLinkMap
    }

    override fun onDestroy() {

    }
}