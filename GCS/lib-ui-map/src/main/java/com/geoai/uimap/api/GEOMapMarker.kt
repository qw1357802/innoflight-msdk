package com.geoai.uimap.api

import android.graphics.Bitmap
import androidx.annotation.DrawableRes
import com.geoai.uimap.R
import com.geoai.uimap.geo.ILatLng
import com.geoai.uimap.model.GEOLatLng

abstract class GEOMapMarker : IGEOMapGeometry {
    protected var markerPoint: ILatLng? = GEOLatLng()
    protected var mAngel: Float = 0f
    protected var mPeriod: Int = 2000
    protected var mTop = false

    @DrawableRes
    protected var mImageResId = R.mipmap.ic_launcher

    protected var mImageBitmap: Bitmap? = null

    var title: String? = null

    open fun setImageId(@DrawableRes resId: Int): GEOMapMarker {
        mImageResId = resId
        return this
    }

    open fun setImage(resId: Bitmap?): GEOMapMarker {
        mImageBitmap = resId
        return this
    }

    /**
     * 设置当前的Marker在最顶层，一般Marker不建议使用
     * 高德地图有单独的方法
     * MapBox只有自己封装，用MapboxGeometryManager的topSymbolManager来实现
     */
    open fun setToTop(top: Boolean = true): GEOMapMarker {
        mTop = top
        return this
    }

    open fun setPosition(position: ILatLng): GEOMapMarker {
        markerPoint = position
        return this
    }

    open fun getPosition(): ILatLng {
        return markerPoint!!
    }

    open fun period(period: Int): GEOMapMarker {
        mPeriod = period
        return this
    }

    /**
     * 顺时针方向的角度
     */
    open fun setRotateAngle(angel: Float): GEOMapMarker {
        mAngel = angel
        return this
    }

    open fun showInfoWindow(): GEOMapMarker {
        return this
    }

    open fun hideInfoWindow() {}

    abstract fun show(): GEOMapMarker


    abstract fun getMarkerId(): String

}