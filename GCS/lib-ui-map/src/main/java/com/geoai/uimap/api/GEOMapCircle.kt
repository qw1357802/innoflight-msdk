package com.geoai.uimap.api

import android.graphics.Color
import com.geoai.uimap.model.GEOLatLng

abstract class GEOMapCircle : IGEOMapGeometry {
    protected var centerPoint: GEOLatLng? = null
    protected var mRadius: Double = 0.0
    protected var mColor = Color.parseColor("#ff000000")
    protected var mStrokeWidth = 1f
    protected var mStrokeColor = Color.parseColor("#ff000000")


    open fun setCircleCenter(point: GEOLatLng): GEOMapCircle {
        centerPoint = point
        return this
    }

    fun setRadius(radius: Double): GEOMapCircle {
        mRadius = radius
        return this
    }

    fun setCircleColor(color: Int): GEOMapCircle {
        mColor = color
        return this
    }

    fun setCircleColor(colorStr: String): GEOMapCircle {
        mColor = Color.parseColor(colorStr)
        return this
    }

    fun setCircleStrokeWidth(width: Float): GEOMapCircle {
        mStrokeWidth = width
        return this
    }

    fun setCircleStrokeColor(color: Int): GEOMapCircle {
        mStrokeColor = color
        return this
    }

    fun setCircleStrokeColor(colorStr: String): GEOMapCircle {
        mStrokeColor = Color.parseColor(colorStr)
        return this
    }

    fun getCircleCenterPoint(): GEOLatLng? {
        return centerPoint
    }

    fun getRadius(): Double {
        return mRadius
    }


    abstract fun show(): GEOMapCircle

}