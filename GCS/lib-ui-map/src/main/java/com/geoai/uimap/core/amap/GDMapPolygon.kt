package com.geoai.uimap.core.amap

import com.amap.api.maps.model.LatLng
import com.amap.api.maps.model.Polygon
import com.amap.api.maps.model.PolygonOptions
import com.geoai.uimap.api.IGEOMap
import com.geoai.uimap.api.GEOMapPolygon
import com.geoai.uimap.geo.ILatLng
import com.geoai.uimap.utils.dpToPx
import com.geoai.uimap.utils.getAMap

class GDMapPolygon(private val map: IGEOMap) : GEOMapPolygon() {

    private var mPolygon: Polygon? = null
    override fun show(): GEOMapPolygon {
        if (mPointList.isNullOrEmpty()) return this
        val points = mutableListOf<LatLng>()
        mPointList?.forEach {
            points.add(IGEOMap.getGdLatlng(map, it))
        }
        val polygonOptions = PolygonOptions()
            .fillColor(mFillColor)
            .strokeColor(mStrokeColor)
            .strokeWidth(dpToPx(mStrokeWidth))
        polygonOptions.points = points
        mPolygon = map.getAMap()?.addPolygon(polygonOptions)
        return this
    }

    override fun setPositions(positions: List<ILatLng>): GEOMapPolygon {
        mPointList = positions
        mPolygon?.points = positions.map {
            IGEOMap.getGdLatlng(map, it)
        }
        return this
    }

    override fun destroy() {
        mPolygon?.remove()
    }
}