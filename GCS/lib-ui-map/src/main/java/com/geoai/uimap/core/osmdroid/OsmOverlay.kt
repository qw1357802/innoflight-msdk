package com.geoai.uimap.core.osmdroid

import com.geoai.uimap.api.IGEOMap
import com.geoai.uimap.api.IGEOMapOverlay
import com.geoai.uimap.model.GEOLatLng
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.Overlay

class OsmOverlay(private val map: IGEOMap) : IGEOMapOverlay, Overlay() {

    override fun show(): IGEOMapOverlay {
        getMapView().overlayManager.add(this)
        return this
    }

    override fun destroy() {
        getMapView().overlayManager.remove(this)
    }

    private fun getMapView(): MapView {
        return map.getMapView() as MapView
    }

    override fun onMapClick(GEOLatLng: GEOLatLng): Boolean {

        return false
    }

}