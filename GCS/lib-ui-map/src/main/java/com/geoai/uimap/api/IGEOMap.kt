package com.geoai.uimap.api

import android.content.Context
import android.graphics.Bitmap
import android.os.Bundle
import android.view.View
import com.amap.api.maps.model.LatLng
import com.geoai.uimap.core.amap.GDMap
import com.geoai.uimap.core.geomap.GEOMapCustomMapStyleOptions
import com.geoai.uimap.geo.ILatLng
import com.geoai.uimap.model.GEOLatLng
import com.geoai.uimap.model.toGdPoint
import com.geoai.uimap.utils.toGEOLatLng

interface IGEOMap {
    companion object {
        private fun isUseOriginCoordinate(map: IGEOMap) = map is GDMap && map.useOriginCorrdinate
        fun getGdLatlng(map: IGEOMap, geoLatLng: ILatLng) = if (isUseOriginCoordinate(map)) LatLng(geoLatLng.latitude, geoLatLng.longitude) else geoLatLng.toGdPoint(map.getContext())
        fun getGeoLatlng(map: IGEOMap, latLng: LatLng) = if (isUseOriginCoordinate(map)) GEOLatLng(latLng.latitude, latLng.longitude) else latLng.toGEOLatLng()
        fun getGeoLatlng(map: IGEOMap, geoLatLng: ILatLng) = if (isUseOriginCoordinate(map)) LatLng(geoLatLng.latitude, geoLatLng.longitude).toGEOLatLng() else geoLatLng
    }

    fun onCreate(bundle: Bundle?, mapType: Int)
    fun onResume()
    fun onPause()
    fun onStart()
    fun onStop()
    fun onDestroy()
    fun getContext(): Context
    fun getMapView(): View
    fun getMapCamera(): IGEOMapCamera
    fun getMapOverlayManager(): IGEOMapOverlayManager
    fun getMapProjection(): IGEOMapProjection
    fun setMapType(type: Int)
    fun showHdLayer(show: Boolean)
    fun getMapType(): Int
    fun isLoaded(): Boolean
    fun getMapUiSettings(): IGEOMapSettings?
    fun setCustomMapStyle(options: GEOMapCustomMapStyleOptions)
    fun initMapLocation(locationIcon: Int, isMyLocationEnabled: Boolean = false)
    fun moveToLocation()
    fun removeCache(listener: GEOMapListener.OnCacheRemoveListener)
    fun setOnMapLoadedListener(listener: GEOMapListener.OnMapLoadedListener)
    fun setOnMapClickListener(listener: GEOMapListener.OnMapClickListener)
    fun setOnMapLongClickListener(listener: GEOMapListener.OnMapLongClickListener)
    fun setOnCameraChangeListener(listener: GEOMapListener.OnCameraChangeListener)
    fun addOnMapTouchListener(listener: GEOMapListener.OnMapTouchListener)
    fun removeOnMapTouchListener(listener: GEOMapListener.OnMapTouchListener)
    fun setOnMarkerClickListener(listener: GEOMapListener.OnMarkerClickListener)
    fun getMapScreenShot(function: (Bitmap?) -> Unit)
    fun setInfoWindowAdapter(listener: GEOMapListener.InfoWindowAdapter)
}