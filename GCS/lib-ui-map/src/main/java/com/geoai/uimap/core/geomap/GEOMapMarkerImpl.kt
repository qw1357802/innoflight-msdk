package com.geoai.uimap.core.geomap

import com.geoai.uimap.api.IGEOMap
import com.geoai.uimap.api.GEOMapMarker
import com.geoai.uimap.core.amap.GDMap
import com.geoai.uimap.core.amap.GDMapMarker
import com.geoai.uimap.core.mapbox.MapboxImpl
import com.geoai.uimap.core.mapbox.MapboxMarker
import com.geoai.uimap.core.osmdroid.OsmMap
import com.geoai.uimap.core.osmdroid.OsmMarker

class GEOMapMarkerImpl(private val map: IGEOMap) : GEOMapMarker() {
    override fun show(): GEOMapMarker {
//        if (markerPoint == null || mImageResId == -1) return this
        when (map) {
            is GDMap -> {
                return GDMapMarker(map)
                        .setImageId(mImageResId)
                        .setImage(mImageBitmap)
                        .setPosition(markerPoint!!)
                        .setRotateAngle(mAngel)
                        .period(mPeriod)
                        .setToTop(mTop)
                        .show()
            }
            is OsmMap ->
                return OsmMarker(map)
                        .setImageId(mImageResId)
                        .setImage(mImageBitmap)
                        .setPosition(markerPoint!!)
                        .setRotateAngle(mAngel)
                        .period(mPeriod)
                        .show()
            is MapboxImpl -> {
                return MapboxMarker(map)
                        .setImageId(mImageResId)
                        .setPosition(markerPoint!!)
                        .setImage(mImageBitmap)
                        .setRotateAngle(mAngel)
                        .period(mPeriod)
                        .setToTop(mTop)
                        .show()
            }
            else -> return this
        }
    }

    override fun getMarkerId(): String {
        return ""
    }

    override fun destroy() {

    }
}