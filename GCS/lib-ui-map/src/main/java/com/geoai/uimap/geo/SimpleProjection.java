package com.geoai.uimap.geo;

/**
 * 简单投影，将椭球当球计算，小距离误差小
 */
public class SimpleProjection implements IProjection {
    //    private double R = 6371.009 * 1000;//地球半径

//    6378137

    private static final double R = 6378.137 * 1000;   //地球赤道半径
//    private static final double R      = 6371.393 * 1000; //地球平均半径


    private double deltaR = 0;

    private ILatLng base;

    /**
     * 初始化投影
     *
     * @param lat 基准点纬度
     * @param lng 基准点经度
     */
    public SimpleProjection(double lat, double lng) {
        this(new LatLng(lat, lng));
    }

    /**
     * 初始化投影
     *
     * @param base 基准点经纬度
     */
    public SimpleProjection(ILatLng base) {
        this.base = base;
        deltaR = R * Math.cos(this.base.getLatitude() * Math.PI / 180);
    }

    @Override
    public IPoint GeoPoint2Point(ILatLng latLng) {
        return new Point(deltaR * (latLng.getLongitude() - base.getLongitude()) * Math.PI / 180,
                R * (latLng.getLatitude() - base.getLatitude()) * Math.PI / 180);
    }

    @Override
    public ILatLng Point2GeoPoint(IPoint point) {
        return Point2GeoPoint(point.getX(), point.getY());
    }

    @Override
    public ILatLng Point2GeoPoint(double x, double y) {
        return new LatLng(y * 180 / (R * Math.PI) + base.getLatitude(),
                x * 180 / (deltaR * Math.PI) + base.getLongitude());
    }

    /**
     * 获取基准点
     *
     * @return 经纬度
     */
    public ILatLng getBase() {
        return base;
    }

}
