package com.geoai.uimap.model

class GEOMapException(private val code: Int, message: String) : Exception(message) {
    override fun toString(): String {
        return "GEOMapException(code=$code, message=$message)"
    }
}