package com.geoai.uimap.api

import android.graphics.Color
import com.geoai.uimap.geo.ILatLng

abstract class GEOMapPolyline : IGEOMapGeometry {
    protected var mPoints: List<ILatLng>? = null
    protected var mWidth = 1f
    protected var mLineColor = Color.parseColor("#ff000000")
    protected var mDotted = false

    /**
     * 设置线段的点
     */
    open fun setPositions(positions: List<ILatLng>): GEOMapPolyline {
        this.mPoints = positions
        return this
    }


    /**
     * 设置线段的宽度
     */
    fun setLineWidth(width: Float): GEOMapPolyline {
        mWidth = width
        return this
    }

    /**
     * 设置线的颜色
     */
    fun setLineColor(color: Int): GEOMapPolyline {
        mLineColor = color
        return this
    }

    open fun setDottedLine(dotted: Boolean): GEOMapPolyline {
        mDotted = dotted
        return this
    }

    fun getPositions(): List<ILatLng>? {
        return mPoints
    }


    abstract fun show(): GEOMapPolyline


}