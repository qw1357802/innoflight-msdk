package com.geoai.uimap.core.geomap

import com.geoai.uimap.api.IGEOMap
import com.geoai.uimap.api.GEOMapPolygon
import com.geoai.uimap.core.amap.GDMap
import com.geoai.uimap.core.amap.GDMapPolygon
import com.geoai.uimap.core.mapbox.MapboxImpl
import com.geoai.uimap.core.mapbox.MapboxPolygon
import com.geoai.uimap.core.osmdroid.OsmMap
import com.geoai.uimap.core.osmdroid.OsmMapPolygon

class GEOMapPolygonImpl(private val map: IGEOMap) : GEOMapPolygon() {

    override fun show(): GEOMapPolygon {
        if (mPointList.isNullOrEmpty()) return this
        when (map) {
            is GDMap -> {
                return GDMapPolygon(map)
                        .setPositions(mPointList!!)
                        .setStrokeWidth(mStrokeWidth)
                        .setStrokeColor(mStrokeColor)
                        .setFillColor(mFillColor)
                        .show()
            }
            is OsmMap -> {
                return OsmMapPolygon(map)
                        .setPositions(mPointList!!)
                        .setStrokeColor(mStrokeColor)
                        .setStrokeWidth(mStrokeWidth)
                        .setFillColor(mFillColor)
                        .show()
            }
            is MapboxImpl -> {
                return MapboxPolygon(map)
                        .setPositions(mPointList!!)
                        .setStrokeColor(mStrokeColor)
                        .setStrokeWidth(mStrokeWidth)
                        .setFillColor(mFillColor)
                        .show()
            }
            else -> return this
        }
    }


    override fun destroy() {

    }
}