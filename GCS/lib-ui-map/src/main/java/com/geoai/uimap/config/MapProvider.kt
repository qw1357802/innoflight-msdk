package com.geoai.uimap.config

enum class MapProvider(val index: Int, val description: String) {
    GD_MAP(1, "高德地图"),
    MAP_BOX(2, "Mapbox"),
    OSM_MAP(3, "Osm")

}