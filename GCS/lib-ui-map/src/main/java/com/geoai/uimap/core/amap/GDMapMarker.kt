package com.geoai.uimap.core.amap

import android.graphics.Bitmap
import android.util.Log
import com.amap.api.maps.model.BitmapDescriptorFactory
import com.amap.api.maps.model.Marker
import com.amap.api.maps.model.MarkerOptions
import com.geoai.uimap.api.IGEOMap
import com.geoai.uimap.api.GEOMapMarker
import com.geoai.uimap.geo.ILatLng
import com.geoai.uimap.utils.GEOMapCacheUtils
import com.geoai.uimap.utils.getAMap

class GDMapMarker(private val map: IGEOMap) : GEOMapMarker() {
    private var marker: Marker? = null

    override fun destroy() {
        marker?.remove()
        marker?.destroy()
    }

    override fun show(): GEOMapMarker {
        markerPoint?.let {
            val icon = if (mImageBitmap != null) {
                BitmapDescriptorFactory.fromBitmap(mImageBitmap)
            } else {
                BitmapDescriptorFactory.fromBitmap(GEOMapCacheUtils.getBitmapCache(map.getContext(), mImageResId))
            }
            val markerOption = MarkerOptions()
                    .position(IGEOMap.getGdLatlng(map, it))
                    .icon(icon)
                    .anchor(0.5f, 0.5f)
                    .period(mPeriod)
                    .rotateAngle(-mAngel)
//        markerPoint?.let {
//            markerOption.position(IXAMap.getGdLatlng(map, it))
//        }
//        if (mImageResId != -1){
//            val icon = BitmapDescriptorFactory.fromBitmap(XAMapCacheUtils.getBitmapCache(map.getContext(), mImageResId))
//            markerOption.icon(icon)
//        }
            marker = map.getAMap()?.addMarker(markerOption)
        }

        return this
    }

    override fun setToTop(top: Boolean): GEOMapMarker {
        marker?.setToTop()
        return this
    }
//    override fun setImage(icon: Bitmap): XAMapMarker {
//        markerIcon = icon
//        val icon = BitmapDescriptorFactory.fromBitmap(markerIcon)
//        marker?.setIcon(icon)
//        return this
//    }

    override fun setImageId(resId: Int): GEOMapMarker {
        marker?.let {
            val icon = BitmapDescriptorFactory.fromBitmap(GEOMapCacheUtils.getBitmapCache(map.getContext(), resId))
            it.setIcon(icon)
        }
        mImageResId = resId

        return this
    }

    override fun setImage(resId: Bitmap?): GEOMapMarker {
        marker?.setIcon(BitmapDescriptorFactory.fromBitmap(resId))
        mImageBitmap = resId
        return this
    }

    override fun setRotateAngle(angel: Float): GEOMapMarker {
        mAngel = angel
        marker?.rotateAngle = -angel
        return this
    }

    override fun period(period: Int): GEOMapMarker {
        mPeriod = period
        marker?.period = period
        return this
    }

    override fun setPosition(position: ILatLng): GEOMapMarker {
        markerPoint = position
        marker?.position = IGEOMap.getGdLatlng(map, position)
        return this
    }

    override fun showInfoWindow(): GEOMapMarker {
        marker?.showInfoWindow()
        return this
    }

    override fun getMarkerId(): String {
        return marker?.id.toString()
    }

    override fun hideInfoWindow(){
        marker?.hideInfoWindow()
    }
}