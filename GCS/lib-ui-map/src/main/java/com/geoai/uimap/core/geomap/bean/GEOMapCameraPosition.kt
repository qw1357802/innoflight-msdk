package com.geoai.uimap.core.geomap.bean

import com.geoai.uimap.model.GEOLatLng
import java.io.Serializable

class GEOMapCameraPosition: Serializable{
    var padding: DoubleArray? = null
    var zoom: Float = 0f
    var bearing: Float = 0f
    var target: GEOLatLng? = null
}