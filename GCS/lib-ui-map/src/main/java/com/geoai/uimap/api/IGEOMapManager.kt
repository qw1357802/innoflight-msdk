package com.geoai.uimap.api

import android.content.Context
import android.os.Bundle
import com.geoai.uimap.config.MapProvider

interface IGEOMapManager {

    fun init(context: Context, mapProvider: MapProvider, bundle: Bundle?, mapType: Int)

    fun onSaveInstanceState(outState: Bundle?)

    fun onResume()

    fun onPause()

    fun onStart()

    fun onStop()

    fun onDestroy()

    fun getMap(): IGEOMap?


}