package com.geoai.uimap.api

import android.os.Bundle
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LifecycleOwner

class GEOMapViewLifecycleObserver(val map: GEOMapManagerImpl, val savedInstanceState: Bundle?) : LifecycleEventObserver {

    override fun onStateChanged(source: LifecycleOwner, event: Lifecycle.Event) {
        when (event) {
            Lifecycle.Event.ON_CREATE -> map.onSaveInstanceState(savedInstanceState)
            Lifecycle.Event.ON_STOP -> map.onStop()
            Lifecycle.Event.ON_START -> map.onStart()
            Lifecycle.Event.ON_DESTROY -> map.onDestroy()
            Lifecycle.Event.ON_PAUSE -> map.onPause()
            Lifecycle.Event.ON_RESUME -> map.onResume()
            else -> {}
        }
    }
}