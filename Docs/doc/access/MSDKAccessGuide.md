# Android SDK 接入指南

欢迎使用我们的Android SDK！本文档将指导您如何将我们的SDK集成到您的Android应用程序中。

## 步骤 1：下载和安装 Android Studio

1. 访问 [Android Studio 官方网站](https://developer.android.com/studio)。
2. 根据您的操作系统，选择合适的下载选项并下载 Android Studio 安装程序。
3. 运行安装程序，并按照安装向导的指示完成 Android Studio 的安装。

![image-20231116160925561](https://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/img/image-20231116160925561.png)

## 步骤 2：创建新工程

1. 打开 Android Studio。
2. 在欢迎界面点击 "Start a new Android Studio project"，或者在菜单栏选择 "File" -> "New" -> "New Project"。
3. 在 "Configure your new project" 对话框中，填写应用程序的名称、包名等信息，并选择目标 Android 版本。
4. 选择适合您的项目需求的项目模板，并点击 "Next"。
5. 在 "Add an Activity to Mobile" 对话框中，选择您想要的Activity模板，并点击 "Next"。
6. 根据您的需要，配置 Activity 的名称、布局文件等信息，并点击 "Finish"。
7. Android Studio 将自动为您创建并打开新的工程。

![image-dji-new-project](https://terra-1-g.djicdn.com/71a7d383e71a4fb8887a310eb746b47f/msdk/Documentation/v5.3/empty%20project%20config.jpeg)

## 步骤 3：修改 build.gradle (Project) 文件

1. 在 Android Studio 的项目导航窗格中，找到并展开您的项目根目录。
2. 找到并打开名为 `build.gradle` 的文件。
3. 在 `allprojects ` 代码块中添加您需要集成的依赖仓库位置。例如：

   ```groovy
   allprojects {
        maven {
            allowInsecureProtocol = true
            url 'http://121.37.203.145:8081/repository/geoai-sonatype/'
        }
   }
   ```
4. 保存并关闭 build.gradle 文件。  
   **根据gradle版本不同，此处配置可能存在差异**

## 步骤 4：修改 build.gradle (Module: app) 文件
1. 在 android 项中的defaultConfig中配置 sdkversion 以及 ndk，配置 packagingOptions。  
   建议版本：
   1. compileSdkVersion 和 targetSdkVersion 为 31。
   2. minSdkVersion 的最低版本号需要升级到 24。
2. 配置ndk属性
   ```groovy
    ndk {
        abiFilters 'arm64-v8a'
    }
   ```
3. 配置MSDK依赖
   ```groovy
    //远程依赖-核心库
    implementation("com.geoai.mavlink.msdk-provided:mavsdk:v0.9.0-20231115.085616-1")
    //远程依赖-播放器
    implementation("com.geoai.mavlink.videoplayer-geoairtsp:mavsdk:v1.0.0")
    //远程依赖-APM框架
    implementation("com.geoai.mavlink.apm:mavsdk:v1.0.0")
    //远程依赖-视觉校准库
    implementation("com.geoai.mavlink.vision-calibration:mavsdk:v1.0.0")
   ```

## 步骤 5：修改 AndroidManifest.xml 文件
1. 在 Android Studio 的项目导航窗格中，找到并展开您的项目根目录。
2. 找到并打开名为 AndroidManifest.xml 的文件。
3. 参照添加 SDK 需要的最基础权限。
   ```xml
    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
    <uses-permission android:name="android.permission.INTERNET" />
    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
    <uses-permission android:name="android.permission.ACCESS_WIFI_STATE" />
    <uses-permission android:name="android.permission.READ_PHONE_STATE" />
    <uses-permission android:name="android.permission.READ_SETTINGS" />
    <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />
    <uses-permission android:name="android.permission.WRITE_SETTINGS" />
    <uses-permission android:name="android.permission.CHANGE_NETWORK_STATE" />
    <uses-permission android:name="android.permission.BLUETOOTH" />
   ```
4. 如果应用需要使用USB连接手持遥控器，请增加以下配置。
   ```xml
    <uses-feature
        android:name="android.hardware.usb.host"
        android:required="false" />
    <uses-feature
        android:name="android.hardware.usb.accessory"
        android:required="true" />
   ```
   ```xml
   <meta-data
        android:name="android.hardware.usb.action.USB_ACCESSORY_ATTACHED"
        android:resource="@xml/accessory_filter" />
   ```
6. 保存并关闭 AndroidManifest.xml 文件。

## 步骤 6：新建 MainActivity.kt 文件
1. 将MainActivity.kt的内容替换成如下代码。
   ```kotlin
       override fun onCreate() {
    
           super.onCreate()

           StarLinkClientManager.getInstance().register(this, externalCacheDir?.path)
         
           StarLinkClientManager.getInstance().openConnection()
   }
   ```
   
## 步骤7：监听飞行器连接
1. 当有符合协议版本的无人机设备接入后，SDK会通过异步回调的形式返回连接对象。
   ```kotlin
   StarLinkClientManager.getInstance().setProductConnectionListener(new IProductsConnectionListener())
   ```
2. 开发者也可用主动获取当前无人机的连接对象
   ```kotlin
   val productManager = StarLinkClientManager.getInstance().getProductManager()
   ```

## 步骤8：使用相关组件API接口
1. 用户通过以上步骤可以持有无人机设备连接对象`productManager`
2. 通过productManager，用户可以实例化出组件管理模块
   ```kotlin
   val productManager = StarLinkClientManager.getInstance().getProductManager()
   val flightControllerManager = productManager.getFlyControllerManager() //飞控模块
   val cameraManager = productManager.getCameraManager() //相机模块
   val airLinkManager = productManager.getAirlinkManager() //无线电模块
   ```
3. 针对特殊的管理模块，同时也提供单例形式获取实例化对象
   ```kotlin
   val airLinkManager = AirlinkManager.getInstance()
   ```

**恭喜！您已成功将我们的SDK集成到您的Android应用程序中，并熟练掌握相关接口的使用。  
您可以根据需要进行进一步的配置和开发。**

## 注意事项
- 在进行任何修改之前，建议您备份项目文件，以防意外情况发生。
- 请确保您已仔细阅读并理解我们SDK的文档和要求，并按照要求进行相应的配置和设置。

---

**如果您需要进一步的帮助或遇到任何问题，请参阅我们的官方文档或联系我们的技术支持团队。**

**祝您在集成我们的Android SDK时取得成功！如有任何疑问，请随时与我们联系。**