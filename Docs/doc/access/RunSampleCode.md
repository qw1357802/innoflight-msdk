# 运行实例代码

欢迎使用我们的安卓SDK工程！本文档将指导您如何运行示例代码并快速了解SDK的功能。

## 步骤 1：下载和导入工程

1. 确保您已安装最新版本的Android Studio。
2. 下载我们提供的SDK工程文件（例如：`SampleCode.zip`）。
3. 解压缩下载的ZIP文件，并记下解压后的工程文件夹路径。

![image-20231116160328640](https://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/img/image-20231116160328640.png)

## 步骤 2：导入工程

1. 打开Android Studio。
2. 在欢迎界面选择 "Open an existing Android Studio project"，或者在菜单栏选择 "File" -> "Open"。
3. 在弹出的窗口中，导航到解压后的工程文件夹，并选择工程根目录（包含 `build.gradle` 文件）。
4. 点击 "OK"，Android Studio 将加载并导入工程。

## 步骤 3：配置设备或模拟器

在运行示例代码之前，您需要配置一个真实的设备或使用Android模拟器。

1. 连接您的Android设备到计算机，并确保已启用开发者选项和USB调试。
2. 或者，打开Android Studio，选择 "Tools" -> "AVD Manager"，创建一个模拟器。

![image-20231116160517276](https://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/img/image-20231116160517276.png)

## 步骤 4：运行示例代码

1. 在Android Studio 中，打开 `MsdkApplication.kt` 文件。您可以在工程的 `com/example/geoaimavlink_android/activity/MsdkApplication.kt` 目录下找到此文件。
2. 在 `MsdkApplication.kt` 文件中，您将找到示例代码的入口点。
3. 连接您的设备或选择一个模拟器作为运行目标。
4. 点击工具栏上的 "Run" 按钮，或使用快捷键（例如：Shift + F10）来运行示例代码。
5. Android Studio 将构建和安装应用，并在您的设备或模拟器上运行示例代码。

![image-20231116160701371](https://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/img/image-20231116160701371.png)

恭喜！您已成功运行示例代码。您可以根据需要修改和扩展代码，以满足您的需求。

## 注意事项

- 在运行示例代码之前，请确保您已按照工程文档中的要求配置和设置工程。
- 如果遇到任何错误或问题，请参阅工程文档中的故障排除部分，或联系我们的技术支持团队。

**如果您需要进一步的帮助或有其他问题，请随时联系我们的技术支持团队。**
