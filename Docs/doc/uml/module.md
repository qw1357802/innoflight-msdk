# 子模块UML类图

## 飞控模块
```plantuml
@startuml
class com.geoai.mavlink.geoainet.flycontroller.info.AircraftDiagnosticsStateInfo {
+ String getName()
+ void setName(String)
+ String getMessage()
+ void setMessage(String)
+ String getDescription()
+ void setDescription(String)
+ long getAlertCode()
+ void setAlertCode(long)
+ long getAlertIndex()
+ void setAlertIndex(long)
+ HmsSeverityLevel getAlertLevel()
+ void setAlertLevel(HmsSeverityLevel)
+ long getTimestamp()
+ void setTimestamp(long)
+ long getHoldTime()
+ void setHoldTime(long)
+ short[] getArgs()
+ void setArgs(short[])
}
enum com.geoai.mavlink.geoainet.flycontroller.enums.AircraftDestinationType {
+  RTL_DEST_UNKNOWN
+  RTL_DEST_TYPE_HOME
+  RTL_DEST_TYPE_LAND
+  RTL_DEST_TYPE_RALLY
+ {static} AircraftDestinationType valueOf(int)
+ int value()
+ boolean _equals(int)
}
enum com.geoai.mavlink.geoainet.flycontroller.enums.AircraftReturnState {
+  RTL_STATE_NONE
+  RTL_STATE_CLIMB
+  RTL_STATE_RETURN
+  RTL_STATE_DESCEND
+  RTL_STATE_LOITER
+  RTL_STATE_TRANSITION_TO_MC
+  RTL_MOVE_TO_LAND_HOVER_VTOL
+  RTL_STATE_LAND
+  RTL_STATE_LANDED
+  RTL_STATE_HEAD_TO_CENTER
+  RTL_STATE_BY_PASS
+ {static} AircraftReturnState valueOf(int)
+ int value()
+ boolean _equals(int)
}
interface com.geoai.mavlink.geoainet.flycontroller.interfaces.IFlyControllerStateListener {
~ void onFlightControllerState(FlyControllerStateInfo)
}
class com.geoai.mavlink.geoainet.flycontroller.utils.EventReportBuilder {
+ RetEvent getEventMessage(AircraftDiagnosticsStateInfo)
}
class com.geoai.mavlink.geoainet.flycontroller.utils.EventReportBuilder$RetEvent {
+ long getEventCode()
+ void setEventCode(long)
+ String getName()
+ void setName(String)
+ String getMessage()
+ void setMessage(String)
+ String getSolution()
+ void setSolution(String)
+ String toString()
}
class com.geoai.mavlink.geoainet.flycontroller.utils.EventReportBuilder$Event {
+ String getName()
+ void setName(String)
+ String getMessage()
+ void setMessage(String)
+ String getDescription()
+ void setDescription(String)
+ List<Arguments> getArguments()
+ void setArguments(List<Arguments>)
}
class com.geoai.mavlink.geoainet.flycontroller.utils.EventReportBuilder$Event$Arguments {
+ String getType()
+ void setType(String)
+ String getName()
+ void setName(String)
}
interface com.geoai.mavlink.geoainet.flycontroller.interfaces.IDiagnosticsStateListener {
~ void onDiagnosticsStateListener(List<AircraftDiagnosticsStateInfo>)
}
class com.geoai.mavlink.geoainet.flycontroller.FlyControllerManager {
+ void setFlyControllerStateListener(IFlyControllerStateListener)
+ void setDiagnosticsStateListener(IDiagnosticsStateListener)
+ void onFlyControllerStateHeartBeat()
+ void onAircraftDiagnosticsHeartBeat()
+ void getJestonID(CompletionCallback.ICompletionCallbackWith<String>)
+ void getAircraftID(CompletionCallback.ICompletionCallbackWith<String>)
+ void startAircraftTakeoff(CompletionCallback.ICompletionCallback)
+ void startAircraftTakeoff(float,CompletionCallback.ICompletionCallback)
+ void stopAircraftGoHome(CompletionCallback.ICompletionCallback)
+ void startAircraftLand(CompletionCallback.ICompletionCallback)
+ void stopAircraftLand(CompletionCallback.ICompletionCallback)
+ void startAircraftGoHome(CompletionCallback.ICompletionCallback)
+ void startAircraftReturn(AircraftReturnMode,CompletionCallback.ICompletionCallback)
+ void startAircraftReturnAssignRally(int,CompletionCallback.ICompletionCallback)
+ void setGoHomeAltitude(int,CompletionCallback.ICompletionCallback)
+ void getGoHomeAltitude(CompletionCallback.ICompletionCallbackWith<Integer>)
+ void getGoHomeLocation(CompletionCallback.ICompletionCallbackWith<double[]>)
+ void stopAllAction(CompletionCallback.ICompletionCallback)
+ void startForceLanding(CompletionCallback.ICompletionCallback)
+ void setDroneArm(CompletionCallback.ICompletionCallback)
+ void setDroneDisarm(CompletionCallback.ICompletionCallback)
+ void setJoyStickEnable(boolean,CompletionCallback.ICompletionCallback)
+ void sendJoyStickControl(short,short,short,short,CompletionCallback.ICompletionCallback)
+ void getAlternateLandingPoints(CompletionCallback.ICompletionCallbackWith<Point[]>)
+ void setAlternateLandingPoints(Point[],CompletionCallback.ICompletionCallback)
+ void setFcAltitudeLimit(int,CompletionCallback.ICompletionCallback)
+ void getFcAltitudeLimit(CompletionCallback.ICompletionCallbackWith<Integer>)
+ void setFcDistanceLimit(int,CompletionCallback.ICompletionCallback)
+ void getFcDistanceLimit(CompletionCallback.ICompletionCallbackWith<Integer>)
+ void setSimulateMode(boolean,CompletionCallback.ICompletionCallback)
+ void getSimulateModeEnable(CompletionCallback.ICompletionCallbackWith<Boolean>)
+ void setHorizonSpeedLimit(int,CompletionCallback.ICompletionCallback)
+ void getHorizonSpeedLimit(CompletionCallback.ICompletionCallbackWith<Integer>)
+ void setConnectionFailSafeBehavior(AircraftFailSafeBehaviorMode,CompletionCallback.ICompletionCallback)
+ void getConnectionFailSafeBehavior(CompletionCallback.ICompletionCallbackWith<AircraftFailSafeBehaviorMode>)
+ void setHomeLocationUsingAircraftCurrentLocation(CompletionCallback.ICompletionCallback)
+ void setHomeLocation(double,double,CompletionCallback.ICompletionCallback)
+ void setPrecisionLandModeEnable(AircraftPrecisionLandMode,CompletionCallback.ICompletionCallback)
+ void getPrecisionLandModeEnable(CompletionCallback.ICompletionCallbackWith<AircraftPrecisionLandMode>)
}
enum com.geoai.mavlink.geoainet.flycontroller.enums.AircraftReturnMode {
+  RTL_TYPE_DEFAULT
+  RTL_TYPE_MISSION_LANDING
+  RTL_TYPE_MISSION_LANDING_REVERSED
+  RTL_TYPE_CLOSEST
+  RTL_TYPE_CLOSEST_HOME_LAND
+  RTL_TYPE_HOME
+  RTL_TYPE_LAND
+  RTL_TYPE_CLOSEST_RALLY
+  RTL_TYPE_ASSIGN_RALLY
+  RTL_TYPE_BY_PASS_CLOSEST_HOME_LAND
+  RTL_TYPE_BY_PASS_CLOSEST_HOME
+  RTL_TYPE_BY_PASS_LAND
+  RTL_TYPE_HOME_OR_RALLY
+ {static} AircraftReturnMode valueOf(int)
+ int value()
+ boolean _equals(int)
}
enum com.geoai.mavlink.geoainet.flycontroller.enums.HmsSeverityLevel {
+  MAV_SEVERITY_EMERGENCY
+  MAV_SEVERITY_ALERT
+  MAV_SEVERITY_CRITICAL
+  MAV_SEVERITY_ERROR
+  MAV_SEVERITY_WARNING
+  MAV_SEVERITY_NOTICE
+  MAV_SEVERITY_INFO
+  MAV_SEVERITY_DEBUG
+ {static} HmsSeverityLevel valueOf(int)
+ int value()
+ boolean _equals(int)
}
abstract class com.geoai.mavlink.geoainet.flycontroller.FlyControllerInfoParse {
# long mVirtualStickControlTimeStamp
# short mDroneControlDataSource
+ FlyControllerStateInfo getFlyControllerInfo()
+ RallyProtocolAgentUtil getMissionProtocolAgentUtil()
+ List<AircraftDiagnosticsStateInfo> getAircraftDiagnosticsInfo()
+ void parseMAVLinkMessage(MAVLinkMessage)
+ AircraftFlyMode buildFlyMode(int,int)
+ void setProductConnectedListener(boolean)
# void registerPusher_100()
# void registerPusher_1000()
+ {abstract}void onFlyControllerStateHeartBeat()
+ {abstract}void onAircraftDiagnosticsHeartBeat()
+ {abstract}void getJestonID(CompletionCallback.ICompletionCallbackWith<String>)
+ {abstract}void getAircraftID(CompletionCallback.ICompletionCallbackWith<String>)
}
enum com.geoai.mavlink.geoainet.flycontroller.enums.AircraftPrecisionLandMode {
+  NO_PRECISION_LANDING
+  AUTO_PRECISION_LANDING
+  REQUIRED_PRECISION_LANDING
+ {static} AircraftPrecisionLandMode valueOf(int)
+ int value()
+ boolean _equals(int)
}
interface com.geoai.mavlink.geoainet.flycontroller.interfaces.IBaseFlyControllerManager {
~ void setFlyControllerStateListener(IFlyControllerStateListener)
~ void setDiagnosticsStateListener(IDiagnosticsStateListener)
~ void startAircraftTakeoff(CompletionCallback.ICompletionCallback)
~ void startAircraftTakeoff(float,CompletionCallback.ICompletionCallback)
~ void startAircraftLand(CompletionCallback.ICompletionCallback)
~ void stopAircraftLand(CompletionCallback.ICompletionCallback)
~ void startAircraftGoHome(CompletionCallback.ICompletionCallback)
~ void startAircraftReturn(AircraftReturnMode,CompletionCallback.ICompletionCallback)
~ void startAircraftReturnAssignRally(int,CompletionCallback.ICompletionCallback)
~ void stopAircraftGoHome(CompletionCallback.ICompletionCallback)
~ void setGoHomeAltitude(int,CompletionCallback.ICompletionCallback)
~ void getGoHomeAltitude(CompletionCallback.ICompletionCallbackWith<Integer>)
~ void getGoHomeLocation(CompletionCallback.ICompletionCallbackWith<double[]>)
~ void stopAllAction(CompletionCallback.ICompletionCallback)
~ void startForceLanding(CompletionCallback.ICompletionCallback)
~ void setDroneArm(CompletionCallback.ICompletionCallback)
~ void setDroneDisarm(CompletionCallback.ICompletionCallback)
~ void setJoyStickEnable(boolean,CompletionCallback.ICompletionCallback)
~ void sendJoyStickControl(short,short,short,short,CompletionCallback.ICompletionCallback)
~ void getAlternateLandingPoints(CompletionCallback.ICompletionCallbackWith<Point[]>)
~ void setAlternateLandingPoints(Point[],CompletionCallback.ICompletionCallback)
~ void setFcAltitudeLimit(int,CompletionCallback.ICompletionCallback)
~ void getFcAltitudeLimit(CompletionCallback.ICompletionCallbackWith<Integer>)
~ void setFcDistanceLimit(int,CompletionCallback.ICompletionCallback)
~ void getFcDistanceLimit(CompletionCallback.ICompletionCallbackWith<Integer>)
~ void setSimulateMode(boolean,CompletionCallback.ICompletionCallback)
~ void getSimulateModeEnable(CompletionCallback.ICompletionCallbackWith<Boolean>)
~ void setHorizonSpeedLimit(int,CompletionCallback.ICompletionCallback)
~ void getHorizonSpeedLimit(CompletionCallback.ICompletionCallbackWith<Integer>)
~ void setConnectionFailSafeBehavior(AircraftFailSafeBehaviorMode,CompletionCallback.ICompletionCallback)
~ void getConnectionFailSafeBehavior(CompletionCallback.ICompletionCallbackWith<AircraftFailSafeBehaviorMode>)
~ void setHomeLocationUsingAircraftCurrentLocation(CompletionCallback.ICompletionCallback)
~ void setHomeLocation(double,double,CompletionCallback.ICompletionCallback)
~ void setPrecisionLandModeEnable(AircraftPrecisionLandMode,CompletionCallback.ICompletionCallback)
~ void getPrecisionLandModeEnable(CompletionCallback.ICompletionCallbackWith<AircraftPrecisionLandMode>)
}
enum com.geoai.mavlink.geoainet.flycontroller.enums.AircraftFailSafeBehaviorMode {
+  HOVER
+  LANDING
+  GO_HOME
+  UNKNOWN
+ {static} AircraftFailSafeBehaviorMode valueOf(int)
+ int value()
+ boolean _equals(int)
}
enum com.geoai.mavlink.geoainet.flycontroller.enums.AircraftFlyMode {
+  ORBIT
+  MANUAL
+  ATTI
+  JOYSTICK
+  STABILIZED
+  READY
+  AUTO_TAKEOFF
+  GPS_ATTI
+  GPS_WAYPOINT
+  GO_HOME
+  LANDING
+  PRECLAND
+  POSITION
+  UNKNOWN
+ String value()
+ boolean _equals(String)
}
class com.geoai.mavlink.geoainet.flycontroller.info.FlyControllerStateInfo {
+ GPSPosType getPosType()
+ void setPosType(GPSPosType)
+ int getFlyTime()
+ void setFlyTime(int)
+ double getAircraftLatitude()
+ void setAircraftLatitude(double)
+ double getAircraftLongitude()
+ void setAircraftLongitude(double)
+ double getAircraftAltitude()
+ void setAircraftAltitude(double)
+ float getAircraftPitch()
+ void setAircraftPitch(float)
+ float getAircraftRoll()
+ void setAircraftRoll(float)
+ float getAircraftYaw()
+ void setAircraftYaw(float)
+ float getAircraftVelocityX()
+ void setAircraftVelocityX(float)
+ float getAircraftVelocityY()
+ void setAircraftVelocityY(float)
+ float getAircraftVelocityZ()
+ void setAircraftVelocityZ(float)
+ int getAircraftGpsSatelliteNumber()
+ void setAircraftGpsSatelliteNumber(int)
+ AircraftFlyMode getFlyMode()
+ void setFlyMode(AircraftFlyMode)
+ boolean isMotorsOn()
+ void setMotorsOn(boolean)
+ boolean isFlying()
+ void setFlying(boolean)
+ double getAircraftHomeLatitude()
+ void setAircraftHomeLatitude(double)
+ double getAircraftHomeLongitude()
+ void setAircraftHomeLongitude(double)
+ double getAircraftReturnLatitude()
+ void setAircraftReturnLatitude(double)
+ double getAircraftReturnLongitude()
+ void setAircraftReturnLongitude(double)
+ AircraftDestinationType getAircraftReturnDestinationType()
+ void setAircraftReturnDestinationType(AircraftDestinationType)
+ AircraftReturnMode getAircraftReturnMode()
+ void setAircraftReturnMode(AircraftReturnMode)
+ AircraftReturnState getAircraftReturnState()
+ void setAircraftReturnState(AircraftReturnState)
+ float getAircraftHomeAltitude()
+ void setAircraftHomeAltitude(float)
}
class com.geoai.mavlink.geoainet.flycontroller.utils.NmeaGGABuilder {
+ {static} NmeaGGABuilder getInstance()
+ NmeaGGABuilder setTimestamp(long)
+ NmeaGGABuilder setLat(long)
+ NmeaGGABuilder setLon(long)
+ NmeaGGABuilder setPosType(int)
+ NmeaGGABuilder setSatCount(int)
+ NmeaGGABuilder setHdop(float)
+ NmeaGGABuilder setMslAltitude(float)
+ NmeaGGABuilder setEllipsoidAltitude(float)
+ String buildGGA()
}


java.io.Serializable <|.. com.geoai.mavlink.geoainet.flycontroller.info.AircraftDiagnosticsStateInfo
com.geoai.mavlink.geoainet.base.mavlinkcore.interfaces.IBaseStateListener <|-- com.geoai.mavlink.geoainet.flycontroller.interfaces.IFlyControllerStateListener
com.geoai.mavlink.geoainet.flycontroller.utils.EventReportBuilder +.. com.geoai.mavlink.geoainet.flycontroller.utils.EventReportBuilder$RetEvent
com.geoai.mavlink.geoainet.flycontroller.utils.EventReportBuilder +.. com.geoai.mavlink.geoainet.flycontroller.utils.EventReportBuilder$Event
com.geoai.mavlink.geoainet.flycontroller.utils.EventReportBuilder$Event +.. com.geoai.mavlink.geoainet.flycontroller.utils.EventReportBuilder$Event$Arguments
com.geoai.mavlink.geoainet.base.mavlinkcore.interfaces.IBaseStateListener <|-- com.geoai.mavlink.geoainet.flycontroller.interfaces.IDiagnosticsStateListener
com.geoai.mavlink.geoainet.flycontroller.interfaces.IBaseFlyControllerManager <|.. com.geoai.mavlink.geoainet.flycontroller.FlyControllerManager
com.geoai.mavlink.geoainet.flycontroller.FlyControllerInfoParse <|-- com.geoai.mavlink.geoainet.flycontroller.FlyControllerManager
com.geoai.mavlink.geoainet.base.BaseManager <|-- com.geoai.mavlink.geoainet.flycontroller.FlyControllerInfoParse
java.io.Serializable <|.. com.geoai.mavlink.geoainet.flycontroller.info.FlyControllerStateInfo
@enduml
```

## 相机模块
```plantuml
@startuml
enum com.geoai.mavlink.geoainet.camera.enums.CameraZoomRatio {
+  CAMERA_ELECTRON_ZOOM_X1
+  CAMERA_ELECTRON_ZOOM_X2
+  CAMERA_ELECTRON_ZOOM_X4
+  CAMERA_ELECTRON_ZOOM_X8
+  CAMERA_ELECTRON_ZOOM_X16
+  CAMERA_ELECTRON_ZOOM_UNKNOWN
+ boolean _equals(int)
+ {static} CameraZoomRatio find(int)
+ int getValue()
}
class com.geoai.mavlink.geoainet.camera.info.RecognizeStateInfo {
+ short x
+ short y
+ short width
+ short height
+ short order_num
+ short frame_id
+ short total_num
+ String name
+ short getX()
+ void setX(short)
+ short getY()
+ void setY(short)
+ short getWidth()
+ void setWidth(short)
+ short getHeight()
+ void setHeight(short)
+ short getOrder_num()
+ void setOrder_num(short)
+ short getFrame_id()
+ void setFrame_id(short)
+ short getTotal_num()
+ void setTotal_num(short)
+ String getName()
+ void setName(String)
}
enum com.geoai.mavlink.geoainet.camera.enums.CameraResolutionRatio {
+  CAPTURE_RESOLUTION_8000X6000
+  CAPTURE_RESOLUTION_4000X3000
+  CAPTURE_RESOLUTION_UNKNOWN
+ int getValue()
}
class com.geoai.mavlink.geoainet.camera.info.CameraHardwareInfo {
+ CameraISO getCameraISO()
+ void setCameraISO(CameraISO)
+ CameraShutterSpeed getCameraShutterSpeed()
+ void setCameraShutterSpeed(CameraShutterSpeed)
+ CameraAperture getCameraAperture()
+ void setCameraAperture(CameraAperture)
+ CameraExposure getCameraExposure()
+ void setCameraExposure(CameraExposure)
+ int getResolutionHeight()
+ void setResolutionHeight(int)
+ int getResolutionWidth()
+ void setResolutionWidth(int)
+ CameraMode getCameraMode()
+ void setCameraMode(CameraMode)
+ float getCameraZoomRatio()
+ void setCameraZoomRatio(float)
+ int getCameraFocusRing()
+ void setCameraFocusRing(int)
}
enum com.geoai.mavlink.geoainet.camera.enums.CameraShutterSpeed {
+  CAMERA_MANAGER_SHUTTER_SPEED_1_8000
+  CAMERA_MANAGER_SHUTTER_SPEED_1_6400
+  CAMERA_MANAGER_SHUTTER_SPEED_1_6000
+  CAMERA_MANAGER_SHUTTER_SPEED_1_5000
+  CAMERA_MANAGER_SHUTTER_SPEED_1_4000
+  CAMERA_MANAGER_SHUTTER_SPEED_1_3200
+  CAMERA_MANAGER_SHUTTER_SPEED_1_3000
+  CAMERA_MANAGER_SHUTTER_SPEED_1_2500
+  CAMERA_MANAGER_SHUTTER_SPEED_1_2000
+  CAMERA_MANAGER_SHUTTER_SPEED_1_1600
+  CAMERA_MANAGER_SHUTTER_SPEED_1_1500
+  CAMERA_MANAGER_SHUTTER_SPEED_1_1250
+  CAMERA_MANAGER_SHUTTER_SPEED_1_1000
+  CAMERA_MANAGER_SHUTTER_SPEED_1_800
+  CAMERA_MANAGER_SHUTTER_SPEED_1_725
+  CAMERA_MANAGER_SHUTTER_SPEED_1_640
+  CAMERA_MANAGER_SHUTTER_SPEED_1_500
+  CAMERA_MANAGER_SHUTTER_SPEED_1_400
+  CAMERA_MANAGER_SHUTTER_SPEED_1_350
+  CAMERA_MANAGER_SHUTTER_SPEED_1_320
+  CAMERA_MANAGER_SHUTTER_SPEED_1_250
+  CAMERA_MANAGER_SHUTTER_SPEED_1_240
+  CAMERA_MANAGER_SHUTTER_SPEED_1_200
+  CAMERA_MANAGER_SHUTTER_SPEED_1_180
+  CAMERA_MANAGER_SHUTTER_SPEED_1_160
+  CAMERA_MANAGER_SHUTTER_SPEED_1_125
+  CAMERA_MANAGER_SHUTTER_SPEED_1_120
+  CAMERA_MANAGER_SHUTTER_SPEED_1_100
+  CAMERA_MANAGER_SHUTTER_SPEED_1_90
+  CAMERA_MANAGER_SHUTTER_SPEED_1_80
+  CAMERA_MANAGER_SHUTTER_SPEED_1_60
+  CAMERA_MANAGER_SHUTTER_SPEED_1_50
+  CAMERA_MANAGER_SHUTTER_SPEED_1_40
+  CAMERA_MANAGER_SHUTTER_SPEED_1_30
+  CAMERA_MANAGER_SHUTTER_SPEED_1_25
+  CAMERA_MANAGER_SHUTTER_SPEED_1_20
+  CAMERA_MANAGER_SHUTTER_SPEED_1_15
+  CAMERA_MANAGER_SHUTTER_SPEED_1_12DOT5
+  CAMERA_MANAGER_SHUTTER_SPEED_1_10
+  CAMERA_MANAGER_SHUTTER_SPEED_1_8
+  CAMERA_MANAGER_SHUTTER_SPEED_1_6DOT25
+  CAMERA_MANAGER_SHUTTER_SPEED_1_5
+  CAMERA_MANAGER_SHUTTER_SPEED_1_4
+  CAMERA_MANAGER_SHUTTER_SPEED_1_3
+  CAMERA_MANAGER_SHUTTER_SPEED_1_2DOT5
+  CAMERA_MANAGER_SHUTTER_SPEED_1_2
+  CAMERA_MANAGER_SHUTTER_SPEED_1_1DOT67
+  CAMERA_MANAGER_SHUTTER_SPEED_1_1DOT25
+  CAMERA_MANAGER_SHUTTER_SPEED_1
+  CAMERA_MANAGER_SHUTTER_SPEED_1DOT3
+  CAMERA_MANAGER_SHUTTER_SPEED_1DOT6
+  CAMERA_MANAGER_SHUTTER_SPEED_2
+  CAMERA_MANAGER_SHUTTER_SPEED_2DOT5
+  CAMERA_MANAGER_SHUTTER_SPEED_3
+  CAMERA_MANAGER_SHUTTER_SPEED_3DOT2
+  CAMERA_MANAGER_SHUTTER_SPEED_4
+  CAMERA_MANAGER_SHUTTER_SPEED_5
+  CAMERA_MANAGER_SHUTTER_SPEED_6
+  CAMERA_MANAGER_SHUTTER_SPEED_7
+  CAMERA_MANAGER_SHUTTER_SPEED_8
+  CAMERA_MANAGER_SHUTTER_SPEED_9
+  CAMERA_MANAGER_SHUTTER_SPEED_10
+  CAMERA_MANAGER_SHUTTER_SPEED_13
+  CAMERA_MANAGER_SHUTTER_SPEED_15
+  CAMERA_MANAGER_SHUTTER_SPEED_20
+  CAMERA_MANAGER_SHUTTER_SPEED_25
+  CAMERA_MANAGER_SHUTTER_SPEED_30
+  CAMERA_MANAGER_SHUTTER_SPEED_UNKNOWN
+ boolean _equals(int)
+ {static} CameraShutterSpeed find(int)
+ int getValue()
}
interface com.geoai.mavlink.geoainet.camera.interfaces.ICameraNewGeneratedInfoListener {
~ void onResult(CameraNewGeneratedInfo)
}
enum com.geoai.mavlink.geoainet.camera.enums.CameraVideoStreamSource {
+  LIVEVIEW_CAMERA_SOURCE_DEFAULT
+  LIVEVIEW_CAMERA_SOURCE_NIGHT
+  LIVEVIEW_CAMERA_SOURCE_WIDE
+  LIVEVIEW_CAMERA_SOURCE_ZOOM
+  LIVEVIEW_CAMERA_SOURCE_IR
+ boolean _equals(int)
+ {static} CameraVideoStreamSource find(int)
+ int getValue()
}
class com.geoai.mavlink.geoainet.camera.info.CameraNewGeneratedInfo {
+ int getIndex()
+ void setIndex(int)
+ double getLatitude()
+ void setLatitude(double)
+ double getLongitude()
+ void setLongitude(double)
+ float getAltitude()
+ void setAltitude(float)
+ String getPath()
+ void setPath(String)
}
enum com.geoai.mavlink.geoainet.camera.enums.CameraVideoStatus {
+  CAMERA_VIDEO_IDLE
+  CAMERA_VIDEO_CAPTURING
+  CAMERA_VIDEO_UNKNOWN
+ boolean _equals(int)
+ {static} CameraVideoStatus find(int)
+ int getValue()
}
interface com.geoai.mavlink.geoainet.camera.interfaces.ICameraHardwareState {
~ void onResult(CameraHardwareInfo)
}
enum com.geoai.mavlink.geoainet.camera.enums.CameraAperture {
+  CAMERA_APERTURE_F_1_DOT_6
+  CAMERA_APERTURE_F_1_DOT_7
+  CAMERA_APERTURE_F_1_DOT_8
+  CAMERA_APERTURE_F_2
+  CAMERA_APERTURE_F_2_DOT_2
+  CAMERA_APERTURE_F_2_DOT_4
+  CAMERA_APERTURE_F_2_DOT_5
+  CAMERA_APERTURE_F_2_DOT_6
+  CAMERA_APERTURE_F_2_DOT_8
+  CAMERA_APERTURE_F_3_DOT_2
+  CAMERA_APERTURE_F_3_DOT_4
+  CAMERA_APERTURE_F_3_DOT_5
+  CAMERA_APERTURE_F_4
+  CAMERA_APERTURE_F_4_DOT_5
+  CAMERA_APERTURE_F_4_DOT_8
+  CAMERA_APERTURE_F_5
+  CAMERA_APERTURE_F_5_DOT_6
+  CAMERA_APERTURE_F_6_DOT_3
+  CAMERA_APERTURE_F_6_DOT_8
+  CAMERA_APERTURE_F_7_DOT_1
+  CAMERA_APERTURE_F_8
+  CAMERA_APERTURE_F_9
+  CAMERA_APERTURE_F_9_DOT_6
+  CAMERA_APERTURE_F_10
+  CAMERA_APERTURE_F_11
+  CAMERA_APERTURE_F_13
+  CAMERA_APERTURE_F_14
+  CAMERA_APERTURE_F_16
+  CAMERA_APERTURE_F_18
+  CAMERA_APERTURE_F_19
+  CAMERA_APERTURE_F_20
+  CAMERA_APERTURE_F_22
+  CAMERA_APERTURE_F_UNKNOWN
+ boolean _equals(int)
+ {static} CameraAperture find(int)
+ int getValue()
}
interface com.geoai.mavlink.geoainet.camera.interfaces.IRecognizeStateListener {
~ void onRecognizeState(RecognizeStateInfo)
}
abstract class com.geoai.mavlink.geoainet.camera.CameraParse {
+ RecognizeStateInfo getRecognizeStateInfo()
+ CameraNewGeneratedInfo getMediaNewGeneratedInfo()
+ CameraSystemInfo getCameraSystemInfo()
+ CameraHardwareInfo getCameraHardwareInfo()
+ void parseMAVLinkMessage(MAVLinkMessage)
# void registerPusher_5000()
+ {abstract}void onRecognizeStateHeartBeat()
+ {abstract}void onMediaNewGeneratedHeartBeat()
+ {abstract}void onCameraSystemHeartBeat()
+ {abstract}void onCameraHardwareHeartBeat()
}
enum com.geoai.mavlink.geoainet.camera.enums.CameraMode {
+  CAMERA_MODE_IMAGE
+  CAMERA_MODE_VIDEO
+  CAMERA_MODE_UNKNOWN
+ boolean _equals(int)
+ {static} CameraMode find(int)
+ int getValue()
}
enum com.geoai.mavlink.geoainet.camera.enums.CameraImageStatus {
+  CAMERA_IMAGE_IDLE
+  CAMERA_IMAGE_CAPTURING
+  CAMERA_IMAGE_INTERVAL_IDLE
+  CAMERA_IMAGE_INTERVAL_CAPTURING
+  CAMERA_IMAGE_UNKNOWN
+ boolean _equals(int)
+ {static} CameraImageStatus find(int)
+ int getValue()
}
enum com.geoai.mavlink.geoainet.camera.enums.CameraISO {
+  CAMERA_ISO_AUTO
+  CAMERA_ISO_100
+  CAMERA_ISO_200
+  CAMERA_ISO_400
+  CAMERA_ISO_800
+  CAMERA_ISO_1600
+  CAMERA_ISO_3200
+  CAMERA_ISO_6400
+  CAMERA_ISO_FIXED
+ boolean _equals(int)
+ {static} CameraISO find(int)
+ int getValue()
}
enum com.geoai.mavlink.geoainet.camera.enums.CameraVideoStreamBitrate {
+  VIDEOSTREAM_BITRATE_UNKNOWN
+  VIDEOSTREAM_BITRATE_1Mbps
+  VIDEOSTREAM_BITRATE_2Mbps
+  VIDEOSTREAM_BITRATE_3Mbps
+  VIDEOSTREAM_BITRATE_4Mbps
+  VIDEOSTREAM_BITRATE_5Mbps
+  VIDEOSTREAM_BITRATE_6Mbps
+  VIDEOSTREAM_BITRATE_7Mbps
+  VIDEOSTREAM_BITRATE_8Mbps
+ boolean _equals(int)
+ {static} CameraVideoStreamBitrate find(int)
+ int getValue()
}
interface com.geoai.mavlink.geoainet.camera.interfaces.ICameraManager {
~ void getCameraVideoStreamSource(CompletionCallback.ICompletionCallbackWith<CameraVideoStreamSource>)
~ void setCameraVideoStreamSource(CameraVideoStreamSource,CompletionCallback.ICompletionCallback)
~ void getCameraVideoStreamResolution(CompletionCallback.ICompletionCallbackWith<CameraVideoStreamResolution>)
~ void setCameraVideoStreamResolution(CameraVideoStreamResolution,CompletionCallback.ICompletionCallback)
~ void getCameraVideoStreamBitrate(CompletionCallback.ICompletionCallbackWith<CameraVideoStreamBitrate>)
~ void setCameraVideoStreamBitrate(CameraVideoStreamBitrate,CompletionCallback.ICompletionCallback)
~ void getCameraName(CompletionCallback.ICompletionCallbackWithParam)
~ void setCameraHardwareStateListener(ICameraHardwareState)
~ void setCameraSystemStateListener(ICameraSystemState)
~ void setCameraNewGeneratedInfoCallback(ICameraNewGeneratedInfoListener)
~ void setRecognizeStateListener(IRecognizeStateListener)
~ void startSingleCapture(CompletionCallback.ICompletionCallback)
~ void startComboCapture(int,CompletionCallback.ICompletionCallback)
~ void startIntervalCapture(int,CompletionCallback.ICompletionCallback)
~ void stopIntervalCapture(CompletionCallback.ICompletionCallback)
~ void startRecord(CompletionCallback.ICompletionCallback)
~ void stopRecord(CompletionCallback.ICompletionCallback)
~ void setCameraMode(CameraMode,CompletionCallback.ICompletionCallback)
~ void getCameraMode(CompletionCallback.ICompletionCallbackWith<CameraMode>)
~ void setZoomRatio(float,CompletionCallback.ICompletionCallback)
~ void getZoomRatio(CompletionCallback.ICompletionCallbackWith<Float>)
~ void setFocusRing(boolean,int,CompletionCallback.ICompletionCallback)
~ void getFocusRing(CompletionCallback.ICompletionCallbackWith<Integer>)
~ void setISO(CameraISO,CompletionCallback.ICompletionCallback)
~ void setAperture(CameraAperture,CompletionCallback.ICompletionCallback)
~ void setShutterSpeed(CameraShutterSpeed,CompletionCallback.ICompletionCallback)
~ void setExposure(CameraExposure,CompletionCallback.ICompletionCallback)
~ void resetCameraConfigure(CompletionCallback.ICompletionCallback)
}
class com.geoai.mavlink.geoainet.camera.info.CameraSystemInfo {
+ CameraImageStatus getImageStatus()
+ void setImageStatus(CameraImageStatus)
+ CameraVideoStatus getVideoStatus()
+ void setVideoStatus(CameraVideoStatus)
+ float getImageIntervalUnit()
+ void setImageIntervalUnit(float)
+ long getRecordingTimeMs()
+ void setRecordingTimeMs(long)
+ float getAvailableCapacity()
+ void setAvailableCapacity(float)
}
interface com.geoai.mavlink.geoainet.camera.interfaces.ICameraSystemState {
~ void onResult(CameraSystemInfo)
}
class com.geoai.mavlink.geoainet.camera.CameraManager {
+ void setCameraHardwareStateListener(ICameraHardwareState)
+ void setCameraSystemStateListener(ICameraSystemState)
+ void setCameraNewGeneratedInfoCallback(ICameraNewGeneratedInfoListener)
+ void setRecognizeStateListener(IRecognizeStateListener)
+ void getCameraVideoStreamSource(CompletionCallback.ICompletionCallbackWith<CameraVideoStreamSource>)
+ void setCameraVideoStreamSource(CameraVideoStreamSource,CompletionCallback.ICompletionCallback)
+ void getCameraVideoStreamResolution(CompletionCallback.ICompletionCallbackWith<CameraVideoStreamResolution>)
+ void setCameraVideoStreamResolution(CameraVideoStreamResolution,CompletionCallback.ICompletionCallback)
+ void getCameraVideoStreamBitrate(CompletionCallback.ICompletionCallbackWith<CameraVideoStreamBitrate>)
+ void setCameraVideoStreamBitrate(CameraVideoStreamBitrate,CompletionCallback.ICompletionCallback)
+ void getCameraName(CompletionCallback.ICompletionCallbackWithParam)
+ void startSingleCapture(CompletionCallback.ICompletionCallback)
+ void startComboCapture(int,CompletionCallback.ICompletionCallback)
+ void startIntervalCapture(int,CompletionCallback.ICompletionCallback)
+ void stopIntervalCapture(CompletionCallback.ICompletionCallback)
+ void startRecord(CompletionCallback.ICompletionCallback)
+ void stopRecord(CompletionCallback.ICompletionCallback)
+ void setCameraMode(CameraMode,CompletionCallback.ICompletionCallback)
+ void getCameraMode(CompletionCallback.ICompletionCallbackWith<CameraMode>)
+ void setZoomRatio(float,CompletionCallback.ICompletionCallback)
+ void getZoomRatio(CompletionCallback.ICompletionCallbackWith<Float>)
+ void setFocusRing(boolean,int,CompletionCallback.ICompletionCallback)
+ void getFocusRing(CompletionCallback.ICompletionCallbackWith<Integer>)
+ void setISO(CameraISO,CompletionCallback.ICompletionCallback)
+ void setAperture(CameraAperture,CompletionCallback.ICompletionCallback)
+ void setShutterSpeed(CameraShutterSpeed,CompletionCallback.ICompletionCallback)
+ void setExposure(CameraExposure,CompletionCallback.ICompletionCallback)
+ void resetCameraConfigure(CompletionCallback.ICompletionCallback)
+ void onRecognizeStateHeartBeat()
+ void onMediaNewGeneratedHeartBeat()
+ void onCameraSystemHeartBeat()
+ void onCameraHardwareHeartBeat()
}
enum com.geoai.mavlink.geoainet.camera.enums.CameraVideoStreamResolution {
+  VIDEOSTREAM_RESOLUTION_UNKNOWN
+  VIDEOSTREAM_RESOLUTION_1080P
+  VIDEOSTREAM_RESOLUTION_720P
+  VIDEOSTREAM_RESOLUTION_VGA
+ boolean _equals(int)
+ {static} CameraVideoStreamResolution find(int)
+ int getValue()
}
enum com.geoai.mavlink.geoainet.camera.enums.CameraExposure {
+  CAMERA_EXPOSURE_COMPENSATION_N_5_0
+  CAMERA_EXPOSURE_COMPENSATION_N_4_7
+  CAMERA_EXPOSURE_COMPENSATION_N_4_3
+  CAMERA_EXPOSURE_COMPENSATION_N_4_0
+  CAMERA_EXPOSURE_COMPENSATION_N_3_7
+  CAMERA_EXPOSURE_COMPENSATION_N_3_3
+  CAMERA_EXPOSURE_COMPENSATION_N_3_0
+  CAMERA_EXPOSURE_COMPENSATION_N_2_7
+  CAMERA_EXPOSURE_COMPENSATION_N_2_3
+  CAMERA_EXPOSURE_COMPENSATION_N_2_0
+  CAMERA_EXPOSURE_COMPENSATION_N_1_7
+  CAMERA_EXPOSURE_COMPENSATION_N_1_3
+  CAMERA_EXPOSURE_COMPENSATION_N_1_0
+  CAMERA_EXPOSURE_COMPENSATION_N_0_7
+  CAMERA_EXPOSURE_COMPENSATION_N_0_3
+  CAMERA_EXPOSURE_COMPENSATION_N_0_0
+  CAMERA_EXPOSURE_COMPENSATION_P_0_3
+  CAMERA_EXPOSURE_COMPENSATION_P_0_7
+  CAMERA_EXPOSURE_COMPENSATION_P_1_0
+  CAMERA_EXPOSURE_COMPENSATION_P_1_3
+  CAMERA_EXPOSURE_COMPENSATION_P_1_7
+  CAMERA_EXPOSURE_COMPENSATION_P_2_0
+  CAMERA_EXPOSURE_COMPENSATION_P_2_3
+  CAMERA_EXPOSURE_COMPENSATION_P_2_7
+  CAMERA_EXPOSURE_COMPENSATION_P_3_0
+  CAMERA_EXPOSURE_COMPENSATION_P_3_3
+  CAMERA_EXPOSURE_COMPENSATION_P_3_7
+  CAMERA_EXPOSURE_COMPENSATION_P_4_0
+  CAMERA_EXPOSURE_COMPENSATION_P_4_3
+  CAMERA_EXPOSURE_COMPENSATION_P_4_7
+  CAMERA_EXPOSURE_COMPENSATION_P_5_0
+  CAMERA_EXPOSURE_COMPENSATION_FIXED
+ boolean _equals(int)
+ {static} CameraExposure find(int)
+ int getValue()
}


com.geoai.mavlink.geoainet.base.mavlinkcore.interfaces.IBaseStateListener <|-- com.geoai.mavlink.geoainet.camera.interfaces.ICameraNewGeneratedInfoListener
com.geoai.mavlink.geoainet.base.mavlinkcore.interfaces.IBaseStateListener <|-- com.geoai.mavlink.geoainet.camera.interfaces.ICameraHardwareState
com.geoai.mavlink.geoainet.base.mavlinkcore.interfaces.IBaseStateListener <|-- com.geoai.mavlink.geoainet.camera.interfaces.IRecognizeStateListener
com.geoai.mavlink.geoainet.base.BaseManager <|-- com.geoai.mavlink.geoainet.camera.CameraParse
com.geoai.mavlink.geoainet.base.mavlinkcore.interfaces.IBaseStateListener <|-- com.geoai.mavlink.geoainet.camera.interfaces.ICameraSystemState
com.geoai.mavlink.geoainet.camera.interfaces.ICameraManager <|.. com.geoai.mavlink.geoainet.camera.CameraManager
com.geoai.mavlink.geoainet.camera.CameraParse <|-- com.geoai.mavlink.geoainet.camera.CameraManager
@enduml
```

## 电池模块
```plantuml
@startuml
abstract class com.geoai.mavlink.geoainet.battery.BatteryParse {
# ConcurrentHashMap<Integer,BatteryStateInfo> mBatteryGroupInfo
# long getBatteryDamagedCode()
+ ArrayList<BatteryStateInfo> getBatteryInfo()
+ void parseMAVLinkMessage(MAVLinkMessage)
# {abstract}void onBatteryStateHeartBeat()
}
enum com.geoai.mavlink.geoainet.battery.enums.BatteryDamagedState {
+  BATTERY_FAULT_DEEP_DISCHARGE
+  BATTERY_FAULT_SPIKES
+  BATTERY_FAULT_CELL_FAIL
+  BATTERY_FAULT_OVER_CURRENT
+  BATTERY_FAULT_OVER_TEMPERATURE
+  BATTERY_FAULT_UNDER_TEMPERATURE
+  BATTERY_FAULT_INCOMPATIBLE_VOLTAGE
+  BATTERY_FAULT_INCOMPATIBLE_FIRMWARE
+  BATTERY_FAULT_INCOMPATIBLE_CELLS_CONFIGURATION
}
class com.geoai.mavlink.geoainet.battery.info.ExtBatteryStateInfo {
+ String getSerialNumber()
+ void setSerialNumber(String)
+ int getCellsInSeries()
+ void setCellsInSeries(int)
+ long getCapacityFullSpecification()
+ void setCapacityFullSpecification(long)
+ long getCapacityFull()
+ void setCapacityFull(long)
+ int getCycleCount()
+ void setCycleCount(int)
+ int getDischargeMinimumVoltage()
+ void setDischargeMinimumVoltage(int)
+ int getChargingMinimumVoltage()
+ void setChargingMinimumVoltage(int)
+ int getChargingMaximumVoltage()
+ void setChargingMaximumVoltage(int)
+ long getDischargeMaximumCurrent()
+ void setDischargeMaximumCurrent(long)
}
enum com.geoai.mavlink.geoainet.battery.enums.BatteryChargeState {
+  BATTERY_CHARGE_STATE_UNDEFINED
+  BATTERY_CHARGE_STATE_OK
+  BATTERY_CHARGE_STATE_LOW
+  BATTERY_CHARGE_STATE_CRITICAL
+  BATTERY_CHARGE_STATE_EMERGENCY
+  BATTERY_CHARGE_STATE_FAILED
+  BATTERY_CHARGE_STATE_UNHEALTHY
+  BATTERY_CHARGE_STATE_CHARGING
+ {static} BatteryChargeState valueOf(int)
+ int value()
+ boolean _equals(int)
}
interface com.geoai.mavlink.geoainet.battery.interfaces.IBatteryStateListener {
~ void onBatteryState(ArrayList<BatteryStateInfo>)
}
class com.geoai.mavlink.geoainet.battery.BatteryManager {
+ void setBatteryStateListener(IBatteryStateListener)
+ int getBatteryCellsCount()
+ List<BatteryDamagedState> getBatteryDamagedInfo()
+ void getBatteryCriticalThreshold(CompletionCallback.ICompletionCallbackWith<Float>)
+ void setBatteryCriticalThreshold(float,CompletionCallback.ICompletionCallback)
+ void getBatteryEmergencyThreshold(CompletionCallback.ICompletionCallbackWith<Float>)
+ void setBatteryEmergencyThreshold(float,CompletionCallback.ICompletionCallback)
+ void getBatteryLowThreshold(CompletionCallback.ICompletionCallbackWith<Float>)
+ void setBatteryLowThreshold(float,CompletionCallback.ICompletionCallback)
+ void setSmartReturnToHomeEnabled(boolean,CompletionCallback.ICompletionCallback)
+ void getSmartReturnToHomeEnabled(CompletionCallback.ICompletionCallbackWith<Boolean>)
# void onBatteryStateHeartBeat()
}
class com.geoai.mavlink.geoainet.battery.info.BatteryStateInfo {
+ boolean isBatteryDamaged()
+ void setBatteryDamaged(boolean)
+ BatteryChargeState getBatteryChargeState()
+ void setBatteryChargeState(BatteryChargeState)
+ float getBatteryTemperature()
+ void setBatteryTemperature(float)
+ float getTotalVoltage()
+ void setTotalVoltage(float)
+ int[] getCellVoltage()
+ void setCellVoltage(int[])
+ int getBatteryDischarge()
+ void setBatteryDischarge(int)
+ int getBatteryChargeConsumed()
+ void setBatteryChargeConsumed(int)
+ int getBatteryEnergyConsumed()
+ void setBatteryEnergyConsumed(int)
+ int getBatteryRemainingPercentage()
+ void setBatteryRemainingPercentage(int)
+ int getBatteryRemainingTimes()
+ void setBatteryRemainingTimes(int)
+ int getAveragePower()
+ void setAveragePower(int)
+ int getCycleCount()
+ void setCycleCount(int)
+ int getBatteryHealth()
+ void setBatteryHealth(int)
+ int getBatteryFullChargeCapacity()
+ void setBatteryFullChargeCapacity(int)
}
interface com.geoai.mavlink.geoainet.battery.interfaces.IBatteryManager {
~ void setBatteryStateListener(IBatteryStateListener)
~ int getBatteryCellsCount()
~ List<BatteryDamagedState> getBatteryDamagedInfo()
~ void getBatteryCriticalThreshold(CompletionCallback.ICompletionCallbackWith<Float>)
~ void setBatteryCriticalThreshold(float,CompletionCallback.ICompletionCallback)
~ void getBatteryEmergencyThreshold(CompletionCallback.ICompletionCallbackWith<Float>)
~ void setBatteryEmergencyThreshold(float,CompletionCallback.ICompletionCallback)
~ void getBatteryLowThreshold(CompletionCallback.ICompletionCallbackWith<Float>)
~ void setBatteryLowThreshold(float,CompletionCallback.ICompletionCallback)
~ void setSmartReturnToHomeEnabled(boolean,CompletionCallback.ICompletionCallback)
~ void getSmartReturnToHomeEnabled(CompletionCallback.ICompletionCallbackWith<Boolean>)
}


com.geoai.mavlink.geoainet.base.BaseManager <|-- com.geoai.mavlink.geoainet.battery.BatteryParse
com.geoai.mavlink.geoainet.base.mavlinkcore.interfaces.IBaseStateListener <|-- com.geoai.mavlink.geoainet.battery.interfaces.IBatteryStateListener
com.geoai.mavlink.geoainet.battery.interfaces.IBatteryManager <|.. com.geoai.mavlink.geoainet.battery.BatteryManager
com.geoai.mavlink.geoainet.battery.BatteryParse <|-- com.geoai.mavlink.geoainet.battery.BatteryManager
@enduml
```

## 云台模块
```plantuml
@startuml
class com.geoai.mavlink.geoainet.gimbal.GimbalManager {
+ void setGimbalStateListener(IGimbalStateListener)
+ void setGimbalPitch(float,CompletionCallback.ICompletionCallback)
+ void setGimbalAngleControl(boolean,float,float,CompletionCallback.ICompletionCallback)
+ void setGimbalAngleIncrementControl(float,float,CompletionCallback.ICompletionCallback)
+ void setGimbalSpeedControl(float,float,CompletionCallback.ICompletionCallback)
+ void resetGimbal(CompletionCallback.ICompletionCallback)
+ void resetGimbalAndDown(CompletionCallback.ICompletionCallback)
+ void setGimbalPitchRate(int,CompletionCallback.ICompletionCallback)
+ void getGimbalPitchRate(CompletionCallback.ICompletionCallbackWith<Integer>)
+ void setGimbalYawRate(int,CompletionCallback.ICompletionCallback)
+ void getGimbalYawRate(CompletionCallback.ICompletionCallbackWith<Integer>)
# void onGimbalStateHeartBeat()
}
interface com.geoai.mavlink.geoainet.gimbal.interfaces.IGimbalStateListener {
~ void onGimbalState(GimbalStateInfo)
}
interface com.geoai.mavlink.geoainet.gimbal.interfaces.IBaseGimbalManager {
~ void setGimbalStateListener(IGimbalStateListener)
~ void setGimbalSpeedControl(float,float,CompletionCallback.ICompletionCallback)
~ void setGimbalAngleControl(boolean,float,float,CompletionCallback.ICompletionCallback)
~ void setGimbalAngleIncrementControl(float,float,CompletionCallback.ICompletionCallback)
~ void setGimbalPitch(float,CompletionCallback.ICompletionCallback)
~ void resetGimbal(CompletionCallback.ICompletionCallback)
~ void resetGimbalAndDown(CompletionCallback.ICompletionCallback)
~ void setGimbalPitchRate(int,CompletionCallback.ICompletionCallback)
~ void getGimbalPitchRate(CompletionCallback.ICompletionCallbackWith<Integer>)
~ void setGimbalYawRate(int,CompletionCallback.ICompletionCallback)
~ void getGimbalYawRate(CompletionCallback.ICompletionCallbackWith<Integer>)
}
class com.geoai.mavlink.geoainet.gimbal.info.GimbalStateInfo {
+ Integer getPrimaryControlSystemID()
+ void setPrimaryControlSystemID(int)
+ boolean isConnected()
+ void setConnected(boolean)
+ int getDeviceID()
+ void setDeviceID(int)
+ float getPitchAngle()
+ void setPitchAngle(float)
+ float getYawAngle()
+ void setYawAngle(float)
+ float getRollAngle()
+ void setRollAngle(float)
+ float getPitchRadian()
+ void setPitchRadian(float)
+ float getYawRadian()
+ void setYawRadian(float)
+ float getRollRadian()
+ void setRollRadian(float)
+ void setAngularVelocityX(Float)
+ void setAngularVelocityY(Float)
+ void setAngularVelocityZ(Float)
+ float getAngularVelocityX()
+ void setAngularVelocityX(float)
+ float getAngularVelocityY()
+ void setAngularVelocityY(float)
+ float getAngularVelocityZ()
+ void setAngularVelocityZ(float)
}
abstract class com.geoai.mavlink.geoainet.gimbal.GimbalInfoParse {
# long mObtainGimbalManagerControllerTimeStamp
+ GimbalStateInfo getGimbalStateInfo()
+ void parseMAVLinkMessage(MAVLinkMessage)
# {abstract}void onGimbalStateHeartBeat()
# void registerPusher_1000()
}


com.geoai.mavlink.geoainet.gimbal.interfaces.IBaseGimbalManager <|.. com.geoai.mavlink.geoainet.gimbal.GimbalManager
com.geoai.mavlink.geoainet.gimbal.GimbalInfoParse <|-- com.geoai.mavlink.geoainet.gimbal.GimbalManager
com.geoai.mavlink.geoainet.base.mavlinkcore.interfaces.IBaseStateListener <|-- com.geoai.mavlink.geoainet.gimbal.interfaces.IGimbalStateListener
com.geoai.mavlink.geoainet.base.BaseManager <|-- com.geoai.mavlink.geoainet.gimbal.GimbalInfoParse
@enduml
```

## RTK模块
```plantuml
@startuml
class com.geoai.mavlink.geoainet.rtk.component.NtripClientRtkManager {
+ void setAccount(NtripClientAccount)
+ NtripClientAccount getAccount()
+ void connect()
+ void disconnect()
+ void sendGGA(byte[])
}
class com.geoai.mavlink.geoainet.rtk.info.rtcm.RtcmConstant {
+ {static} String GPGGA
+ {static} String GNGGA
+ {static} String GPGSA
+ {static} String GNGSA
+ {static} String GPGST
+ {static} String GNGST
+ {static} String GPRMC
+ {static} String GNRMC
+ {static} String GPGSV
+ {static} String GLGSV
+ {static} String GAGSV
+ {static} String BDGSV
+ {static} String GPS
+ {static} String GLO
+ {static} String GAL
+ {static} String BDS
+ {static} int ARP_1005
+ {static} int ARP_1006
+ {static} int MSM4_GPS
+ {static} int MSM4_GLO
+ {static} int MSM4_GAL
+ {static} int MSM4_BDS
}
enum com.geoai.mavlink.geoainet.rtk.enums.GPSPosType {
+  GPS_FIX_TYPE_NO_GPS
+  GPS_FIX_TYPE_NO_FIX
+  GPS_FIX_TYPE_2D_FIX
+  GPS_FIX_TYPE_3D_FIX
+  GPS_FIX_TYPE_DGPS
+  GPS_FIX_TYPE_RTK_FLOAT
+  GPS_FIX_TYPE_RTK_FIXED
+  GPS_FIX_TYPE_STATIC
+  GPS_FIX_TYPE_PPP
+ {static} GPSPosType valueOf(int)
+ int value()
+ boolean _equals(int)
}
interface com.geoai.mavlink.geoainet.rtk.component.base.IHardwareRTKListener {
~ void onRTCMResult(byte[])
~ void onResult(RtkServiceCode)
}
interface com.geoai.mavlink.geoainet.rtk.component.base.INetworkRTKListener {
~ void onRTCMResult(byte[])
~ void onResult(RtkServiceCode)
}
class com.geoai.mavlink.geoainet.rtk.component.account.NtripClientAccount {
+ {static} Creator<NtripClientAccount> CREATOR
+ String getIpAddress()
+ void setIpAddress(String)
+ int getPort()
+ void setPort(int)
+ String getUsername()
+ void setUsername(String)
+ String getPassword()
+ void setPassword(String)
+ String getMountPoint()
+ void setMountPoint(String)
+ int describeContents()
+ void writeToParcel(Parcel,int)
}
class com.geoai.mavlink.geoainet.rtk.info.rtcm.sarp.GnssData {
+ long getUtc()
+ void setUtc(long)
+ int getSatellite()
+ void setSatellite(int)
+ void addSatelliteData(SatelliteData)
+ SatelliteData getSatelliteData(String,int)
+ HashMap<Integer,SatelliteData> getSatelliteDatas(String)
}
class com.geoai.mavlink.geoainet.rtk.utils.RTCMParse {
+ void buildMavlinkRtcmData(byte[],OnRealRtcmPayload2MavlinkDataCallback)
+ BaseStationInfo parseRTCM(byte[])
}
interface com.geoai.mavlink.geoainet.rtk.utils.RTCMParse$OnRealRtcmPayload2MavlinkDataCallback {
~ void onCall(short,byte[])
}
class com.geoai.mavlink.geoainet.rtk.component.account.SixentsAccount {
+ {static} Creator<SixentsAccount> CREATOR
+ int describeContents()
+ void writeToParcel(Parcel,int)
+ String getDeviceId()
+ void setDeviceId(String)
+ String getDeviceType()
+ void setDeviceType(String)
+ String getKey()
+ void setKey(String)
+ String getSecret()
+ void setSecret(String)
+ {static} Creator<SixentsAccount> getCREATOR()
}
abstract class com.geoai.mavlink.geoainet.rtk.RtkParse {
# NtripClientRtkManager mNtripClientRtkManager
# QxRtkManager mQxRtkManager
# SixentsRtkManager mSixentsRtkManager
# BluetoothRtkManager mBluetoothRtkManager
# CmccRtkManager mCmccRtkManager
# RTCMParse mRtcmParse
+ RtkEntryInfo getRtkStateInfo()
+ void parseMAVLinkMessage(MAVLinkMessage)
+ void onRTCMResult(byte[])
+ void onResult(RtkServiceCode)
# {abstract}void onRtkStateHeartBeat()
# {abstract}void sendRTCM(byte[])
# void registerPusher_500()
# void registerPusher_1000()
}
interface com.geoai.mavlink.geoainet.rtk.interfaces.IBaseRtkManager {
~ void sendRTCM(byte[])
~ void setRtkStateListener(IRtkStateListener)
~ void setRtkEnable(boolean,CompletionCallback.ICompletionCallback)
~ boolean getRtkEnable()
~ void setRtkComponentType(RtkComponentType)
~ RtkComponentType getRtkComponentType()
~ INetworkRTKManager<?> getNetWorkRtkManager()
~ IHardwareRTKManager<?> getHardwareRtkManager()
~ void disconnectRTK()
~ void connectRTK()
}
class com.geoai.mavlink.geoainet.rtk.info.rtcm.msm.MSMHeader {
+ int getMessageNumber()
+ void setMessageNumber(int)
+ int getReferenceStationId()
+ void setReferenceStationId(int)
+ int getEpochTime()
+ void setEpochTime(int)
+ int getMultipleMessageFlag()
+ void setMultipleMessageFlag(int)
+ int getIods()
+ void setIods(int)
+ int getClockSteeringIndicator()
+ void setClockSteeringIndicator(int)
+ int getExternalClockIndicator()
+ void setExternalClockIndicator(int)
+ int getSmoothIndicator()
+ void setSmoothIndicator(int)
+ int getSmoothInterval()
+ void setSmoothInterval(int)
+ long getSatelliteMask()
+ void setSatelliteMask(long)
+ long getSignalMask()
+ void setSignalMask(long)
+ long getCellMask()
+ void setCellMask(long)
+ int getCellCount()
+ void setCellCount(int)
+ int getValidCellCount()
+ void setValidCellCount(int)
+ int getSatelliteCount()
+ void setSatelliteCount(int)
+ int getSignalCount()
+ void setSignalCount(int)
+ int getHeaderLength()
+ void setHeaderLength(int)
+ List<Integer> getSatelliteList()
+ void setSatelliteList(List<Integer>)
+ List<Integer> getSignalList()
+ void setSignalList(List<Integer>)
+ boolean isValidCell(int)
+ String[] getFrequencyBand()
+ String toString()
}
class com.geoai.mavlink.geoainet.rtk.component.SixentsRtkManager {
+ void setAccount(SixentsAccount)
+ SixentsAccount getAccount()
+ void connect()
+ void disconnect()
+ void sendGGA(byte[])
}
class com.geoai.mavlink.geoainet.rtk.component.account.BluetoothNode {
+ String getMac()
+ void setMac(String)
+ String getName()
+ void setName(String)
}
class com.geoai.mavlink.geoainet.rtk.RtkManager {
+ void sendRTCM(byte[])
+ void setRtkStateListener(IRtkStateListener)
+ boolean getRtkEnable()
+ void setRtkEnable(boolean,CompletionCallback.ICompletionCallback)
+ void setRtkComponentType(RtkComponentType)
+ RtkComponentType getRtkComponentType()
+ INetworkRTKManager<? extends IAccount> getNetWorkRtkManager()
+ IHardwareRTKManager<?> getHardwareRtkManager()
+ void disconnectRTK()
+ void connectRTK()
# void onRtkStateHeartBeat()
}
class com.geoai.mavlink.geoainet.rtk.info.RtkEntryInfo {
+ boolean isFlying()
+ void setFlying(boolean)
+ double getLatitude()
+ void setLatitude(double)
+ double getLongitude()
+ void setLongitude(double)
+ double getAltitude()
+ void setAltitude(double)
+ GPSPosType getPosType()
+ void setPosType(GPSPosType)
+ short getIsHealth()
+ void setIsHealth(short)
+ String getNmeaGGA()
+ void setNmeaGGA(String)
+ RtkServiceCode getServiceCode()
+ void setServiceCode(RtkServiceCode)
+ long getBaselineTimestamp()
+ void setBaselineTimestamp(long)
+ long getGpsTimeOfWeek()
+ void setGpsTimeOfWeek(long)
+ short getRtkReceiverIdentity()
+ void setRtkReceiverIdentity(short)
+ int getGpsWeekNumber()
+ void setGpsWeekNumber(int)
+ short isHealth()
+ void setHealth(short)
+ short getRateOfGpsReceived()
+ void setRateOfGpsReceived(short)
+ short getSatelliteCount()
+ void setSatelliteCount(short)
+ long getAccuracy()
+ void setAccuracy(long)
+ BaseStationInfo getBaseStationInfo()
+ void setBaseStationInfo(BaseStationInfo)
}
class com.geoai.mavlink.geoainet.rtk.component.account.UsbNode {
}
class com.geoai.mavlink.geoainet.rtk.info.BaseStationInfo {
+ double getBaseStationLatitude()
+ void setBaseStationLatitude(double)
+ double getBaseStationLongitude()
+ void setBaseStationLongitude(double)
+ double getBaseStationAltitude()
+ void setBaseStationAltitude(double)
+ double getBaseStationITRF()
+ void setBaseStationITRF(double)
+ int getReferenceStationId()
+ void setReferenceStationId(int)
+ SatelliteInfo getGpsSatelliteInfo()
+ void setGpsSatelliteInfo(SatelliteInfo)
+ SatelliteInfo getBdsSatelliteInfo()
+ void setBdsSatelliteInfo(SatelliteInfo)
+ SatelliteInfo getGlonassSatelliteInfo()
+ void setGlonassSatelliteInfo(SatelliteInfo)
+ SatelliteInfo getGalileoSatelliteInfo()
+ void setGalileoSatelliteInfo(SatelliteInfo)
}
class com.geoai.mavlink.geoainet.rtk.info.BaseStationInfo$SatelliteInfo {
+ int getEpochTime()
+ void setEpochTime(int)
+ List<Integer> getSatelliteNum()
+ void addSatelliteNum(int)
}
enum com.geoai.mavlink.geoainet.rtk.info.rtcm.GnssType {
+  GPS
+  GLO
+  GAL
+  BDS
+ int getType()
+ String getName()
+ String getGsv()
+ int getMsm()
+ {static} GnssType getGnssType(int)
+ {static} int getType(String)
+ {static} String getSatlliteSystem(int)
}
abstract class com.geoai.mavlink.geoainet.rtk.component.base.INetworkRTKManager {
+ {abstract}void setAccount(I)
+ {abstract}I getAccount()
+ {abstract}void connect()
+ {abstract}void disconnect()
+ {abstract}void sendGGA(byte[])
}
interface com.geoai.mavlink.geoainet.rtk.interfaces.IRtkStateListener {
~ void onRtkState(RtkEntryInfo)
}
enum com.geoai.mavlink.geoainet.rtk.enums.RtkComponentType {
+  NONE
+  QX
+  SIXENTS
+  CUSTOM
+  CMCC
+  BLUETOOTH
+  USB
+ {static} RtkComponentType valueOf(int)
+ int value()
+ boolean _equals(int)
}
class com.geoai.mavlink.geoainet.rtk.component.CmccRtkManager {
+ void setAccount(CmccAccount)
+ CmccAccount getAccount()
+ void connect()
+ void disconnect()
+ void sendGGA(byte[])
}
abstract class com.geoai.mavlink.geoainet.rtk.component.base.INode {
}
class com.geoai.mavlink.geoainet.rtk.info.rtcm.msm.MSMSignal {
+ List<Integer> getFinePseudoRangeList()
+ void setFinePseudoRangeList(List<Integer>)
+ List<Integer> getFinePhaseRangeList()
+ void setFinePhaseRangeList(List<Integer>)
+ List<Integer> getPhaseRangeLockTimeIndicatorList()
+ void setPhaseRangeLockTimeIndicatorList(List<Integer>)
+ List<Integer> getHalfCycleAmbiguityIndicatorList()
+ void setHalfCycleAmbiguityIndicatorList(List<Integer>)
+ List<Integer> getSnrList()
+ void setSnrList(List<Integer>)
+ void addFinePseudorange(int)
+ void addFinePhaseRange(int)
+ void addPhaseRangeLockTimeIndicator(int)
+ void addHalfCycleAmbiguityIndicator(int)
+ void addSnr(int)
+ String toString()
}
class com.geoai.mavlink.geoainet.rtk.info.rtcm.sarp.SignalData {
+ String getFrequencyBand()
+ void setFrequencyBand(String)
+ String getSignalCode()
+ void setSignalCode(String)
+ double getPseudoRange()
+ void setPseudoRange(double)
+ double getPhaseRange()
+ void setPhaseRange(double)
+ double getSnr()
+ void setSnr(double)
+ long getLockTime()
+ void setLockTime(long)
}
class com.geoai.mavlink.geoainet.rtk.info.rtcm.msm.MSMSatellite {
+ List<Integer> getMillisecondsRoughRangeList()
+ void setMillisecondsRoughRangeList(List<Integer>)
+ List<Integer> getDotMillisecondsRoungRangeList()
+ void setDotMillisecondsRoungRangeList(List<Integer>)
+ void addMillisecondsRoughRange(int)
+ void addDotMillisecondsRoughRange(int)
+ int getSatelliteLength()
+ String toString()
}
class com.geoai.mavlink.geoainet.rtk.info.rtcm.sarp.ReferenceStation {
+ int getMessageNumber()
+ void setMessageNumber(int)
+ int getReferenceStationId()
+ void setReferenceStationId(int)
+ int getItrf()
+ void setItrf(int)
+ int getGpsIndicator()
+ void setGpsIndicator(int)
+ int getGloIndicator()
+ void setGloIndicator(int)
+ int getGalIndicator()
+ void setGalIndicator(int)
+ int getReferenceStationIndicator()
+ void setReferenceStationIndicator(int)
+ double getEcefX()
+ void setEcefX(double)
+ int getReceiverOscillatorIndicator()
+ void setReceiverOscillatorIndicator(int)
+ double getEcefY()
+ void setEcefY(double)
+ int getQuarterCycleIndicator()
+ void setQuarterCycleIndicator(int)
+ double getEcefZ()
+ void setEcefZ(double)
+ int getHeight()
+ void setHeight(int)
+ double getLon()
+ void setLon(double)
+ double getLat()
+ void setLat(double)
+ double getAlt()
+ void setAlt(double)
}
abstract class com.geoai.mavlink.geoainet.rtk.component.base.IHardwareRTKManager {
+ {abstract}List<I> getAvailableNodeLists()
+ {abstract}void setNode(I)
+ {abstract}I getNode()
+ {abstract}void connect()
+ {abstract}void disconnect()
}
abstract class com.geoai.mavlink.geoainet.rtk.component.base.IAccount {
}
class com.geoai.mavlink.geoainet.rtk.component.BluetoothRtkManager {
+ List<BluetoothNode> getAvailableNodeLists()
+ void setNode(BluetoothNode)
+ BluetoothNode getNode()
+ void connect()
+ void disconnect()
}
class com.geoai.mavlink.geoainet.rtk.component.QxRtkManager {
+ void setAccount(QxAccount)
+ QxAccount getAccount()
+ void connect()
+ void disconnect()
+ void sendGGA(byte[])
}
class com.geoai.mavlink.geoainet.rtk.component.UsbRtkManager {
+ List<UsbNode> getAvailableNodeLists()
+ void setNode(UsbNode)
+ UsbNode getNode()
+ void connect()
+ void disconnect()
}
class com.geoai.mavlink.geoainet.rtk.info.NtripAuthInfo {
+ {static} Creator<NtripAuthInfo> CREATOR
+ String getIpAddress()
+ void setIpAddress(String)
+ int getPort()
+ void setPort(int)
+ String getUsername()
+ void setUsername(String)
+ String getPassword()
+ void setPassword(String)
+ String getMountPoint()
+ void setMountPoint(String)
+ int describeContents()
+ void writeToParcel(Parcel,int)
}
class com.geoai.mavlink.geoainet.rtk.component.account.CmccAccount {
+ {static} Creator<CmccAccount> CREATOR
+ String getDeviceID()
+ void setDeviceID(String)
+ int describeContents()
+ void writeToParcel(Parcel,int)
}
class com.geoai.mavlink.geoainet.rtk.component.account.QxAccount {
+ {static} Creator<QxAccount> CREATOR
+ String getDeviceId()
+ void setDeviceId(String)
+ String getDeviceType()
+ void setDeviceType(String)
+ String getKey()
+ void setKey(String)
+ String getSecret()
+ void setSecret(String)
+ int describeContents()
+ void writeToParcel(Parcel,int)
}
class com.geoai.mavlink.geoainet.rtk.info.rtcm.sarp.SatelliteData {
+ String getSatelliteSystem()
+ void setSatelliteSystem(String)
+ int getPrn()
+ void setPrn(int)
+ Map<String,SignalData> getSignalDataMap()
+ void setSignalDataMap(Map<String,SignalData>)
+ void addSignalData(SignalData)
+ SignalData getSignalData(String)
+ Collection<SignalData> getAllSignalData()
}
class com.geoai.mavlink.geoainet.rtk.utils.BitUtils {
+ {static} long bytesDecode(byte[],int,int)
+ {static} long bytesDecodeR(byte[],int,int)
+ {static} double bytesDouble(byte[],int,int)
+ {static} long bytesToDouble(byte[],int,int)
+ {static} long get(byte,int,int)
+ {static} int getLow(byte,int)
+ {static} int getHigh(byte,int)
+ {static} long getDouble(long,int)
+ {static} long getDouble(double,int)
+ {static} int exor(long,int)
}


com.geoai.mavlink.geoainet.rtk.component.base.INetworkRTKManager <|-- com.geoai.mavlink.geoainet.rtk.component.NtripClientRtkManager
android.os.Parcelable <|.. com.geoai.mavlink.geoainet.rtk.component.account.NtripClientAccount
com.geoai.mavlink.geoainet.rtk.component.base.IAccount <|-- com.geoai.mavlink.geoainet.rtk.component.account.NtripClientAccount
com.geoai.mavlink.geoainet.rtk.utils.RTCMParse +.. com.geoai.mavlink.geoainet.rtk.utils.RTCMParse$OnRealRtcmPayload2MavlinkDataCallback
android.os.Parcelable <|.. com.geoai.mavlink.geoainet.rtk.component.account.SixentsAccount
com.geoai.mavlink.geoainet.rtk.component.base.IAccount <|-- com.geoai.mavlink.geoainet.rtk.component.account.SixentsAccount
com.geoai.mavlink.geoainet.rtk.component.base.INetworkRTKListener <|.. com.geoai.mavlink.geoainet.rtk.RtkParse
com.geoai.mavlink.geoainet.rtk.component.base.IHardwareRTKListener <|.. com.geoai.mavlink.geoainet.rtk.RtkParse
com.geoai.mavlink.geoainet.base.BaseManager <|-- com.geoai.mavlink.geoainet.rtk.RtkParse
com.geoai.mavlink.geoainet.rtk.component.base.INetworkRTKManager <|-- com.geoai.mavlink.geoainet.rtk.component.SixentsRtkManager
com.geoai.mavlink.geoainet.rtk.component.base.INode <|-- com.geoai.mavlink.geoainet.rtk.component.account.BluetoothNode
com.geoai.mavlink.geoainet.rtk.interfaces.IBaseRtkManager <|.. com.geoai.mavlink.geoainet.rtk.RtkManager
com.geoai.mavlink.geoainet.rtk.RtkParse <|-- com.geoai.mavlink.geoainet.rtk.RtkManager
com.geoai.mavlink.geoainet.rtk.component.base.INode <|-- com.geoai.mavlink.geoainet.rtk.component.account.UsbNode
com.geoai.mavlink.geoainet.rtk.info.BaseStationInfo +.. com.geoai.mavlink.geoainet.rtk.info.BaseStationInfo$SatelliteInfo
com.geoai.mavlink.geoainet.base.mavlinkcore.interfaces.IBaseStateListener <|-- com.geoai.mavlink.geoainet.rtk.interfaces.IRtkStateListener
com.geoai.mavlink.geoainet.rtk.component.base.INetworkRTKManager <|-- com.geoai.mavlink.geoainet.rtk.component.CmccRtkManager
com.geoai.mavlink.geoainet.rtk.component.base.IHardwareRTKManager <|-- com.geoai.mavlink.geoainet.rtk.component.BluetoothRtkManager
com.geoai.mavlink.geoainet.rtk.component.base.INetworkRTKManager <|-- com.geoai.mavlink.geoainet.rtk.component.QxRtkManager
com.geoai.mavlink.geoainet.rtk.component.base.IHardwareRTKManager <|-- com.geoai.mavlink.geoainet.rtk.component.UsbRtkManager
android.os.Parcelable <|.. com.geoai.mavlink.geoainet.rtk.info.NtripAuthInfo
android.os.Parcelable <|.. com.geoai.mavlink.geoainet.rtk.component.account.CmccAccount
com.geoai.mavlink.geoainet.rtk.component.base.IAccount <|-- com.geoai.mavlink.geoainet.rtk.component.account.CmccAccount
android.os.Parcelable <|.. com.geoai.mavlink.geoainet.rtk.component.account.QxAccount
com.geoai.mavlink.geoainet.rtk.component.base.IAccount <|-- com.geoai.mavlink.geoainet.rtk.component.account.QxAccount
@enduml
@enduml
```

## 避障模块
```plantuml
@startuml
abstract class com.geoai.mavlink.geoainet.obstacle.ObstacleParse {
+ void parseMAVLinkMessage(MAVLinkMessage)
# {abstract}void onObstacleStateHeartbeat(ObstacleStateInfo)
# String getVisionCalibrationVersion()
# int initVisionCalibration(String,float,int)
# int startCalibration(byte[],byte[],int,int)
# int clearVisionCalibrationInfo()
}
enum com.geoai.mavlink.geoainet.obstacle.enums.ObstacleAvoidanceType {
+  BRAKE
+  BYPASS
+  UNKNOWN
+ {static} ObstacleAvoidanceType valueOf(int)
+ int value()
+ boolean _equals(int)
}
enum com.geoai.mavlink.geoainet.obstacle.enums.ObstacleCameraSource {
+  LIVEVIEW_CAMERA_SOURCE_DEFAULT
+  OBSTACLE_CAMERA_FPV
+  OBSTACLE_CAMERA_FRONT
+  OBSTACLE_CAMERA_TAIL
+  OBSTACLE_CAMERA_LEFT
+  OBSTACLE_CAMERA_RIGHT
+  OBSTACLE_CAMERA_UP
+  OBSTACLE_CAMERA_DOWN
+  OBSTACLE_CAMERA_ALL
+ boolean _equals(int)
+ {static} ObstacleCameraSource find(int)
+ int getValue()
}
class com.geoai.mavlink.geoainet.obstacle.ObstacleManager {
+ void setObstacleDataListener(IObstacleStateListener)
+ void setObstacleAvoidanceType(ObstacleAvoidanceType,CompletionCallback.ICompletionCallback)
+ void getObstacleAvoidanceType(CompletionCallback.ICompletionCallbackWith<ObstacleAvoidanceType>)
+ void setObstacleAvoidanceBrakingDistance(float,CompletionCallback.ICompletionCallback)
+ void getObstacleAvoidanceBrakingDistance(CompletionCallback.ICompletionCallbackWith<Float>)
+ void setObstacleAvoidanceEnable(boolean,CompletionCallback.ICompletionCallback)
+ void getObstacleAvoidanceEnable(CompletionCallback.ICompletionCallbackWith<Boolean>)
+ int initObstacleCalibrationLibrary(String,float,int)
+ int releaseObstacleCalibration()
+ int startObstacleCalibration(byte[],byte[],int,int)
+ void setObstacleCameraSource(ObstacleCameraSource,CompletionCallback.ICompletionCallback)
+ void getObstacleCameraSource(CompletionCallback.ICompletionCallbackWith<ObstacleCameraSource>)
+ String getObstacleCalibrationVersion()
# void onObstacleStateHeartbeat(ObstacleStateInfo)
}
interface com.geoai.mavlink.geoainet.obstacle.interfaces.IObstacleManager {
+ void setObstacleDataListener(IObstacleStateListener)
+ void setObstacleAvoidanceType(ObstacleAvoidanceType,CompletionCallback.ICompletionCallback)
+ void getObstacleAvoidanceType(CompletionCallback.ICompletionCallbackWith<ObstacleAvoidanceType>)
+ void setObstacleAvoidanceBrakingDistance(float,CompletionCallback.ICompletionCallback)
+ void getObstacleAvoidanceBrakingDistance(CompletionCallback.ICompletionCallbackWith<Float>)
+ void setObstacleAvoidanceEnable(boolean,CompletionCallback.ICompletionCallback)
+ void getObstacleAvoidanceEnable(CompletionCallback.ICompletionCallbackWith<Boolean>)
+ int initObstacleCalibrationLibrary(String,float,int)
+ int releaseObstacleCalibration()
+ int startObstacleCalibration(byte[],byte[],int,int)
+ void setObstacleCameraSource(ObstacleCameraSource,CompletionCallback.ICompletionCallback)
+ void getObstacleCameraSource(CompletionCallback.ICompletionCallbackWith<ObstacleCameraSource>)
+ String getObstacleCalibrationVersion()
}
class com.geoai.mavlink.geoainet.obstacle.info.ObstacleStateInfo {
+ int getHorizontalAngleInterval()
+ void setHorizontalAngleInterval(int)
+ int[] getHorizontalObstacleDistance()
+ void setHorizontalObstacleDistance(int[])
+ int getMinObstacleDistance()
+ void setMinObstacleDistance(int)
+ int getMaxObstacleDistance()
+ void setMaxObstacleDistance(int)
}
interface com.geoai.mavlink.geoainet.obstacle.interfaces.IObstacleStateListener {
~ void onObstacleDistance(ObstacleStateInfo)
}


com.geoai.mavlink.geoainet.base.BaseManager <|-- com.geoai.mavlink.geoainet.obstacle.ObstacleParse
com.geoai.mavlink.geoainet.obstacle.interfaces.IObstacleManager <|.. com.geoai.mavlink.geoainet.obstacle.ObstacleManager
com.geoai.mavlink.geoainet.obstacle.ObstacleParse <|-- com.geoai.mavlink.geoainet.obstacle.ObstacleManager
com.geoai.mavlink.geoainet.base.mavlinkcore.interfaces.IBaseStateListener <|-- com.geoai.mavlink.geoainet.obstacle.interfaces.IObstacleStateListener
@enduml
```

## 系统模块
```plantuml
@startuml
class com.geoai.mavlink.geoainet.system.info.ParamValueInfo {
+ int getIndex()
+ void setIndex(int)
+ float getValue()
+ void setValue(float)
+ ParamValueTypeState getType()
+ void setType(ParamValueTypeState)
+ String getName()
+ void setName(String)
+ ParameterEntryInfo.Parameters getDesc()
+ void setDesc(ParameterEntryInfo.Parameters)
}
interface com.geoai.mavlink.geoainet.system.interfaces.ISshServerListener {
~ void onProcess(long,long)
~ void onResult(boolean,GEOAIError)
}
class com.geoai.mavlink.geoainet.system.info.VersionInfo {
+ int getComponentType()
+ void setComponentType(int)
+ long getSwVersionCode()
+ void setSwVersionCode(long)
+ long getHwVersionCode()
+ void setHwVersionCode(long)
}
interface com.geoai.mavlink.geoainet.system.interfaces.IFlightLogDownloadListener {
~ void onProcess(long,long)
~ void onResult(boolean,int)
}
class com.geoai.mavlink.geoainet.system.utils.FlightLogDownloadManager2 {
+ {static} FlightLogDownloadManager2 getInstance()
+ int initDownloadProcess(short,String,FlightLogEntryInfo,OnMsgAgent)
+ void downloadFlightLog(IFlightLogDownloadListener)
+ void onMavlinkMsgResponse(msg_log_data)
+ void heartBeat()
}
interface com.geoai.mavlink.geoainet.system.utils.FlightLogDownloadManager2$OnMsgAgent {
~ void onMsg(MAVLinkMessage)
}
interface com.geoai.mavlink.geoainet.system.interfaces.IFetchFlightLogListListener {
~ void onSync(float)
~ void onResult(boolean,List<FlightLogEntryInfo>)
}
interface com.geoai.mavlink.geoainet.system.interfaces.IFirmwareUpgradeListener {
~ void onStep(ProcessTag)
~ void onPercentage(float)
~ void onResult(boolean,GEOAIError)
}
enum com.geoai.mavlink.geoainet.system.enums.ParamValueTypeState {
+  MAV_PARAM_TYPE_UINT8
+  MAV_PARAM_TYPE_INT8
+  MAV_PARAM_TYPE_UINT16
+  MAV_PARAM_TYPE_INT16
+  MAV_PARAM_TYPE_UINT32
+  MAV_PARAM_TYPE_INT32
+  MAV_PARAM_TYPE_UINT64
+  MAV_PARAM_TYPE_INT64
+  MAV_PARAM_TYPE_REAL32
+  MAV_PARAM_TYPE_REAL64
+ {static} ParamValueTypeState valueOf(int)
+ int value()
+ boolean _equals(int)
}
interface com.geoai.mavlink.geoainet.system.interfaces.ISingleExtParameterSyncListener {
~ void onResult(boolean,ExtParamValueInfo,GEOAIError)
}
interface com.geoai.mavlink.geoainet.system.interfaces.IBaseSystemManager {
~ void updateParameterFromDrone(CompletionCallback.ICompletionCallback)
~ void readAllParameter(IAllParameterSyncListener)
~ void readSingleParameter(String,ISingleParameterSyncListener)
~ void writeSingleParameter(ParamValueInfo,CompletionCallback.ICompletionCallback)
~ void setTransmitRateListener(ITransmitRateListener)
~ void fetchFlightLogList(LoggerSystemType,IFetchFlightLogListListener)
~ void fetchFlightLog(LoggerSystemType,FlightLogEntryInfo,String,IFlightLogDownloadListener)
~ void cancelFetchFlightLog()
~ void ftpFileList(MFTPManager.SystemComponent,String,CompletionCallback.ICompletionCallbackWith<ArrayList<MFTPManager.FileDirectoryList>>)
~ void ftpDelFile(MFTPManager.SystemComponent,String,CompletionCallback.ICompletionCallback)
~ void ftpDelDirectory(MFTPManager.SystemComponent,String,CompletionCallback.ICompletionCallback)
~ void ftpCreateDirectory(MFTPManager.SystemComponent,String,CompletionCallback.ICompletionCallback)
~ void ftpCalcFileCrc32(MFTPManager.SystemComponent,String,CompletionCallback.ICompletionCallbackWith<short[]>)
~ void ftpDownloadFile(MFTPManager.SystemComponent,String,IFtpFileDownloadListener)
~ void ftpUploadFile(MFTPManager.SystemComponent,String,String,IFtpFileUploadListener)
~ void ftpTerminateControl(MFTPManager.SystemComponent,CompletionCallback.ICompletionCallback)
~ Map<Short,VersionInfo> getComponentVersionInfo()
~ Map<Integer,String> getSDKVersionInfo()
~ void startFirmwareUpgrade(String,IFirmwareUpgradeListener)
~ void setUpgradeStatusListener(IUpgradeStatusListener)
~ void sendSerialControl(String)
~ void setSerialControlMessageResponse(ISerialControlListener)
}
class com.geoai.mavlink.geoainet.system.utils.HttpServer {
+ void uploadFile(File,String,HttpMonitorListener)
+ void downloadFile(String,String,HttpMonitorListener)
}
interface com.geoai.mavlink.geoainet.system.utils.HttpServer$HttpMonitorListener {
~ void onProgress(long,long)
~ void onFinish()
~ void onError(GEOAIError)
}
class com.geoai.mavlink.geoainet.system.info.ParametersTableSyncInfo {
+ AtomicBoolean getParamAllSynchronizing()
+ long getTimestamp()
+ void setTimestamp(long)
+ float getSyncPercentage()
+ void setSyncPercentage(float)
+ ParametersTableSyncState getState()
+ void setState(ParametersTableSyncState)
}
class com.geoai.mavlink.geoainet.system.info.ExtParameterEntryInfo {
+ Integer getVersion()
+ void setVersion(Integer)
+ List<Parameters> getParameters()
+ void setParameters(List<Parameters>)
}
class com.geoai.mavlink.geoainet.system.info.ExtParameterEntryInfo$Parameters {
+ String getName()
+ void setName(String)
+ String getType()
+ void setType(String)
+ Float getDefaultX()
+ void setDefaultX(Float)
+ String getGroup()
+ void setGroup(String)
+ String getCategory()
+ void setCategory(String)
+ String getShortDesc()
+ void setShortDesc(String)
+ String getLongDesc()
+ void setLongDesc(String)
+ Float getMin()
+ void setMin(Float)
+ Float getMax()
+ void setMax(Float)
+ String getUnits()
+ void setUnits(String)
+ Integer getDecimalPlaces()
+ void setDecimalPlaces(Integer)
+ List<Values> getValues()
+ void setValues(List<Values>)
+ List<Bitmask> getBitmask()
+ void setBitmask(List<Bitmask>)
+ Boolean getRebootRequired()
+ void setRebootRequired(Boolean)
+ Boolean getVolatileX()
+ void setVolatileX(Boolean)
+ Float getIncrement()
+ void setIncrement(Float)
}
class com.geoai.mavlink.geoainet.system.info.ExtParameterEntryInfo$Parameters$Values {
+ Integer getValue()
+ void setValue(Integer)
+ String getDescription()
+ void setDescription(String)
}
class com.geoai.mavlink.geoainet.system.info.ExtParameterEntryInfo$Parameters$Bitmask {
+ Integer getIndex()
+ void setIndex(Integer)
+ String getDescription()
+ void setDescription(String)
}
class com.geoai.mavlink.geoainet.system.info.ExtParamValueInfo {
+ int getIndex()
+ void setIndex(int)
+ byte[] getValue()
+ void setValue(byte[])
+ String getValue2()
+ void setValue2(String)
+ ParamValueTypeState getType()
+ void setType(ParamValueTypeState)
+ String getName()
+ void setName(String)
+ ParameterEntryInfo.Parameters getDesc()
+ void setDesc(ParameterEntryInfo.Parameters)
}
enum com.geoai.mavlink.geoainet.system.enums.FlightLogTableSyncState {
+  UNKNOWN
+  COMPLETED
+  SYNC
}
abstract class com.geoai.mavlink.geoainet.system.SystemParse {
+ ProcessController getFirmwareUpgradeProcessController()
+ FlightLogTableSyncInfo getFlightLogTableSyncInfo()
+ LinkedHashMap<Integer,FlightLogEntryInfo> getFlightLogEntryListHashMap()
+ LinkNodeTransmitRateInfo getLinkNodeTransmitRateInfo()
+ FirmwareUpgradeInfo getFirmwareUpgradeInfo()
+ HashMap<Short,VersionInfo> getComponentVersionInfo()
+ HashMap<Integer,String> getSDKVersionInfo()
+ void parseMAVLinkMessage(MAVLinkMessage)
# Process buildUpgradeFileUpload(File,String,IFirmwareUpgradeListener)
# Process buildCalcRemoteHash(String,File)
# Process sendUpgradeCommand()
# {abstract}void onAirLinkNodeTransmitRate()
# {abstract}void onFlightLogTableSynchronisation(boolean,int)
# {abstract}void onFirmwareUpgradeHeartBeat()
# {abstract}void onSerialMessageResponseHeartBeat(String)
}
enum com.geoai.mavlink.geoainet.system.enums.ParametersTableSyncState {
+  UNKNOWN
+  COMPLETED
+  ALL_SYNC
}
interface com.geoai.mavlink.geoainet.system.interfaces.IFtpFileDownloadListener {
~ void onResponse(boolean,GEOAIError)
~ void onData(boolean,short[],long)
}
class com.geoai.mavlink.geoainet.system.info.FlightLogEntryInfo {
+ int getId()
+ void setId(int)
+ long getTimestamp()
+ void setTimestamp(long)
+ long getSize()
+ void setSize(long)
}
class com.geoai.mavlink.geoainet.system.utils.ParamManager {
+ {static} ParamManager getInstance()
+ void readAllParameter(BaseComponentConst,IAllParameterSyncListener)
+ void readParameter(BaseComponentConst,int,ISingleParameterSyncListener)
+ void readParameter(BaseComponentConst,String,ISingleParameterSyncListener)
+ void writeParameter(BaseComponentConst,ParamValueInfo,CompletionCallback.ICompletionCallback)
+ void onMavlinkMsgResponse(msg_param_value)
+ void onMavlinkResponseTimeoutChecker()
}
class com.geoai.mavlink.geoainet.system.utils.ParamManager$ParamRequest {
+ short getRepeat()
+ void setRepeat(short)
+ BaseComponentConst getBaseComponentConst()
+ MAVLinkMessage getMavLinkMessage()
+ void setMavLinkMessage(MAVLinkMessage)
+ void setBaseComponentConst(BaseComponentConst)
+ Object getCallback()
+ void setCallback(Object)
}
class com.geoai.mavlink.geoainet.system.info.FirmwareUpgradeInfo {
+ int getCurrentUpgradeIndex()
+ void setCurrentUpgradeIndex(int)
+ int getTotalUpgradeComponentCount()
+ void setTotalUpgradeComponentCount(int)
+ short getCurrentComponentType()
+ void setCurrentComponentType(short)
+ int getFirmwareTransferPercent()
+ void setFirmwareTransferPercent(int)
+ int getFirmwareUpgradeStatus()
+ void setFirmwareUpgradeStatus(int)
+ int getUpgradeErrorCode()
+ void setUpgradeErrorCode(int)
}
class com.geoai.mavlink.geoainet.system.utils.ExtParamManager {
+ {static} ExtParamManager getInstance()
+ void readParameter(BaseComponentConst,String,ISingleExtParameterSyncListener)
+ void writeParameter(BaseComponentConst,ExtParamValueInfo,CompletionCallback.ICompletionCallback)
+ void onMavlinkMsgResponse(MAVLinkMessage)
+ void onMavlinkResponseTimeoutChecker()
}
interface com.geoai.mavlink.geoainet.system.interfaces.ISingleParameterSyncListener {
~ void onResult(boolean,ParamValueInfo,GEOAIError)
}
class com.geoai.mavlink.geoainet.system.info.ParameterEntryInfo {
+ Integer getVersion()
+ void setVersion(Integer)
+ List<Parameters> getParameters()
+ void setParameters(List<Parameters>)
}
class com.geoai.mavlink.geoainet.system.info.ParameterEntryInfo$Parameters {
+ String getName()
+ void setName(String)
+ String getType()
+ void setType(String)
+ Float getDefaultX()
+ void setDefaultX(Float)
+ String getGroup()
+ void setGroup(String)
+ String getCategory()
+ void setCategory(String)
+ String getShortDesc()
+ void setShortDesc(String)
+ String getLongDesc()
+ void setLongDesc(String)
+ Float getMin()
+ void setMin(Float)
+ Float getMax()
+ void setMax(Float)
+ String getUnits()
+ void setUnits(String)
+ Integer getDecimalPlaces()
+ void setDecimalPlaces(Integer)
+ List<Values> getValues()
+ void setValues(List<Values>)
+ List<Bitmask> getBitmask()
+ void setBitmask(List<Bitmask>)
+ Boolean getRebootRequired()
+ void setRebootRequired(Boolean)
+ Boolean getVolatileX()
+ void setVolatileX(Boolean)
+ Float getIncrement()
+ void setIncrement(Float)
}
class com.geoai.mavlink.geoainet.system.info.ParameterEntryInfo$Parameters$Values {
+ Integer getValue()
+ void setValue(Integer)
+ String getDescription()
+ void setDescription(String)
}
class com.geoai.mavlink.geoainet.system.info.ParameterEntryInfo$Parameters$Bitmask {
+ Integer getIndex()
+ void setIndex(Integer)
+ String getDescription()
+ void setDescription(String)
}
class com.geoai.mavlink.geoainet.system.SystemManager {
+ void readAllParameter(IAllParameterSyncListener)
+ void readSingleParameter(String,ISingleParameterSyncListener)
+ void writeSingleParameter(ParamValueInfo,CompletionCallback.ICompletionCallback)
+ void fetchFlightLogList(LoggerSystemType,IFetchFlightLogListListener)
+ void fetchFlightLog(LoggerSystemType,FlightLogEntryInfo,String,IFlightLogDownloadListener)
+ void cancelFetchFlightLog()
+ void ftpFileList(MFTPManager.SystemComponent,String,CompletionCallback.ICompletionCallbackWith<ArrayList<MFTPManager.FileDirectoryList>>)
+ void ftpDelFile(MFTPManager.SystemComponent,String,CompletionCallback.ICompletionCallback)
+ void ftpDelDirectory(MFTPManager.SystemComponent,String,CompletionCallback.ICompletionCallback)
+ void ftpCreateDirectory(MFTPManager.SystemComponent,String,CompletionCallback.ICompletionCallback)
+ void ftpCalcFileCrc32(MFTPManager.SystemComponent,String,CompletionCallback.ICompletionCallbackWith<short[]>)
+ void ftpDownloadFile(MFTPManager.SystemComponent,String,IFtpFileDownloadListener)
+ void ftpUploadFile(MFTPManager.SystemComponent,String,String,IFtpFileUploadListener)
+ void ftpTerminateControl(MFTPManager.SystemComponent,CompletionCallback.ICompletionCallback)
+ void startFirmwareUpgrade(String,IFirmwareUpgradeListener)
+ void setUpgradeStatusListener(IUpgradeStatusListener)
+ void sendSerialControl(String)
+ void setSerialControlMessageResponse(ISerialControlListener)
+ void updateParameterFromDrone(CompletionCallback.ICompletionCallback)
+ void setTransmitRateListener(ITransmitRateListener)
# void onAirLinkNodeTransmitRate()
# void onFlightLogTableSynchronisation(boolean,int)
# void onFirmwareUpgradeHeartBeat()
# void onSerialMessageResponseHeartBeat(String)
+ void setProductConnectedListener(boolean)
}
interface com.geoai.mavlink.geoainet.system.interfaces.IAllParameterSyncListener {
~ void process(float)
~ void onResult(boolean,List<ParamValueInfo>)
}
class com.geoai.mavlink.geoainet.system.info.FlightLogTableSyncInfo {
+ AtomicBoolean getFlightLogSynchronizing()
+ FlightLogTableSyncState getState()
+ void setState(FlightLogTableSyncState)
+ long getTimestamp()
+ void setTimestamp(long)
+ float getSyncPercentage()
+ void setSyncPercentage(float)
}
interface com.geoai.mavlink.geoainet.system.interfaces.IFtpFileUploadListener {
~ void onResponse(boolean,GEOAIError)
~ void onData(long)
}
interface com.geoai.mavlink.geoainet.system.interfaces.IUpgradeStatusListener {
~ void onResult(FirmwareUpgradeInfo)
}
interface com.geoai.mavlink.geoainet.system.interfaces.ISerialControlListener {
~ void onResult(String)
}
class com.geoai.mavlink.geoainet.system.utils.MFTPManager {
+ {static} MFTPManager getInstance()
+ void createdDirectory(SystemComponent,OnMFTPCommandSender,File)
+ void removeDirectory(SystemComponent,OnMFTPCommandSender,File)
+ void removeFile(SystemComponent,OnMFTPCommandSender,File)
+ void listDirectory(SystemComponent,OnMFTPFileListSender,File)
+ void calcFileCrc32(SystemComponent,OnMFTPCalcCrcSender,File)
+ void terminateSession(SystemComponent,OnMFTPCommandSender,File)
+ void readFile(SystemComponent,OnMFTPReadFileSender,File)
+ void writeFile(SystemComponent,OnMFTPWriteFileSender,File,File)
+ void onMavlinkMsgResponse(msg_file_transfer_protocol)
+ void onMavlinkResponseTimeoutChecker()
}
enum com.geoai.mavlink.geoainet.system.utils.SystemComponent {
+  CREST
+  FC
}
interface com.geoai.mavlink.geoainet.system.utils.MFTPManager$OnMFTPCommandSender {
~ void onResponse(boolean,GEOAIError)
}
interface com.geoai.mavlink.geoainet.system.utils.MFTPManager$OnMFTPFileListSender {
~ void onResponse(boolean,GEOAIError,ArrayList<FileDirectoryList>)
}
interface com.geoai.mavlink.geoainet.system.utils.MFTPManager$OnMFTPCalcCrcSender {
~ void onResponse(boolean,GEOAIError,short[])
}
interface com.geoai.mavlink.geoainet.system.utils.MFTPManager$OnMFTPCreateFileSender {
~ void onResponse(boolean,GEOAIError,long)
}
interface com.geoai.mavlink.geoainet.system.utils.MFTPManager$OnMFTPReadFileSender {
~ void onResponse(boolean,GEOAIError)
~ void onData(boolean,short[],long)
}
interface com.geoai.mavlink.geoainet.system.utils.MFTPManager$OnMFTPWriteFileSender {
~ void onResponse(boolean,GEOAIError)
~ void onData(boolean,long)
}
interface com.geoai.mavlink.geoainet.system.utils.MFTPManager$OnMavlinkFileTransProtocolBuilder {
~ void onBuilder(MAVLinkMessage)
}
class com.geoai.mavlink.geoainet.system.utils.MFTPManager$FileDirectoryList {
+ boolean isFile()
+ void setFile(boolean)
+ boolean isDirectory()
+ void setDirectory(boolean)
+ String getFileName()
+ void setFileName(String)
+ long getFileSize()
+ void setFileSize(long)
}
interface com.geoai.mavlink.geoainet.system.interfaces.IAllExtParameterSyncListener {
~ void process(float)
~ void onResult(boolean,List<ExtParamValueInfo>)
}
class com.geoai.mavlink.geoainet.system.info.SshAuthInfo {
+ String getUsername()
+ void setUsername(String)
+ String getHost()
+ void setHost(String)
+ Integer getPort()
+ void setPort(Integer)
+ String getPassword()
+ void setPassword(String)
}
class com.geoai.mavlink.geoainet.system.info.LinkNodeTransmitRateInfo {
+ long getTimestamp()
+ void setTimestamp(long)
+ long getAirLinkTxRate()
+ void setAirLinkTxRate(long)
+ long getAirLinkRxRate()
+ void setAirLinkRxRate(long)
}
interface com.geoai.mavlink.geoainet.system.interfaces.ITransmitRateListener {
~ void onRate(LinkNodeTransmitRateInfo)
}
class com.geoai.mavlink.geoainet.system.utils.FlightLogDownloadManager {
+ {static} FlightLogDownloadManager getInstance()
+ void downloadFlightLog(BaseComponentConst,IFlightLogDownloadListener)
+ void onMavlinkMsgResponse(msg_log_data)
+ int initDownloadProcess(FlightLogEntryInfo,String,short)
}
class com.geoai.mavlink.geoainet.system.utils.CRC32Calculator {
+ {static} long calculateCRC32(File)
+ {static} int[] getCrcTable()
}
enum com.geoai.mavlink.geoainet.system.enums.LoggerSystemType {
+  FC
+  CREST
+  CAMERA
}


com.geoai.mavlink.geoainet.system.utils.FlightLogDownloadManager2 +.. com.geoai.mavlink.geoainet.system.utils.FlightLogDownloadManager2$OnMsgAgent
com.geoai.mavlink.geoainet.system.utils.HttpServer +.. com.geoai.mavlink.geoainet.system.utils.HttpServer$HttpMonitorListener
com.geoai.mavlink.geoainet.system.info.ExtParameterEntryInfo +.. com.geoai.mavlink.geoainet.system.info.ExtParameterEntryInfo$Parameters
com.geoai.mavlink.geoainet.system.info.ExtParameterEntryInfo$Parameters +.. com.geoai.mavlink.geoainet.system.info.ExtParameterEntryInfo$Parameters$Values
com.geoai.mavlink.geoainet.system.info.ExtParameterEntryInfo$Parameters +.. com.geoai.mavlink.geoainet.system.info.ExtParameterEntryInfo$Parameters$Bitmask
com.geoai.mavlink.geoainet.base.BaseManager <|-- com.geoai.mavlink.geoainet.system.SystemParse
com.geoai.mavlink.geoainet.system.utils.ParamManager +.. com.geoai.mavlink.geoainet.system.utils.ParamManager$ParamRequest
com.geoai.mavlink.geoainet.system.info.ParameterEntryInfo +.. com.geoai.mavlink.geoainet.system.info.ParameterEntryInfo$Parameters
com.geoai.mavlink.geoainet.system.info.ParameterEntryInfo$Parameters +.. com.geoai.mavlink.geoainet.system.info.ParameterEntryInfo$Parameters$Values
com.geoai.mavlink.geoainet.system.info.ParameterEntryInfo$Parameters +.. com.geoai.mavlink.geoainet.system.info.ParameterEntryInfo$Parameters$Bitmask
com.geoai.mavlink.geoainet.system.interfaces.IBaseSystemManager <|.. com.geoai.mavlink.geoainet.system.SystemManager
com.geoai.mavlink.geoainet.system.SystemParse <|-- com.geoai.mavlink.geoainet.system.SystemManager
com.geoai.mavlink.geoainet.system.utils.MFTPManager +.. com.geoai.mavlink.geoainet.system.utils.MFTPManager$OnMFTPCommandSender
com.geoai.mavlink.geoainet.system.utils.OnMavlinkFileTransProtocolBuilder <|-- com.geoai.mavlink.geoainet.system.utils.MFTPManager$OnMFTPCommandSender
com.geoai.mavlink.geoainet.system.utils.MFTPManager +.. com.geoai.mavlink.geoainet.system.utils.MFTPManager$OnMFTPFileListSender
com.geoai.mavlink.geoainet.system.utils.OnMavlinkFileTransProtocolBuilder <|-- com.geoai.mavlink.geoainet.system.utils.MFTPManager$OnMFTPFileListSender
com.geoai.mavlink.geoainet.system.utils.MFTPManager +.. com.geoai.mavlink.geoainet.system.utils.MFTPManager$OnMFTPCalcCrcSender
com.geoai.mavlink.geoainet.system.utils.OnMavlinkFileTransProtocolBuilder <|-- com.geoai.mavlink.geoainet.system.utils.MFTPManager$OnMFTPCalcCrcSender
com.geoai.mavlink.geoainet.system.utils.MFTPManager +.. com.geoai.mavlink.geoainet.system.utils.MFTPManager$OnMFTPCreateFileSender
com.geoai.mavlink.geoainet.system.utils.OnMavlinkFileTransProtocolBuilder <|-- com.geoai.mavlink.geoainet.system.utils.MFTPManager$OnMFTPCreateFileSender
com.geoai.mavlink.geoainet.system.utils.MFTPManager +.. com.geoai.mavlink.geoainet.system.utils.MFTPManager$OnMFTPReadFileSender
com.geoai.mavlink.geoainet.system.utils.OnMavlinkFileTransProtocolBuilder <|-- com.geoai.mavlink.geoainet.system.utils.MFTPManager$OnMFTPReadFileSender
com.geoai.mavlink.geoainet.system.utils.MFTPManager +.. com.geoai.mavlink.geoainet.system.utils.MFTPManager$OnMFTPWriteFileSender
com.geoai.mavlink.geoainet.system.utils.OnMavlinkFileTransProtocolBuilder <|-- com.geoai.mavlink.geoainet.system.utils.MFTPManager$OnMFTPWriteFileSender
com.geoai.mavlink.geoainet.system.utils.MFTPManager +.. com.geoai.mavlink.geoainet.system.utils.MFTPManager$OnMavlinkFileTransProtocolBuilder
com.geoai.mavlink.geoainet.system.utils.MFTPManager +.. com.geoai.mavlink.geoainet.system.utils.MFTPManager$FileDirectoryList
@enduml
```

## 任务模块
```plantuml
@startuml
class com.geoai.mavlink.geoainet.mission.util.MissionUuidAgentUtil {
+ void setMessageSenderAgent(OnMsgAgent)
+ void sendMissionUUID(short[],OnMissionUuidUploadResult)
+ void onMavlinkMessageReceived(msg_mission_info)
+ void onReceivedHeartBeatDispatch()
}
interface com.geoai.mavlink.geoainet.mission.util.MissionUuidAgentUtil$OnMsgAgent {
~ void onMsg(MAVLinkMessage)
}
interface com.geoai.mavlink.geoainet.mission.util.MissionUuidAgentUtil$OnMissionUuidUploadResult {
~ void onResult(boolean,GEOAIError)
}
class com.geoai.mavlink.geoainet.mission.enums.WaypointDescription {
+ double getLatitude()
+ double getLongitude()
+ double getAltitude()
+ float getSpeed()
+ int getSeq()
+ int getAction()
+ int getWaypoint_index()
+ float getParam1()
+ float getParam2()
+ float getParam3()
+ float getParam4()
+ void setLatitude(double)
+ void setLongitude(double)
+ void setAltitude(double)
+ void setSpeed(float)
+ void setSeq(int)
+ void setWaypoint_index(int)
+ void setAction(int)
+ void setParam1(float)
+ void setParam2(float)
+ void setParam3(float)
+ void setParam4(float)
}
abstract class com.geoai.mavlink.geoainet.mission.MissionInfoParse {
# MissionDownloadAgentUtil mMissionDownloadAgent
# FenceProtocolAgentUtil mFenceProtocolAgent
# MissionUuidAgentUtil mMissionUuidAgent
# MissionStateInfo mMissionInfo
# Integer[] mAircraftCurrentLocation
# IMissionUploadListener mMissionUploadStateListener
# IMissionStateListener mMissionStateListener
# ArrayList<MAVLinkMessage> mMissionMavlinkMsgLinkList
# ArrayList<Integer> mMissionWaypointIndexPool
# long mMissionUploadWatchDog
# int mMissionExecutingSeq
+ MissionStateInfo getMissionInfo()
+ void parseMAVLinkMessage(MAVLinkMessage)
# ArrayList<Integer> buildWaypointIndexPool(List<MAVLinkMessage>)
# void registerPusher_500()
# {abstract}void uploadWaypoint(int)
# {abstract}void uploadWaypointFinish(int)
# {abstract}void onMissionCompleted()
# {abstract}void onMissionStateHeartBeat()
}
class com.geoai.mavlink.geoainet.mission.factory.action.ActionAircraftStay {
+ int getSecond()
+ MAVLinkMessage buildMavlinkMessage(MissionItemBuilder.Waypoint,int)
}
interface com.geoai.mavlink.geoainet.mission.interfaces.IMissionUploadListener {
~ void onMissionUploadState(MissionUploadState)
~ void onUploadProgress(int,int)
~ void onMissionUploadResult(boolean,GEOAIError)
}
abstract class com.geoai.mavlink.geoainet.mission.factory.action.BaseAction {
+ {abstract}MAVLinkMessage buildMavlinkMessage(MissionItemBuilder.Waypoint,int)
}
class com.geoai.mavlink.geoainet.mission.factory.XmlToJson {
+ JSONObject toJson()
+ String toString()
+ String toFormattedString(String)
+ String toFormattedString()
}
class com.geoai.mavlink.geoainet.mission.factory.XmlToJson$Builder {
+ Builder forceList(String)
+ Builder setAttributeName(String,String)
+ Builder setContentName(String,String)
+ Builder forceIntegerForPath(String)
+ Builder forceLongForPath(String)
+ Builder forceDoubleForPath(String)
+ Builder forceBooleanForPath(String)
+ Builder skipTag(String)
+ Builder skipAttribute(String)
+ XmlToJson build()
}
class com.geoai.mavlink.geoainet.mission.factory.XmlToJson$Tag {
~ void addChild(Tag)
~ void setContent(String)
~ String getName()
~ String getContent()
~ ArrayList<Tag> getChildren()
~ boolean hasChildren()
~ int getChildrenCount()
~ Tag getChild(int)
~ HashMap<String,ArrayList<Tag>> getGroupedElements()
~ String getPath()
+ String toString()
}
interface com.geoai.mavlink.geoainet.mission.interfaces.IBaseMissionManager {
~ void uploadMission(MissionItemBuilder,IMissionUploadListener)
~ void uploadMission(short[],MissionItemBuilder,IMissionUploadListener)
~ void startMission(CompletionCallback.ICompletionCallback)
~ void pauseMission(CompletionCallback.ICompletionCallback)
~ void continueMission(CompletionCallback.ICompletionCallback)
~ void stopMission(CompletionCallback.ICompletionCallback)
~ void stopMissionUpload(CompletionCallback.ICompletionCallback)
~ void setMissionStateListener(IMissionStateListener)
~ void sendPositionCommand(double,double,float,float,CompletionCallback.ICompletionCallback)
~ void sendOrbitCommand(double,double,float,float,float,OrbitYawBehaviour,CompletionCallback.ICompletionCallback)
~ void downloadMission(MissionReloadMode,CompletionCallback.ICompletionCallbackWith<MissionItemBuilder.Builder>)
~ void createBreakPointInfo(CompletionCallback.ICompletionCallbackWith<MissionItemBuilder.Builder>)
~ void coverWPMLMission(String,IWpmlCoverListener)
}
interface com.geoai.mavlink.geoainet.mission.interfaces.IMissionStateListener {
~ void onMissionState(MissionState,int,int,int)
~ void onMissionStart()
~ void onMissionFinish()
}
enum com.geoai.mavlink.geoainet.mission.enums.MissionState {
+  READY_TO_UPLOAD
+  UPLOADING
+  READY_TO_EXECUTE
+  EXECUTING
+  PAUSING
+  FINISHED
+  UNKNOWN
+ {static} MissionState valueOf(int)
+ int value()
+ boolean _equals(int)
}
class com.geoai.mavlink.geoainet.mission.factory.MissionWpmlTransition {
+ MissionItemBuilder.Builder transition(String)
}
class com.geoai.mavlink.geoainet.mission.factory.MissionWpmlTransition$WpmlFileFormatError {
}
class com.geoai.mavlink.geoainet.mission.factory.MissionWpmlTransition$WpmlParamValueNotSupportError {
}
class com.geoai.mavlink.geoainet.mission.factory.action.ActionRoi {
+ boolean isEnableRoi()
+ float getLatitude()
+ float getLongitude()
+ float getAltitude()
+ MAVLinkMessage buildMavlinkMessage(MissionItemBuilder.Waypoint,int)
}
class com.geoai.mavlink.geoainet.mission.factory.action.ActionCameraRecord {
+ boolean isEnableRecord()
+ MAVLinkMessage buildMavlinkMessage(MissionItemBuilder.Waypoint,int)
}
interface com.geoai.mavlink.geoainet.mission.interfaces.IWpmlCoverListener {
~ void onFailure(GEOAIError)
~ void onSuccess(MissionItemBuilder.Builder)
}
class com.geoai.mavlink.geoainet.mission.factory.action.ActionGimbalRotate {
+ float getPitch()
+ Float getYaw()
+ MAVLinkMessage buildMavlinkMessage(MissionItemBuilder.Waypoint,int)
}
enum com.geoai.mavlink.geoainet.mission.enums.MissionReloadMode {
+  CACHE
+  ONLINE
+ {static} MissionReloadMode valueOf(int)
+ int value()
+ boolean _equals(int)
}
enum com.geoai.mavlink.geoainet.mission.enums.WaypointHeadingMode {
+  MISSION_HEADING_MODE_AUTO
+  MISSION_HEADING_MODE_WAYPOINT
+  MISSION_HEADING_MODE_WAYPOINT_SYNERGY
+ {static} WaypointHeadingMode valueOf(int)
+ int value()
+ boolean _equals(int)
}
class com.geoai.mavlink.geoainet.mission.factory.action.ActionCameraZoom {
+ int getRatio()
+ float getFocalRing()
+ MAVLinkMessage buildMavlinkMessage(MissionItemBuilder.Waypoint,int)
}
class com.geoai.mavlink.geoainet.mission.factory.action.ActionCameraIntervalTrigger {
+ int getTime()
+ int getDistance()
+ MAVLinkMessage buildMavlinkMessage(MissionItemBuilder.Waypoint,int)
}
enum com.geoai.mavlink.geoainet.mission.enums.WaypointTrajectoryMode {
+  TRAJECTORY_TYPE_COORDINATE
+  TRAJECTORY_TYPE_ARRIVE
+  TRAJECTORY_TYPE_ARC
+  TRAJECTORY_TYPE_ARRIVE_TURN_WHILE_MOVE
+ {static} WaypointTrajectoryMode valueOf(int)
+ int value()
+ boolean _equals(int)
}
class com.geoai.mavlink.geoainet.mission.util.MissionDownloadAgentUtil {
+ void setMessageSenderAgent(OnMsgAgent)
+ void downloadMission(OnDownloadMissionResult)
+ void onMavlinkMessageReceived(MAVLinkMessage)
+ void parseMAVLinkMessage(MAVLinkMessage)
+ void onReceivedHeartBeatDispatch()
}
interface com.geoai.mavlink.geoainet.mission.util.MissionDownloadAgentUtil$OnMsgAgent {
~ void onMsg(MAVLinkMessage)
}
interface com.geoai.mavlink.geoainet.mission.util.MissionDownloadAgentUtil$OnDownloadMissionResult {
~ void onResult(boolean,GEOAIError,MAVLinkMessage[])
}
class com.geoai.mavlink.geoainet.mission.factory.MissionMavlinkTransition {
+ MissionItemBuilder.Builder transition(MAVLinkMessage[])
}
class com.geoai.mavlink.geoainet.mission.info.MissionCoverNotSupportNode {
+ String getNodeName()
+ void setNodeName(String)
+ String getNodeParam()
+ void setNodeParam(String)
}
enum com.geoai.mavlink.geoainet.mission.enums.MissionFlyToWaylineMode {
+  USING_FIRST_WAYPOINT_ALTITUDE
+  POINT_TO_POINT
+ {static} MissionFlyToWaylineMode valueOf(int)
+ int value()
+ boolean _equals(int)
}
class com.geoai.mavlink.geoainet.mission.enums.MissionDescription {
+ List<WaypointDescription> getWaypointList()
+ void setWaypointList(List<WaypointDescription>)
+ int getTotalAction()
+ void setTotalAction(int)
+ int getHeadingMode()
+ void setHeadingMode(int)
+ int getForceWaypointMode()
+ void setForceWaypointMode(int)
+ int getWaypointPathMode()
+ void setWaypointPathMode(int)
+ int getFinishAction()
+ void setFinishAction(int)
+ float getMissionAutoSpeed()
+ void setMissionAutoSpeed(float)
}
enum com.geoai.mavlink.geoainet.mission.enums.OrbitYawBehaviour {
+  ORBIT_YAW_BEHAVIOUR_HOLD_FRONT_TO_CIRCLE_CENTER
+  ORBIT_YAW_BEHAVIOUR_HOLD_INITIAL_HEADING
+  ORBIT_YAW_BEHAVIOUR_UNCONTROLLED
+  ORBIT_YAW_BEHAVIOUR_HOLD_FRONT_TANGENT_TO_CIRCLE
+  ORBIT_YAW_BEHAVIOUR_RC_CONTROLLED
+  UNKNOWN
+ {static} OrbitYawBehaviour valueOf(int)
+ int value()
+ boolean _equals(int)
}
class com.geoai.mavlink.geoainet.mission.info.MissionStateInfo {
+ MissionUploadState getMissionUploadState()
+ void setMissionUploadState(MissionUploadState)
+ MissionState getMissionState()
+ void setMissionState(MissionState)
+ int getTotalWaypoint()
+ void setTotalWaypoint(int)
+ int getCurrentReachWaypointIndex()
+ void setCurrentReachWaypointIndex(int)
+ int getCurrentActionIndex()
+ void setCurrentActionIndex(int)
+ String toString()
}
class com.geoai.mavlink.geoainet.mission.factory.action.ActionAircraftRotate {
+ {static} int MISSION_MAVLINK_WAYPOINT
+ float getAngle()
+ MAVLinkMessage buildMavlinkMessage(MissionItemBuilder.Waypoint,int)
}
enum com.geoai.mavlink.geoainet.mission.enums.MissionFinishAction {
+  MISSION_FINISH_ACTION_NO_ACTION
+  MISSION_FINISH_ACTION_GO_HOME
+  MISSION_FINISH_ACTION_AUTO_LAND
+  MISSION_FINISH_ACTION_GO_RALLY
+  MISSION_FINISH_GO_FIRST_WAYPOINT_HOVER
+ {static} MissionFinishAction valueOf(int)
+ int value()
+ boolean _equals(int)
}
class com.geoai.mavlink.geoainet.mission.factory.MissionItemBuilder {
+ {static} int MISSION_MAVLINK_WAYPOINT
+ GEOAIError missionParamChecker()
+ ArrayList<MAVLinkMessage> buildMission(int,int)
}
class com.geoai.mavlink.geoainet.mission.factory.MissionItemBuilder$Builder {
+ MissionItemBuilder build()
+ MissionFlyToWaylineMode getFlyToWaylineMode()
+ Builder setFlyToWaylineMode(MissionFlyToWaylineMode)
+ float getTakeOffSecurityHeight()
+ Builder setTakeOffSecurityHeight(float)
+ MissionFinishAction getFinishAction()
+ float getMissionSpeed()
+ List<Waypoint> getWaypointList()
+ Builder setFinishAction(MissionFinishAction)
+ Builder setMissionSpeed(float)
+ Builder setWaypointList(List<Waypoint>)
}
class com.geoai.mavlink.geoainet.mission.factory.MissionItemBuilder$Waypoint {
+ BaseAction[] getActions()
+ void setActions(BaseAction[])
+ double getWayPointLatitude()
+ void setWayPointLatitude(double)
+ double getWayPointLongitude()
+ void setWayPointLongitude(double)
+ float getWayPointAltitude()
+ void setWayPointAltitude(float)
+ float getWayPointSpeed()
+ void setWayPointSpeed(float)
+ float getHeading()
+ void setHeading(float)
+ boolean isSafePoint()
+ void setSafePoint(boolean)
+ WaypointTrajectoryMode getTrajectoryMode()
+ void setTrajectoryMode(WaypointTrajectoryMode)
+ WaypointAltitudeFrame getAltitudeFrame()
+ void setAltitudeFrame(WaypointAltitudeFrame)
+ WaypointHeadingMode getWaypointHeadingMode()
+ void setWaypointHeadingMode(WaypointHeadingMode)
}
class com.geoai.mavlink.geoainet.mission.factory.action.ActionCameraTakePhoto {
+ boolean isShootPhotoEnable()
+ int getCount()
+ MAVLinkMessage buildMavlinkMessage(MissionItemBuilder.Waypoint,int)
}
class com.geoai.mavlink.geoainet.mission.MissionManager {
# void onMissionCompleted()
# void onMissionStateHeartBeat()
# void uploadWaypoint(int)
# void uploadWaypointFinish(int)
+ void uploadMission(MissionItemBuilder,IMissionUploadListener)
+ void uploadMission(short[],MissionItemBuilder,IMissionUploadListener)
+ void startMission(CompletionCallback.ICompletionCallback)
+ void pauseMission(CompletionCallback.ICompletionCallback)
+ void continueMission(CompletionCallback.ICompletionCallback)
+ void stopMission(CompletionCallback.ICompletionCallback)
+ void stopMissionUpload(CompletionCallback.ICompletionCallback)
+ void setMissionStateListener(IMissionStateListener)
+ void sendPositionCommand(double,double,float,float,CompletionCallback.ICompletionCallback)
+ void sendOrbitCommand(double,double,float,float,float,OrbitYawBehaviour,CompletionCallback.ICompletionCallback)
+ void downloadMission(MissionReloadMode,CompletionCallback.ICompletionCallbackWith<MissionItemBuilder.Builder>)
+ void createBreakPointInfo(CompletionCallback.ICompletionCallbackWith<MissionItemBuilder.Builder>)
+ void coverWPMLMission(String,IWpmlCoverListener)
}
enum com.geoai.mavlink.geoainet.mission.enums.WaypointAltitudeFrame {
+  MSL
+  AGL
+ {static} WaypointAltitudeFrame valueOf(int)
+ int value()
+ boolean _equals(int)
}
class com.geoai.mavlink.geoainet.mission.factory.action.ActionCameraFocus {
+ boolean isAutoFocus()
+ int getFocusMeter()
+ MAVLinkMessage buildMavlinkMessage(MissionItemBuilder.Waypoint,int)
}
class com.geoai.mavlink.geoainet.mission.util.FenceProtocolAgentUtil {
# AtomicBoolean mBanFlyZonesLoaded
+ List<msg_mission_item_int> buildUnlockBanflyZonesItem(List<UnlockZoneInfo>)
+ List<msg_mission_item_int> buildFenceMissionItem(List<DaoBanFly>)
+ void onFenceMissionUpload(List<msg_mission_item_int>)
+ void parseMAVLinkMessage(MAVLinkMessage)
+ void setMessageSenderAgent(OnMsgAgent)
}
interface com.geoai.mavlink.geoainet.mission.util.FenceProtocolAgentUtil$OnMsgAgent {
~ void onMsg(MAVLinkMessage)
}
enum com.geoai.mavlink.geoainet.mission.enums.MissionUploadState {
+  READY_TO_UPLOAD
+  INITIALLING
+  INITIALLED
+  UPLOADING
+  UPLOADED
+  READY_TO_EXECUTE
+  FAILED
+  UNKNOWN
}


com.geoai.mavlink.geoainet.mission.util.MissionUuidAgentUtil +.. com.geoai.mavlink.geoainet.mission.util.MissionUuidAgentUtil$OnMsgAgent
com.geoai.mavlink.geoainet.mission.util.MissionUuidAgentUtil +.. com.geoai.mavlink.geoainet.mission.util.MissionUuidAgentUtil$OnMissionUuidUploadResult
com.geoai.mavlink.geoainet.base.BaseManager <|-- com.geoai.mavlink.geoainet.mission.MissionInfoParse
com.geoai.mavlink.geoainet.mission.factory.action.BaseAction <|-- com.geoai.mavlink.geoainet.mission.factory.action.ActionAircraftStay
com.geoai.mavlink.geoainet.mission.factory.XmlToJson +.. com.geoai.mavlink.geoainet.mission.factory.XmlToJson$Builder
com.geoai.mavlink.geoainet.mission.factory.XmlToJson +.. com.geoai.mavlink.geoainet.mission.factory.XmlToJson$Tag
com.geoai.mavlink.geoainet.mission.factory.MissionWpmlTransition +.. com.geoai.mavlink.geoainet.mission.factory.MissionWpmlTransition$WpmlFileFormatError
com.geoai.mavlink.geoainet.mission.factory.Exception <|-- com.geoai.mavlink.geoainet.mission.factory.MissionWpmlTransition$WpmlFileFormatError
com.geoai.mavlink.geoainet.mission.factory.MissionWpmlTransition +.. com.geoai.mavlink.geoainet.mission.factory.MissionWpmlTransition$WpmlParamValueNotSupportError
com.geoai.mavlink.geoainet.mission.factory.Exception <|-- com.geoai.mavlink.geoainet.mission.factory.MissionWpmlTransition$WpmlParamValueNotSupportError
com.geoai.mavlink.geoainet.mission.factory.action.BaseAction <|-- com.geoai.mavlink.geoainet.mission.factory.action.ActionRoi
com.geoai.mavlink.geoainet.mission.factory.action.BaseAction <|-- com.geoai.mavlink.geoainet.mission.factory.action.ActionCameraRecord
com.geoai.mavlink.geoainet.mission.factory.action.BaseAction <|-- com.geoai.mavlink.geoainet.mission.factory.action.ActionGimbalRotate
com.geoai.mavlink.geoainet.mission.factory.action.BaseAction <|-- com.geoai.mavlink.geoainet.mission.factory.action.ActionCameraZoom
com.geoai.mavlink.geoainet.mission.factory.action.BaseAction <|-- com.geoai.mavlink.geoainet.mission.factory.action.ActionCameraIntervalTrigger
com.geoai.mavlink.geoainet.mission.util.MissionDownloadAgentUtil +.. com.geoai.mavlink.geoainet.mission.util.MissionDownloadAgentUtil$OnMsgAgent
com.geoai.mavlink.geoainet.mission.util.MissionDownloadAgentUtil +.. com.geoai.mavlink.geoainet.mission.util.MissionDownloadAgentUtil$OnDownloadMissionResult
java.io.Serializable <|.. com.geoai.mavlink.geoainet.mission.info.MissionStateInfo
com.geoai.mavlink.geoainet.mission.factory.action.BaseAction <|-- com.geoai.mavlink.geoainet.mission.factory.action.ActionAircraftRotate
com.geoai.mavlink.geoainet.mission.factory.MissionItemBuilder +.. com.geoai.mavlink.geoainet.mission.factory.MissionItemBuilder$Builder
com.geoai.mavlink.geoainet.mission.factory.MissionItemBuilder +.. com.geoai.mavlink.geoainet.mission.factory.MissionItemBuilder$Waypoint
com.geoai.mavlink.geoainet.mission.factory.action.BaseAction <|-- com.geoai.mavlink.geoainet.mission.factory.action.ActionCameraTakePhoto
com.geoai.mavlink.geoainet.mission.interfaces.IBaseMissionManager <|.. com.geoai.mavlink.geoainet.mission.MissionManager
com.geoai.mavlink.geoainet.mission.MissionInfoParse <|-- com.geoai.mavlink.geoainet.mission.MissionManager
com.geoai.mavlink.geoainet.mission.factory.action.BaseAction <|-- com.geoai.mavlink.geoainet.mission.factory.action.ActionCameraFocus
com.geoai.mavlink.geoainet.mission.util.FenceProtocolAgentUtil +.. com.geoai.mavlink.geoainet.mission.util.FenceProtocolAgentUtil$OnMsgAgent
@enduml
```