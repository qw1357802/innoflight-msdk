# 核心连接逻辑

## 连接方式
根据用户的使用场景设计，当前版本MSDK支持以下三种形式连接，用户可以按需进行连接使用。
- UDP
- MQTT
- Zerotier(P2P)

## 初始化过程的时序图
下面的UML时序图展示了MSDK底层通讯库中，在线检测部分（心跳连接）的相关交互过程。  
其中`openConnection()`方法表示用户调用`StarLineClientManager`的初始化方法。
```plantuml
@startuml
actor User as User
boundary StarLinkClientManager as SM
control HeartbeatManager as HB
control MavlinkManager as MVM
control MAVLinkUdpConnection as Connection
entity Socket as socket

User -> SM: openConnection
SM -> HB: HeartbeatManager.setActive(true)
activate HB

loop
    HB -> MVM: sendHeartbeat
    
    activate MVM
    MVM -> Connection: sendMessage
    note right: the effect is the same by use MQTT/P2P \n mode to connect product.
    
    activate Connection
    
    Connection -> socket: ping
    note right: Usually we use udp to connect
    
    activate socket
    socket --> Connection: pong
    deactivate socket
    
    Connection --> MVM: receivedMessage
    
    deactivate Connection

    MVM --> HB: Received Hearbeat ACK
    deactivate MVM
    
    HB --> SM: online checker
    SM --> User: product online
end

@enduml
```

## 接口调用流程图
在这里，我们假设需要调用`设置飞控最大飞行高度`：
```plantuml
@startuml
:StarlinkClientManager.getInstance();
:getProductManager(int droneID);
if (DroneID Online) then (online)
    :getFlyControllerManager();
    if (FlyController Online) then (online)
        :setMaxFlyAltitude();
        stop
    else (not online)
        :showNotOnlineTips();
        stop
    endif
else (not online)
    :showNotOnlineTips();
    stop
endif
@enduml
```

## 核心通讯连接过程UML类图
```plantuml
@startuml
class MAVLinkMqttConnection {
- MqttAsyncClient mMqttClient
- IMavlinkDataReceived mDataReceivedListener
- ArrayList<String> subscribeCrestID
- boolean mqttEnable
+ void closeConnection()
+ void openConnection()
+ int readDataBlock(byte[])
+ void sendBuffer(byte[],String)
+ void setDataReceived(IMavlinkDataReceived)
+ String getTargetAddress()
+ int getConnectedType()
+ void addTransCrestID(String)
+ void removeTransCrestID(String)
- void subscribe(String)
  }
  class MAVLinkP2pConnection {
- String PLANET_NAME
- String remoteUdpSocketTarget
- int remoteUdpSocketTargetPort
- {static} long networkId
- ZeroTierNode node
- ZeroTierDatagramSocket udpSocket
- IMavlinkDataReceived mDataReceivedListener
- Thread mZerotierReceivedThread
- boolean isEnable
- Runnable mMsgReceivedRunnable
+ <<Create>> MAVLinkP2pConnection()
+ void closeConnection()
+ void openConnection()
+ int readDataBlock(byte[])
+ void sendBuffer(byte[],String)
+ void setDataReceived(IMavlinkDataReceived)
+ String getTargetAddress()
+ int getConnectedType()
+ void setRemoteUdpIpAddress(String)
  }
  abstract class MAVLinkConnection {
+ {static} int MSG_CONNECT
+ {static} int MSG_DISCONNECT
+ {static} int MSG_CONNECTED
+ {static} int MSG_ANALYZE
- {static} int READ_BUFFER_SIZE
- Parser mParser
+ AtomicInteger mSocketConnectionStatus
- ConcurrentHashMap<String,IMAVLinkConnectionListener> mConnectionListeners
- ConcurrentHashMap<Integer,CompletionCallback.Callback> mCompletionCallback
+ LinkedBlockingQueue<MAVLinkPacketQueue> mPacketsToSend
- ScheduledExecutorService mCheckTimeOutThreadPool
- TraceMavlinkLogger mTraceMavlinkLogger
- HandlerThread mHandlerConnectionThread
- ServerConnectHandler mConnectHandler
- MavlinkAgentHandler mMavlinkMsgHandler
- int global_seq
- Runnable mMavlinkAnalyzeRunnable
- Runnable mMavlinkSendingRunnable
# <<Create>> MAVLinkConnection()
# int getSeq()
- void reportReceivedMessage(MAVLinkMessage,int,String)
- boolean reportCommandLongMessage(MAVLinkMessage,int,String)
- void handleData(int,byte[],int,String)
+ void connect()
+ void disconnect()
+ boolean isSocketConnected()
+ void addIMavLinkConnectionListener(String,IMAVLinkConnectionListener)
+ void sendMavPacket(MAVLinkPacketQueue)
+ void sendMavPacket(String,int,MAVLinkPacket,CompletionCallback.Callback)
+ void reportConnected()
+ void reportDisconnected()
+ void reportComError(String)
+ {abstract}void closeConnection()
+ {abstract}void openConnection()
+ {abstract}int readDataBlock(byte[])
+ {abstract}void sendBuffer(byte[],String)
+ {abstract}void setDataReceived(MAVLinkConnection.IMavlinkDataReceived)
+ {abstract}String getTargetAddress()
+ {abstract}int getConnectedType()
  }
  enum MAVLINK_SOCKET_CONNECTED {
+  MAVLINK_CONNECTED
+  MAVLINK_CONNECTING
+  MAVLINK_DISCONNECTED
   ~ int value
   ~ <<Create>> MAVLINK_SOCKET_CONNECTED(int)
   }
   class MAVLinkConnection$ServerConnectHandler {
- WeakReference<MAVLinkConnection> mainActivityWeakReference
  ~ <<Create>> ServerConnectHandler(Looper,MAVLinkConnection)
+ void handleMessage(Message)
  }
  class MAVLinkConnection$MavlinkAgentHandler {
- WeakReference<MAVLinkConnection> mainActivityWeakReference
  ~ <<Create>> MavlinkAgentHandler(Looper,MAVLinkConnection)
+ void handleMessage(Message)
  }
  class MAVLinkConnection$CheckTimeoutRunnable {
- short COMMAND_REPEAT_TIMES
- short COMMAND_REPEAT_DELAY_MS
- CompletionCallback.Callback iCompletionCallbackWith
- int requestId
- MAVLinkPacketQueue mavLinkPacketQueue
- int timeout
+ <<Create>> CheckTimeoutRunnable(int,CompletionCallback.Callback,MAVLinkPacketQueue)
+ <<Create>> CheckTimeoutRunnable(int,CompletionCallback.Callback,MAVLinkPacketQueue,int)
+ void run()
  }
  class MAVLinkConnection$MAVLinkPacketQueue {
- String keyMsg
- MAVLinkPacket packet
+ <<Create>> MAVLinkPacketQueue(String,MAVLinkPacket)
+ String getKeyMsg()
+ void setKeyMsg(String)
+ MAVLinkPacket getPacket()
+ void setPacket(MAVLinkPacket)
  }
  interface MAVLinkConnection$IMavlinkDataReceived {
  ~ void onReceived(byte[])
  }
  interface IMAVLinkConnection {
+ {abstract}void closeConnection()
+ {abstract}void openConnection()
+ {abstract}int readDataBlock(byte[])
+ {abstract}void sendBuffer(byte[],String)
  }
  class MAVLinkManager {
+ {static} int MAVLINK_CONN_TYPE_UDP
+ {static} int MAVLINK_CONN_TYPE_MQTT
+ {static} int MAVLINK_CONN_TYPE_P2P
- {static} MAVLinkManager instance
- MAVLinkConnection mMavlinkUdpConnection
- MAVLinkConnection mMavlinkMqttConnection
- MAVLinkConnection mMavlinkP2pConnection
- IReceiveMessageListener mReceivedMessageListener
- <<Create>> MAVLinkManager()
+ {static} MAVLinkManager getInstance()
+ void connect()
+ void sendMAVCommonPacket(BaseComponentConst,MAVLinkMessage,CompletionCallback.Callback)
- void sendMsgPackage(BaseComponentConst,MAVLinkMessage,CompletionCallback.Callback,int)
+ void sendHeartBeat(MAVLinkPacket,String)
+ void onReceiveMessage(IReceiveMessageListener)
+ MAVLinkUdpConnection getMavlinkUdpConnection()
+ MAVLinkMqttConnection getMavlinkMqttConnection()
+ MAVLinkP2pConnection getMavlinkP2pConnection()
- String getKeyMsg(BaseComponentConst)
+ void onDestroy()
  }
  interface MAVLinkManager$IReceiveMessageListener {
  ~ void onReceived(MAVLinkMessage,int,int,String)
  }
  class MAVLinkUdpConnection {
- String mSocketTarget
- int mPort_Route
- DatagramSocket socket
- AtomicBoolean mAccessoryConnectionMode
- AtomicBoolean mUsbConnectionMode
- MAVLinkAccessoryConnection mavLinkAccessoryConnection
- MAVLinkUsbConnection mavLinkUsbConnection
+ <<Create>> MAVLinkUdpConnection()
- void getUdpStream()
+ void openConnection()
+ void closeConnection()
+ void setDataReceived(IMavlinkDataReceived)
+ int readDataBlock(byte[])
+ void sendBuffer(byte[],String)
- void sentData(byte[])
+ String getTargetAddress()
+ int getConnectedType()
  }
  class UsbDetachedReceiver {
- UsbDetachedListener mUsbDetachedListener
+ <<Create>> UsbDetachedReceiver(UsbDetachedListener)
+ void onReceive(Context,Intent)
  }
  interface UsbDetachedReceiver$UsbDetachedListener {
  ~ void usbDetached()
  }
  class UsbAttachedReceiver {
- UsbAttachedListener mUsbAttachedListener
+ <<Create>> UsbAttachedReceiver(UsbAttachedListener)
+ void onReceive(Context,Intent)
  }
  interface UsbAttachedReceiver$UsbAttachedListener {
  ~ void usbAttached()
  }
  class MAVLinkAccessoryConnection {
- {static} String USB_ACTION
- OnUsbConnection mOnUsbConnectedListener
- UsbManager mUsbManager
- UsbDetachedReceiver mUsbDetachedReceiver
- OpenAccessoryReceiver mUsbOpenAccessoryReceiver
- ParcelFileDescriptor mParcelFileDescriptor
- FileInputStream mFileInputStream
- FileOutputStream mFileOutputStream
+ <<Create>> MAVLinkAccessoryConnection(OnUsbConnection)
- void initUsbAccessory()
- void openAccessory(UsbManager,UsbAccessory)
+ void usbDetached()
+ FileInputStream getFileInputStream()
+ FileOutputStream getFileOutputStream()
+ void onDestroy()
+ void usbAttached()
+ void openAccessoryModel(UsbAccessory)
+ void openAccessoryError()
  }
  interface MAVLinkAccessoryConnection$OnUsbConnection {
  ~ void onConnected(boolean)
  }
  class OpenAccessoryReceiver {
- OpenAccessoryListener mOpenAccessoryListener
+ <<Create>> OpenAccessoryReceiver(OpenAccessoryListener)
+ void onReceive(Context,Intent)
  }
  interface OpenAccessoryReceiver$OpenAccessoryListener {
  ~ void openAccessoryModel(UsbAccessory)
  ~ void openAccessoryError()
  }
  class MAVLinkUsbConnection {
- String PRODUCT_V1
- {static} String ACTION_USB_PERMISSION
- OnUsbConnection mOnUsbConnectedListener
- UsbManager mUsbManager
- UsbDeviceConnection mUsbDeviceConnection
- UsbInterface mUsbInterface
- UsbEndpoint mUsbEndpointOut
- UsbEndpoint mUsbEndpointIn
- BroadcastReceiver mUsbReceiver
+ <<Create>> MAVLinkUsbConnection(OnUsbConnection)
- void initUsb()
- void openDevices(UsbDevice)
+ int write(byte[])
+ int read(byte[])
+ void onDestroy()
  }
  interface MAVLinkUsbConnection$OnUsbConnection {
  ~ void onConnected(boolean)
  }


MAVLinkConnection <|-- MAVLinkMqttConnection
MAVLinkConnection <|-- MAVLinkP2pConnection
IMAVLinkConnection <|.. MAVLinkConnection
MAVLinkConnection +.. MAVLinkConnection$ServerConnectHandler
Handler <|-- MAVLinkConnection$ServerConnectHandler
MAVLinkConnection +.. MAVLinkConnection$MavlinkAgentHandler
Handler <|-- MAVLinkConnection$MavlinkAgentHandler
MAVLinkConnection +.. MAVLinkConnection$CheckTimeoutRunnable
Runnable <|.. MAVLinkConnection$CheckTimeoutRunnable
MAVLinkConnection +.. MAVLinkConnection$MAVLinkPacketQueue
MAVLinkConnection +.. MAVLinkConnection$IMavlinkDataReceived
MAVLinkManager +.. MAVLinkManager$IReceiveMessageListener
MAVLinkConnection <|-- MAVLinkUdpConnection
BroadcastReceiver <|-- UsbDetachedReceiver
UsbDetachedReceiver +.. UsbDetachedReceiver$UsbDetachedListener
BroadcastReceiver <|-- UsbAttachedReceiver
UsbAttachedReceiver +.. UsbAttachedReceiver$UsbAttachedListener
UsbDetachedListener <|.. MAVLinkAccessoryConnection
UsbAttachedListener <|.. MAVLinkAccessoryConnection
OpenAccessoryListener <|.. MAVLinkAccessoryConnection
MAVLinkAccessoryConnection +.. MAVLinkAccessoryConnection$OnUsbConnection
BroadcastReceiver <|-- OpenAccessoryReceiver
OpenAccessoryReceiver +.. OpenAccessoryReceiver$OpenAccessoryListener
MAVLinkUsbConnection +.. MAVLinkUsbConnection$OnUsbConnection
@enduml
```