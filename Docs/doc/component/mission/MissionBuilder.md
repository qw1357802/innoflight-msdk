# 任务相关接口文档

## Builder

---

航线朝向

|  | 名称 | 描述 |
| --- | --- | --- |
| 方法名 | setFinishAction | 设置任务结束动作 |
| 参数类型 | MissionFinishAction | 枚举值 |
| 参数 | MISSION_FINISH_ACTION_NO_ACTION = 0 | 无动作 |
|  | MISSION_FINISH_ACTION_GO_HOME = 5 | 返航 |
|  | MISSION_FINISH_ACTION_AUTO_LAND = 6 | 原地降落 |
|  | MISSION_FINISH_ACTION_GO_RALLY = 7 | 前往备降点 |
|  | MISSION_FINISH_GO_FIRST_WAYPOINT_HOVER = 11 | 前往首航点 |

---

任务结束动作

|  | 名称 | 描述 |
| --- | --- | --- |
| 方法名 | setHeadingMode | 设置任务朝向模式 |
| 参数类型 | MissionHeadingMode | 枚举值 |
| 参数 | MISSION_HEADING_MODE_AUTO = 1 | 自动朝向模式 |
|  | MISSION_HEADING_MODE_WAYPOINT = 3 | 使用航点朝向 |

---

起飞至首航点模式

|  | 名称 | 描述 |
| --- | --- | --- |
| 方法名 | flyToWaylineMode | 设置任务朝向模式 |
| 参数类型 | MissionFlyToWaylineMode | 枚举值 |
| 参数 | USING_FIRST_WAYPOINT_ALTITUDE = 0 | 爬升至安全起飞高度 |
|  | POINT_TO_POINT = 1 | 行器爬升至安全起飞高度后，倾斜飞行至首航点 |

---

任务全局速度

|  | 名称 | 描述 |
| --- | --- | --- |
| 方法名 | setMissionSpeed | 设置航线缺省速度 |
| 参数类型 | speed | 浮点值 |

<aside>
💡 此处的速度为缺省配置，如果所有的waypoint中均设置了航点速度，则此速度不会生效

</aside>

---

起飞安全航高；如若需要参数生效，请设置飞至首航点模式改成POINT_TO_POINT

|  | 名称 | 描述 |
| --- | --- | --- |
| 方法名 | takeOffSecurityHeight | 安全起飞航高 |
| 参数类型 | altitude | 浮点值 |

---

航点列表

|  | 名称 | 描述 |
| --- | --- | --- |
| 方法名 | setWaypointList | 设置航点列表 |
| 参数类型 | List<Waypoint> | 列表 |

<aside>
💡 通过此接口设置Waypoint细节，具体看Waypoint实例文档
</aside>

---

## Waypoint

航点纬度

|  | 名称 | 描述 |
| --- | --- | --- |
| 方法名 | setWayPointLatitude | 设置航点纬度 |
| 参数类型 | latitude | 浮点值 |

---

航点经度

|  | 名称 | 描述 |
| --- | --- | --- |
| 方法名 | setWayPointLongitude | 设置航点经度 |
| 参数类型 | longitude | 浮点值 |

---

航点高度

|  | 名称 | 描述 |
| --- | --- | --- |
| 方法名 | setWayPointAltitude | 设置航点高度 |
| 参数类型 | altitude | 浮点值 |

<aside>
💡 如需设置高度类型，请使用AltitudeFrame定义

</aside>

---

航点速度

|  | 名称 | 描述 |
| --- | --- | --- |
| 方法名 | setWayPointSpeed | 设置航点速度 |
| 参数类型 | speed (m/s) | 浮点值 |

---

航点转弯类型

|  | 名称 | 描述 |
| --- | --- | --- |
| 方法名 | trajectoryMode | 设置转弯类型 |
| 参数类型 | WaypointTrajectoryMode | 转弯类型 |

---

航点高度类型

|  | 名称 | 描述 |
| --- | --- | --- |
| 方法名 | altitudeFrame | 设置航高模式 |
| 参数类型 | WaypointAltitudeFrame | 航点高度类型 |

---


航向

|  | 名称 | 描述 |
| --- | --- | --- |
| 方法名 | heading | 设置飞行航向 |
| 参数类型 | heading(deg) | 浮点值 |

---

航点动作

|  | 名称 | 描述 |
| --- | --- | --- |
| 方法名 | setActions | 设置航点动作 |
| 参数类型 | BaseAction[] | Action数组 |

<aside>
💡 通过此接口配置航点动作，具体查阅BaseAction实例文档

</aside>

---


## BaseAction

| 参数名 | 参数单位 |
| --- | --- |
| ActionAircraftStay | second |
| ActionAircraftRotate | deg（-pi ~ +pi） |
| ActionCameraIntervalTrigger | int(meters), int(millisecond) |
| ActionCameraRecord | boolean(isStartRecord) |
| ActionCameraTakePhoto | boolean(isEnableShootPhoto), int(count) |
| ActionRoi | boolean(isEnable), lat, lng, alt |
| ActionCameraZoom | A：ratio（0-100）, B：focal ring |
| ActionCameraFocus | A：Focus value in metres. B：isAutoFocus |
| ActionGimbalRotate | pitch deg(-pi ~ +pi), yaw deg(-pi ~ +pi) |

<aside>
💡 基于v1.2，当前飞控、相机针对变焦、对焦的动作暂未支持

</aside>