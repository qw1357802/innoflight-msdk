
##INTERFACE
### uploadMission
|方法名|uploadMission|
| :--------  | :-----  |
|描述|* 上传任务|
|返回值|void|
|请求参数|listener（listener）
|请求参数|missionItemBuilder（任务构建器{link MissionItemBuilder}）
### uploadMission
|方法名|uploadMission|
| :--------  | :-----  |
|描述|* 上传任务|
|返回值|void|
|请求参数|listener（listener）
|请求参数|missionItemBuilder（任务构建器{link MissionItemBuilder}）
|请求参数|uuid（mission uuid）
### startMission
|方法名|startMission|
| :--------  | :-----  |
|描述|* 启动任务|
|返回值|void|
|请求参数|callback（callback）
### pauseMission
|方法名|pauseMission|
| :--------  | :-----  |
|描述|* 暂停任务|
|返回值|void|
|请求参数|callback（callback）
### continueMission
|方法名|continueMission|
| :--------  | :-----  |
|描述|* 继续任务|
|返回值|void|
|请求参数|callback（callback）
### stopMission
|方法名|stopMission|
| :--------  | :-----  |
|描述|* 停止任务|
|返回值|void|
|请求参数|callback（callback）
### stopMissionUpload
|方法名|stopMissionUpload|
| :--------  | :-----  |
|描述|* 停止上传任务|
|返回值|void|
|请求参数|callback（callback）
### setMissionStateListener
|方法名|setMissionStateListener|
| :--------  | :-----  |
|描述|* 设置任务航线执行过程监听|
|返回值|void|
|请求参数|mMissionStateListener（listener）
### getMissionUUID
|方法名|getMissionUUID|
| :--------  | :-----  |
|描述|* 获取当前任务的UUID，仅当MissionState为[执行中]时生效|
|返回值|void|
|请求参数|callback（callback）
### setMissionSignalLostBehavior
|方法名|setMissionSignalLostBehavior|
| :--------  | :-----  |
|描述|* 设置任务失联动作|
|返回值|void|
|请求参数|mode（mode）
|请求参数|callback（callback）
### getMissionSignalLostBehavior
|方法名|getMissionSignalLostBehavior|
| :--------  | :-----  |
|描述|* 获取任务失联动作|
|返回值|void|
|请求参数|callback（callback）
### sendPositionCommand
|方法名|sendPositionCommand|
| :--------  | :-----  |
|描述|* 直接通知飞行器前往指定坐标，不会触发所有任务逻辑|
|返回值|void|
|请求参数|altitude（高度(MSL)）
|请求参数|latitude（纬度）
|请求参数|callback（callback）
|请求参数|speed（速度）
|请求参数|longitude（经度）
### sendOrbitCommand
|方法名|sendOrbitCommand|
| :--------  | :-----  |
|描述|* 使飞行器在指定位置盘旋，不会触发所有任务逻辑|
|返回值|void|
|请求参数|altitude（高度(MSL)）
|请求参数|yawBehaviour（航向模式）
|请求参数|latitude（纬度）
|请求参数|callback（callback）
|请求参数|radius（半径）
|请求参数|speed（速度）
|请求参数|longitude（经度）
### downloadMission
|方法名|downloadMission|
| :--------  | :-----  |
|描述|* 下载任务队列|
|返回值|void|
|请求参数|mode（下载模式）
|请求参数|callback（callback）
### createBreakPointInfo
|方法名|createBreakPointInfo|
| :--------  | :-----  |
|描述|* 返回从当前节点构造的断点任务|
|返回值|void|
|请求参数|callback（callback）
### coverWPMLMission
|方法名|coverWPMLMission|
| :--------  | :-----  |
|描述|* 转化KMZ航线|
|返回值|void|
|请求参数|callback（callback）
|请求参数|wpml（wpml）

##INFO
### MissionCoverNotSupportNode
 * WPML转换过程中不支持的节点集合

|名称|描述|
| :--------  | :----:  |
|nodeName|节点名称|
|nodeParam|节点参数|
### MissionStateInfo
 * 用于描述任务整体状态

|名称|描述|
| :--------  | :----:  |
|missionUploadState|任务上传状态|
|missionState|航线执行状态|
|totalWaypoint|航点总数|
|currentReachWaypointIndex|当前到达航点序列|
|currentActionIndex|当前执行的航点动作序列|
|isReachedWaypoint|是否到达航点|

##ENUM
### MissionDescription
 * 任务描述

|名称|值|描述|
| :--------  | :-----  | :----:  |
|private int totalAction;|-1|总共的动作数|
|private int headingMode;|-1|偏航朝向模式|
|private int forceWaypointMode;|-1|@deprecated 暂不使用|
|private int waypointPathMode;|-1|航点飞行路径模式|
|private int finishAction = 1;|-1|任务结束动作|
|private float missionAutoSpeed;|-1|任务速度，若航点未标记速度则用此速度|
|private List<WaypointDescription> waypointList;|-1|航点描述|
### MissionFinishAction
 * 任务结束动作

|名称|值|描述|
| :--------  | :-----  | :----:  |
|MISSION_FINISH_ACTION_NO_ACTION|0|悬停|
|MISSION_FINISH_ACTION_GO_HOME|5|返航|
|MISSION_FINISH_ACTION_AUTO_LAND|6|自动降落|
|MISSION_FINISH_ACTION_GO_RALLY|7|前往就近备降点|
|MISSION_FINISH_GO_FIRST_WAYPOINT_HOVER|11|直线前往第一个航点结束任务悬停|
### MissionFlyToWaylineMode
 * 起飞至首航点模式

|名称|值|描述|
| :--------  | :-----  | :----:  |
|USING_FIRST_WAYPOINT_ALTITUDE|0|如果首航高度低于安全起飞高度，则飞行器会先爬升至安全起飞高度；否则飞行器将爬升至首航高度|
|POINT_TO_POINT|1|飞行器爬升至安全起飞高度后，倾斜飞行至首航点; 当设置了该模式，请确保[安全起飞高度]被合理赋值(相对高AGL)|
### MissionReloadMode
 * 任务加载的模式

|名称|值|描述|
| :--------  | :-----  | :----:  |
|CACHE|0|获取缓存任务，一般来说，缓存任务即是最新的任务|
|ONLINE|1|从无人机获取即时任务|
### MissionSignalLostMode
 * 飞行器返航失控策略

|名称|值|描述|
| :--------  | :-----  | :----:  |
|DISABLE|0|不响应|
|HOVER|1|悬停|
|LANDING|3|原地降落|
|GO_HOME|2|返航（默认）|
|UNKNOWN|255|未知|
### MissionState
 * 用于描述航线执行状态

|名称|值|描述|
| :--------  | :-----  | :----:  |
|BUSY|-1|当前任务管理器正忙|
|READY_TO_UPLOAD|0|等待上传|
|UPLOADING|1|上传中|
|READY_TO_EXECUTE|2|等待执行|
|EXECUTING|3|执行中|
|PAUSING|4|暂停中|
|FINISHED|5|已完成|
|UNKNOWN|55|未知|
### MissionUploadState
 * 用于判断当前任务上传进度

|名称|值|描述|
| :--------  | :-----  | :----:  |
|READY_TO_UPLOAD,|-1|等待上传|
|INITIALLING,|-1|正在建立连接|
|INITIALLED,|-1|已建立连接，准备传输|
|UPLOADING,|-1|上传中|
|UPLOADED,|-1|上传完成|
|READY_TO_EXECUTE,|-1|等待执行|
|FAILED,|-1|上传失败|
|UNKNOWN|-1|未知|
### OrbitYawBehaviour
 * 环绕任务模式

|名称|值|描述|
| :--------  | :-----  | :----:  |
|ORBIT_YAW_BEHAVIOUR_HOLD_FRONT_TO_CIRCLE_CENTER|0|指向兴趣点|
|ORBIT_YAW_BEHAVIOUR_HOLD_INITIAL_HEADING|1|保持初始航向|
|ORBIT_YAW_BEHAVIOUR_UNCONTROLLED|2|航向不进行控制|
|ORBIT_YAW_BEHAVIOUR_HOLD_FRONT_TANGENT_TO_CIRCLE|3|航向遵循飞行路径（相切）|
|ORBIT_YAW_BEHAVIOUR_RC_CONTROLLED|4|由RC输入控制|
|UNKNOWN|0|默认0.|
### WaypointAltitudeFrame
 * 航点高度类型

|名称|值|描述|
| :--------  | :-----  | :----:  |
|MSL|5|海拔高|
|AGL|6|相对高|
### WaypointDescription
 * 航点描述

|名称|值|描述|
| :--------  | :-----  | :----:  |
|private double latitude;|-1|航点纬度|
|private double longitude;|-1|航点经度|
|private double altitude;|-1|航点高度|
|private float speed;|-1|航点速度|
|private int seq;|-1|航点序列|
### WaypointHeadingMode
 * 航点朝向模式

|名称|值|描述|
| :--------  | :-----  | :----:  |
|MISSION_HEADING_MODE_AUTO|1|自动朝向：飞行过程中指向下一航点|
|MISSION_HEADING_MODE_WAYPOINT|3|航点朝向：根据航点中的heading进行配置|
|MISSION_HEADING_MODE_WAYPOINT_SYNERGY|11|边飞边转：到达航点后根据设定的heading值边飞边转(无法与Trajectory配置同时生效)|
### WaypointTrajectoryMode
 * 航点轨迹类型

|名称|值|描述|
| :--------  | :-----  | :----:  |
|TRAJECTORY_TYPE_COORDINATE|1|协调转弯(不过点，航向边飞边转)|
|TRAJECTORY_TYPE_ARRIVE|2|直线飞行(到点刹停)|
|TRAJECTORY_TYPE_ARC|3|圆弧转弯(暂未开放)|
