
##INTERFACE
### sendRTCM
|方法名|sendRTCM|
| :--------  | :-----  |
|描述|* 用于单独向飞行控制器发送RTCM数据。     * 它要求开发者自己验证差分数据的真实性。|
|返回值|void|
|请求参数|bytes（RTCM数据）
|云冠|不支持|
### setRtkStateListener
|方法名|setRtkStateListener|
| :--------  | :-----  |
|描述|* 这是一个异步接口，用于获取RTK组件状态。     * 用户需要提供一个回调函数来处理RTK组件返回的消息。|
|返回值|void|
|请求参数|listener（提供飞行器位置和网络rtk相关状态信息的RtkEntryInfo对象）
|see|RtkEntryInfo|
|云冠|不支持|
### setRtkEnable
|方法名|setRtkEnable|
| :--------  | :-----  |
|描述|* 启用或禁用RTK功能。     * 请注意，如果飞机在空中获得了固定解，调用此方法禁用RTK后将保持RTK精度一段时间。     * 但是飞行的过程可能导致飞机位置跳跃。|
|返回值|void|
|请求参数|callback（callback）
|请求参数|isEnable（启动或关闭RTK功能）
|云冠|不支持|
### getRtkEnable
|方法名|getRtkEnable|
| :--------  | :-----  |
|描述|* 获取飞行器当前是否处于RTK模式|
|返回值|boolean（当前是否处于RTK模式）|
|云冠|不支持|
### setRtkComponentType
|方法名|setRtkComponentType|
| :--------  | :-----  |
|描述|* 设置当前RTK连接模式|
|返回值|void|
|请求参数|type（RTK模式）
### getRtkComponentType
|方法名|getRtkComponentType|
| :--------  | :-----  |
|描述|* 获取当前RTK连接模式|
|返回值|RtkComponentType（type）|
### getNetWorkRtkManager
|方法名|getNetWorkRtkManager|
| :--------  | :-----  |
|描述|* 获取当前网络RTK连接实例|
|返回值|INetworkRTKManager<?>（rtk）|
### getHardwareRtkManager
|方法名|getHardwareRtkManager|
| :--------  | :-----  |
|描述|* 获取当前硬件RTK连接实例|
|返回值|IHardwareRTKManager<?>（rtk）|
### disconnectRTK
|方法名|disconnectRTK|
| :--------  | :-----  |
|描述|* 断开所有RTK连接|
|返回值|void|
### connectRTK
|方法名|connectRTK|
| :--------  | :-----  |
|描述|* SDK内部判断当前RtkComponentType进行网络RTK连接|
|返回值|void|
|apiNote|目前仅支持网络RTK自动连接；在连接前请确保参数已正确配置|
### connectRTK
|方法名|connectRTK|
| :--------  | :-----  |
|描述|* SDK内部判断当前RtkComponentType进行网络RTK连接|
|返回值|void|
|请求参数|callback（callback）

##INFO
### BaseStationInfo
 * 基准站信息源

|名称|描述|
| :--------  | :----:  |
|baseStationLatitude|基准站纬度|
|baseStationLongitude|基准站经度|
|baseStationAltitude|基准站高度（MSL）|
|baseStationITRF|ITRF|
|referenceStationId|ID|
|gpsSatelliteInfo|gps卫星信息|
|bdsSatelliteInfo|北斗卫星信息|
|glonassSatelliteInfo|格洛纳斯卫星信息|
|galileoSatelliteInfo|伽利略卫星信息|
### NtripAuthInfo
 * ntrip服务器身份验证

|名称|描述|
| :--------  | :----:  |
|ipAddress|Ntrip服务器ip地址|
|port|NtripServer服务端口|
|username|用户名|
|password|密码|
|mountPoint|挂载点|
### RtkEntryInfo
 * RTK状态信息返回

|名称|描述|
| :--------  | :----:  |
|isFlying|是否处于飞行模式|
|latitude|飞行器融合返回的纬度|
|longitude|飞行器融合返回的经度|
|altitude|飞行器融合返回的高度MSL|
|yaw|飞行器融合返回的yaw[0.00 - 360.00]|
|satelliteCount|RTK参与计算卫星数|
|ntripServiceCode|RTK网络服务返回码|
|nmeaGGA|飞行器NMEA报文-GGA|
|posType|当前RTK定位状态|
|baseStationInfo|基准站差分数据|
### null

|名称|描述|
| :--------  | :----:  |
|{|ntrip服务响应码|
|CODE_RTK_ERROR_CONNECTED_FAILED(-10210),|连接失败|
|CODE_RTK_ERROR_SERVER_RECEIVED_DATA_FAILED(-10211),|ntrip数据错误|
|CODE_RTK_ERROR_SERVER_UNKNOWN_EXCEPTION(-10212),|ntrip server未知错误|
|CODE_RTK_ERROR_NTRIP_AUTH_FAILED(-10213),|ntrip认证失败|
|CODE_RTK_ERROR_UNABLE_RECEIVED_GGA(-10214),|无法接收飞控gga报文|
|CODE_RTK_ERROR_UNABLE_RECEIVED_RTCM(-10215),|无法接收rtcm数据|
|CODE_RTK_CAP_ERROR(-10217),|RTK启动能力失败|
|CODE_RTK_NETWORK_ERROR(-10218),|RTK网络异常|
|CODE_RTK_NOT_CONNECTED(0),|RTK未连接|
|CODE_RTK_CONNECTING(10200),|RTK连接中|
|CODE_RTK_CONNECTED(10201)|RTK已连接|

##ENUM
### GPSPosType
 * GPS定位类型

|名称|值|描述|
| :--------  | :-----  | :----:  |
|GPS_FIX_TYPE_NO_GPS|0|none|
|GPS_FIX_TYPE_NO_FIX|1|no_fix|
|GPS_FIX_TYPE_2D_FIX|2|2d_fix|
|GPS_FIX_TYPE_3D_FIX|3|3d_fix|
|GPS_FIX_TYPE_DGPS|4|gps|
|GPS_FIX_TYPE_RTK_FLOAT|5|rtk_float|
|GPS_FIX_TYPE_RTK_FIXED|6|rtk_fixed|
|GPS_FIX_TYPE_STATIC|7|static|
|GPS_FIX_TYPE_PPP|8|ppp|
### RtkComponentType
 * RTK连接方式

|名称|值|描述|
| :--------  | :-----  | :----:  |
|NONE|0|无|
|QX|1|千寻|
|SIXENTS|2|六分|
|CUSTOM|3|自定义（NTRIP）|
|CMCC|4|中国移动|
|BLUETOOTH|10|蓝牙|
|USB|11|USB|
