
##INTERFACE
### setFlyControllerStateListener
|方法名|setFlyControllerStateListener|
| :--------  | :-----  |
|描述|* 设置飞行器状态监听|
|返回值|void|
|请求参数|listener（listener）
|云冠|支持|
### setDiagnosticsStateListener
|方法名|setDiagnosticsStateListener|
| :--------  | :-----  |
|描述|* 设置飞行器HMS系统状态监听回调（健康管理系统）|
|返回值|void|
|请求参数|listener（listener）
|云冠|不支持|
### startAircraftTakeoff
|方法名|startAircraftTakeoff|
| :--------  | :-----  |
|描述|* 发送无人机起飞指令；无人机会起飞到1.2m后悬停|
|返回值|void|
|请求参数|callback（callback）
|云冠|支持|
### startAircraftTakeoff
|方法名|startAircraftTakeoff|
| :--------  | :-----  |
|描述|* 发送无人机起飞指令|
|返回值|void|
|请求参数|altitude（起飞悬停高度）
|请求参数|callback（callback）
|云冠|不支持|
### startAircraftLand
|方法名|startAircraftLand|
| :--------  | :-----  |
|描述|* 发送无人机降落指令|
|返回值|void|
|请求参数|callback（callback）
|请求参数|isNeedPrecland（是否需要精准降落）
|云冠|支持|
### stopAircraftLand
|方法名|stopAircraftLand|
| :--------  | :-----  |
|描述|* 发送无人机取消降落指令|
|返回值|void|
|请求参数|callback（callback）
|云冠|支持|
### startAircraftGoHome
|方法名|startAircraftGoHome|
| :--------  | :-----  |
|描述|* 发送无人机返航指令；无人机会先升高到返航高度后返航|
|返回值|void|
|请求参数|callback（callback）
|云冠|支持|
### startAircraftReturn
|方法名|startAircraftReturn|
| :--------  | :-----  |
|描述|* 发送无人机返航指令；具体返航动作详看说明书|
|返回值|void|
|请求参数|callback（callback）
|请求参数|type（返航模式）
|云冠|支持|
### startAircraftReturnAssignRally
|方法名|startAircraftReturnAssignRally|
| :--------  | :-----  |
|描述|* 发送无人机返航指令；前往指定的备降点|
|返回值|void|
|请求参数|index（index）
|请求参数|callback（callback）
|云冠|不支持|
### stopAircraftGoHome
|方法名|stopAircraftGoHome|
| :--------  | :-----  |
|描述|* 发送无人机取消返航指令|
|返回值|void|
|请求参数|callback（callback）
|云冠|支持|
### setGoHomeAltitude
|方法名|setGoHomeAltitude|
| :--------  | :-----  |
|描述|* 设置无人机返航高度|
|返回值|void|
|请求参数|altitude（返航高度）
|请求参数|callback（callback）
|云冠|不支持|
### getGoHomeAltitude
|方法名|getGoHomeAltitude|
| :--------  | :-----  |
|描述|* 获取无人机返航高度|
|返回值|void|
|请求参数|callback（callback）
|云冠|不支持|
### getGoHomeLocation
|方法名|getGoHomeLocation|
| :--------  | :-----  |
|描述|* 获取无人机当前返航点坐标|
|返回值|void|
|请求参数|callback（callback）
|云冠|支持|
### stopAllAction
|方法名|stopAllAction|
| :--------  | :-----  |
|描述|* 终止飞行器动作指令并悬停|
|返回值|void|
|请求参数|callback（callback）
|云冠|支持|
### startForceLanding
|方法名|startForceLanding|
| :--------  | :-----  |
|描述|* 发送无人机强制降落（不会进入视觉降落程序）|
|返回值|void|
|请求参数|callback（callback）
|云冠|支持|
### setDroneArm
|方法名|setDroneArm|
| :--------  | :-----  |
|描述|* 飞机上锁|
|返回值|void|
|请求参数|callback（callback）
|云冠|不支持|
### setDroneDisarm
|方法名|setDroneDisarm|
| :--------  | :-----  |
|描述|* 飞机解锁|
|返回值|void|
|请求参数|callback（callback）
|云冠|不支持|
### setJoyStickEnable
|方法名|setJoyStickEnable|
| :--------  | :-----  |
|描述|* 飞行器启动虚拟摇杆(已过时)|
|返回值|void|
|请求参数|callback（callback）
|云冠|支持|
### setNewJoyStickEnable
|方法名|setNewJoyStickEnable|
| :--------  | :-----  |
|描述|* 飞行器启动虚拟摇杆（速度控制）|
|返回值|void|
|请求参数|callback（callback）
|请求参数|isEnable（enable）
### sendJoyStickControl
|方法名|sendJoyStickControl|
| :--------  | :-----  |
|描述|* 发送虚拟摇杆指令（美国手）(已过时)|
|返回值|void|
|请求参数|r（yaw）
|请求参数|x（pitch）
|请求参数|y（roll）
|请求参数|callback（callback）
|请求参数|z（throttle）
|云冠|支持|
### sendNewJoyStickControl
|方法名|sendNewJoyStickControl|
| :--------  | :-----  |
|描述|* 发送虚拟摇杆指令（速度控制）（北东地）（飞机坐标系）|
|返回值|void|
|请求参数|speed_r（r(弧度)）
|请求参数|speed_z（z(m/s)）
|请求参数|callback（callback）
|请求参数|speed_x（x(m/s)）
|请求参数|speed_y（y(m/s)）
### getAlternateLandingPoints
|方法名|getAlternateLandingPoints|
| :--------  | :-----  |
|描述|* 获取备降点|
|返回值|void|
|请求参数|callback（callback）
|云冠|不支持|
### setAlternateLandingPoints
|方法名|setAlternateLandingPoints|
| :--------  | :-----  |
|描述|* 设置备降点|
|返回值|void|
|请求参数|callback（callback）
|请求参数|points（备降点列表）
|云冠|不支持|
### setFcAltitudeLimit
|方法名|setFcAltitudeLimit|
| :--------  | :-----  |
|描述|* 设置飞行器最大飞行高度|
|返回值|void|
|请求参数|altitude（AGL高度）
|请求参数|callback（callback）
|云冠|不支持|
### getFcAltitudeLimit
|方法名|getFcAltitudeLimit|
| :--------  | :-----  |
|描述|* 获取飞行器最大飞行高度|
|返回值|void|
|请求参数|callback（callback）
|云冠|不支持|
### setFcDistanceLimit
|方法名|setFcDistanceLimit|
| :--------  | :-----  |
|描述|* 设置飞行器最远飞行距离|
|返回值|void|
|请求参数|distance（水平飞行距离）
|请求参数|callback（callback）
|云冠|不支持|
### getFcDistanceLimit
|方法名|getFcDistanceLimit|
| :--------  | :-----  |
|描述|* 获取飞行器最远飞行距离|
|返回值|void|
|请求参数|callback（callback）
### setSimulateMode
|方法名|setSimulateMode|
| :--------  | :-----  |
|描述|* 设置模拟器模式|
|返回值|void|
|请求参数|callback（callback）
|请求参数|isEnable（是否开启模拟器）
|云冠|不支持|
### getSimulateModeEnable
|方法名|getSimulateModeEnable|
| :--------  | :-----  |
|描述|* 获取当前是否处于模拟器模式|
|返回值|void|
|请求参数|callback（callback）
|云冠|不支持|
### setHorizonSpeedLimit
|方法名|setHorizonSpeedLimit|
| :--------  | :-----  |
|描述|* 设置最大水平飞行速度|
|返回值|void|
|请求参数|callback（callback）
|请求参数|speedLimit（飞行速度m/s）
|云冠|不支持|
### getHorizonSpeedLimit
|方法名|getHorizonSpeedLimit|
| :--------  | :-----  |
|描述|* 获取最大水平飞行速度|
|返回值|void|
|请求参数|callback（callback）
|云冠|不支持|
### getVerticalUpSpeedLimit
|方法名|getVerticalUpSpeedLimit|
| :--------  | :-----  |
|描述|* 获取最大爬升速度|
|返回值|void|
|请求参数|callback（callback）
### getVerticalDownSpeedLimit
|方法名|getVerticalDownSpeedLimit|
| :--------  | :-----  |
|描述|* 获取最大下降速度|
|返回值|void|
|请求参数|callback（callback）
### setConnectionFailSafeBehavior
|方法名|setConnectionFailSafeBehavior|
| :--------  | :-----  |
|描述|* 设置飞控失联动作（已过时v1.4）|
|返回值|void|
|请求参数|mode（mode）
|请求参数|callback（callback）
### getConnectionFailSafeBehavior
|方法名|getConnectionFailSafeBehavior|
| :--------  | :-----  |
|描述|* 获取飞控失联动作（已过时v1.4）|
|返回值|void|
|请求参数|callback（callback）
### setRcLostBehavior
|方法名|setRcLostBehavior|
| :--------  | :-----  |
|描述|* 设置遥控器失联动作（仅对单手控生效）     * 如需设置虚拟摇杆模式/任务模式的失联动作，请翻阅文档|
|返回值|void|
|请求参数|mode（mode）
|请求参数|callback（callback）
### getRcLostBehavior
|方法名|getRcLostBehavior|
| :--------  | :-----  |
|描述|* 获取遥控器失联动作|
|返回值|void|
|请求参数|callback（callback）
### setJoystickCommandLostBehavior
|方法名|setJoystickCommandLostBehavior|
| :--------  | :-----  |
|描述|* 设置虚拟摇杆失联动作|
|返回值|void|
|请求参数|mode（mode）
|请求参数|callback（callback）
### getJoystickCommandLostBehavior
|方法名|getJoystickCommandLostBehavior|
| :--------  | :-----  |
|描述|* 获取虚拟摇杆失联动作|
|返回值|void|
|请求参数|callback（callback）
### setHomeLocationUsingAircraftCurrentLocation
|方法名|setHomeLocationUsingAircraftCurrentLocation|
| :--------  | :-----  |
|描述|* 用当前无人机位置设置为返航点|
|返回值|void|
|请求参数|callback（callback）
### setHomeLocation
|方法名|setHomeLocation|
| :--------  | :-----  |
|描述|* 设置返航点位置|
|返回值|void|
|请求参数|latitude（纬度）
|请求参数|callback（callback）
|请求参数|longitude（经度）
### setPrecisionLandModeEnable
|方法名|setPrecisionLandModeEnable|
| :--------  | :-----  |
|描述|* 设置精准降落模式|
|返回值|void|
|请求参数|precisionLandModeEnable（精准降落模式）
|请求参数|callback（callback）
### getPrecisionLandModeEnable
|方法名|getPrecisionLandModeEnable|
| :--------  | :-----  |
|描述|* 获取精准降落模式|
|返回值|void|
|请求参数|callback（callback）
### setLedEnable
|方法名|setLedEnable|
| :--------  | :-----  |
|描述|* 设置机身LED开关|
|返回值|void|
|请求参数|callback（callback）
|请求参数|isEnable（是否打开）
### getLedEnable
|方法名|getLedEnable|
| :--------  | :-----  |
|描述|* 获取机身LED开关|
|返回值|void|
|请求参数|callback（callback）
### rebootFC
|方法名|rebootFC|
| :--------  | :-----  |
|描述|* 重启飞机|
|返回值|void|
|请求参数|callback（callback）
### startCalibrationCompass
|方法名|startCalibrationCompass|
| :--------  | :-----  |
|描述|* 启动指南针校准|
|返回值|void|
|请求参数|callback（callback）
### stopCalibrationCompass
|方法名|stopCalibrationCompass|
| :--------  | :-----  |
|描述|* 终止指南针校准|
|返回值|void|
|请求参数|callback（callback）
### setCalibrationCompassListener
|方法名|setCalibrationCompassListener|
| :--------  | :-----  |
|描述|* 设置指南针校准状态监听|
|返回值|void|
|请求参数|callback（callback）
### setMotorTestCommand
|方法名|setMotorTestCommand|
| :--------  | :-----  |
|描述|* 飞行器电机电调测试指令|
|返回值|void|
|请求参数|callback（callback）
### flightTermination
|方法名|flightTermination|
| :--------  | :-----  |
|描述|* 紧急终止飞行，电机强制上锁|
|返回值|void|
|请求参数|callback（callback）
### setDownwardFillLightMode
|方法名|setDownwardFillLightMode|
| :--------  | :-----  |
|描述|* 设置底部补光灯模式|
|返回值|void|
|请求参数|fillLightMode（补光灯模式）
|请求参数|callback（callback）
### getDownwardFillLightMode
|方法名|getDownwardFillLightMode|
| :--------  | :-----  |
|描述|* 获取底部补光灯模式|
|返回值|void|
|请求参数|callback（callback）
### setFcLoggingControl
|方法名|setFcLoggingControl|
| :--------  | :-----  |
|描述|* 设置无人机日志输出开关|
|返回值|void|
|请求参数|callback（callback）
|请求参数|isEnable（开关）
### setNestLandingStatus
|方法名|setNestLandingStatus|
| :--------  | :-----  |
|描述|* 上报当前机巢降落标志位|
|返回值|void|
|请求参数|isReady（是否已经准备就绪）
### getSn
|方法名|getSn|
| :--------  | :-----  |
|描述|* 获取飞控SN码|
|返回值|void|
|请求参数|callback（callback）

##INFO
### AircraftDiagnosticsStateInfo
 * hms健康管理系统状态回调

|名称|描述|
| :--------  | :----:  |
|alertCode|告警错误码|
|alertIndex|告警序列|
|alertLevel|告警等级|
|timestamp|告警时间|
|holdTime|持续时间|
|args|内部参数|
### CompassCalibrationStateInfo
 * 指南针校准状态

|名称|描述|
| :--------  | :----:  |
|mCompassCalibrationRunning|是否处于校准模式|
|mCompassCalibrationPaddings|需要执行的校准动作|
|mCurrentPadding|当前校准动作|
|mCompassCalibrationState|校准状态|
|percent|校准进度|
### FlyControllerStateInfo
 * 飞控系统状态回调

|名称|描述|
| :--------  | :----:  |
|isReadyToFly|飞行准备完成标志位|
|aircraftLatitude|飞行器融合纬度|
|aircraftLongitude|飞行器融合经度|
|aircraftAltitude|飞行器融合高度(m)|
|aircraftPitch|飞行器pitch姿态角(-pi..+pi)|
|aircraftRoll|飞行器roll姿态角(-pi..+pi)|
|aircraftYaw|飞行器yaw姿态角(-pi..+pi)|
|aircraftVelocityX|飞行器机身坐标系x轴速度(rad/s)|
|aircraftVelocityY|飞行器机身坐标系y轴速度(rad/s)|
|aircraftVelocityZ|飞行器机身坐标系z轴速度(rad/s)|
|aircraftGpsSatelliteNumber|飞行器GPS卫星数|
|aircraftHomeLatitude|飞行器返航点纬度|
|aircraftHomeLongitude|飞行器返航点经度|
|aircraftHomeAltitude|飞行器返航点高度（MSL）(m)|
|flyMode|当前飞控模式|
|isMotorsOn|当前电机是否启动|
|isFlying|当前飞机是否在空中|
|flyTime|飞行器飞行时间|
|posType|当前飞行器定位模式|
|aircraftReturnLatitude|飞行器返回点纬度|
|aircraftReturnLongitude|飞行器返回点经度|
|aircraftReturnDestinationType|飞行器返航点类型|
|aircraftReturnMode|飞行器返航路径类型|
|aircraftReturnState|飞行器返航动作|
|flightMileage|飞行里程(m)|

##ENUM
### AircraftDestinationType
 * 返航目标点类型

|名称|值|描述|
| :--------  | :-----  | :----:  |
|RTL_DEST_UNKNOWN|0|未知|
|RTL_DEST_TYPE_HOME|1|返航|
|RTL_DEST_TYPE_LAND|2|降落|
|RTL_DEST_TYPE_RALLY|3|备降点|
### AircraftFailSafeBehaviorMode
 * 飞行器返航失控策略

|名称|值|描述|
| :--------  | :-----  | :----:  |
|HOVER|1|悬停|
|LANDING|3|原地降落|
|GO_HOME|2|返航（默认）|
|UNKNOWN|0|未知|
### AircraftFlyMode
 * 飞行模式

|名称|值|描述|
| :--------  | :-----  | :----:  |
|ORBIT|-1|轨道/环绕模式：仅在使用Orbit功能时生效。|
|MANUAL|-1|手动模式：仅在固定翼模式下生效。|
|ATTI|-1|定高模式：仅能依靠气压计在竖直方向上相对稳定，在无GPS环境下生效。|
|JOYSTICK|-1|虚拟摇杆模式：在进入虚拟摇杆功能时生效。|
|STABILIZED|-1|增稳模式：手动控制俯仰、横滚、偏航、油门指令。|
|AUTO_TAKEOFF|-1|自动起飞中：表示正在进行起飞指令|
|HOVER|-1|悬停模式：表示正在悬停等待下一步指令。|
|GPS_WAYPOINT|-1|航点任务模式：表示正在执行航点任务。|
|GO_HOME|-1|返航模式：表示正在执行返航指令。|
|LANDING|-1|降落模式：表示正在执行降落指令|
|PRECLAND|-1|精准降落模式：表示正在执行精准降落指令。|
|POSITION|-1|定位模式/手控模式：表示能在当前三维坐标下保持相对静止，并且RC可以进行控制|
|UNKNOWN|-1|未知|
### AircraftPrecisionLandMode
 * 精准降落配置参数

|名称|值|描述|
| :--------  | :-----  | :----:  |
|NO_PRECISION_LANDING|0|不需要精准降落|
|AUTO_PRECISION_LANDING|1|在有条件的情况下精准降落|
|REQUIRED_PRECISION_LANDING|2|强制精准降落|
### AircraftReturnMode
 * 飞机返航模式枚举类型，用于描述飞机的返航模式。

|名称|值|描述|
| :--------  | :-----  | :----:  |
|RTL_TYPE_DEFAULT|0|默认方式返航。（返航方式取决于参数RTL_TYPE）|
|RTL_TYPE_MISSION_LANDING|1|直线返回（最近的）降落点、备降点。|
|RTL_TYPE_MISSION_LANDING_REVERSED|2|如果存在降落点，则沿路径返回降落点，否则沿原路径返回home点。|
|RTL_TYPE_CLOSEST|3|直线返回（最近的）home点、降落点、备降点。|
|RTL_TYPE_CLOSEST_HOME_LAND|4|直线返回（最近的）home点、降落点。|
|RTL_TYPE_HOME|5|直线返回home点。|
|RTL_TYPE_LAND|6|直线返回降落点（如果不存在降落点，则返回home点）。|
|RTL_TYPE_CLOSEST_RALLY|7|直线返回最近的备降点。（如果不存在备降点，则返回最近的home点或降落点）|
|RTL_TYPE_ASSIGN_RALLY|8|直线返回指定的备降点。（如果不存在备降点，则返回最近的home点或降落点）|
|RTL_TYPE_BY_PASS_CLOSEST_HOME_LAND|9|沿路径返回（最近的）home点、降落点。|
|RTL_TYPE_BY_PASS_CLOSEST_HOME|10|沿路径返回home点。|
|RTL_TYPE_BY_PASS_LAND|11|沿路径返回降落点。（如果不存在降落点，则返回home点）|
|RTL_TYPE_HOME_OR_RALLY|12|直线返回（最近的）home点、备降点。|
### AircraftReturnState
 * 飞机返航状态枚举类型，用于描述飞机当前的返航状态。

|名称|值|描述|
| :--------  | :-----  | :----:  |
|RTL_STATE_NONE|0|未返航|
|RTL_STATE_CLIMB|1|爬升|
|RTL_STATE_RETURN|2|直线飞往返航目标点|
|RTL_STATE_DESCEND|3|固定翼模式下降|
|RTL_STATE_LOITER|4|悬停等待|
|RTL_STATE_TRANSITION_TO_MC|5|切换成旋翼|
|RTL_MOVE_TO_LAND_HOVER_VTOL|6|垂起飞机旋翼模式飞往目标点|
|RTL_STATE_LAND|7|降落|
|RTL_STATE_LANDED|8|降落完成|
|RTL_STATE_HEAD_TO_CENTER|9|调整返航朝向|
|RTL_STATE_BY_PASS|10|沿路径飞往目标返航点|
|RTL_STATE_BREAK|11|刹车|
### CompassCalibrationPadding
 * 罗盘朝向

|名称|值|描述|
| :--------  | :-----  | :----:  |
|UNKNOWN,|-1|unknown|
|LEFT,|-1|left|
|RIGHT,|-1|right|
|UP,|-1|up|
|DOWN,|-1|down|
|FRONT,|-1|front|
|BACK;|-1|back|
### CompassCalibrationStateMode
 * 罗盘校准状态

|名称|值|描述|
| :--------  | :-----  | :----:  |
|NORMAL|0|Compass calibration has not been triggered.|
|CALCULATE|2|Calculate the calibration.|
|SUCCESS_TO_NEXT|4|The calibration is successful.|
|SUCCESS|5|The calibration is successful.|
|HOLD_ON_SIDE|7|Hold vehicle still on a pending side|
|UNKNOWN|-1|Unknown status.|
|NOT_FOUND|-2|Compass not found|
|CANCELLED|-3|User cancel|
### FillLightMode
 * 补光灯模式

|名称|值|描述|
| :--------  | :-----  | :----:  |
|AUTO|255|自动模式|
|ON|1|常亮|
|OFF|0|常关|
|UNKNOWN|-1|状态未知|
### HmsSeverityLevel
 * HMS告警等级

|名称|值|描述|
| :--------  | :-----  | :----:  |
|MAV_SEVERITY_EMERGENCY|0|紧急突发事件|
|MAV_SEVERITY_ALERT|1|提醒事件|
|MAV_SEVERITY_CRITICAL|2|危机事件|
|MAV_SEVERITY_ERROR|3|错误事件|
|MAV_SEVERITY_WARNING|4|告警事件|
|MAV_SEVERITY_NOTICE|5|通知事件|
|MAV_SEVERITY_INFO|6|常规事件|
|MAV_SEVERITY_DEBUG|7|调试输出|
### JoystickCommandLostMode
 * 飞行器返航失控策略

|名称|值|描述|
| :--------  | :-----  | :----:  |
|DISABLE|-1|不响应|
|HOVER|1|悬停|
|LANDING|0|原地降落|
|GO_HOME|2|返航（默认）|
|UNKNOWN|255|未知|
