
##INTERFACE
### void setObstacleDataListener
|方法名|void setObstacleDataListener|
| :--------  | :-----  |
|描述|* 设置避障数据监听|
|返回值|void|
|请求参数|listener（listener）
### void setObstacleAvoidanceType
|方法名|void setObstacleAvoidanceType|
| :--------  | :-----  |
|描述|* 设置避障飞行模式|
|返回值|void|
|请求参数|callback（callback）
|请求参数|obstacleAvoidanceType（避障模式）
### void getObstacleAvoidanceType
|方法名|void getObstacleAvoidanceType|
| :--------  | :-----  |
|描述|* 获取当前避障飞行模式|
|返回值|void|
|请求参数|callback（callback）
### void setObstacleAvoidanceBrakingDistance
|方法名|void setObstacleAvoidanceBrakingDistance|
| :--------  | :-----  |
|描述|* 设置避障刹车距离     * 注意：设置刹车距离表示打开避障模块|
|返回值|void|
|请求参数|distance（刹停距离(unit m)）
|请求参数|callback（callback）
### void getObstacleAvoidanceBrakingDistance
|方法名|void getObstacleAvoidanceBrakingDistance|
| :--------  | :-----  |
|描述|* 获取避障刹车距离     * 注意：当关闭避障时，api返回距离 <= 0|
|返回值|void|
|请求参数|callback（callback）
### void setObstacleAvoidanceEnable
|方法名|void setObstacleAvoidanceEnable|
| :--------  | :-----  |
|描述|* 设置避障开关|
|返回值|void|
|请求参数|callback（callback）
|请求参数|isEnable（开关）
### void getObstacleAvoidanceEnable
|方法名|void getObstacleAvoidanceEnable|
| :--------  | :-----  |
|描述|* 获取当前避障是否打开|
|返回值|void|
|请求参数|callback（callback）
### int initObstacleCalibrationLibrary
|方法名|int initObstacleCalibrationLibrary|
| :--------  | :-----  |
|描述|* 初始化校准库，用于后期库文件缓加载|
|返回值|void|
|请求参数|chesSize（棋盘格尺寸 (unit:mm)）
|请求参数|visionParamPath（校准文件存储目录）
|请求参数|deviation（冗余误差 (default: 2000)）
### int releaseObstacleCalibration
|方法名|int releaseObstacleCalibration|
| :--------  | :-----  |
|描述|* 校准算法析构|
|返回值|void|
### int startObstacleCalibration
|方法名|int startObstacleCalibration|
| :--------  | :-----  |
|描述|* 启动校准算法     * notion: 这是一个同步算法，避免在主线程调用|
|返回值|void|
|请求参数|retData（回调数据(yuv frame)）
|请求参数|yuvData（yuv数据  i420）
### void setObstacleCameraSource
|方法名|void setObstacleCameraSource|
| :--------  | :-----  |
|描述|* 设置避障相机视频源|
|返回值|void|
|请求参数|cameraSource（视频源）
|请求参数|callback（callback）
### void getObstacleCameraSource
|方法名|void getObstacleCameraSource|
| :--------  | :-----  |
|描述|* 获取避障相机数据源|
|返回值|void|
|请求参数|callback（callback）
### void uploadObstacleCalibrationFile
|方法名|void uploadObstacleCalibrationFile|
| :--------  | :-----  |
|描述|* 上传避障文件|
|返回值|void|
|请求参数|filePath（文件路径）
|请求参数|callback（callback）
|请求参数|source（该文件表示的相机源）
### String getObstacleCalibrationVersion
|方法名|String getObstacleCalibrationVersion|
| :--------  | :-----  |
|描述|* 获取校准库版本号|
|返回值|public（version）|

##INFO
### ObstacleStateInfo
 * 避障数据回调

|名称|描述|
| :--------  | :----:  |
|horizontalAngleInterval|水平角度间距|
|horizontalObstacleDistance|避障数据|
|minObstacleDistance|最小避障距离；当数据中值小于最小避障数据，则数据无效|
|maxObstacleDistance|最大避障距离；当数据中值大于最大避障数据，则数据无效|

##ENUM
### ObstacleAvoidanceType
 * 避障类型

|名称|值|描述|
| :--------  | :-----  | :----:  |
|BRAKE|0|避障刹停|
|BYPASS|1|绕行|
|UNKNOWN|255|未知|
### ObstacleCalibrationSource
 * 双目校准视频源

|名称|值|描述|
| :--------  | :-----  | :----:  |
|OBSTACLE_CAMERA_FRONT|6|前视双目|
|OBSTACLE_CAMERA_TAIL|4|后视双目|
|OBSTACLE_CAMERA_LEFT|5|左视双目|
|OBSTACLE_CAMERA_RIGHT|2|右视双目|
|OBSTACLE_CAMERA_UP|3|上视双目|
|OBSTACLE_CAMERA_DOWN|1|下视双目|
### ObstacleCameraSource
 * 避障相机源

|名称|值|描述|
| :--------  | :-----  | :----:  |
|LIVEVIEW_CAMERA_SOURCE_DEFAULT|255|默认类型（未知类型）|
|OBSTACLE_CAMERA_FPV|10|fpv|
|OBSTACLE_CAMERA_FRONT|11|前视双目|
|OBSTACLE_CAMERA_TAIL|12|后视双目|
|OBSTACLE_CAMERA_LEFT|13|左视双目|
|OBSTACLE_CAMERA_RIGHT|14|右视双目|
|OBSTACLE_CAMERA_UP|15|上视双目|
|OBSTACLE_CAMERA_DOWN|16|下视双目|
|OBSTACLE_CAMERA_ALL|20|六组双目|
