
##INTERFACE
### setAirlinkComponentConnectedListener
|方法名|setAirlinkComponentConnectedListener|
| :--------  | :-----  |
|描述|* 设置Airlink相关组件连接监听器|
|返回值|void|
|请求参数|listener（listener）
### getDsp
|方法名|getDsp|
| :--------  | :-----  |
|描述|* 获取DSP组件|
|返回值|IBaseDspListener（dsp）|
### getRc
|方法名|getRc|
| :--------  | :-----  |
|描述|* 获取RC组件|
|返回值|IBaseRemoteControllerListener（rc）|
### getCellular
|方法名|getCellular|
| :--------  | :-----  |
|描述|* 获取CELLULAR组件|
|返回值|IBaseCellularListener（cellular）|
## cellular-CellularModule
### setVideoChannelQualityListener
|方法名|setVideoChannelQualityListener|
| :--------  | :-----  |
|描述|* 设置LTE图传信号质量回调|
|返回值|void|
|请求参数|cellularStateListener（listener）
### getCellularConnectionState
|方法名|getCellularConnectionState|
| :--------  | :-----  |
|描述|* 获取当前LTE连接状态|
|返回值|void|
|请求参数|callback（callback）
### setCellularEnhancedTransmissionType
|方法名|setCellularEnhancedTransmissionType|
| :--------  | :-----  |
|描述|* 设置LTE增强传输模式|
|返回值|void|
|请求参数|callback（callback）
|请求参数|transmissionType（模式）
### getCellularEnhancedTransmissionType
|方法名|getCellularEnhancedTransmissionType|
| :--------  | :-----  |
|描述|* 获取当前LTE增强传输模式|
|返回值|void|
|请求参数|callback（callback）
## dsp-AbstractDsp
### setWirelessSignalQualityListener
|方法名|setWirelessSignalQualityListener|
| :--------  | :-----  |
|描述|* 获取无线电上下行信号信号质量|
|返回值|void|
|请求参数|listener（listener）
### getWirelessSignalQuality
|方法名|getWirelessSignalQuality|
| :--------  | :-----  |
|描述|* 获取无线电上下行信号质量|
|返回值|WirelessSignalInfo（info）|
### startPairing
|方法名|startPairing|
| :--------  | :-----  |
|描述|* 启动对频|
|返回值|void|
|请求参数|callback（callback）
## dsp-BigFishModule
### getHardwareSystemInfo
|方法名|getHardwareSystemInfo|
| :--------  | :-----  |
|描述|* 获取无线电硬件信息|
|返回值|void|
|请求参数|listener（listener）
### setDataTransmissionRateListener
|方法名|setDataTransmissionRateListener|
| :--------  | :-----  |
|描述|* 获取无线电系统信道传输能力|
|返回值|void|
|请求参数|listener（listener）
### setAntennaSignalQualityListener
|方法名|setAntennaSignalQualityListener|
| :--------  | :-----  |
|描述|* 获取无线电天线物理层通讯质量|
|返回值|void|
|请求参数|listener（listener）
### getChannelInterfaceStatus
|方法名|getChannelInterfaceStatus|
| :--------  | :-----  |
|描述|* 设置无线电频点扫描监听|
|返回值|void|
|请求参数|listener（listener）
### getFrequencyHopping
|方法名|getFrequencyHopping|
| :--------  | :-----  |
|描述|* 获取频点配置|
|返回值|void|
|请求参数|listener（listener）
### getSignalBandwidth
|方法名|getSignalBandwidth|
| :--------  | :-----  |
|描述|* 获取上下行信号带宽配置|
|返回值|void|
|请求参数|listener（listener）
## dsp-P301DModule
### getWirelessDataStateInfo
|方法名|getWirelessDataStateInfo|
| :--------  | :-----  |
|描述|* 获取实时无线电硬件状态信息|
|返回值|void|
|请求参数|callback（callback）
### getWirelessVersion
|方法名|getWirelessVersion|
| :--------  | :-----  |
|描述|* 获取地面端图传模块版本号|
|返回值|void|
|请求参数|callback（callback）
### getWirelessDataStateInfo
|方法名|getWirelessDataStateInfo|
| :--------  | :-----  |
|描述|* 获取实时无线电硬件状态信息|
|返回值|void|
## rc-AbstractRc
### setAircraftMappingStyle
|方法名|setAircraftMappingStyle|
| :--------  | :-----  |
|描述|* 设置遥控器映射模式|
|返回值|void|
|请求参数|callback（callback）
|请求参数|style（映射模式）
### getAircraftMappingStyle
|方法名|getAircraftMappingStyle|
| :--------  | :-----  |
|描述|* 获取遥控器映射模式|
|返回值|void|
|请求参数|callback（callback）
## rc-K3QModule
### setHardwareStateListener
|方法名|setHardwareStateListener|
| :--------  | :-----  |
|描述|* 设置硬件状态监听|
|返回值|void|
|请求参数|listener（listener）
## rc-K3RModule
### setHardwareStateListener
|方法名|setHardwareStateListener|
| :--------  | :-----  |
|描述|* 设置硬件状态监听|
|返回值|void|
|请求参数|listener（listener）

##INFO
### AntennaSignalInfo
 * 无线电模块天线物理层数据反馈

|名称|描述|
| :--------  | :----:  |
|masterNode|主节点（地面端）|
|slaveNode|从节点（天空端）|
|mainAntenna|主天线|
|subAntenna|辅天线|
|agc|自动增益|
|level|无线电信号等级|
|rsrp|接收信号功率|
|rssi|接收信号强度|
|snr|信噪比|
### AutoReturnButtonInfo
 * 自动回弹按钮

|名称|描述|
| :--------  | :----:  |
|isClicked|是否按下|
### null

|名称|描述|
| :--------  | :----:  |
### CellularQualityInfo
 * LTE图传信号质量

|名称|描述|
| :--------  | :----:  |
|packetLoss|丢包|
|receivedBitrate|接收波特率|
|outputFrameRate|输出帧率|
|remoteWidth|远端分辨率宽|
|remoteHeight|远端分辨率高|
### CellularServerConnectionInfo
 * LTE服务器连接状态

|名称|描述|
| :--------  | :----:  |
|connectState|当前连接状态|
|cellularServerConnectionChangedState|连接被改变的原因：当状态回调返回JOIN_SUCCESS或REJOIN_SUCCESS表示当前服务已连接|
### DialInfo
 * 波轮按钮

|名称|描述|
| :--------  | :----:  |
|value|值|
### K3QButtonHardwareInfo
 * K3Q遥控器硬件属性

|名称|描述|
| :--------  | :----:  |
|leftStick|左摇杆|
|rightStick|右摇杆|
|leftDial|左拨轮|
|rightDial|右波轮|
|leftTopButton|左上的按钮|
|leftBottomButton|左下的按钮|
|rightTopButton|右上的按钮|
|rightBottomButton|右下的按钮|
|leftCustomButton|左侧自定义键|
|rightCustomButton|右侧自定义键|
|leftSwitchButton|左侧三档开关|
|rightSwitchButton|右侧三档开关|
### ManualReturnButtonInfo
 * 自锁按钮

|名称|描述|
| :--------  | :----:  |
|value|值|
### P301WirelessDataStateInfo
 * P301无线电模块硬件数据回调

|名称|描述|
| :--------  | :----:  |
|ts|ts|
|isLinkNodeConnected|当前链路是否正常连接|
|frequentPoint|当前频点|
|slotTxFrequent|模组TX时隙的频点|
|slotRxFrequent|模组RX时隙的频点|
|gndMcs|基带调制编码策略|
|airMcs|天空调制编码策略|
|gndSnr|地面端模组接收信号的信噪比|
|gndRssi|接收端模组接收信号的信号强渡|
|airSnr|天空端模组接收信号的信噪比|
|airRssi|天空端模组接收信号的信号强渡|
|band|无线电频段|
|channelPower|通道功率|
### StickInfo
 * 波轮值

|名称|描述|
| :--------  | :----:  |
|horizontalPosition|水平摇杆|
|verticalPosition|垂直摇杆|
### ThreeWaySwitchInfo
 * 三档开关

|名称|描述|
| :--------  | :----:  |
|value|值|
### WirelessBandwidthInfo
 * 无线电带宽信息

|名称|描述|
| :--------  | :----:  |
|uplinkSignalBandwidth|上行带宽设定|
|downloadSignalBandwidth|下行带宽设定|
### WirelessChannelInterfaceInfo
 * 无线电频点信息

|名称|描述|
| :--------  | :----:  |
|freqCount|频点列表数量|
|freqPrefer|当前选用频点|
|freqList|频点列表|
|interferenceList|干扰值列表|
### WirelessDataTransmissionInfo
 * 无线电信号传输状态

|名称|描述|
| :--------  | :----:  |
|upLinkPhysicsCapacity|上行带宽模式|
|downLinkPhysicsCapacity|下行带宽模式|
|wirelessFrequencyPoint|当前选用频点|
|downLinkDataCapacity|下行数据带宽理论值|
|downLinkDataSpeed|当前下行数据带宽|
|upLinkDataCapacity|上行数据带宽理论值|
|upLinkDataSpeed|当前上行数据带宽|
### WirelessFrequencyInfo
 * 无线电频点设定

|名称|描述|
| :--------  | :----:  |
|isAutoHopping|是否自动跳频|
|frequencyPoint|手动频点-当hopping=true时无效|
### WirelessHardwareInfo
 * 无线电硬件信息

|名称|描述|
| :--------  | :----:  |
|mainNode|主节点（地面端）|
|subNode|从节点（天空端）|
|id|设备ID|
|version|设备版本|
|mac|设备MAC地址|
|uptime|设备上电时间|
### WirelessSignalInfo
 * 无线电信号等级

|名称|描述|
| :--------  | :----:  |
|workBandInfo|工作频段|
|uplinkSignalLevel|上行信号等级|
|downlinkSignalLevel|下行信号等级|

##ENUM
### BandWidthState
 * 无线电带宽模式

|名称|值|描述|
| :--------  | :-----  | :----:  |
|BW_1Dot4MHz|0|1.4Mhz|
|BW_10MHz|3|10Mhz|
|BW_20MHz|5|20Mhz|
|BW_UNKNOWN|-1|unknown|
### CellularEnhancedTransmissionType
 * LTE增强模式

|名称|值|描述|
| :--------  | :-----  | :----:  |
|WIRELESS_ONLY|0|仅用无线电|
|CELLULAR_ONLY|1|仅用LTE|
|WIRELESS_CELLULAR|2|混合增强模式|
### CellularServerConnectionChangedState
 * LTE与服务器连接改变原因

|名称|值|描述|
| :--------  | :-----  | :----:  |
|CONNECTING|0|连接中|
|JOIN_SUCCESS|1|接入频道完成|
|INTERRUPTED|2|中断|
|BANNED_BY_SERVER|3|服务拒绝|
|JOIN_FAILED|4|加入失败|
|LEAVE_CHANNEL|5|离开频道|
|INVALID_APP_ID|6|非法的APPID|
|INVALID_CHANNEL_NAME|7|非法的频道名|
|INVALID_TOKEN|8|非法的token|
|TOKEN_EXPIRED|9|token过时|
|REJECTED_BY_SERVER|10|服务器拒绝连接|
|SETTING_PROXY_SERVER|11|正在设置代理|
|RENEW_TOKEN|12|需要重写token|
|CLIENT_IP_ADDRESS_CHANGED|13|终端的ip被改变|
|KEEP_ALIVE_TIMEOUT|14|连接保持超时|
|REJOIN_SUCCESS|15|重新加入频道成功|
|LOST|16|连接丢失|
|ECHO_TEST|17|TEST|
|CLIENT_IP_ADDRESS_CHANGED_BY_USER|18|终端IP被改变|
|SAME_UID_LOGIN|19|频道内有相同的UUID|
|TOO_MANY_BROADCASTERS|20|拉流端超出上限|
|LICENSE_VALIDATION_FAILURE|21|凭证验证失败|
|STREAM_CHANNEL_NOT_AVAILABLE|22|通道不可用|
|INCONSISTENT_APPID|23|APPID不一致|
|LOGIN_SUCCESS|10001|登录成功|
|LOGOUT|10002|已登出|
|CONNECTION_SDK_ERROR|255|SDK错误|
### CellularServerConnectState
 * LTE与服务器连接状态

|名称|值|描述|
| :--------  | :-----  | :----:  |
|DISCONNECTED|1|断开连接|
|CONNECTING|2|连接中|
|CONNECTED|3|已连接|
|RECONNECTING|4|重连中|
|FAILED|5|连接失败|
### ModuleDspType
 * 图传模块类型

|名称|值|描述|
| :--------  | :-----  | :----:  |
|BIG_FISH|1|大鱼图传|
|P301D|2|酷芯微|
|UNKNOWN|-1|未知|
### ModuleRcType
 * 遥控器模组类型

|名称|值|描述|
| :--------  | :-----  | :----:  |
|K3Q|0|酷天|
|DJI|1|DJI|
|K3R|2|自研遥控|
|UNKNOWN|-1|未知|
### RcMappingStyleType
 * 遥控器映射模式

|名称|值|描述|
| :--------  | :-----  | :----:  |
|MODE_1_JAPANESE,|-1|日本手|
|MODE_2_AMERICAN,|-1|美国手|
|UNKNOWN;|-1|未知|
### ReturnButtonType
 * 按钮动作

|名称|值|描述|
| :--------  | :-----  | :----:  |
|DOWN,|-1|按下|
|UP,|-1|抬起|
|UNKNOWN|-1|未知|
### ThreeWayButtonType
 * 三档开关

|名称|值|描述|
| :--------  | :-----  | :----:  |
|LEFT,|-1|左|
|MIDDLE,|-1|中|
|RIGHT,|-1|右|
|UNKNOWN;|-1|未知|
### WirelessLevelState
 * 无线电信号等级

|名称|值|描述|
| :--------  | :-----  | :----:  |
|A|1|A等级；表示信号极佳|
|B|2|B等级|
|C|3|C等级|
|D|4|D等级|
|E|5|E等级；表示信号差劲|
|UNKNOWN|-1|未知|
### WirelessWorkBandInfo
 * 无线电工作频段

|名称|值|描述|
| :--------  | :-----  | :----:  |
|BAND_2_4Ghz|0|2.4Ghz|
|BAND_5_8Ghz|1|5.8Ghz|
|UNKNOWN|-1|未知|
