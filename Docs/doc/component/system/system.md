
##INTERFACE
### updateParameterFromDrone
|方法名|updateParameterFromDrone|
| :--------  | :-----  |
|描述|* 从无人机下载实时参数列表（非参数数据）|
|返回值|void|
|请求参数|callback（callback）
|云冠|不支持|
|throws|IOException|
### readAllParameter
|方法名|readAllParameter|
| :--------  | :-----  |
|描述|* 读取飞控参数列表全部数据, 其中包含飞控算法核心参数|
|返回值|void|
|请求参数|listener（listener）
|云冠|不支持|
### readSingleParameter
|方法名|readSingleParameter|
| :--------  | :-----  |
|描述|* 通过序列index读取飞控参数列表单个属性|
|返回值|void|
|请求参数|name（需要读取的name）
|请求参数|listener（listener）
|云冠|不支持|
### writeSingleParameter
|方法名|writeSingleParameter|
| :--------  | :-----  |
|描述|* 修改某项飞控参数属性；参数内包含飞控核心算法，修改前请确认参数是否正确|
|返回值|void|
|请求参数|callback（callback）
|请求参数|info（修改参数的属性）
|云冠|不支持|
### setTransmitRateListener
|方法名|setTransmitRateListener|
| :--------  | :-----  |
|描述|* 获取当前GCS系统数传实时传输速率|
|返回值|void|
|请求参数|listener（listener）
|云冠|不支持|
### fetchFlightLogList
|方法名|fetchFlightLogList|
| :--------  | :-----  |
|描述|* 获取飞行器日志列表|
|返回值|void|
|请求参数|listener（listener）
|请求参数|type（飞控类型）
|云冠|不支持|
### fetchFlightLog
|方法名|fetchFlightLog|
| :--------  | :-----  |
|描述|* 通过指定的entry下载飞行器日志|
|返回值|void|
|请求参数|listener（listener）
|请求参数|saveName（保存的文件名）
|请求参数|type（飞控类型）
|请求参数|saveFilePath（日志文件下载保存的路径）
|请求参数|info（请先通过{{link #fetchFlightLogList(LoggerSystemType, IFetchFlightLogListListener)})}获取文件列表}）
|云冠|不支持|
### cancelFetchFlightLog
|方法名|cancelFetchFlightLog|
| :--------  | :-----  |
|描述|* 取消飞行器日志文件下载的动作|
|返回值|void|
|成功调用后会通过{link|IFlightLogDownloadListener}返回取消指令码|
|云冠|不支持|
### ftpFileList
|方法名|ftpFileList|
| :--------  | :-----  |
|描述|* 文件传输 - 展示飞控指定目录下文件|
|返回值|void|
|请求参数|filePath（飞控内部路径）
|请求参数|listener（listener）
|请求参数|type（文件管理系统）
|云冠|不支持|
### ftpDelFile
|方法名|ftpDelFile|
| :--------  | :-----  |
|描述|* 文件传输 - 删除飞控内部指定文件|
|返回值|void|
|请求参数|filePath（需删除的文件路径）
|请求参数|callback（callback）
|请求参数|type（文件管理系统）
|云冠|不支持|
### ftpDelDirectory
|方法名|ftpDelDirectory|
| :--------  | :-----  |
|描述|* 文件传输 - 删除飞控内部指定文件夹|
|返回值|void|
|请求参数|filePath（需删除的文件夹路径）
|请求参数|callback（callback）
|请求参数|type（文件管理系统）
|apiNote|非递归删除，请确保文件夹内部无占用文件|
|云冠|不支持|
### ftpCreateDirectory
|方法名|ftpCreateDirectory|
| :--------  | :-----  |
|描述|* 文件传输 - 在飞控内部创建文件夹|
|返回值|void|
|请求参数|filePath（需要创建的文件夹路径）
|请求参数|callback（callback）
|请求参数|type（文件管理系统）
|云冠|不支持|
### ftpCalcFileCrc32
|方法名|ftpCalcFileCrc32|
| :--------  | :-----  |
|描述|* 文件传输 - 生成文件CRC32校验码|
|返回值|void|
|请求参数|filePath（文件路径）
|请求参数|listener（listener）
|请求参数|type（文件管理系统）
|云冠|不支持|
### ftpDownloadFile
|方法名|ftpDownloadFile|
| :--------  | :-----  |
|描述|* 文件传输 - 下载飞控存储系统内部文件|
|返回值|void|
|请求参数|filePath（文件路径）
|请求参数|listener（listener）
|请求参数|type（文件管理系统）
|云冠|不支持|
### ftpUploadFile
|方法名|ftpUploadFile|
| :--------  | :-----  |
|描述|* 文件传输 - 上传文件到飞控存储指定文件夹|
|返回值|void|
|请求参数|listener（listener）
|请求参数|uploadFilePath（上传文件的完整路径）
|请求参数|type（文件管理系统）
|请求参数|saveFilePath（保存文件路径）
|云冠|不支持|
### ftpTerminateControl
|方法名|ftpTerminateControl|
| :--------  | :-----  |
|描述|* 文件传输 - 终止[上传\下载]相关操作|
|返回值|void|
|请求参数|callback（callback）
|请求参数|type（文件管理系统）
|云冠|不支持|
### VersionInfo> getComponentVersionInfo
|方法名|VersionInfo> getComponentVersionInfo|
| :--------  | :-----  |
|描述|* 获取组件版本信息|
|返回值|Map<Short,（各组件版本信息）|
### String> getSDKVersionInfo
|方法名|String> getSDKVersionInfo|
| :--------  | :-----  |
|描述|* 获取SDK版本信息|
|返回值|Map<Integer,（SDK版本信息）|
### startModuleFirmwareUpgrade
|方法名|startModuleFirmwareUpgrade|
| :--------  | :-----  |
|描述|* 启动模块固件升级|
|返回值|void|
|请求参数|upgradeStatusListener（升级状态监听）
|请求参数|firmwareUpgradeListener（固件升级监听）
|请求参数|upgradeFile（升级文件）
### startWirelessFirmwareUpgrade
|方法名|startWirelessFirmwareUpgrade|
| :--------  | :-----  |
|描述||
|返回值|void|
|请求参数|firmwareUpgradeListener（固件升级监听）
|请求参数|upgradeFile（升级文件）
### sendSerialControl
|方法名|sendSerialControl|
| :--------  | :-----  |
|描述|* 发送串口指令至飞控（仅调试使用）|
|返回值|void|
|请求参数|msg（消息）
### setSerialControlMessageResponse
|方法名|setSerialControlMessageResponse|
| :--------  | :-----  |
|描述|* 设置串口回传监听器|
|返回值|void|
|请求参数|listener（listener）
### setUosCloudServerAuthorization
|方法名|setUosCloudServerAuthorization|
| :--------  | :-----  |
|描述|* 设置UOS服务器连接授权(重启后生效)|
|返回值|void|
|请求参数|password（密码）
|请求参数|port（端口）
|请求参数|devices（设备名）
|请求参数|host（服务器地址）
|请求参数|user（用户名）

##INFO
### ExtParameterEntryInfo
 * 额外参数

|名称|描述|
| :--------  | :----:  |
|version|版本|
|parameters|参数列表|
### ExtParamValueInfo
 * 参数列表实体

|名称|描述|
| :--------  | :----:  |
|index|参数序列|
|value|参数值1|
|value2|参数值2|
|type|参数类型|
|name|参数名|
|desc|参数描述|
### FirmwareUpgradeInfo
 * 子模块固件升级信息

|名称|描述|
| :--------  | :----:  |
|currentUpgradeIndex|当前升级的子模块序列|
|totalUpgradeComponentCount|总共需要升级子模块数量|
|currentComponentType|当前升级子模块类型|
|firmwareTransferPercent|固件内部传输进度|
|firmwareUpgradeStatus|固件升级状态|
|upgradeErrorCode|固件升级错误码|
### FlightLogEntryInfo
 * 飞控日志实体

|名称|描述|
| :--------  | :----:  |
|id|飞行器日志序列ID|
|timestamp|飞行器日志最后写入时间|
|size|飞行器日志大小|
### FlightLogTableSyncInfo
 * 飞控日志同步进度

|名称|描述|
| :--------  | :----:  |
|flightLogSynchronizing|是否同步完成|
|state|同步状态|
|timestamp|同步时间戳|
|syncPercentage|同步进度|
### LinkNodeTransmitRateInfo
 * 无线电链路上下行速率统计

|名称|描述|
| :--------  | :----:  |
|timestamp|速率统计回调的时间戳|
|airLinkTxRate|地面站数传上行速率|
|airLinkRxRate|地面站数传下行速率|
### ParameterEntryInfo
 * 参数列表实例

|名称|描述|
| :--------  | :----:  |
|version|版本|
|parameters|参数列表|
### ParametersTableSyncInfo
 * 参数列表同步状态

|名称|描述|
| :--------  | :----:  |
|paramAllSynchronizing|是否同步完成|
|timestamp|同步时间戳|
|syncPercentage|同步进度|
|state|同步状态|
### ParamValueInfo
 * 参数列表实体

|名称|描述|
| :--------  | :----:  |
|index|参数序列|
|value|参数值|
|type|参数类型|
|name|参数名|
|desc|参数描述|
### SshAuthInfo
 * SSH登录实体

|名称|描述|
| :--------  | :----:  |
|username|用户名|
|host|服务器|
|port|端口|
|password|密码|
### VersionInfo
 * 版本信息

|名称|描述|
| :--------  | :----:  |
|mComponentType|子模块类型|
|mSwVersionCode|软件版本|
|mHwVersionCode|硬件版本|

##ENUM
### FirmwareUpgradeErrorCodeType
 * 固件升级错误码

|名称|值|描述|
| :--------  | :-----  | :----:  |
|FIRMWARE_UPGRADE_CODE_ERROR_NONE|0|没有错误。|
|FIRMWARE_UPGRADE_CODE_PACKAGE_NOT_FOUND|1|升级包找不到。|
|FIRMWARE_UPGRADE_CODE_PACKAGE_FORMAT_ERROR|2|升级包格式不正确。|
|FIRMWARE_UPGRADE_CODE_REQUEST_UPGRADE_REJECTED|3|请求升级被拒绝。|
|FIRMWARE_UPGRADE_CODE_UPLOAD_REJECTED|4|上传被拒绝。|
|FIRMWARE_UPGRADE_CODE_UPLOAD_TIMEOUT|5|上传超时。|
|FIRMWARE_UPGRADE_CODE_VERIFY_ERROR|6|校验失败。|
|FIRMWARE_UPGRADE_CODE_NO_ENOUGH_SPACE|7|设备存储空间不足以完成升级。|
|FIRMWARE_UPGRADE_CODE_UPGRADE_TIMEOUT|8|升级过程中超时。|
|FIRMWARE_UPGRADE_CODE_PARTITION_NOT_FOUND|9|升级需要更新的分区未找到。|
|FIRMWARE_UPGRADE_CODE_PARTITION_ERASE_FAILED|10|升级分区擦除失败。|
|FIRMWARE_UPGRADE_CODE_PARTITION_WRITE_FAILED|11|升级分区写入失败。|
|FIRMWARE_UPGRADE_CODE_DEVICE_OFFLINE|12|设备不在线。|
|FIRMWARE_UPGRADE_CODE_UNSUPPORT_DEVICE_TYPE|13|不支持升级此设备类型。|
|FIRMWARE_UPGRADE_CODE_CURRENT_NOT_ALLOW|14|当前状态不允许升级。|
|FIRMWARE_UPGRADE_CODE_PACKAGE_HEAD_SHORT|15|固件包头太短。|
|FIRMWARE_UPGRADE_CODE_OPEN_PACKAGE_FAIL|16|打开固件包失败。|
|FIRMWARE_UPGRADE_CODE_FRAMWARE_LEN_NO_MATCH|17|固件长度不匹配。|
|FIRMWARE_UPGRADE_CODE_FRAMWARE_TOO_BIG|18|固件包太大。|
|FIRMWARE_UPGRADE_CODE_FRAMWARE_CHECK_FAIL|19|固件数据校验失败。|
|FIRMWARE_UPGRADE_CODE_FLASH_CHECK_FAIL|20|固件存储区校验失败。|
|FIRMWARE_UPGRADE_CODE_DIR_OPEN_FAIL|21|固件包目录打开失败。|
|FIRMWARE_UPGRADE_CODE_BOARD_ID_NO_MATCH|22|板子ID不匹配。|
|FIRMWARE_UPGRADE_CODE_UNKNOWN|255|未知状态。|
### FirmwareUpgradeStatusType
 * 固件升级状态

|名称|值|描述|
| :--------  | :-----  | :----:  |
|FIRMWARE_UPGRADE_IDLE|0|固件升级空闲状态。|
|FIRMWARE_UPGRADE_PREPARING|1|固件升级准备状态。|
|FIRMWARE_UPGRADE_UPLOADING|2|正在上传固件文件。|
|FIRMWARE_UPGRADE_VERIFYING|3|正在校验固件文件。|
|FIRMWARE_UPGRADE_UPGRADING|4|正在执行固件升级操作。|
|FIRMWARE_UPGRADE_REBOOTING|5|正在重启设备。|
|FIRMWARE_UPGRADE_SUCCESS|6|固件升级成功。|
|FIRMWARE_UPGRADE_FAILED|7|固件升级失败。|
### FlightLogTableSyncState
 * 飞控日志同步状态

|名称|值|描述|
| :--------  | :-----  | :----:  |
|UNKNOWN,|-1|未知|
|COMPLETED,|-1|已完成|
|SYNC;|-1|同步中|
### LoggerSystemType
 * 日志系统模式

|名称|值|描述|
| :--------  | :-----  | :----:  |
|FC,|-1|飞控|
|CREST,|-1|云冠|
|CAMERA|-1|相机|
### ParametersTableSyncState
 * 参数同步状态

|名称|值|描述|
| :--------  | :-----  | :----:  |
|UNKNOWN,|-1|未知|
|COMPLETED,|-1|同步完成|
|ALL_SYNC|-1|最新(无需同步)|
### null
null
|名称|值|描述|
| :--------  | :-----  | :----:  |
