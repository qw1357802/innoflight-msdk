
##INTERFACE
### setCameraComponentConnectChangedListener
|方法名|setCameraComponentConnectChangedListener|
| :--------  | :-----  |
|描述|* 设置相机连接监听|
|返回值|void|
|请求参数|listener（listener）
### setSpeakerComponentConnectChangedListener
|方法名|setSpeakerComponentConnectChangedListener|
| :--------  | :-----  |
|描述|* 设置喊话器连接监听|
|返回值|void|
|请求参数|listener（listener）
### getCamera
|方法名|getCamera|
| :--------  | :-----  |
|描述|* 获取相机挂载实例（p.s请不要长时间持有对象，在相机离线后会被释放）|
|返回值|IBaseCameraListener（camera）|
### getSpeaker
|方法名|getSpeaker|
| :--------  | :-----  |
|描述|* 获取喊话器实例（p.s请不要长时间持有对象，在喊话器离线后会被释放）|
|返回值|IBaseSpeakerListener（speaker）|
## camera-AbstractCamera
### getCameraModuleName
|方法名|getCameraModuleName|
| :--------  | :-----  |
|描述|* 获取相机型号名称|
|返回值|CameraModuleType（camera）|
### getCameraLens
|方法名|getCameraLens|
| :--------  | :-----  |
|描述|* 获取相机支持的镜头能力|
|返回值|CameraLensType[]（camera）|
### setCameraMode
|方法名|setCameraMode|
| :--------  | :-----  |
|描述|* 设置相机模式|
|返回值|void|
|请求参数|cameraMode（相机模式）
|请求参数|callback（callback）
### getCameraMode
|方法名|getCameraMode|
| :--------  | :-----  |
|描述|* 获取相机模式|
|返回值|void|
|请求参数|callback（callback）
### setCameraInformationListener
|方法名|setCameraInformationListener|
| :--------  | :-----  |
|描述|* 设置相机参数信息监听|
|返回值|void|
|请求参数|cameraInformationListener（listener）
### setCaptureStatusListener
|方法名|setCaptureStatusListener|
| :--------  | :-----  |
|描述|* 设置相机成像状态监听|
|返回值|void|
|请求参数|cameraCaptureInfoListener（listener）
### setNewGeneratedInfoListener
|方法名|setNewGeneratedInfoListener|
| :--------  | :-----  |
|描述|* 设置相机新媒体生成监听|
|返回值|void|
|请求参数|listener（listener）
### setCameraStorageInfoListener
|方法名|setCameraStorageInfoListener|
| :--------  | :-----  |
|描述|* 设置相机存储系统监听|
|返回值|void|
|请求参数|listener（listener）
### startSingleCapture
|方法名|startSingleCapture|
| :--------  | :-----  |
|描述|* 单拍|
|返回值|void|
|请求参数|callback（callback）
### startComboCapture
|方法名|startComboCapture|
| :--------  | :-----  |
|描述|* 连拍|
|返回值|void|
|请求参数|count（count）
|请求参数|callback（callback）
### startIntervalCapture
|方法名|startIntervalCapture|
| :--------  | :-----  |
|描述|* 定时拍|
|返回值|void|
|请求参数|callback（callback）
|请求参数|interval（时间间隔 s）
### stopIntervalCapture
|方法名|stopIntervalCapture|
| :--------  | :-----  |
|描述|* 停止定时拍|
|返回值|void|
|请求参数|callback（callback）
### startRecord
|方法名|startRecord|
| :--------  | :-----  |
|描述|* 启动录制|
|返回值|void|
|请求参数|callback（callback）
### stopRecord
|方法名|stopRecord|
| :--------  | :-----  |
|描述|* 停止录制|
|返回值|void|
|请求参数|callback（callback）
### formatStorage
|方法名|formatStorage|
| :--------  | :-----  |
|描述|* 格式化存储|
|返回值|void|
|请求参数|callback（callback）
|请求参数|type（格式化类型）
## camera-NV2Camera
### setCameraSource
|方法名|setCameraSource|
| :--------  | :-----  |
|描述|* 设置相机图传视频源|
|返回值|void|
|请求参数|callback（callback）
|请求参数|streamSource（视频源）
### getCameraSource
|方法名|getCameraSource|
| :--------  | :-----  |
|描述|* 获取相机图传视频源|
|返回值|void|
|请求参数|callback（callback）
### setCameraZoomRatio
|方法名|setCameraZoomRatio|
| :--------  | :-----  |
|描述|* 设置相机电子变倍|
|返回值|void|
|请求参数|callback（callback）
|请求参数|zoom（倍率）
### getCameraZoomRatio
|方法名|getCameraZoomRatio|
| :--------  | :-----  |
|描述|* 获取相机电子变倍倍率|
|返回值|void|
|请求参数|callback（callback）
### startSingleCapture
|方法名|startSingleCapture|
| :--------  | :-----  |
|描述|* 指定镜头单拍|
|返回值|void|
|请求参数|callback（callback）
### setISO
|方法名|setISO|
| :--------  | :-----  |
|描述|* 设置ISO|
|返回值|void|
|请求参数|isAutoISO（isAuto）
|请求参数|callback（callback）
|请求参数|cameraISO（iso）
### getISO
|方法名|getISO|
| :--------  | :-----  |
|描述|* 获取ISO|
|返回值|CameraISO（ISO）|
### setShutterSpeed
|方法名|setShutterSpeed|
| :--------  | :-----  |
|描述|* 设置快门|
|返回值|void|
|请求参数|isAutoShutterSpeed（isAuto）
|请求参数|callback（callback）
|请求参数|shutterSpeed（ShutterSpeed）
### getShutterSpeed
|方法名|getShutterSpeed|
| :--------  | :-----  |
|描述|* 获取快门|
|返回值|CameraShutterSpeed（ShutterSpeed）|
### setEv
|方法名|setEv|
| :--------  | :-----  |
|描述|* 设置曝光补偿|
|返回值|void|
|请求参数|ev（ev）
|请求参数|callback（callback）
### getEv
|方法名|getEv|
| :--------  | :-----  |
|描述|* 获取曝光补偿|
|返回值|float（ev）|
## camera-NV3Camera
### setCameraSource
|方法名|setCameraSource|
| :--------  | :-----  |
|描述|* 设置相机图传视频源|
|返回值|void|
|请求参数|callback（callback）
|请求参数|streamSource（视频源）
### getCameraSource
|方法名|getCameraSource|
| :--------  | :-----  |
|描述|* 获取相机图传视频源|
|返回值|void|
|请求参数|callback（callback）
### setCameraZoomRatio
|方法名|setCameraZoomRatio|
| :--------  | :-----  |
|描述|* 设置相机电子变倍|
|返回值|void|
|请求参数|callback（callback）
|请求参数|zoom（倍率）
### getCameraZoomRatio
|方法名|getCameraZoomRatio|
| :--------  | :-----  |
|描述|* 获取相机电子变倍倍率|
|返回值|void|
|请求参数|callback（callback）
### startSingleCapture
|方法名|startSingleCapture|
| :--------  | :-----  |
|描述|* 单拍|
|返回值|void|
|请求参数|callback（callback）
### setISO
|方法名|setISO|
| :--------  | :-----  |
|描述|* 设置ISO|
|返回值|void|
|请求参数|isAutoISO（isAuto）
|请求参数|callback（callback）
|请求参数|cameraISO（iso）
### getISO
|方法名|getISO|
| :--------  | :-----  |
|描述|* 获取ISO|
|返回值|CameraISO（ISO）|
### setShutterSpeed
|方法名|setShutterSpeed|
| :--------  | :-----  |
|描述|* 设置快门|
|返回值|void|
|请求参数|isAutoShutterSpeed（isAuto）
|请求参数|callback（callback）
|请求参数|shutterSpeed（ShutterSpeed）
### getShutterSpeed
|方法名|getShutterSpeed|
| :--------  | :-----  |
|描述|* 获取快门|
|返回值|CameraShutterSpeed（ShutterSpeed）|
### setEv
|方法名|setEv|
| :--------  | :-----  |
|描述|* 设置曝光补偿|
|返回值|void|
|请求参数|ev（ev）
|请求参数|callback（callback）
### getEv
|方法名|getEv|
| :--------  | :-----  |
|描述|* 获取曝光补偿|
|返回值|float（ev）|
### setThermalPalette
|方法名|setThermalPalette|
| :--------  | :-----  |
|描述|* 设置红外调色盘|
|返回值|void|
|请求参数|callback（callback）
|请求参数|palette（调色盘）
### getThermalPalette
|方法名|getThermalPalette|
| :--------  | :-----  |
|描述|*获取调色盘配置|
|返回值|void|
|请求参数|callback（callback）
### setThermalFFCMode
|方法名|setThermalFFCMode|
| :--------  | :-----  |
|描述|* 设置红外温度补偿校准模式（FFC）|
|返回值|void|
|请求参数|thermalFFCMode（FFC）
|请求参数|callback（callback）
### getThermalFFCMode
|方法名|getThermalFFCMode|
| :--------  | :-----  |
|描述|*获取红外温度补偿校准模式（FFC）|
|返回值|void|
|请求参数|callback（callback）
### setThermalGain
|方法名|setThermalGain|
| :--------  | :-----  |
|描述|* 设置红外相机GAIN值|
|返回值|void|
|请求参数|callback（callback）
|请求参数|gain（gain（0-255））
### getThermalGain
|方法名|getThermalGain|
| :--------  | :-----  |
|描述|* 获取红外相机GAIN值|
|返回值|void|
|请求参数|callback（callback）
### setThermal2DNoiseFilter
|方法名|setThermal2DNoiseFilter|
| :--------  | :-----  |
|描述|* 设置红外相机降噪等级|
|返回值|void|
|请求参数|thermal2DNoiseMode（noiseMode）
|请求参数|callback（callback）
### getThermal2DNoiseFilter
|方法名|getThermal2DNoiseFilter|
| :--------  | :-----  |
|描述|* 获取红外相机降噪等级|
|返回值|void|
|请求参数|callback（callback）
### setThermalDebunchingEnable
|方法名|setThermalDebunchingEnable|
| :--------  | :-----  |
|描述|* 设置红外相机去波纹开关|
|返回值|void|
|请求参数|callback（callback）
|请求参数|isEnable（isEnable）
### getThermalDebunchingEnable
|方法名|getThermalDebunchingEnable|
| :--------  | :-----  |
|描述|* 获取红外相机去波纹开关|
|返回值|void|
|请求参数|callback（callback）
### setThermalImageModel
|方法名|setThermalImageModel|
| :--------  | :-----  |
|描述|* 设置红外相机图像模式|
|返回值|void|
|请求参数|thermalImageModel（image model）
|请求参数|callback（callback）
### getThermalImageModel
|方法名|getThermalImageModel|
| :--------  | :-----  |
|描述|* 获取红外相机图像模式|
|返回值|void|
|请求参数|callback（callback）
### setThermalImageTone
|方法名|setThermalImageTone|
| :--------  | :-----  |
|描述|* 设置红外相机图像色调|
|返回值|void|
|请求参数|thermalImageTone（image tone）
|请求参数|callback（callback）
### getThermalImageTone
|方法名|getThermalImageTone|
| :--------  | :-----  |
|描述|* 获取红外相机图像色调|
|返回值|void|
|请求参数|callback（callback）
### setThermalLight
|方法名|setThermalLight|
| :--------  | :-----  |
|描述|* 设置红外相机亮度|
|返回值|void|
|请求参数|light（亮度）
|请求参数|callback（callback）
### getThermalLight
|方法名|getThermalLight|
| :--------  | :-----  |
|描述|* 获取红外相机亮度|
|返回值|void|
|请求参数|callback（callback）
### setThermalContrast
|方法名|setThermalContrast|
| :--------  | :-----  |
|描述|* 设置红外相机对比度|
|返回值|void|
|请求参数|contrast（对比度）
|请求参数|callback（callback）
### getThermalContrast
|方法名|getThermalContrast|
| :--------  | :-----  |
|描述|* 获取红外相机对比度|
|返回值|void|
|请求参数|callback（callback）
### setThermalDistance
|方法名|setThermalDistance|
| :--------  | :-----  |
|描述|* 设置红外相机探测距离|
|返回值|void|
|请求参数|distance（距离）
|请求参数|callback（callback）
### getThermalDistance
|方法名|getThermalDistance|
| :--------  | :-----  |
|描述|* 获取红外相机探测距离|
|返回值|void|
|请求参数|callback（callback）
### setThermalEmissivity
|方法名|setThermalEmissivity|
| :--------  | :-----  |
|描述|* 设置红外相机发射率|
|返回值|void|
|请求参数|emissivity（发射率）
|请求参数|callback（callback）
### getThermalEmissivity
|方法名|getThermalEmissivity|
| :--------  | :-----  |
|描述|* 获取红外相机发射率|
|返回值|void|
|请求参数|callback（callback）
### setThermalFCVEEnable
|方法名|setThermalFCVEEnable|
| :--------  | :-----  |
|描述|* 设置红外相机伪彩视觉增强开关|
|返回值|void|
|请求参数|callback（callback）
|请求参数|isEnable（开关）
### getThermalFCVEEnable
|方法名|getThermalFCVEEnable|
| :--------  | :-----  |
|描述|* 获取红外相机伪彩视觉增强开关|
|返回值|void|
|请求参数|callback（callback）
### setThermalFCVELowTemperature
|方法名|setThermalFCVELowTemperature|
| :--------  | :-----  |
|描述|* 设置红外相机伪彩视觉增强最低温度|
|返回值|void|
|请求参数|temp（最低温度）
|请求参数|callback（callback）
### getThermalFCVELowTemperature
|方法名|getThermalFCVELowTemperature|
| :--------  | :-----  |
|描述|* 获取红外相机伪彩视觉增强最低温度|
|返回值|void|
|请求参数|callback（callback）
### setThermalFCVEHighTemperature
|方法名|setThermalFCVEHighTemperature|
| :--------  | :-----  |
|描述|* 设置红外相机伪彩视觉增强最高温度|
|返回值|void|
|请求参数|temp（最高温度）
|请求参数|callback（callback）
### getThermalFCVEHighTemperature
|方法名|getThermalFCVEHighTemperature|
| :--------  | :-----  |
|描述|* 获取红外相机伪彩视觉增强最高温度|
|返回值|void|
|请求参数|callback（callback）
### startSpotThermalMeasurement
|方法名|startSpotThermalMeasurement|
| :--------  | :-----  |
|描述|* 启动点测温|
|返回值|void|
|请求参数|callback（callback）
### startAreaThermalMeasurement
|方法名|startAreaThermalMeasurement|
| :--------  | :-----  |
|描述|* 启动区域测温|
|返回值|void|
|请求参数|rw（rw）
|请求参数|rx（rx）
|请求参数|rh（rh）
|请求参数|ry（ry）
|请求参数|callback（callback）
### stopThermalMeasurement
|方法名|stopThermalMeasurement|
| :--------  | :-----  |
|描述|* 关闭红外测温|
|返回值|void|
|请求参数|callback（callback）
## speaker-AbstractSpeaker
### setSpeakerVolume
|方法名|setSpeakerVolume|
| :--------  | :-----  |
|描述|* 设置当前喊话器音量|
|返回值|void|
|请求参数|volume（音量百分比）
|请求参数|callback（callback）
### getSpeakerVolume
|方法名|getSpeakerVolume|
| :--------  | :-----  |
|描述|* 获取当前喊话器音量|
|返回值|void|
|请求参数|callback（callback）
### startPushingTTSToSpeaker
|方法名|startPushingTTSToSpeaker|
| :--------  | :-----  |
|描述|* 启动TTS文字转义播放|
|返回值|void|
|请求参数|tts（文本）
|请求参数|isSinglePlay（是否单次播放）
|请求参数|listener（listener）
### startPushingOpusToSpeaker
|方法名|startPushingOpusToSpeaker|
| :--------  | :-----  |
|描述|* 启动点播文件播放|
|返回值|void|
|请求参数|opusFile（pcm文件）
|请求参数|isSinglePlay（是否单次播放）
|请求参数|listener（listener）
### getFileListSnapshot
|方法名|getFileListSnapshot|
| :--------  | :-----  |
|描述|* 获取点播的文件列表|
|返回值|void|
|请求参数|callback（获取文件列表）
### startPlayByMediaIndex
|方法名|startPlayByMediaIndex|
| :--------  | :-----  |
|描述|* 启动播放|
|返回值|void|
|请求参数|isSinglePlay（是否单次播放）
|请求参数|index（通过getFileListSnapshot获取的index）
|请求参数|callback（callback）
### removeSpeakerMedia
|方法名|removeSpeakerMedia|
| :--------  | :-----  |
|描述|* 移除音频文件|
|返回值|void|
|请求参数|fileInfo（音频文件通过getFileListSnapshot获取）
|请求参数|callback（callback）
### renameSpeakerMedia
|方法名|renameSpeakerMedia|
| :--------  | :-----  |
|描述|* 重命名音频文件|
|返回值|void|
|请求参数|fileInfo（音频文件通过getFileListSnapshot获取）
|请求参数|newFileName（新名字）
|请求参数|callback（callback）
### uploadSpeakerMedia
|方法名|uploadSpeakerMedia|
| :--------  | :-----  |
|描述|* 上传音频文件|
|返回值|void|
|请求参数|listener（listener）
|请求参数|saveName（存储的名称, 如果为空，使用文件名）
|请求参数|pcmFile（pcm文件）
### stopPlay
|方法名|stopPlay|
| :--------  | :-----  |
|描述|* 停止播放|
|返回值|void|
|请求参数|callback（callback）
### getSpeakerWorkType
|方法名|getSpeakerWorkType|
| :--------  | :-----  |
|描述|* 获取当前喊话器的工作状态|
|返回值|void|
|请求参数|callback（callback）
## speaker-SS125Speaker
### setLightEnable
|方法名|setLightEnable|
| :--------  | :-----  |
|描述|* 设置当前照明灯开关|
|返回值|void|
|请求参数|isBlink（是否需要闪烁）
|请求参数|callback（callback）
|请求参数|isEnable（开关）
### getLightEnable
|方法名|getLightEnable|
| :--------  | :-----  |
|描述|* 获取当前照明灯开关|
|返回值|void|
|请求参数|callback（callback）
### setLightBrightness
|方法名|setLightBrightness|
| :--------  | :-----  |
|描述|* 设置照明灯亮度|
|返回值|void|
|请求参数|callback（callback）
### getLightBrightness
|方法名|getLightBrightness|
| :--------  | :-----  |
|描述|* 获取照明灯亮度|
|返回值|void|
|请求参数|callback（callback）
### setGimbalFollowingControl
|方法名|setGimbalFollowingControl|
| :--------  | :-----  |
|描述|* 设置云台随动模式|
|返回值|void|
|请求参数|isFollow（随动模式）
|请求参数|callback（callback）
### getGimbalFollowingControl
|方法名|getGimbalFollowingControl|
| :--------  | :-----  |
|描述|* 获取云台随动模式|
|返回值|void|
|请求参数|callback（callback）
### setGimbalAngle
|方法名|setGimbalAngle|
| :--------  | :-----  |
|描述|* 设置云台电机角度|
|返回值|void|
|请求参数|gimbalAngle（角度）
|请求参数|callback（callback）
### getGimbalAngle
|方法名|getGimbalAngle|
| :--------  | :-----  |
|描述|* 获取云台电机角度|
|返回值|void|
|请求参数|callback（callback）

##INFO
### CameraCaptureStatusInfo
 * 相机成像状态

|名称|描述|
| :--------  | :----:  |
|cameraMode|相机模式|
|imageStatus|相机拍照状态|
|videoStatus|相机录像状态|
|imageIntervalUnit|相机定时拍照参数|
|recordingTimeMs|相机录制时长|
|availableCapacity|相机剩余存储|
|availableSnapCount|相机可拍照数量|
|availableRecordMin|相机可录制时长|
|isAutoIso|是否自动iso|
|isAutoShutterSpeed|是否自动快门|
|cameraISO|ISO|
|cameraShutterSpeed|快门|
|cameraZoomRatio|变焦倍率（数码变焦）|
### CameraHardwareInfo
 * 相机硬件参数属性

|名称|描述|
| :--------  | :----:  |
|firmwareVersion|软件版本|
|focalLength|相机焦距|
|sensorHeight|传感器尺寸-高|
|sensorWidth|传感器尺寸-宽|
|resolutionHeight|图像分辨率-高|
|resolutionWidth|图像分辨率-宽|
### CameraNewGeneratedInfo
 * 相机新媒体文件生成类型

|名称|描述|
| :--------  | :----:  |
|index|媒体序列|
|cameraLensType|镜头类型|
|latitude|纬度|
|longitude|经度|
|agl|相对高|
|msl|海拔|
|path|存储路径|
### CameraStorageInfo
 * 相机存储系统信息

|名称|描述|
| :--------  | :----:  |
|cameraStorageStatusType|相机存储状态|
|totalCapacity|总空间|
|usedCapacity|已用空间|
|availableCapacity|可用空间|
### null

|名称|描述|
| :--------  | :----:  |
### null

|名称|描述|
| :--------  | :----:  |
### ThermalAreaMeasureInfo
 * 区域测温数据

|名称|描述|
| :--------  | :----:  |
|rx|rect x|
|ry|rect y|
|rw|rect width|
|rh|rect height|
|maxSpot|区域最高温位置|
|minSpot|区域最低温位置|
|maxTemperature|最高温度|
|minTemperature|最低温度|
|averageTemperature|平均温度|
### ThermalSpotMeasureInfo
 * 点测温数据

|名称|描述|
| :--------  | :----:  |
|spot|当前测温点|
|maxTemperature|最高温度|
|minTemperature|最低温度|
|averageTemperature|平均温度|

##ENUM
### CameraImageStatus
 * 相机拍照模式

|名称|值|描述|
| :--------  | :-----  | :----:  |
|CAMERA_IMAGE_IDLE|0|空闲|
|CAMERA_IMAGE_CAPTURING|1|单拍中|
|CAMERA_IMAGE_INTERVAL_IDLE|2|定时拍照空闲|
|CAMERA_IMAGE_INTERVAL_CAPTURING|3|定时拍照中|
|CAMERA_IMAGE_UNKNOWN|255|状态未知|
### CameraISO
 * 相机ISO

|名称|值|描述|
| :--------  | :-----  | :----:  |
|CAMERA_ISO_100|100|ISO-100|
|CAMERA_ISO_200|200|ISO-200|
|CAMERA_ISO_400|400|ISO-400|
|CAMERA_ISO_800|800|ISO-800|
|CAMERA_ISO_3200|3200|ISO-3200|
|CAMERA_ISO_6400|6400|ISO-6400|
|CAMERA_ISO_UNKNOWN|255|ISO-UNKNOWN|
### CameraLensType
 * 相机镜头类型

|名称|值|描述|
| :--------  | :-----  | :----:  |
|WV|0|广角可见光|
|NV|1|夜视镜头|
|IV|2|红外镜头|
|UNKNOWN|-1|未知镜头|
### CameraMode
 * 相机模式

|名称|值|描述|
| :--------  | :-----  | :----:  |
|SHOOT_PHOTO|1|拍照模式|
|RECORD_VIDEO|0|录像模式|
|UNKNOWN|-1|未知|
### CameraModuleType
 * 相机类型

|名称|值|描述|
| :--------  | :-----  | :----:  |
|NV2|1|Y3C|
|NV3|0|Y3|
|UNKNOWN|-1|UNKNOWN|
### CameraNV2StreamSource
 * 相机视频流类型

|名称|值|描述|
| :--------  | :-----  | :----:  |
|LIVEVIEW_CAMERA_SOURCE_DEFAULT|255|默认类型（未知类型）|
|LIVEVIEW_CAMERA_SOURCE_NIGHT|1|夜视|
|LIVEVIEW_CAMERA_SOURCE_WIDE|0|广角|
### CameraNV3StreamSource
 * 相机视频流类型

|名称|值|描述|
| :--------  | :-----  | :----:  |
|LIVEVIEW_CAMERA_SOURCE_DEFAULT|255|默认类型（未知类型）|
|LIVEVIEW_CAMERA_SOURCE_NIGHT|1|夜视|
|LIVEVIEW_CAMERA_SOURCE_WIDE|0|广角|
|LIVEVIEW_CAMERA_SOURCE_IR|2|红外|
### CameraShutterSpeed
 * 相机快门速度

|名称|值|描述|
| :--------  | :-----  | :----:  |
|CAMERA_MANAGER_SHUTTER_SPEED_1_8000|8000|SHUTTER_SPEED_1/8000|
|CAMERA_MANAGER_SHUTTER_SPEED_1_6400|6400|SHUTTER_SPEED_1/6400|
|CAMERA_MANAGER_SHUTTER_SPEED_1_5000|5000|SHUTTER_SPEED_1/5000|
|CAMERA_MANAGER_SHUTTER_SPEED_1_4000|4000|SHUTTER_SPEED_1/4000|
|CAMERA_MANAGER_SHUTTER_SPEED_1_3200|3200|SHUTTER_SPEED_1/3200|
|CAMERA_MANAGER_SHUTTER_SPEED_1_2500|2500|SHUTTER_SPEED_1/2500|
|CAMERA_MANAGER_SHUTTER_SPEED_1_2000|2000|SHUTTER_SPEED_1/2000|
|CAMERA_MANAGER_SHUTTER_SPEED_1_1600|1600|SHUTTER_SPEED_1/1600|
|CAMERA_MANAGER_SHUTTER_SPEED_1_1250|1250|SHUTTER_SPEED_1/1250|
|CAMERA_MANAGER_SHUTTER_SPEED_1_1000|1000|SHUTTER_SPEED_1/1000|
|CAMERA_MANAGER_SHUTTER_SPEED_1_800|800|SHUTTER_SPEED_1/800|
|CAMERA_MANAGER_SHUTTER_SPEED_1_640|640|SHUTTER_SPEED_1/640|
|CAMERA_MANAGER_SHUTTER_SPEED_1_500|500|SHUTTER_SPEED_1/500|
|CAMERA_MANAGER_SHUTTER_SPEED_1_400|400|SHUTTER_SPEED_1/400|
|CAMERA_MANAGER_SHUTTER_SPEED_1_320|320|SHUTTER_SPEED_1/320|
|CAMERA_MANAGER_SHUTTER_SPEED_1_240|240|SHUTTER_SPEED_1/240|
|CAMERA_MANAGER_SHUTTER_SPEED_1_160|160|SHUTTER_SPEED_1/160|
|CAMERA_MANAGER_SHUTTER_SPEED_1_120|120|SHUTTER_SPEED_1/120|
|CAMERA_MANAGER_SHUTTER_SPEED_1_100|100|SHUTTER_SPEED_1/100|
|CAMERA_MANAGER_SHUTTER_SPEED_1_80|80|SHUTTER_SPEED_1/80|
|CAMERA_MANAGER_SHUTTER_SPEED_1_60|60|SHUTTER_SPEED_1/60|
|CAMERA_MANAGER_SHUTTER_SPEED_1_50|50|SHUTTER_SPEED_1/50|
|CAMERA_MANAGER_SHUTTER_SPEED_1_40|40|SHUTTER_SPEED_1/40|
|CAMERA_MANAGER_SHUTTER_SPEED_1_30|30|SHUTTER_SPEED_1/30|
|CAMERA_MANAGER_SHUTTER_SPEED_UNKNOWN|255|SHUTTER_SPEED_UNKNOWN|
### CameraStorageStatusType
 * 相机存储系统状态

|名称|值|描述|
| :--------  | :-----  | :----:  |
|STORAGE_UNLOAD|0|存储未装载|
|STORAGE_UNFORMATTED|1|存储未格式化（格式不正确）|
|STORAGE_READY|2|存储已就绪|
|STORAGE_NOT_SUPPORTED|3|存储不支持|
|UNKNOWN|-1|UNKNOWN|
### CameraThermalMeasureMode
 * 测温模式

|名称|值|描述|
| :--------  | :-----  | :----:  |
|CAMERA_THERMAL_MEASURE_SPOT|1|点测温|
|CAMERA_THERMAL_MEASURE_AREA|2|区域测温|
|CAMERA_THERMAL_MEASURE_UNKNOWN|0|未知|
### CameraThermalPalette
 * 红外调色板

|名称|值|描述|
| :--------  | :-----  | :----:  |
|CAMERA_THERMAL_PALETTE_INCANDESCENCE|0|白热|
|CAMERA_THERMAL_PALETTE_LAVA|1|熔岩|
|CAMERA_THERMAL_PALETTE_IRON_RED|2|铁红|
|CAMERA_THERMAL_PALETTE_HOT_IRON|3|热铁|
|CAMERA_THERMAL_PALETTE_MEDICAL_TREATMENT|4|医疗|
|CAMERA_THERMAL_PALETTE_NORTH_POLE|5|北极|
|CAMERA_THERMAL_PALETTE_RAINBOW1|6|彩虹1|
|CAMERA_THERMAL_PALETTE_RAINBOW2|7|彩虹2|
|CAMERA_THERMAL_PALETTE_TRACE_RED|8|描红|
|CAMERA_THERMAL_PALETTE_BLACK_HEAT|9|黑热|
|CAMERA_THERMAL_PALETTE_UNKNOWN|255|未知|
### CameraVideoStatus
 * 相机录像状态

|名称|值|描述|
| :--------  | :-----  | :----:  |
|CAMERA_VIDEO_IDLE|0|录像状态空闲|
|CAMERA_VIDEO_CAPTURING|1|录像中|
|CAMERA_VIDEO_UNKNOWN|255|未知状态|
### CameraZoomRatio
 * 相机变焦倍率

|名称|值|描述|
| :--------  | :-----  | :----:  |
|CAMERA_ELECTRON_ZOOM_X1|1|X1|
|CAMERA_ELECTRON_ZOOM_X2|2|X2|
|CAMERA_ELECTRON_ZOOM_X4|4|X4|
|CAMERA_ELECTRON_ZOOM_X8|8|X4|
|CAMERA_ELECTRON_ZOOM_X16|16|X16|
|CAMERA_ELECTRON_ZOOM_UNKNOWN|255|未知|
### SpeakerModeType
 * 喊话器状态

|名称|值|描述|
| :--------  | :-----  | :----:  |
|SINGLE_PLAY|0|单次播放|
|LOOP_PLAY|1|循环播放|
|UNKNOWN|-1|UNKNOWN|
### SpeakerModuleType
 * 相机类型

|名称|值|描述|
| :--------  | :-----  | :----:  |
|SS125|1|SS125|
|UNKNOWN|-1|UNKNOWN|
### SpeakerRealTimeSenderType
 * 喊话器实时传输过程状态（实时喊话、TTS）

|名称|值|描述|
| :--------  | :-----  | :----:  |
|INIT,|-1|初始化中|
|TRANS,|-1|传输中|
|SYNC,|-1|同步中|
|PLAY,|-1|播放|
|IDLE,|-1|空闲|
|UNKNOWN;|-1|未知|
### SpeakerWorkType
 * 喊话器状态

|名称|值|描述|
| :--------  | :-----  | :----:  |
|IDLE|0|空闲中|
|REAL_TIME_SPEAKING|1|实时喊话中|
|TTS_SPEAKING|2|TTS喊话中|
|DEMAND_PLAYING|3|点播中|
|AUDIO_DECODING|4|音频编码中|
|TTS_SYNTHESIS|5|TTS转义中|
|UNKNOWN|-1|UNKNOWN|
### StorageFormatType
 * 格式化存储类型

|名称|值|描述|
| :--------  | :-----  | :----:  |
|WIDE_DATA|0|清空数据（正常情况下请选用此模式）|
|FORMAT|1|格式化存储|
|UNKNOWN|-1|UNKNOWN|
### Thermal2DNoiseMode
 * 红外降噪模式

|名称|值|描述|
| :--------  | :-----  | :----:  |
|LOW|0|低降噪|
|MEDIUM|1|中降噪|
|HIGH|2|高降噪|
|UNKNOWN|-1|UNKNOWN|
### ThermalFFCMode
 * 红外FFC增益模式

|名称|值|描述|
| :--------  | :-----  | :----:  |
|SHUTTER_CORRECTION|0|快门增益|
|SCENE_CORRECTION|1|场景增益|
|SHUTTER_NORMAL_ON|2|快门常开|
|SHUTTER_NORMAL_OFF|3|快门常闭|
|UNKNOWN|-1|UNKNOWN|
### ThermalImageModel
 * 红外图像模式

|名称|值|描述|
| :--------  | :-----  | :----:  |
|GENTLE|0|柔和|
|STANDARD|1|标准|
|ENHANCE|2|增强|
|UNKNOWN|-1|UNKNOWN|
### ThermalImageTone
 * 红外相机图像色调

|名称|值|描述|
| :--------  | :-----  | :----:  |
|WARM_TONED|0|暖色调|
|COOL_TONE|1|冷色调|
|UNKNOWN|-1|UNKNOWN|
### TTSModeType
 * 喊话器状态

|名称|值|描述|
| :--------  | :-----  | :----:  |
|IDLE|0|空闲中|
|TRANSCODING|1|转义中|
|UNKNOWN|-1|UNKNOWN|
