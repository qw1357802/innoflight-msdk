
##INTERFACE
### setLteStateListener
|方法名|setLteStateListener|
| :--------  | :-----  |
|描述|* 设置机载电脑LTE模块状态|
|返回值|void|
|请求参数|listener（listener）
|云冠|支持|
### setYoloV5DetectionEnable
|方法名|setYoloV5DetectionEnable|
| :--------  | :-----  |
|描述|* 设置云冠Yolo识别框架是否启动|
|返回值|void|
|请求参数|callback（callback）
|请求参数|isYoloEnable（是否开启视觉识别）
### getYoloV5DetectionEnable
|方法名|getYoloV5DetectionEnable|
| :--------  | :-----  |
|描述|* 获取云冠Yolo识别框架是否启动|
|返回值|void|
|请求参数|callback（callback）
### setCrestLteEnable
|方法名|setCrestLteEnable|
| :--------  | :-----  |
|描述|* 设置机载电脑LTE模块开关|
|返回值|void|
|请求参数|callback（callback）
|请求参数|isEnable（LTE是否打开）
### getCrestLteEnable
|方法名|getCrestLteEnable|
| :--------  | :-----  |
|描述|* 获取机载电脑LTE模块开关|
|返回值|void|
|请求参数|callback（callback）
### isDroneLTERegister
|方法名|isDroneLTERegister|
| :--------  | :-----  |
|描述|* 获取当前无人机LTE注册状态|
|返回值|boolean（注册是否成功）|
### void getSn
|方法名|void getSn|
| :--------  | :-----  |
|描述|* 获取云冠SN|
|返回值|void|
|请求参数|callback（callback）

##INFO
### LteStatus
 * LTE模块状态

|名称|描述|
| :--------  | :----:  |
|moduleTemperature|模块温度|
|rsrp|lte空中信噪比|
|snr|lte空中增益强度|
|latency|lte网络延迟|
|signalLevel|lte信号质量|
|simExist|sim卡是否插入|
|lteWirelessCarrier|sim卡运营商|

##ENUM
### LteSignalLevel
 * LTE信号质量

|名称|值|描述|
| :--------  | :-----  | :----:  |
|GREAT|2|良好|
|GOOD|1|一般|
|BAD|0|较差|
|UNKNOWN|-1|未知|
### LteWirelessCarrier
 * lte运营商

|名称|值|描述|
| :--------  | :-----  | :----:  |
|ChinaTelecom|1|中国电信|
|ChinaMobile|0|中国移动|
|ChinaUnicom|2|中国联通|
|UNKNOWN|-1|未知|
### SimExist
 * 当前sim卡是否已插入

|名称|值|描述|
| :--------  | :-----  | :----:  |
|SIM_EXIST|1|sim卡已插入|
|SIM_NOT_INSIDE|0|sim卡未插入|
|UNKNOWN|-1|未知|
