# 下载中心

## GCS地面站软件
**工程名称：** GCS  
**版本名称：** v1.3.0  
**版本号：** 120

### 下载链接
- [GCS-v0.6.1（APK）](https://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/gcs/GCS_V0.6.1_release.apk) - APK安卓应用程序
- [GCS-v1.0.0-alpha3（APK）](https://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/gcs/GCS_V1.0.0-alpha3_release.apk) - APK安卓应用程序
- [GCS-v1.0.0-alpha4（APK）](https://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/gcs/GCS_V1.0.0-alpha4_release.apk) - APK安卓应用程序
- [GCS-v1.0.0-alpha5（APK）](https://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/gcs/GCS_V1.0.0-alpha5_release.apk) - APK安卓应用程序
- [GCS-v1.0.0-alpha6（APK）](https://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/gcs/GCS_V1.0.0-alpha6_release.apk) - APK安卓应用程序
- [GCS-v1.1.0-新春特别版（APK）](https://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/gcs/GCS-v1.1.0-happy_new_year_v3.apk) - APK安卓应用程序
- [GCS-v1.3.0（APK）](http://qiniu.geoai.com/%E6%AD%A3%E5%BC%8F%E5%9B%BA%E4%BB%B6/%E5%9C%B0%E9%9D%A2%E7%AB%99/GCS-V1.3.0.apk?sign=c4115f8130163ab25eb12e24dd8e64ad&t=65f3c7c3) - APK安卓应用程序

---

## MSDK示例工程
**工程名称：** MSDK-Sample  
**版本号：** v1.3.0

### 下载链接
- [MSDK-SampleCode示例程序（APK）](http://qiniu.geoai.com/%E6%AD%A3%E5%BC%8F%E5%9B%BA%E4%BB%B6/%E5%9C%B0%E9%9D%A2%E7%AB%99/MSDK-Sample-v1.3.0.apk?sign=0be5d334ee6d22ea10f72551619e4d63&t=65f3c989) - APK安卓应用程序。
- [MSDK-Sample-v1.4.1含双目校准程序（APK）](http://qiniu.geoai.com/%E8%B0%83%E8%AF%95%E5%9B%BA%E4%BB%B6/%E5%9C%B0%E9%9D%A2%E7%AB%99/MSDK-Sample-v1.4.1-dev-obstacle-calibration.apk?sign=1787d29a13ec80e30f5ca9649fc659ee&t=6625e6b8) - 双目校准专用 
- [MSDK-SampleCode示例程序（ZIP）](https://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/msdk/SampleCode-v1.0.0-a1.zip) - 包含完整的SDK示例工程文件。

---

## MSDK库文件

**工程名称：** MSDK  
**版本号：** v1.3.0

### 下载链接

- [MSDK工程压缩包（AAR）](http://121.37.203.145:8081/repository/geoai-release/com/geoai/mavlink/msdk-provided/mavsdk/v1.3.0/mavsdk-v1.3.0.aar) - 包含完整的SDK工程文件。
- [MSDK依赖（POM）](http://121.37.203.145:8081/repository/geoai-release/com/geoai/mavlink/msdk-provided/mavsdk/v1.3.0/mavsdk-v1.3.0.pom) - 提供Maven项目对象配置文件。

### 依赖库
如果您使用依赖的形式使用MSDK工程，您可能需要引用以下依赖库：

**请参考[空白工程继承MSDK]()添加maven仓库地址**

 | 名称 | 依赖源 | 
 | --- | --- |
 | 主工程 | implementation("com.geoai.mavlink.msdk-provided:mavsdk:v1.4.0") | 
 | 第三方依赖 | implementation("com.geoai.mavlink.common:mavsdk:v1.2.0") | 
 | 通讯协议 | implementation("com.geoai.mavlink.protocol:mavsdk:v1.2.7") | 
 | RTK依赖 | implementation("com.geoai.mavlink.rtk:mavsdk:v1.1") | 
 | APM依赖 | implementation("com.geoai.mavlink.apm:mavsdk:v1.1") | 
 | OpenCV | implementation("com.geoai.mavlink.opencv:mavsdk:v1.1") | 
 | 双目校准 | implementation("com.geoai.mavlink.vision-calibration:mavsdk:v1.1") | 
 | WebRTC | implementation("com.geoai.mavlink.videoplayer-webrtc:mavsdk:v1.0.0") | 
 | EasyRtsp | implementation("com.geoai.mavlink.videoplayer-easyrtsp:mavsdk:v1.0.0") | 
 | 自研播放器 | implementation("com.geoai.mavlink.videoplayer-geoairtsp:mavsdk:v1.3.5") |
 | 增强图传 | implementation("com.geoai.mavlink.agora:mavsdk:v1.2.5") |

### 注意事项
- 主工程包含RTK、Common、Protocol，如果使用maven形式使用，则无需重复依赖
- 如果使用AAR形式使用，请**按需手动**进行关联库依赖
- 由于播放器整体框架较大，请按需进行选择使用
- OpenCV与双目模块请关联依赖使用

---
## MSDK-YFLog日志解析工具
**工程名称：** MSDK-YFLog-Decode  
**版本号：** v1120

[使用说明](https://ronnyxie.gitee.io/geoai_msdk_docs/#/doc/other/高性能日志框架YFLog.md)

### 下载链接
- [YFLogDecode-withJRE(ZIP)](https://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/msdk/decode-ui-with-jre-win_1120.zip) - 完整的EXE可执行文件

------
## C60固件仓库
**工程名称：** C60FirmwareCenter  
**版本号：** 1.0.3

[使用说明](https://ronnyxie.gitee.io/geoai_msdk_docs/#/doc/module/FirmwareUpgrade.md)

### 下载链接
- [C60固件仓库(ZIP)](https://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/tmp/C60FirmwareCenter.zip) - 完整的EXE可执行文件

---

**请注意，下载链接和库地址可能会根据更新而发生变化。如果您遇到任何问题或需要进一步帮助，请联系我们团队。**

感谢您选择使用我们的SDK工程！祝您愉快的开发体验！