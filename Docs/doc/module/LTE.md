# LTE模块

## LTE（Cellular）介绍
为满足用户在多个领域对使用无人机进行远距离作业的需求，譬如测绘，巡检，安防等领域，MSDK 开放了无人机的 LTE 功能。LTE 功能的开启，让 4G 图传可以参与到图像传输过程，保障图传稳定性。在图传信号较差的情况下，4G 图传将自动开启，进入增强模式，即 无线电 图传与 4G图传共同工作。若图传信号断开，4G 图传将独立工作。

通过LTE功能可以解决以下核心问题

- 在无线电环境受到干扰时，增强链路可以及时介入避免链路干扰
- 在飞行距离过远的场景，使用增强链路可以有效延长作业半径
- 可以使用增强链路传输飞行器的RTK差分数据

## 关联接口

- 设置当前无人机信号连接方式
    ```
    ProductManager -> setProductConnectionListener
    ```
  
    **MSDK会通过此回调向上层反馈当前使用什么方式与无人机进行通讯**


- 设置无人机LTE模块状态监听
    ```
    CrestManager -> setLteStateListener
    ```
    在LTE状态监听接口中，可以获取机载电脑LTE模块关联状态：
  - 温度
  - 硬件模块信号质量
  - 网络延迟
  - 信号质量
  - 网络运营商


- LTE模式
    ```
    AirlinkManager -> CellularModule -> setCellularEnhancedTransmissionType
    ```
    通常来说，开发者使用这个接口配置LTE模式即可

    - 仅用无线电（默认状态）
    - 仅用LTE
    - 混合增强模式


- 获取当前LTE图传质量
  ```
    AirlinkManager -> CellularModule -> setVideoChannelQualityListener
    ```
    - 丢包率
    - 码率
    - 帧率
    - 分辨率


- 获取无人机端LTE注册状态
  ```
  CrestManager -> isDroneLTERegister
  ```

- 获取当前LTE连接状态

    ```
    AirlinkManager -> CellularModule -> getCellularConnectionState
    ```
  
    **此接口仅调试使用，用于判断当前LTE服务器连接状态**

## 如何使用LTE（Cellular）图传（RTC）功能

### 1.集成GEOAI自研播放器
```java
implementation("com.geoai.mavlink.videoplayer-geoairtsp:mavsdk:v1.3.3")
```

**请使用v1.3.3及以上的播放器版本**

### 2.使用VideoFeeder5进行实例化
```kotlin
videoFeeder5.initPlayer(
    YFV5Params.Builder()
        .setPlayNode(VideoFeederView5.PlayNode.MAIN_CAM)
        .setVideoCodecMode(VideoFeederView5.CodecMode.HARDWARE)
        .setVideoDecodeMode(VideoFeederView5.DecodeMode.H264)
        .build()) {
    it.startPlayer()
}
```

**在功能使用上与VideoFeeder4基本无异**

以上便完成了LTE图传的接入，如果当前模式为【仅用4G】或【增强模式】时，4G图传会自行启动并渲染展示。

## 如何使用LTE（Cellular）数传（RTM）功能

目前开发者无需特别关注数传数据的来源，MSDK底层会协助开发者进行判断

- 假设当前处于仅用无线电模式，数传的收发均走无线电进行传输
- 假设当前处于仅用LTE模式，数传的收发均走LTE进行传输
- 假设当前处于混合增强模式，数传当无线电信号良好时走无线电传输；当无线电信号差时，则走LTE进行传输


## 请注意：

- 目前Cellular模块在MSDK v1.2版本进行开放，机载电脑预计在2024.03进行接入。
- 由于特殊原因，MSDK在首次启动后会将图传模式设置为**仅用无线电**传输。