# RTK模块

## RTK功能说明

​		RTK（Real-time Kinematic）即载波相位动态实时差分技术，是常用的全球定位测量方法，可提供厘米级的定位精度。RTK可以准确定位飞行器的实时位置，以便快速、准确地完成航空航海航空航空和UAV飞行。它通过分析来自射线跟踪台的GPS/GLONASS的差异数据，提供精确的实时信息，该信息可用于精确的地理测绘，以及在UAV飞行控制上也可产生巨大的改进。它可以在任何时候为飞行提供精准的纠正信息，确保飞行期间的精准稳定，满足用户要求的航空航空航海航空和UAV飞行控制应用，以及各种类型的数据搜集和时间误差补偿。

## MSDK目前支持的模式

​		目前MSDK通过软件集成已经支持绝大多数厂商的RTK服务能力，并且在MSDK底层封装了差分、GNSS算法、惯导，还包括底层网络层通讯、差分数据收发、账号鉴权以及各种其他的服务。具体支持的能力列表如下。

* 千寻位置DSK服务接入
* 中国移动DSK服务接入
* 六分网络AK服务接入
* 自定义网络RTK（NtripClient）接入
* 蓝牙基站

### 千寻位置DSK接入指南

1. 进入[千寻位置官网](https://findcm.my.qxwz.com/)，进入千寻知寸FindCM控制台

2. 在设备服务实例中，记录下设备服务号，密钥，设备ID，设备类型

   ![image-20230927145018045](https://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/img/image-20230927145018045.png)

3. 将步骤二中的信息通过相关API发送至MSDK进行连接，MSDK底层将会处理与千寻相关的交互逻辑

   ![image-20230927145413903](https://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/img/image-20230927145413903.png)

4. 调用RTK连接的接口，即可进行RTK的连接

### 中国移动DSK接入指南

> 目前CMCC服务仅提供商用注册，开发者如有连接需求请提前联系我们。

### 六分网络AK接入指南

1. 进入[六分科技官网](https://www.sixents.com/),并完成注册进入控制台

2. 在厘米级服务的实例列表界面,分别记录下AK和AS

   ![image-20230927145947525](https://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/img/image-20230927145947525.png)

3. 将步骤二中的信息通过相关API发送至MSDK进行连接，MSDK底层将会处理与千寻相关的交互逻辑

   ![image-20230927150019619](https://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/img/image-20230927150019619.png)

### 自定义网络RTK(Ntrip Client)连接指南

> 请确保服务商通过Ntrip协议播发的RTCM数据符合[协议标准](https://gssc.esa.int/wp-content/uploads/2018/07/NtripDocumentation.pdf)，并且确保RTCM数据内容的准确性。MSDK层仅实现Ntrip协议栈，并不会参与RTCM数据的解算验证。

1. 请从服务商处获取Ntrip连接的关键信息

   1. 服务器IP
   2. 服务器连接端口
   3. 账号密码
   4. 挂载点

2. 通过API接口将数据发送至MSDK,MSDK将自主处理RTK Ntrip部分协议的收发

   ![image-20230927150243406](https://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/img/image-20230927150243406.png)

### 蓝牙基站连接指南

> 蓝牙基站需要确保蓝牙设备在**主机模式**下能正常工作。并且播发的内容符合RTCM协议标准。

1. 蓝牙基站上电并处于正常工作状态

2. 地面站终端需要使用Android自带的蓝牙功能与蓝牙基站提前进行配对

3. 配对完成后，可以通过MSDK获取到当前终端已配对的蓝牙模块

   ![image-20230927150841475](https://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/img/image-20230927150841475.png)

3. 选择设备（记录MAC地址）后，可以通过API进行RTK连接，MSDK会将蓝牙电台的数据透传至飞控进行定位解算

---

## MSDKAPI设计框图

![image-20230927160248585](https://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/img/image-20230927160248585.png)

## 其他须知

* 目前MSDK USB-RTK暂未开放，请勿使用。
* 用户可以自行调用sendRTCM接口发送差分数据至飞行器
* 飞行器起飞前单点解，则飞行过程均使用GPS精度进行定位解算

## RTK服务错误码列表

```java
/**
 * 连接失败
 */
CODE_RTK_ERROR_CONNECTED_FAILED(-10210),
/**
 * ntrip数据错误
 */
CODE_RTK_ERROR_SERVER_RECEIVED_DATA_FAILED(-10211),
/**
 * ntrip server未知错误
 */
CODE_RTK_ERROR_SERVER_UNKNOWN_EXCEPTION(-10212),
/**
 * ntrip认证失败
 */
CODE_RTK_ERROR_NTRIP_AUTH_FAILED(-10213),
/**
 * 无法接收飞控gga报文
 */
CODE_RTK_ERROR_UNABLE_RECEIVED_GGA(-10214),
/**
 * 无法接收rtcm数据
 */
CODE_RTK_ERROR_UNABLE_RECEIVED_RTCM(-10215),
/**
 * RTK启动能力失败
 */
CODE_RTK_CAP_ERROR(-10217),

/**
 * RTK网络异常
 */
CODE_RTK_NETWORK_ERROR(-10218),
/**
 * RTK未连接
 */
CODE_RTK_NOT_CONNECTED(0),
/**
 * RTK连接中
 */
CODE_RTK_CONNECTING(10200),
/**
 * RTK已连接
 */
CODE_RTK_CONNECTED(10201);
```