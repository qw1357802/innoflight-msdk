# 航线任务模块交底书

> 以下文档的接口使用有版本要求，请确保在接口调用时软件符合最低版本要求：
>
> MSDK：v0.6.0
>
> GCS：v0.6.0.0

1. ## 技术路线

   ​	MSDK底层使用Mavlink协议驱动，与飞控/云冠间的通讯也是使用UDP+Mavlink的形式。在MSDK中与飞行器的任务交互如下图所示，开发者如果对Mission协议感兴趣可访问[Mavlink协议官网](https://mavlink.io/en/services/mission.html)进行学习。

   ![image-20230824153444198](https://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/img/image-20230824153444198.png)

2. ## UML图

   ​	下面提供MSDK中任务模块核心类的UML图

   ![任务模块UML2](https://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/img/任务模块UML2.png)

3. ## 接口调用逻辑

   MSDK提供的航线功能结构较为简单，调用流程如下图所示。详细的使用接口请查看MSDK API文档中航线任务相关的管理类[MissionManager](http://ronnyxie.gitee.io/geoai_msdk_docs/#/component/mission/all)。

   ![image-20230824183630103](https://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/img/image-20230824183630103.png)

4. ## 任务上传

    1. #### 实例化MissionBuilder

       ​	在架构设计中，MissionItemBuilder以单例的形式暴露给开发者，开发者可以在与无人机建立正常通讯后，通过实例化MissionItemBuilder的方式获取任务构建器。关于任务构建器相关的接口使用方式可以参考[任务构建器](https://ronny-dev.github.io/GEOAI-Mavlink-SDK-DOC/#/component/mission/MissionBuilder)进行查阅。

       ```java
       MissionItemBuilder.Builder builder = new MissionItemBuilder.Builder();
       ```

    2. #### 配置航线属性

       ​	在builder的最外层，用户可以配置任务航线的属性，比如航线速度，航线完成方式等。在builder的设置中还需要传入Waypoint（航点）列表，稍后在下个章节中体现。

       P.S 可以使用构造器模式传入相关参数

       P.P.S 其中有一些属性是默认赋值的，具体请翻阅接口文档

       ```java
       MissionItemBuilder builder = new MissionItemBuilder.Builder()
                  .setMissionSpeed(8.0f)
                  .setFinishAction(MissionItemBuilder.MissionFinishAction.MISSION_FINISH_ACTION_GO_HOME)
                  .setHeadingMode(MissionItemBuilder.MissionHeadingMode.MISSION_HEADING_MODE_AUTO)
                  .setWaypointList(...) //todo WaypointList
                  .build();
       ```

    3. #### 配置航点属性

       ​	在上个章节的示例中需要传入的航点列表参数，在设计中是**数据实例**，用户需要手动创建waypoint列表并针对每个waypoint进行赋值和设定参数。内部变量的单位设定请前往[接口文档](https://ronny-dev.github.io/GEOAI-Mavlink-SDK-DOC/#/component/mission/MissionBuilder?id=waypoint)查阅。

       > **在任务执行的过程中，飞行器会根据列表的放置顺序进行航点任务的执行。**

       ```java
       // init waypoint list
       List<MissionItemBuilder.Waypoint> waypointList = new ArrayList<>();
       // looper configure waypoint parameter
       for (int i = 0; i < 3; i++) {
           MissionItemBuilder.Waypoint waypoint = new MissionItemBuilder.Waypoint();
           waypoint.setWayPointLatitude(23.0d);
           waypoint.setWayPointLongitude(113.0d);
           waypoint.setWayPointAltitude(10.0f);
           waypoint.setActions(...); //todo wyapoint actions
           waypointList.add(waypoint);
       }
       // set mission builder
       missionItemBuilder.setWaypointList(waypointList);
       ```

    4. ### 配置航点动作

       在上个章节的示例中需要传入航点动作参数，用户需要手动创建actions的数组并针对需要执行的action进行赋值和设定参数

       ```java
       BaseAction[] baseActions = new BaseAction[5];
       baseActions[0] = new ActionCameraTakePhoto(true, 1);
       baseActions[1] = new ActionCameraRecord(true);
       baseActions[2] = new ActionAircraftRotate(135);
       baseActions[3] = new ActionCameraZoom(30);
       baseActions[4] = new ActionCameraRecord(false);
       waypoint.setActions(baseActions);
       ```

    5. ### 上传任务

       ​	用户在使用MissionItemBuilder完成了任务的构建后，可以进行任务上传给无人机的动作。

       [MissionUploadState](https://ronny-dev.github.io/GEOAI-Mavlink-SDK-DOC/#/component/mission/all?id=missionuploadstate)

       ```java
       // mission upload
       missionManager.uploadMission(builder, new IMissionUploadListener() {
           @Override
           public void onMissionUploadState(MissionUploadState state) {
                       // mission upload state result
           }
       
           @Override
           public void onUploadProgress(int index, int progress) {
                       // index: current upload seq
                       // progress: percentage upload progress
           }
       
           @Override
           public void onMissionUploadResult(boolean isSuccess, GEOAIError error) {
                       // mission upload result
           }
       });
       ```

    6. ### 任务启动

       ​	用户在使用MissionUpload接口成功上传任务后（MissionUploadState为READY_TO_EXECUTE）并且MissionUploadResult提示上传成功；用户可以使用MissionStart接口进行任务启动。

       > **在调用任务启动前必须确保当前无人机环境允许任务启动，并确保航线设定符合预期。**

       ```java
       missionManager.startMission(new CompletionCallback.ICompletionCallback() {});
       ```

5. ## 任务下载

   ​	为满足用户在MSDK使用过程中下载任务的需求，MSDK提供接口进行任务队列的下载。并最终构建出MissionItemBuilder，用户可以持有该对象直接进行任务执行。

   ```java
   /**
    * 下载任务队列
    * @param mode 下载模式
    * @param callback callback
    */
   void downloadMission(MissionReloadMode mode, @NonNull CompletionCallback.ICompletionCallbackWith<MissionItemBuilder.Builder> callback);
   ```

   · 需要注意的是，该接口提供两种模式（MissionReloadMode）的任务下载

    	1. 缓存下载
    	2. 实时下载

   ​    **MSDK在每次启动时会进行任务缓存（任务预下载）如下图所示；所以建议用户在使用该功能时统一使用<缓存下载>模式，避免浪费一些不必要的交互时间。**

   > **如果在任务飞行过程中调用过：终止任务接口（stopMission），任务将无法下载。因为任务已经从飞控队列中移除。****

   ![image-20230824172415107](https://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/img/image-20230824172415107.png)

6. ## 断点续飞

   ​	断点续飞是指在执行自动飞行任务（航点航线）的过程中，记录已执行的航点位置，可以在航线的任意位置进行断点记录。记录后可进行返航或者手动飞行操作。当操作完成后，可继续上次航点航线的最后一个航点位置进行任务继续执行。

   ​	目前MSDK支持简单的断点续飞动作。**用户可以通过触发以下接口创建一个当前任务位置的航线快照，该接口返回的航迹就是二次续飞的航线。**

   ```java
   /**
    * 返回从当前节点构造的断点任务
    * @param callback callback
    */
   void createBreakPointInfo(@NonNull CompletionCallback.ICompletionCallbackWith<MissionItemBuilder.Builder> callback);
   ```

   **断点续飞建议流程如下：**

    1. 确保用户在进行任务飞行
    2. 在用户需要断点保存时，调用该接口，并终止任务
    3. 将返回的任务快照序列化保存
    4. 当用户需要续飞时，拉取上一次保存的任务快照并执行

   **需要注意的是，当飞行器处于以下模式时，不允许创建断点续飞任务：**

    	1. 飞行器未连接
    	2. 飞行器不处于任务模式
    	3. MSDK未缓存任务队列
    	4. 任务未飞行至首航点
    	5. 任务已经到达最后一个航点
    	6. 任务航点数<2

   **GCS已经支持断点续飞功能，调用入口位置：**

   ![image-20230824174020359](https://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/img/image-20230824174020359.png)

7. ## WPML支持

    1. ### 前言

   ​	WPML 是 WayPoint Markup Language 的缩写，即航线文件格式标准。WPML 航线文件格式标准基于 [KML](https://so.csdn.net/so/search?q=KML&spm=1001.2101.3001.7020)（Keyhole Markup Language）的定义进行扩展。WPML 航线文件遵循 KMZ 归档要求，所有航线文件以 “.kmz” 后缀结尾。

    2. ### 航线生成方式

        1. **移动端可通过DJI Pilot2导出航线文件**
        2. **电脑端通过大疆司空生成航线文件**

   导出航线文件后的文件名称一般为***.kmz，MSDK不能直接支持，需将kmz文件解压，提取WPML文件

   ![image-20230824175303001](https://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/img/image-20230824175303001.png)

    3. ### 调用方式

       用户可以通过读取到wpml文件后，调用MSDK以下接口进行航线转换

       ```java
       /**
        * 转化KMZ航线
        * @param wpml wpml
        * @param callback callback
        */
       void coverWPMLMission(String wpml, @NonNull IWpmlCoverListener callback);
       ```

    4. ### 目前支持的WPML属性

       > 目前仅支持如下版本的WPML文件
       >
       > ```xml
      > KmlXmlns = http://www.opengis.net/kml/2.2
      > ```
       >
       > ```xml
      > WpmlXmlns = http://www.dji.com/wpmz/1.0.3
      > ```

       ​	在MissionWpmlTransition的定义中，核心是对WPML文件的MissionConfig、Folder、PlaceMarks进行解析，目前仅针对如下属性进行相关的适配，**如果在WPML中定义未被识别的属性，将会忽略该属性**。

       **MissionConfig:**

       | 支持的属性                   |
             | ---------------------------- |
       | wpml:finishAction            |
       | wpml:flyToWaylineMode        |
       | wpml:takeOffSecurityHeight   |
       | wpml:globalTransitionalSpeed |

       Folder:

       | 支持的属性             |
             | ---------------------- |
       | wpml:autoFlightSpeed   |
       | wpml:executeHeightMode |

       **PlaceMarker:**

       | 支持的属性                |
             | ------------------------- |
       | Point                     |
       | wpml:waypointHeadingParam |
       | wpml:executeHeight        |
       | wpml:waypointSpeed        |
       | wpml:waypointHeadingParam |
       | wpml:actionGroup          |

       **Action:**

       | 支持的动作   |
             | :----------- |
       | hover        |
       | rotateYaw    |
       | gimbalRotate |
       | zoom         |
       | stopRecord   |
       | startRecord  |
       | takePhoto    |

    5. ### GCS使用WPML航线

        1. 准备好WPML航线
        2. 上传至公网平台保存
        3. 在GCS中在线加载该航线
        4. 执行航线

       ![20230824_182435](https://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/img/20230824_182435.gif)

8. ## 额外支持

   MSDK目前还支持指点飞行和盘旋飞行模式。

   ```java
   /**
    * 直接通知飞行器前往指定坐标，不会触发所有任务逻辑
    * @param latitude 纬度
    * @param longitude 经度
    * @param altitude 高度
    * @param speed 速度
    * @param callback callback
    */
   void sendPositionCommand(double latitude, double longitude, float altitude, float speed, CompletionCallback.ICompletionCallback callback);
   ```

   ```java
   /**
    * 使飞行器在指定位置盘旋，不会触发所有任务逻辑
    * @param latitude 纬度
    * @param longitude 经度
    * @param altitude 高度
    * @param radius 半径
    * @param speed 速度
    * @param yawBehaviour 航向模式
    * @param callback callback
    */
   void sendOrbitCommand(double latitude, double longitude, float altitude, float radius, float speed, OrbitYawBehaviour yawBehaviour, CompletionCallback.ICompletionCallback callback);
   ```

   > **注意：上述两个接口不会触发任务过程的所有逻辑（不属于航点任务）如果用户想要终止该动作，请调用FC的stopAllAction指令进行终止并使飞行器盘旋**