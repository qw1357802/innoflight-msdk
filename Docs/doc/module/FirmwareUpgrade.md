# 固件升级教程

为了解决各个模块之间上传的固件没有一个统一存档的位置，现尝试使用FirmwareCenter进行固件的管理。**研发可以上传调试固件进行固件提测调试**；当固件正式发布后，可以发布至正式固件中存档发布。

如果需要更新说明文档、删除固件，请联系Ronny进行调整。

## 下载地址
[Firmware Center v1.0.3](https://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/tmp/C60FirmwareCenter.zip)

GCS:请前往下载中心获取新版（v1.3.0以上支持FirmwareCenter下载）

## FirmwareCenter使用教程

1. 在下载中心下载FirmwareCenter.zip
2. 解压文件，双击运行
3. 在软件中，可以上传固件、下载固件、查看固件更新日志

![image-20240312112053713](http://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/img/image-20240312112053713.png)

![image-20240312112142667](http://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/img/image-20240312112142667.png)

## GCS-v1.3.0固件升级使用教程
1. 选择右上角Menu菜单
2. 选择最后一个[...]选项
3. 选择固件升级
   ![image-20240312112447908](http://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/img/image-20240312112447908.png)
4. 选择子模块升级
   ![image-20240312112536260](http://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/img/image-20240312112536260.png)
5. 选择需要更新的项目
   ![image-20240312112557582](http://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/img/image-20240312112557582.png)
6. 点击确认，进行下载
7. 下载完成后，会提示是否需要更新，如果确认更新，将会自动进行更新程序
   ![image-20240312112821797](http://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/img/image-20240312112821797.png)