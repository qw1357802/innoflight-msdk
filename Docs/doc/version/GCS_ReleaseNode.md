# GCS版本发布

## GCS V1.3.0 发布记录

下载地址：[GCS-v1.3.0](http://qiniu.geoai.com/%E6%AD%A3%E5%BC%8F%E5%9B%BA%E4%BB%B6/%E5%9C%B0%E9%9D%A2%E7%AB%99/GCS-V1.3.0.apk?sign=c4115f8130163ab25eb12e24dd8e64ad&t=65f3c7c3)

### 发布日期

2024.03.15

### 内容

- 接入MSDK v1.3新特性
- 接入用户系统，接入云端任务

---

## GCS V1.2.0 发布记录

下载地址：[https://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/gcs/GCS_V1.2.0_release.apk](https://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/gcs/GCS_V1.2.0_release.apk)

### 发布日期

2024.02.28

### 内容

- 接入MSDK v1.2新特性

---

## GCS V0.6.1.0 发布记录

下载地址：[https://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/gcs/GCS_V0.6.1_release.apk](https://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/gcs/GCS_V0.6.1_release.apk)

### 发布日期

2023.08.25

### 系统优化

- [重点]将任务页面集成到飞行页面中

### 新增功能

- 增加飞控控制台页面，提供串口透传能力

### BUG修复

- 解决飞控首页设置页面内存泄露问题
- 解决在Release版本下定位异常问题
- 解决Bugly-v4.0.4版本在Android29下导致Native异常

### 其他

- 新增数据库Litepal依赖
- 更新高德地图Key
- 去除首页DPI配置
- 相关控制接口提供ux提示

---

## GCS V0.6.0.0 发布记录

> 此次为版本重大更新，其中针对性能进行大幅优化，并且修改了一些应用软件配置，如果无法正常安装请先卸载v0.5.0版本后进行apk安装

下载地址：[https://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/gcs/GCS_V0.6.0.0_release.apk](https://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/gcs/GCS_V0.6.0.0_release.apk)

### 发布日期

2023.08.25

### 系统优化

- 手动飞行页面性能重构
- 优化禁飞区展示页面
- 重构手动飞行设置页面
- 重构任务规划设置页面
- 集成Bugly分析工具
- 集成Apk自动安装程序
- 删除无用资源、编译Release版本

### 新增功能

- 新任务规划页面
- 增加电调版本展示
- 进入首页默认加载正在执行的任务
- 增加任务进度展示
- 增加断点业务功能
- 增加WPML业务功能
- 增加精准降落控制开关
- 接入飞控虚拟摇杆策略
- 进入飞行页面自动加载附近禁飞区

### BUG修复

- 解决首页Home点定位异常
- 解决任务规划相机录制ActionUI错误
- 解决任务绘制任务航线Index不居中
- 

### 其他

- 新增icon图
- 调整云台角速度控制手感

---

## GCS V0.5.0.1 发布记录

下载地址：[http://rtk.geoai.com:8001/3.%E4%B8%B4%E6%97%B6%E5%88%86%E4%BA%AB%E7%9B%AE%E5%BD%95/GCS/v0.5.0.1/GCS\_V0.5.0.1.apk?download=true](http://rtk.geoai.com:8001/3.%E4%B8%B4%E6%97%B6%E5%88%86%E4%BA%AB%E7%9B%AE%E5%BD%95/GCS/v0.5.0.1/GCS_V0.5.0.1.apk?download=true)

### 发布日期

2023.08.04

### BUG修复

解决gimbal指令错误控制

---

## GCS V0.5.0 发布记录

### 发布日期

2023.07.27

MSDK版本：v0.5.0-20230727.033629-1

下载地址：[http://121.37.203.145:8001/3.%E4%B8%B4%E6%97%B6%E5%88%86%E4%BA%AB%E7%9B%AE%E5%BD%95/GCS/v0.5.0/GCS\_v0.5.0.apk](http://121.37.203.145:8001/3.%E4%B8%B4%E6%97%B6%E5%88%86%E4%BA%AB%E7%9B%AE%E5%BD%95/GCS/v0.5.0/GCS_v0.5.0.apk)

### 新增功能

*   增加任务规划功能

*   使用vf3进行图传监控

*   增加备降点首页展示

*   增加地图无人机轨迹


### 系统优化

*   进入地图默认展示当前位置

*   增加首页启动权限申请

*   增加变焦入口


### BUG修复

*   电池数据电流值展示不正确

*   虚拟摇杆逻辑与Setting绑定

*   调整地图gimbal控制量


### 其他

*   修改部分展示icon