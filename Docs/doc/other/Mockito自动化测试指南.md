# MSDK自动化测试Mockito使用指南

> 目前自动化测试脚本接入版本号为MSDKv0.5.0。并内置单元测试用例
>
> 用例文档：https://www.kdocs.cn/l/coylQmSOI2S1

[toc]

## 为什么要引入自动化测试

​		MSDK（mobile software developer key）在产品中定义为无人机地面站接入程序，用来承担无人机和应用软件之间控制和通讯的桥梁。**由于目前无人机端暂时没有完善可靠的硬件进行相关测试，所以引入自动化测试框架，规范MSDK层相关控制接口、确保消息回调的准确性**。尽可能的在软件发布之前发现错误的代码逻辑。

> ​	由于MSDK层涉及一部分简单的无人机飞控层面的逻辑，所以自动化测试层面需要覆盖相关飞行控制逻辑，将繁琐的逻辑测试工作从以人为驱动改成机器执行。自动化测试可以极大的节省人力、时间和硬件资源，提高测试效率。

​			在传统的白盒测试、灰盒测试流程上，需要测试工程师针对待测产品进行深入的理解，针对容易出错的逻辑位置进行插桩测试。但是对于产品的推动来讲，过程是非常耗费时间的。基于目前存在的问题，MSDK的自动化测试脚本的编写由研发人员进行制作，颗粒度涵盖接口层的所有逻辑，确保在每次发版前针对改动的模块进行回归测试。

- 能有效提高代码回归的效率
- 避免掉一些繁琐的手工测试环节
- 可以产生模拟数据，模拟软件异常故障的场景

![Mockito测试流程](https://s3.bmp.ovh/imgs/2023/07/17/a95ee3837a0bf4b3.png)

## 什么是Mockito框架

​		在MSDK实际开发的过程中，会经常依赖于应用app层调用的入口函数和依赖于飞机飞控层提供的Mavlink协议入口，如果接口无法正常工作，那么也会影响MSDK层的实际调用。遇到这种情况，我们可能会想模拟该接口提供正常的返回值，用来继续MSDK层实际的逻辑工作。

​		目前MSDK采用Mock框架来进行单元测试，我们可以通过Mockito虚拟出一个外部依赖，在设计用例的过程中，我们仅需要重点关于用户的输入（模拟量）和实例输出即可。

- 可以虚拟出复杂的飞控对象进行模拟测试
- 可以配置Mock对象的行为
- 测试用例只注重测试流程与结果
- 减少外部类、系统和依赖给单元测试带来的耦合

![mockito流程](https://pic3.zhimg.com/80/v2-7a9888b86f0406c6e9863465a5628386_720w.webp)

## 在MSDK中如何实现Mockito测试

### 框架依赖

首先需要集成Mockito框架，依赖的地址如下：

```java
androidTestImplementation 'org.mockito:mockito-android:2.+'
```

其次需要明确，测试脚本保存的路径如下：

```wiki
src/androidTest/java
```

​		依赖集成完毕后，我们就可以编写android Test脚本程序了。接下来我们以FlyController举例，编写起飞的测试用例。

### 代码实现

1. 获取全局的Instrumentation Context，方便控制Android Test的上下文对象，在MSDK层，我们可以将Instrumentatino Context传输给EALog进行日志监控。并且从[System中获取java的缓存文件夹](https://stackoverflow.com/questions/16352326/when-does-system-getpropertyjava-io-tmpdir-return-c-temp)进行保存日志信息。

   ```java
   Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
   EALog.init(appContext, System.getProperty("java.io.tmpdir"));
   ```

2. 通过Mockito.doAnswer接口，插桩注入socket中，监听udp socket的read和write动作，用于接收和拦截socket发出和接收的所有消息。

   ```java
   Mockito.doAnswer(invocation -> {
       DatagramPacket packet = invocation.getArgument(0, DatagramPacket.class);
       byte[] data = packet.getData();
       for (byte datum : data) {
       MAVLinkPacket mavLinkPacket = parser.mavlink_parse_char(datum);
       MAVLinkMessage unpack;
       if (!(mavLinkPacket == null || (unpack = mavLinkPacket.unpack()) == null)) {
              receiveList.add(unpack);
              isEnableReadMessage.set(true);
      		}
      }
      return null;
   }).when(socket).send(any());
   ```

3. 通过java反射，将socket对象注入到MSDK层

   ```java
   //反射替换原生的socket对象
    Field myField = MAVLinkUdpConnection.class.getDeclaredField("socket");
   myField.setAccessible(true);
   myField.set(MAVLinkManager.getInstance().getMavlinkUdpConnection(), socket);
   ```

4. 创建Handler对象，模拟用户调用接口的指令



## 如何执行脚本程序

### 代码调用

![image-20230717210154689](https://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/img/image-20230717210154689.png)

### ADB调用

测试人员可以通过ADB指令触发mockito测试的能力：

1. 由于mockito需要完整android架构环境，所以需要提供设备接入pc

2. 打开adb工具，使用<adb devices>确认设备在线

3. 输入以下command，执行mockito单元测试框架

   ```java
   adb shell am instrument -w com.geoai.mavlink.test/androidx.test.runner.AndroidJUnitRunner
   ```

​        当测试结束后，测试人员可以很清晰的看到哪个接口出现的问题。后续只需要针对单元测试发现的问题进行回归测试即可。

![image-20230717150018418](https://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/img/image-20230717150018418.png)

![image-20230717205446116](https://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/img/image-20230717205446116.png)

## 输出测试报告

​		目前已经制定了飞控模块的相关测试用例，并且基本涵盖所有常用的接口。飞控模块用例执行时间约两分钟。当测试人员将接口测试完毕后，可以通过Android Studio原生提供的android test unit export file result功能将测试结果的网页报告输出出来进行归档，测试报告中能明确的体现测试的通过项和不通过项，方便研发人员进行问题排障碍。

![image-20230717205532306](https://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/img/image-20230717205532306.png)

## 举例：用例设计过程

​	从云台接口概要中，我们可以知道飞行器云台转向指定角度接口是通过Mavlink协议控制的。但是在接口控制的过程存在几个变量，均需要插桩进行测试。

> GimbalManager -> setGimbalAngleControl

1. 云台组件不在线（无人机不在线）的情况发送控制指令
2. 控制指令发送后，消息无应答
3. 在云台无权操作的过程中，发送该指令
4. 云台有权控制的情况下，发送指令，并模拟ACK返回

---

我们选取无权操作进行详解单元测试设计。

1. 首先我们需要模拟无人机已连接的状态

   ```java
   Mockito.when(gimbalManager.isProductConnected((CompletionCallback.ICompletionCallback) any())).thenReturn(true);
   ```

2. 我们将云台的控制权设置成与系统不一致

   ```java
   GimbalStateInfo gimbalStateInfo = new GimbalStateInfo();
   gimbalStateInfo.setPrimaryControlSystemID(0);
   Mockito.when(gimbalManager.getGimbalStateInfo()).thenReturn(gimbalStateInfo);
   ```

3. 模拟发送该接口指令五次

   ```java
   for (int i = 0; i < 5; i++) {
       gimbalManager.setGimbalAngleControl(true, 1.0f, 1.0f, callback);
   }
   ```

4. Mockito验证

    1. 因为获取控制权在设计时需要限制发送频率，判断是否回复了四次系统正忙

       ```
       Mockito.verify(callback, atLeast(4)).onResult(GEOAIError.COMMON_BUSY);
       ```

    2. 由于没有使用Mockito进行回复，所以接口最终需要返回一次接口调用超时

       ```
       Mockito.verify(callback, timeout(5000)).onResult(GEOAIError.COMMON_TIMEOUT);
       ```

    3. 由于没有使用Mockito进行回复，所以该接口会进行超时重发的动作，验证是否发送了四包数据

       ```
       assertEquals(receiveList.size(), 4);
       ```

5. 启动测试

   ![image-20230719171301550](https://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/img/image-20230719171301550.png)

6. 模拟异常

   ​	以上的测试用例，我们通过软件手段模拟接口穷尽可能出现的情况，并且验证MSDK底层的通讯逻辑是否符合要求。接下来我们通过修改其中的参数，抛出错误异常。

   ​	我们将该接口的回调修改成[返回错误]，可以看到，该用例的执行是不成功的。并且在控制台中，编译器返回该指令实际返回的并不是[返回超时]。

   ![image-20230719171806193](https://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/img/image-20230719171806193.png)

## 目前自动化测试不支持的范围

> 20230720 MSDK v0.5.0 仅设计了FC模块，BAT模块，GIMBAL模块；其他模块后续增加
>
> ![image-20230719174901051](https://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/img/image-20230719174901051.png)

飞控模块：

| 接口名称                | 接口函数                      | Remark   |
| ----------------------- | ----------------------------- | -------- |
| 设置飞行器状态监听      | setFlyControllerStateListener | 不支持   |
| 置飞行器HMS系统状态监听 | setDiagnosticsStateListener   | 不支持   |
| 获取备降点              | getAlternateLandingPoints     | 暂不支持 |
| 设置备降点              | setAlternateLandingPoints     | 暂不支持 |

云台模块：

| 接口名称         | 接口函数               | Remark |
| ---------------- | ---------------------- | ------ |
| 设置云台状态监听 | setGimbalStateListener | 不支持 |

电池模块：

| 接口名称               | 接口函数                | Remark |
| ---------------------- | ----------------------- | ------ |
| 设置飞行器电池状态监听 | setBatteryStateListener | 不支持 |
| 获取电芯总数           | getBatteryCellsCount    | 不支持 |
| 获取电池故障信息       | getBatteryDamagedInfo   | 不支持 |



