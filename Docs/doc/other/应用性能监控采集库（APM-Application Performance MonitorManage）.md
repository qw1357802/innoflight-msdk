## 应用性能监控采集库（APM-Application Performance Monitor/Manage）

### 一、前言

​	性能问题是App需要长期优化的关键点之一，当App在线上出现性能问题很可能会导致程序异常如卡顿，响应速度慢，发热严重，耗电快等问题。更严重的时候可能会导致程序卡死或崩溃。在目前的业务场景中，单Activity应用/SDK程序占比比较多，传统的APM程序无法满足业务应用落地实用。因此基于先用APM框架进行修改，打造满足使用场景的APM框架迫在眉睫。

### 二、APM的基本原理

市场上有很多商业化的 APM 平台，比如著名的 New Relic，还有国内的 听云、One APM、火山 等等。这些平台的工作流程基本都是一致的：

1. 首先在客户端（Android、iOS、Web等）采集数据；
2. 接着将采集到的数据整理上报到服务器；
3. 服务器接收到数据后建模、存储、挖掘分析，让后将[数据可视化](https://cloud.tencent.com/product/yuntu?from_column=20065&from=20065)，供用户使用。

如下图：

![image-20231107162454928](https://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/img/image-20231107162454928.png)

我们介绍的 Android APM 框架其实就是在 Android 平台上应用的一个数据采集上报 SDK。主要包含三大模块：

1. 数据采集
2. 数据存储
3. 数据上报

其中数据采集是整个 APM 框架的核心。

### 三、需求概述

​	对于App的性能，像CPU、内存、线程、CRASH、ANR等，都是常规APM需要监控的内容，尤其是当App发生崩溃时，更需要获取崩溃时用户的操作和系统的监控日志加以分析，避免出现黑盒分析找不到头绪。对于当前成熟的APM监控或多或少都存在缺陷。

​	目前所需要的功能概述：

* 周期性的性能采集和性能监控

* 可设定的线程、内存阈值，当超过阈值触发一定的机制

* 实时OOM监控

* ...

  ![image-20231107150200048](https://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/img/image-20231107150200048.png)

### 四、选型过程

除去目前线上的成熟付费方案，可供选择的开源方案不多

1. Leakcanary

2. Matrix(https://github.com/Tencent/matrix)

3. KOOM(https://github.com/KwaiAppTeam/KOOM)

4. ....

   #### 3.1 分析一

   ​	针对OOM监控切入点，Leakcanary和Tencent-Matrix方案使用的是传统Weak Reference方案。在dump内存的过程中会堵塞主线程。虽然Matrix方案在生成Hprof文件的过程中，会进行二次分析和过滤，将数据信息和一些不相关的数据直接剔除，可以将Hprof文件缩小，但是仍然有堵塞的风险。

   #### 3.2 分析二

   ​	Bugly目前已经移植至腾讯云平台，并且Plus版本进行收费。

   #### 3.3 分析三

   ​	通过对KOOM开源版本进行分析，可以清晰了解到该方案是通过底层机制fork子线程进行dump内存堆栈，核心是解决了dump的过程主线程卡死被冻结的问题。但是该方案核心的问题是dump的实际并非出现内存泄露的时刻，而是通过预先设定的内存占用预警进行判断。这里会有误判的风险。

   #### 3.4 分析四

   ​	从源码可以得知，Matrix的OOM切入点是Activity Destroy，当业务场景是单Activity或者SDK应用，那么Matrix只能曲线救国。而KOOM在初始化的过程中需要定义默认的预警参数，针对Android平台五花八门的配置，很准确的设定预警参数。

   #### 3.5 分析五

   ​	Matrix-TraceCanary 其本身作为一款性能检测工具，对自身性能的损耗要求比较高，以下是插桩前后的对比数据：

   | item     | trace    | untrace  | info                        |
       | -------- | -------- | -------- | --------------------------- |
   | FPS      | 56.16    | 56.19    | Android7.0 好机器朋友圈帧率 |
   | FPS      | 41.18    | 42.70    | Android4.2 差机器朋友圈帧率 |
   | apk size | 81.91 MB | 81.12 MB | 实际插桩方法总数 163141     |
   | memory   | +7.6M    | ～       | 运行时内存                  |

   ​	从实测数据上看，Matrix-TraceCanary 对于好机器的性能影响可忽略，对差机器性能稍有损耗，但影响很小。 对安装包大小影响，对于微信这种大体量的应用，实际插桩函数 16w+，对安装包增加了 800K 左右。

   ---

   ​	KOOM利用Linux Copy-on-write机制fork子进程dump大大提高了dump效率。

   线上真实用户的内存镜像，普通dump和fork子进程dump阻塞用户使用的耗时对比，耗时差距都在100倍以上。

   ![image-20231107162437469](https://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/img/image-20231107162437469.png)

### 五、实现

根据上面的选型过程，和在CPS中落地使用的经验，目前使用Matrix+KOOM方案进行实现。

使用Matrix-Trace Canary进行卡顿检测

​	· 慢函数检测——EvilMethodTracer

​	` 帧率检测——FrameTracer

​	· ANR检测——LooperAnrTracer、SignalAnrTracer

使用Matrix-IO Canary进行文件IO检测

​	· IO检测——IOCanaryPlugin

使用KOOM-Java Heap进行内存泄漏监控

​	· 内存监控——HeapTracker

​	· 线程监控——ThreadTracker

​	· FD文件监控——FdTracker

​	· 大内存监控？——FastHugeMemoryTracker

​	· CPU监控——CpuTracker

具体的内部实现过程可以参照CPS性能监控概要设计文档。

### 六、使用方式

#### 一、使用aar方式引用

1. 将aar报拷贝到android工程lib目录

2. 在build.gradle中添加ndk限制

   ```gradle
   	ndk {
       	abiFilters 'arm64-v8a'
   	}
   ```

3. 在build.gradle中添加kotlin配置

   ```gradle
       kotlinOptions {
           jvmTarget = JavaVersion.VERSION_1_8
       }
   ```

4. 在build.gradle中添加libc++_sharded错误告警配置

   ```gradle
       packagingOptions {
           pickFirst 'lib/x86/libc++_shared.so'
           pickFirst 'lib/x86_64/libc++_shared.so'
           pickFirst 'lib/armeabi-v7a/libc++_shared.so'
           pickFirst 'lib/arm64-v8a/libc++_shared.so'
       }
   ```

5. 在build.gradle中正确引用库文件

   ```gradle
       implementation files("libs/geoai-msdk-apm-v1.0-debug_******.aar")
   ```

   **如果工程未引用MSDK，则还需要引用common库文件**

   ```
       implementation files("libs/geoai-msdk-common-v1.0-debug_****.aar")
   ```

#### 二、使用maven方式引用

1. 在settings.gradle中添加maven仓库拉取权限并正确配置账密信息
2. 在gradle.gradle中进行依赖

#### 三、调用方式

```kotlin
/**
     * 配置matrix默认参数
     * @param isAnrTraceEnable 是否开启ANR监控
     * @param isFpsTraceEnable 是否开启FPS监控
     * @param isEvilMethodTraceEnable 是否开启慢函数监控
     * @param isAppMethodBeatEnable 未使用
     */
    fun matrixConfig(
        isAnrTraceEnable: Boolean,
        isFpsTraceEnable: Boolean,
        isEvilMethodTraceEnable: Boolean,
        isAppMethodBeatEnable: Boolean
    ): AppPerformanceMonitor {
        //......
    }
```

```kotlin
/**
     * 配置tracker默认参数
     * @param fdLimit file description打开阈值
     * @param threadLimit 线程阈值
     * @param cpuLimit cpu占用阈值(%)
     * @param memoryLimit 内存占用阈值(%)
     * @param forceDumpMaxThreshold 当内存达到告警强制dump（%)
     * @param forceDumpDeltaThreshold 当增值达到告警强制dump(KB)
     * @param overThresholdCount 告警次数（超过告警次数后才会触发dump）
     */
    fun trackerConfig(
        fdLimit: Int,
        threadLimit: Int,
        cpuLimit: Float,
        memoryLimit: Float,
        forceDumpMaxThreshold: Float = 0.9f,
        forceDumpDeltaThreshold: Int = 300 * 1024,
        overThresholdCount: Int = 1
    ): AppPerformanceMonitor {
        //......
    }
```

```kotlin
/**
 * 初始化APM采集框架
 * @param application application
 * @param logOutPath path for storage logs
 * @param isTrackerEnable tracker monitor enable
 * @param isMatrixEnable matrix enable
 * @return 0: start success.
 * @return -1: init apm module failure.
 */
fun initModule(
        application: Application,
        logOutPath: File,
        isTrackerEnable: Boolean = true,
        isMatrixEnable: Boolean = false
): Int {
   //......
}
```

#### 四、存储目录说明

当调用init接口进行初始化时，需要用户提供一个**存在的目录**用来保存APM框架运行过程中记录的日志文件。

![image-20231107160612864](https://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/img/image-20231107160612864.png)