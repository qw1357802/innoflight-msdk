# 高性能日志系统YFLog

## 前言

在传统的移动端日志记录场景中，每有一句日志记录都将需要触发一次GC，并且在记录的过程中还有大量的Android IO需要写入。当日志量级较少时用户对于该操作几乎无感。但是针对于飞行器日志记录的场景这个问题往往容易造成应用程序卡顿。![image-20231120165556634](https://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/img/image-20231120165556634.png)

选用java实现日志模块，在发布版本（release）一般回选择将日志关闭，当有用户反馈线上出现异常，就需要给用户提供一份包含日志的安装程序，再通过复现后取得日志来定位问题。

基于上面描述的java层写日志会导致频繁GC导致的应用程序卡顿异常，**MSDK目前采用Tencent提供的开源xLog方案**解决在日志记录过程中的流畅性、完整性和安全性问题。

## 技术原理

常规使用java进入日志模块的实现会通过一些技术手段进行GC的优化。比如控制读写的频率：写日志时将数据存储在缓存中，在特定的情况下，再将缓存中的数据回写到磁盘中。

- 定时回写
- 内存不足回写
- 主动调用接口回写

通过以上手段可以有效的减少日志模块读写IO的频率，但是容易导致相关的问题。

- 丢日志
- 如果存在日志加密，会导致整个文件解密出现异常
- CPU使用率会短时间拉高

针对以上提到的问题，Tencent开源Mars框架的xLog采用MMAP机制进行日志读写。简单点讲，MMAP是一段内存控件，在日志存储的过程实际上是对内存进行操作。并且回写时机可控

- 内存不足
- 进程crash
- 调用接口同步
- 自动定时回写

![image-20231120174141264](https://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/img/image-20231120174141264.png)

## 性能对比

#### xLog 去掉压缩加密部分，和 LOG4CPP 放在同一个起点上对比， 每条日志为长度为 100 的随机字符串。详细数据如下：

##### 测试时间：2016-12-31

|                               | 1w 条日志耗时 | 1w条日志大小 | 10w 条日志耗时 | 10w条日志大小 |
| ----------------------------- | ------------- | ------------ | -------------- | ------------- |
| iPhone 6s 使用 xlog           | 115 ms        | 1.9 M        | 1155 ms        | 19 M          |
| iPhone 6s 使用 log4cpp        | 329 ms        | 1.4 M        | 3252 ms        | 13.8 M        |
| HUAWEI MATE 8 使用 xlog       | 452 ms        | 1.5 M        | 4685 ms        | 15.4 M        |
| HUAWEI MATE 8 使用 log4cpp    | 581 ms        | 1.38 M       | 6249 ms        | 13.8 M        |
| SAMSUNG GT-I9500 使用 xlog    | 1130 ms       | 1.5 M        | 13997 ms       | 19.5 M        |
| SAMSUNG GT-I9500 使用 log4cpp | 4148 ms       | 1.4 M        | 46631 ms       | 13.8 M        |
| Motorola Nexus 6 使用 xlog    | 1300 ms       | 1.5 M        | 12328 ms       | 15 M          |
| Motorola Nexus 6 使用 log4cpp | 3848 ms       | 1.3 M        | 36110 ms       | 14 M          |

#### xLog 默认的实现(带有压缩)，和 LOG4CPP 对比， 每条日志为长度为 100 的随机字符串。测试时间：2016-12-31，详细数据如下：

|                               | 1w 条日志耗时 | 1w条日志大小 | 10w 条日志耗时 | 10w条日志大小 |
| ----------------------------- | ------------- | ------------ | -------------- | ------------- |
| iPhone 6s 使用 xlog           | 420 ms        | 955 K        | 4175 ms        | 9.3 M         |
| iPhone 6s 使用 log4cpp        | 331 ms        | 1.4 M        | 3236 ms        | 13.8 M        |
| HUAWEI MATE 8 使用 xlog       | 755 ms        | 1 M          | 7585 ms        | 10 M          |
| HUAWEI MATE 8 使用 log4cpp    | 606 ms        | 1.4 M        | 6356 ms        | 14 M          |
| SAMSUNG GT-I9500 使用 xlog    | 1972 ms       | 1 M          | 18489 ms       | 10.4 M        |
| SAMSUNG GT-I9500 使用 log4cpp | 4743 ms       | 1.3 M        | 47944 ms       | 13.8 M        |
| Motorola Nexus 6 使用 xlog    | 1825 ms       | 1 M          | 17831 ms       | 10 M          |
| Motorola Nexus 6 使用 log4cpp | 3776 ms       | 1.3 M        | 36359 ms       | 14 M          |

由于xLog使用MMAP技术进行日志的读写，在存储优秀的机型上（如iPhone和Huawei）没有明显差异。但是在其他机型上差异还是比较明显的。

## 如何使用

在v1.0.0正式版的MSDK中，基于xLog进行二次封装，用于飞控无线电数据上行/下行的存储，解决无人机日志数据流大导致频繁触发Java GC问题。

```java
//初始化过程
YFLog.getInstance()
        .setLogPath(path)
        .setCacheLogPath(path)
        .setAsyncMode(BuildConfig.DEBUG ? YFLog.AsyncMode.sync : YFLog.AsyncMode.async)
        .setWriteLogLevel(YFLog.LogLevel.LEVEL_ALL)
        .setPrefixName("msdk_log")
        .init();//MSDK log框架

//使用过程
YFLog.i(YFLog.TAG.DL, "[ID]-" + msgID + " [T]-" + type + " [B]-" + HexStrConvertUtil.toHex(bArr));
```

目前记录的log文件没有进行加密，但是日志文件需要通过特定的软件进行解析。[下载地址](![image-20231120190922824](https://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/img/image-20231120190922824.png))

![image-20231120181829373](https://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/img/image-20231120181829373.png)

解密完成后，可以通过复制上行数据/下行数据的内容进行日志分析和操作还原。

![image-20231120190922824](https://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/img/image-20231120190922824.png)