<!-- _coverpage.md -->

# MSDK <small>v1.4.0</small>

[MSDK源码地址](https://gitee.com/RonnyXie/geoai_msdk)
[GCS源码地址](https://gitee.com/RonnyXie/geoai_gcs)
[禁飞区服务器源码地址](https://gitee.com/RonnyXie/ban_fly_server)
[自研播放器源码地址](https://gitee.com/RonnyXie/geoai-mav-link_-video-player)
[文档中心](./doc/version/MSDK_ReleaseNode.md)