## 这是什么  
MSDK Sample Code助力于让开发者更快的上手MSDK API。工程旨在帮助您了解MSDK的相关功能以及相关接口的使用。
MSDK包含了与飞机各个模块通讯的服务比如飞控模块、相机模块、云台模块、避障模块、任务模块等等。同时也提供飞机链接状态的监听以及遥控器模块的相关功能。

## Android Studio配置工程  
### 下载工程源码
你可以通过以下方式进行源码下载

- 通过[gitee项目首页](https://gitee.com/qw1357802/innoflight-msdk)进行下载  

- 使用[git](https://git-scm.com/book/zh)进行下载  

- 使用Android Studio导入工程

![image-20240429162937752](http://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/img/image-20240429162937752.png)

### 使用Android Studio打开工程目录

等待AndroidStudio自动下载MSDK所需依赖，当下载完成后，您可以看到左侧`Android`栏目正确构建出当前MSDK Sample工程  

![image-20240429163341874](http://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/img/image-20240429163341874.png)

### 开始使用吧

接下来，您可以进行工程编译，并将MSDK Sample APK安装至遥控器中，并且在遥控器使用MSDK Sample进行飞行器控制！

![image-20240429164324853](http://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/img/image-20240429164324853.png)

## 工程模块

MSDK Sample工程中基本涵盖了所有MSDK支持的接口能力，其中在右侧栏目中封装了一系列的常用入口

```
├── Sample
│   ├── Menu
│   │   ├── 接口调用集合
│   │   ├── 图传视频播放器
│   │   ├── 虚拟摇杆控制
│   │   ├── 诊断管理
│   │   ├── RTK系统
│   │   ├── 任务系统
│   │   ├── 备降点管理
│   │   ├── 避障管理
│   │   ├── 红外镜头
│   │   ├── 避障校准
│   │   ├── 指南针校准
│   │   ├── 日志系统
│   │   ├── 固件升级
│   │   ├── 电机测试
│   │   ├── 自动视觉降落测试
│   │   └── 增强图传
│   └── TOP
│       ├── MSDK版本号
│       ├── 飞控SN
│       ├── 机载电脑SN
│       ├── 电池SN
│       ├── 云日志模块
│       └── UOS模块
└── LEFT
    └──模块连接状态
```

开发者可以根据需求自行调试无人机接口，验证相关能力。

## 贡献

非常欢迎大家为项目贡献力量，可以通过以下方法为项目作出贡献:

- 在 issue 中提交功能需求和 bug report;  
- 在 issues 或者 require feedback 下留下自己的意见;  
- 通过 pull requests 提交代码;  