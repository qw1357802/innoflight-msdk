package com.example.geoaimavlink_android.activity.data

data class FragmentPageItemList(
    val vavGraphId: Int = -1,
    val items: LinkedHashSet<FragmentPageItem> = LinkedHashSet()
)

data class FragmentPageItem(
    val id: Int = -1,
    val title: Int = -1,
    val description: Int = -1
)