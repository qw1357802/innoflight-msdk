package com.example.geoaimavlink_android.activity.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.geoaimavlink_android.R
import com.geoai.mavlink.geoainet.obstacle.enums.ObstacleCameraSource
import com.geoai.video.VideoFeederView4
import com.geoai.video.YFVParams
import kotlinx.android.synthetic.main.frag_video_fedder.btn_primary_codec_mode
import kotlinx.android.synthetic.main.frag_video_fedder.btn_primary_decode_mode
import kotlinx.android.synthetic.main.frag_video_fedder.btn_primary_play_node
import kotlinx.android.synthetic.main.frag_video_fedder.btn_primary_start
import kotlinx.android.synthetic.main.frag_video_fedder.btn_primary_stop
import kotlinx.android.synthetic.main.frag_video_fedder.btn_secondary_codec_mode
import kotlinx.android.synthetic.main.frag_video_fedder.btn_secondary_decode_mode
import kotlinx.android.synthetic.main.frag_video_fedder.btn_secondary_play_node
import kotlinx.android.synthetic.main.frag_video_fedder.btn_secondary_start
import kotlinx.android.synthetic.main.frag_video_fedder.btn_secondary_stop
import kotlinx.android.synthetic.main.frag_video_fedder.primary_video_channel_fragment
import kotlinx.android.synthetic.main.frag_video_fedder.secondary_video_channel_fragment

class VideoFeederFragment : BaseFragment() {

    private var primaryVideoState = VideoFeederState.STOP

    private var secondaryVideoState = VideoFeederState.STOP

    private var primaryVideoCodecMode = VideoFeederView4.CodecMode.SOFTWARE

    private var primaryVideoSource = VideoFeederView4.PlayNode.MAIN_CAM

    private var secondaryVideoCodecMode = VideoFeederView4.CodecMode.SOFTWARE

    private var secondaryVideoSource = VideoFeederView4.PlayNode.FPV_CAM

    private var primaryVideoDecodeMode = VideoFeederView4.DecodeMode.H264

    private var secondaryVideoDecodeMode = VideoFeederView4.DecodeMode.H264

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.frag_video_fedder, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        updateUiState()

        btn_primary_start.setOnClickListener {
            primaryVideoState = VideoFeederState.PLAYING
            startPrimaryVideo()
            updateUiState()
        }
        btn_primary_stop.setOnClickListener {
            primaryVideoState = VideoFeederState.STOP
            primary_video_channel_fragment.releasePlayer()
            updateUiState()
        }
        btn_primary_codec_mode.setOnClickListener {
            primaryVideoCodecMode = if (primaryVideoCodecMode == VideoFeederView4.CodecMode.SOFTWARE) {
                VideoFeederView4.CodecMode.HARDWARE
            } else {
                VideoFeederView4.CodecMode.SOFTWARE
            }
            btn_primary_codec_mode.text = primaryVideoCodecMode.name
        }
        btn_primary_play_node.setOnClickListener {
            primaryVideoSource = if (primaryVideoSource == VideoFeederView4.PlayNode.MAIN_CAM) {
                VideoFeederView4.PlayNode.FPV_CAM
            } else {
                VideoFeederView4.PlayNode.MAIN_CAM
            }
            btn_primary_play_node.text = primaryVideoSource.name
        }

        btn_secondary_start.setOnClickListener {
            secondaryVideoState = VideoFeederState.PLAYING
            startSecondaryVideo()
            updateUiState()
        }
        btn_secondary_stop.setOnClickListener {
            secondaryVideoState = VideoFeederState.STOP
            secondary_video_channel_fragment.releasePlayer()
            updateUiState()
        }
        btn_secondary_codec_mode.setOnClickListener {
            secondaryVideoCodecMode = if (secondaryVideoCodecMode == VideoFeederView4.CodecMode.SOFTWARE) {
                VideoFeederView4.CodecMode.HARDWARE
            } else {
                VideoFeederView4.CodecMode.SOFTWARE
            }
            btn_secondary_codec_mode.text = secondaryVideoCodecMode.name
        }
        btn_primary_decode_mode.setOnClickListener {
            primaryVideoDecodeMode = if (primaryVideoDecodeMode == VideoFeederView4.DecodeMode.H264) {
                VideoFeederView4.DecodeMode.H265
            } else {
                VideoFeederView4.DecodeMode.H264
            }
            btn_primary_decode_mode.text = primaryVideoDecodeMode.name
        }
        btn_secondary_decode_mode.setOnClickListener {
            secondaryVideoDecodeMode = if (secondaryVideoDecodeMode == VideoFeederView4.DecodeMode.H264) {
                VideoFeederView4.DecodeMode.H265
            } else {
                VideoFeederView4.DecodeMode.H264
            }
            btn_secondary_decode_mode.text = secondaryVideoDecodeMode.name
        }
        btn_secondary_play_node.setOnClickListener {
            secondaryVideoSource = when (secondaryVideoSource) {
                VideoFeederView4.PlayNode.MAIN_CAM -> {
                    VideoFeederView4.PlayNode.FPV_CAM
                }
                VideoFeederView4.PlayNode.FPV_CAM -> {
                    VideoFeederView4.PlayNode.VISION_CALIBRATION
                }
                else -> {
                    VideoFeederView4.PlayNode.MAIN_CAM
                }
            }
            btn_secondary_play_node.text = secondaryVideoSource.name

            val obstacleCameraSource = if (secondaryVideoSource == VideoFeederView4.PlayNode.MAIN_CAM) {
                ObstacleCameraSource.OBSTACLE_CAMERA_FPV
            } else {
                ObstacleCameraSource.OBSTACLE_CAMERA_FRONT
            }
            getProduct()?.obstacleManager?.setObstacleCameraSource(obstacleCameraSource) {
                Toast.makeText(requireActivity(), it?.description?:"success", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun startPrimaryVideo() {
        primary_video_channel_fragment.initPlayer(
            YFVParams.Builder()
            .setPlayNode(primaryVideoSource)
            .setSurfaceWidth(1920)
            .setSurfaceHeight(1080)
            .setVideoDecodeMode(primaryVideoDecodeMode)
            .setCustomPlayUrl("rtsp://admin:3333qqqq@192.168.144.111:554/h264/ch1/main/av_stream")
            .setVideoCodecMode(primaryVideoCodecMode)
            .setSurfaceMode(VideoFeederView4.SurfaceMode.TEXTURE)
            .build(), VideoFeederView4::startPlayer)
    }

    private fun startSecondaryVideo() {
        secondary_video_channel_fragment.initPlayer(
            YFVParams.Builder()
            .setPlayNode(secondaryVideoSource)
            .setSurfaceWidth(1920)
            .setSurfaceHeight(1080)
            .setVideoDecodeMode(secondaryVideoDecodeMode)
            .setCustomPlayUrl("rtsp://admin:3333qqqq@192.168.144.111:554/h264/ch1/main/av_stream")
            .setVideoCodecMode(secondaryVideoCodecMode)
            .setSurfaceMode(VideoFeederView4.SurfaceMode.TEXTURE)
            .build(), VideoFeederView4::startPlayer);
    }

    private fun updateUiState() {
        when(primaryVideoState) {
            VideoFeederState.STOP -> {
                btn_primary_start.isEnabled = true
                btn_primary_stop.isEnabled = false
                btn_primary_codec_mode.isEnabled = true
                btn_primary_play_node.isEnabled = true
            }
            VideoFeederState.PLAYING -> {
                btn_primary_start.isEnabled = false
                btn_primary_stop.isEnabled = true
                btn_primary_codec_mode.isEnabled = false
                btn_primary_play_node.isEnabled = false
            }
        }
        when(secondaryVideoState) {
            VideoFeederState.STOP -> {
                btn_secondary_start.isEnabled = true
                btn_secondary_stop.isEnabled = false
                btn_secondary_codec_mode.isEnabled = true
                btn_secondary_play_node.isEnabled = true
            }
            VideoFeederState.PLAYING -> {
                btn_secondary_start.isEnabled = false
                btn_secondary_stop.isEnabled = true
                btn_secondary_codec_mode.isEnabled = false
                btn_secondary_play_node.isEnabled = false
            }
        }
    }
}

enum class VideoFeederState {
    STOP,
    PLAYING
}