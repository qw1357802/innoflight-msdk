package com.example.geoaimavlink_android.activity.data

import java.lang.reflect.Method

interface IMethodInvokeBuilder {

    fun getMethod(): Method

    fun methodInvoke(vararg param: Any?): Pair<Boolean, Any?>

    fun isCanGet(): Boolean

    fun isCanSet(): Boolean

    fun isCanListener(): Boolean

    fun isCanAction(): Boolean

    fun actionInvoke(result: (Any) -> Unit)

    fun getInvoke(result: (Any) -> Unit)

    fun listenerInvoke(mount: Boolean, result: (Any) -> Unit)

    fun setInvoke(vararg param: Any?, result: (Any) -> Unit)
}