package com.example.geoaimavlink_android.activity.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.MAVLink.enums.MAV_TYPE
import com.example.geoaimavlink_android.R
import com.geoai.mavlink.geoainet.base.mavlinkcore.controller.StarLinkClientManager
import com.geoai.mavlink.geoainet.base.mavlinkcore.core.CloudRouter
import com.geoai.mavlink.geoainet.base.mavlinkcore.engine.GEOAIError
import com.geoai.mavlink.geoainet.base.mavlinkcore.interfaces.CompletionCallback
import com.geoai.module.common.thread.ExecutorsManager
import kotlinx.android.synthetic.main.frag_main_title.img_connected_state
import kotlinx.android.synthetic.main.frag_main_title.msdk_info_text_main
import kotlinx.android.synthetic.main.frag_main_title.msdk_info_text_second
import kotlinx.android.synthetic.main.frag_main_title.tv_iot_db
import kotlinx.android.synthetic.main.frag_main_title.tv_uos_server
import kotlinx.android.synthetic.main.view_title_bar.return_btn
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class TitleInfoFragment: BaseFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.frag_main_title, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initView()

        return_btn.setOnClickListener {
            requireActivity().onBackPressedDispatcher.onBackPressed()
        }
    }

    private fun initView() {

        ExecutorsManager.get().executeAtFixedRate({
            val isCloudDBRunning = CloudRouter.getInstance().isCloudDBWorking
            val isUosServerRunning = CloudRouter.getInstance().isUosCloudWorking
            val drawableRight = resources.getDrawable(R.drawable.icon_right).also { it.setBounds(0, 0, 60, 60) }
            val drawableError = resources.getDrawable(R.drawable.icon_error).also { it.setBounds(0, 0, 60, 60) }
            activity?.runOnUiThread {
                if (isUosServerRunning) {
                    tv_uos_server?.setCompoundDrawables(null, null, drawableRight, null)
                } else {
                    tv_uos_server?.setCompoundDrawables(null, null, drawableError, null)
                }
                if (isCloudDBRunning) {
                    tv_iot_db?.setCompoundDrawables(null, null, drawableRight, null)
                } else {
                    tv_iot_db?.setCompoundDrawables(null, null, drawableError, null)
                }
            }
        }, 1000, 3000)

        msdkInfoVm.msdkInfo.observe(viewLifecycleOwner) {
            it?.let {
                val mainInfo = "MSDK Info:[Ver:${it.sdkVersion} ]"
                msdk_info_text_main.text = mainInfo
                val secondInfo = "MSDK NUM:${it.sdkVersionNum} | Drone ID:${it.droneID}"
                msdk_info_text_second?.text = secondInfo
                img_connected_state.setImageResource(if (it.isAircraftConnected) R.drawable.icon_right else R.drawable.icon_error)
            }
        }
        msdkInfoVm.component.observe(viewLifecycleOwner) {
            it?.let {
                if (it == MAV_TYPE.MAV_TYPE_QUADROTOR) {
                    GlobalScope.launch {
                        delay(1000)
                        val product = StarLinkClientManager.getInstance().getProduct(msdkInfoVm.msdkInfo.value!!.droneID)
                        product?.flyControllerManager?.getSn(object : CompletionCallback.ICompletionCallbackWith<String> {
                            override fun onFailure(geoaiError: GEOAIError?) {
                            }

                            override fun onResult(t: String?) {
                                val tmp = msdk_info_text_second?.text.toString()
                                msdk_info_text_second?.text = tmp + " | FC SN:${t}"
                            }
                        })
                    }
                } else if (it == MAV_TYPE.MAV_TYPE_ONBOARD_CONTROLLER) {
                    GlobalScope.launch {
                        delay(1000)
                        val product = StarLinkClientManager.getInstance().getProduct(msdkInfoVm.msdkInfo.value!!.droneID)
                        product?.crestManager?.getSn(object : CompletionCallback.ICompletionCallbackWith<String> {
                            override fun onFailure(geoaiError: GEOAIError?) {
                            }

                            override fun onResult(t: String?) {
                                val tmp = msdk_info_text_second?.text.toString()
                                msdk_info_text_second?.text = tmp + " | CREST SN:${t}"
                            }
                        })
                    }
                }
                else if (it == MAV_TYPE.MAV_TYPE_BATTERY) {
                    GlobalScope.launch {
                        delay(1000)
                        val product = StarLinkClientManager.getInstance().getProduct(msdkInfoVm.msdkInfo.value!!.droneID)
                        product?.batteryManager?.getSn(object : CompletionCallback.ICompletionCallbackWith<String> {
                            override fun onFailure(geoaiError: GEOAIError?) {
                            }

                            override fun onResult(t: String?) {
                                val tmp = msdk_info_text_second?.text.toString()
                                msdk_info_text_second?.text = tmp + " | BAT SN:${t}"
                            }
                        })
                    }
                }
            }
        }
    }
}