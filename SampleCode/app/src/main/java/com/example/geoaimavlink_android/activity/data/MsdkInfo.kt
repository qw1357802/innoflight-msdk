package com.example.geoaimavlink_android.activity.data

data class MsdkInfo(var sdkVersion: String = "-.-") {

    var isAircraftConnected: Boolean = false

    var sdkVersionNum = 0

    var jestonID: Long = -1

    var droneID: Int = -1
}