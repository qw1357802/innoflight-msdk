package com.example.geoaimavlink_android.activity

import android.app.Application
import com.geoai.mavlink.geoainet.base.mavlinkcore.controller.StarLinkClientManager
import com.geoai.module_apm.utils.AppPerformanceMonitor
import java.io.File

class MsdkApplication: Application() {

    override fun onCreate() {
        super.onCreate()

        StarLinkClientManager.getInstance().register(this, externalCacheDir?.path)

        val apmFile = File(externalCacheDir?.path ?: cacheDir.path, "monitor")

        AppPerformanceMonitor.instance.matrixConfig(
            isAnrTraceEnable = true, isFpsTraceEnable = true,
            isEvilMethodTraceEnable = true, isAppMethodBeatEnable = true
        )
            .trackerConfig(500, 100, 0.5f, 0.8f, 0.85f, 250 * 1024, 1)
            .initModule(this, apmFile, isTrackerEnable = true, isMatrixEnable = true)
    }
}