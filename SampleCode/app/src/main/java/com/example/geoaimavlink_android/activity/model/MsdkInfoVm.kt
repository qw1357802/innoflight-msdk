package com.example.geoaimavlink_android.activity.model

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.geoaimavlink_android.activity.data.MsdkInfo
import com.geoai.mavlink.geoainet.base.mavlinkcore.controller.StarLinkClientManager

class MsdkInfoVm : ViewModel() {

    val msdkInfo = MutableLiveData<MsdkInfo>()
    val component = MutableLiveData<Int>()

    init {
        msdkInfo.value = MsdkInfo()

        refreshMsdkInfo()
    }

    private fun refreshMsdkInfo() {
        msdkInfo.postValue(msdkInfo.value)
    }

    fun updateMsdkInfo(droneID: Int) {
        msdkInfo.value?.isAircraftConnected = droneID >= 0
        msdkInfo.value?.droneID = droneID
        StarLinkClientManager.getInstance().getProduct(droneID)?.let { productManager ->
            val systemManager = productManager.systemManager
            val sdkVersionInfo = systemManager.sdkVersionInfo.firstNotNullOfOrNull { it.value }
            val sdkVersionNum = systemManager.sdkVersionInfo.firstNotNullOfOrNull { it.key }
            msdkInfo.value?.sdkVersion = sdkVersionInfo ?: "-.-"
            msdkInfo.value?.sdkVersionNum = sdkVersionNum ?: 0
            refreshMsdkInfo()
        }?:refreshMsdkInfo()
    }

    fun updateMsdkComponentTypeConnected(componentId: Int) {
        component.value = componentId
    }
}