package com.example.geoaimavlink_android.activity.data

enum class ChannelType(val key: String) {

    /**
     * 飞控
     */
    CHANNEL_TYPE_FLIGHT_CONTROL("FlyController"),
    /**
     * 无线电-大鱼图传
     */
    CHANNEL_TYPE_AIRLINK_BIGFISH("AirLink-BigFish"),
    /**
     * 无线电-P301
     */
    CHANNEL_TYPE_AIRLINK_P301("AirLink-P301"),
    /**
     * 4G
     */
    CHANNEL_TYPE_CELLULAR("Cellular"),
    /**
     * 电池
     */
    CHANNEL_TYPE_BATTERY("Battery"),
    /**
     * SS125
     */
    CHANNEL_TYPE_PAYLOAD_SS125("SS125"),
    /**
     * 相机Y2
     */
    CHANNEL_TYPE_CAMERA_Y2("Camera-Y2"),
    /**
     * 相机Y3
     */
    CHANNEL_TYPE_CAMERA_Y3("Camera-Y3"),
    /**
     * 遥控K3Q
     */
    RC_K3Q("RC-K3Q"),
    /**
     * 遥控K3R
     */
    RC_K3R("RC-K3R"),
    /**
     * 云冠
     */
    CHANNEL_TYPE_CREST("Crest"),
    /**
     * 云台
     */
    CHANNEL_TYPE_GIMBAL("Gimbal"),
    /**
     * 媒体
     */
    CHANNEL_TYPE_MEDIA("Media"),
    /**
     * 任务
     */
    CHANNEL_TYPE_MISSION("Mission"),
    /**
     * 避障
     */
    CHANNEL_TYPE_OBSTACLE("Obstacle"),
    /**
     * RTK
     */
    CHANNEL_TYPE_RTK("RTK"),
    /**
     * 系统
     */
    CHANNEL_TYPE_SYSTEM("System"),
    /**
     * 禁飞区
     */
    CHANNEL_TYPE_BANFLY("BanFly");

    private val value: String = name
    override fun toString(): String {
        return value
    }

}