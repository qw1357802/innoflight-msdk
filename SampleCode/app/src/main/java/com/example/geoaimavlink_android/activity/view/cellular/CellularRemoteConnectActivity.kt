package com.example.geoaimavlink_android.activity.view.cellular

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.geoaimavlink_android.R
//import com.geoai.mavlink.geoainet.agora.AgoraAgent
import com.geoai.mavlink.geoainet.agora.delegate.RtcAgent
import com.geoai.mavlink.geoainet.airlink.AirlinkManager
import com.geoai.mavlink.geoainet.airlink.enums.CellularEnhancedTransmissionType
import com.geoai.mavlink.util.MMKVConfigUtil
import io.agora.media.RtcTokenBuilder2
import io.agora.rtc2.video.VideoCanvas
import kotlinx.android.synthetic.main.activity_cellular_remote_connect.btn_connect
import kotlinx.android.synthetic.main.activity_cellular_remote_connect.cellular_surface
import kotlinx.android.synthetic.main.activity_cellular_remote_connect.edt_crest_id

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-06-25 14:17.
 * @Description : 别用，云冠写死了接收终端
 */
class CellularRemoteConnectActivity: AppCompatActivity() {

    companion object {
        private const val APP_ID = "79d2a9e1c46944b682ec9f160e1fa2e2"
        private const val REC_TOPIC = "air_to_gnd"
        private const val SEND_TOPIC = "gnd_to_air"
        private const val RTM_UUID = "32002"
        private const val RTC_UUID = "32001"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cellular_remote_connect)
    }

    override fun onStart() {
        super.onStart()

        Toast.makeText(this, "别用，云冠写死了接收终端", Toast.LENGTH_SHORT).show()

        finish()

        btn_connect?.setOnClickListener {

            MMKVConfigUtil.getInstance().cellularAppID = APP_ID

            MMKVConfigUtil.getInstance().cellularRTMUUID = RTM_UUID.toInt()
            MMKVConfigUtil.getInstance().cellularRTCUUID = RTC_UUID.toInt()

            MMKVConfigUtil.getInstance().setCellularSenderTopic(REC_TOPIC)
            MMKVConfigUtil.getInstance().setCellularReceivedTopic(SEND_TOPIC)

            edt_crest_id?.text?.toString()?.let {

                MMKVConfigUtil.getInstance().cellularChannel = it + "_rtc_chan"

                val rtmToken = buildToken(it + "_rtm_chan", RTM_UUID, 3600)
                val rtcToken = buildToken(it + "_rtc_chan", RTC_UUID, 3600)

                MMKVConfigUtil.getInstance().cellularRTCToken = rtcToken
                MMKVConfigUtil.getInstance().cellularRTMToken = rtmToken

                AirlinkManager.getInstance().cellular?.setCellularEnhancedTransmissionType(CellularEnhancedTransmissionType.CELLULAR_ONLY, null)

//                AgoraAgent.getInstance().rtcAgent.setRtcUserJoinedListener(object : RtcAgent.OnRtcUserJoinListener{
//                    override fun onJoined(uid: Int) {
//                        Log.i("Ronny", "onJoined : $uid")
//                        AgoraAgent.getInstance().rtcEngine?.setupRemoteVideo(VideoCanvas(cellular_surface, VideoCanvas.RENDER_MODE_HIDDEN, uid))
//                    }
//
//                    override fun onLeave() {
//                        Log.i("Ronny", "onLeave")
//                    }
//                })
            }
        }
    }

    private fun buildToken(channelName: String, uid: String, expiredTs: Long): String {
        val rtcTokenBuilder2 = RtcTokenBuilder2()
        return rtcTokenBuilder2.buildTokenWithUid(
            APP_ID,
            "1cc9291d9b8a4acb921d682c0af199ae",
            channelName,
            uid.toInt(),
            RtcTokenBuilder2.Role.ROLE_SUBSCRIBER,
            expiredTs.toInt(),
            expiredTs.toInt()
        )
    }
}