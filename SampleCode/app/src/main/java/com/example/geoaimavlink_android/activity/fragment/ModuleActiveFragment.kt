package com.example.geoaimavlink_android.activity.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.geoaimavlink_android.R
import com.geoai.mavlink.geoainet.airlink.AirlinkManager
import com.geoai.mavlink.geoainet.airlink.module.rc.K3RModule
import com.geoai.mavlink.geoainet.base.mavlinkcore.engine.GEOAIError
import com.geoai.mavlink.geoainet.base.mavlinkcore.interfaces.CompletionCallback
import kotlinx.android.synthetic.main.frag_module_active.btn_obtain_module_active
import kotlinx.android.synthetic.main.frag_module_active.tv_module_rc_active
import kotlinx.android.synthetic.main.frag_module_active.tv_module_rc_connected
import kotlinx.android.synthetic.main.frag_module_active.tv_module_rc_sn
import android.util.Pair
import com.geoai.mavlink.geoainet.base.mavlinkcore.controller.ActivationManager
import com.geoai.mavlink.geoainet.base.mavlinkcore.controller.StarLinkClientManager
import kotlinx.android.synthetic.main.frag_module_active.btn_module_camera_active
import kotlinx.android.synthetic.main.frag_module_active.btn_module_product_active
import kotlinx.android.synthetic.main.frag_module_active.btn_module_rc_active
import kotlinx.android.synthetic.main.frag_module_active.tv_module_bat_connected
import kotlinx.android.synthetic.main.frag_module_active.tv_module_bat_sn
import kotlinx.android.synthetic.main.frag_module_active.tv_module_camera_active
import kotlinx.android.synthetic.main.frag_module_active.tv_module_camera_connected
import kotlinx.android.synthetic.main.frag_module_active.tv_module_camera_sn
import kotlinx.android.synthetic.main.frag_module_active.tv_module_crest_connected
import kotlinx.android.synthetic.main.frag_module_active.tv_module_crest_sn
import kotlinx.android.synthetic.main.frag_module_active.tv_module_fc_connected
import kotlinx.android.synthetic.main.frag_module_active.tv_module_fc_sn
import kotlinx.android.synthetic.main.frag_module_active.tv_module_product_active
import kotlinx.android.synthetic.main.frag_module_active.tv_module_product_connected
import kotlinx.android.synthetic.main.frag_module_active.tv_module_product_sn
import java.text.SimpleDateFormat
import java.util.Locale

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-07-02 14:50.
 * @Description :
 */
class ModuleActiveFragment: BaseFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return View.inflate(context, R.layout.frag_module_active, null)
    }

    override fun onStart() {
        super.onStart()

        btn_obtain_module_active?.setOnClickListener {
            //这里要获取模块激活状态
            getRcActive()
            getProductActive()
            getCameraActive()
            getFcSn()
            getBatSn()
            getCrestSn()
        }

        btn_module_rc_active?.setOnClickListener {
            StarLinkClientManager.getInstance().activationManager.active(
                0,
                ActivationManager.ActiveModule.RC,
                System.currentTimeMillis(),
                object : CompletionCallback.ICompletionCallbackWith<Boolean> {
                    override fun onFailure(geoaiError: GEOAIError?) {
                        showToast("激活遥控")
                    }

                    override fun onResult(t: Boolean?) {
                        showToast("激活遥控 : " + t!!)
                    }
                })
        }

        btn_module_product_active?.setOnClickListener {
            StarLinkClientManager.getInstance().activationManager.active(
                getProduct()!!.flyControllerManager.getComponentConst().targetSysID,
                ActivationManager.ActiveModule.PRODUCT,
                System.currentTimeMillis(),
                object : CompletionCallback.ICompletionCallbackWith<Boolean> {
                override fun onFailure(geoaiError: GEOAIError?) {
                    showToast("激活整机失败")
                }

                override fun onResult(t: Boolean?) {
                    showToast("激活整机 : " + t!!)
                }
            })
        }

        btn_module_camera_active?.setOnClickListener {
            StarLinkClientManager.getInstance().activationManager.active(
                getProduct()!!.flyControllerManager.getComponentConst().targetSysID,
                ActivationManager.ActiveModule.CAMERA,
                System.currentTimeMillis(),
                object : CompletionCallback.ICompletionCallbackWith<Boolean> {
                    override fun onFailure(geoaiError: GEOAIError?) {
                        showToast("激活相机失败")
                    }

                    override fun onResult(t: Boolean?) {
                        showToast("激活相机 : " + t!!)
                    }
                })
        }
    }

    private fun getRcActive() {
        val rc = AirlinkManager.getInstance().rc
        if (rc != null && rc is K3RModule) {
            tv_module_rc_connected?.text = "遥控器已连接"
        } else {
            tv_module_rc_connected?.text = "遥控器未连接"
        }

        StarLinkClientManager.getInstance().activationManager.getModuleSn(0, ActivationManager.ActiveModule.RC, object : CompletionCallback.ICompletionCallbackWith<String> {
            override fun onFailure(geoaiError: GEOAIError?) {
                tv_module_rc_sn?.text = "获取失败"
            }

            override fun onResult(t: String?) {
                tv_module_rc_sn?.text = t
            }
        })

        StarLinkClientManager.getInstance().activationManager.getActiveTimeStamp(0, ActivationManager.ActiveModule.RC, object : CompletionCallback.ICompletionCallbackWith<Long> {
            override fun onFailure(geoaiError: GEOAIError?) {
                tv_module_rc_active?.text = "获取失败"
            }

            override fun onResult(t: Long) {
                if (t > 0) {
                    //格式化时间戳
                    val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
                    tv_module_rc_active?.text = "${sdf.format(t)}"
                } else {
                    tv_module_rc_active?.text = "整机未激活"
                }
            }
        })
    }

    private fun getProductActive() {
        getProduct()?.let { product ->
            if (product.isConnected) {
                tv_module_product_connected?.text = "整机已连接"
            } else {
                tv_module_product_connected?.text = "整机未连接"
            }
        }?: kotlin.run {
            tv_module_product_connected?.text = "整机未连接"
        }

        val sysID = getProduct()?.flyControllerManager?.getComponentConst()?.targetSysID ?: 1

        StarLinkClientManager.getInstance().activationManager.getModuleSn(sysID, ActivationManager.ActiveModule.PRODUCT, object : CompletionCallback.ICompletionCallbackWith<String> {
            override fun onFailure(geoaiError: GEOAIError?) {
                tv_module_product_sn?.text = "获取失败"
            }

            override fun onResult(t: String?) {
                tv_module_product_sn?.text = t
            }
        })

        StarLinkClientManager.getInstance().activationManager.getActiveTimeStamp(sysID, ActivationManager.ActiveModule.PRODUCT, object : CompletionCallback.ICompletionCallbackWith<Long> {
            override fun onFailure(geoaiError: GEOAIError?) {
                tv_module_product_active?.text = "获取失败"
            }

            override fun onResult(t: Long) {
                if (t > 0) {
                    //格式化时间戳
                    val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
                    tv_module_product_active?.text = "${sdf.format(t)}"
                } else {
                    tv_module_product_active?.text = "整机未激活"
                }
            }
        })
    }

    private fun getCameraActive() {
        getProduct()?.payloadManager?.camera?.let {
            tv_module_camera_connected?.text = "相机已连接"
        }?: kotlin.run {
            tv_module_camera_connected?.text = "相机未连接"
        }

        val sysID = getProduct()?.flyControllerManager?.getComponentConst()?.targetSysID ?: 1

        StarLinkClientManager.getInstance().activationManager.getModuleSn(sysID, ActivationManager.ActiveModule.CAMERA, object : CompletionCallback.ICompletionCallbackWith<String> {
            override fun onFailure(geoaiError: GEOAIError?) {
                tv_module_camera_sn?.text = "获取失败"
            }

            override fun onResult(t: String?) {
                tv_module_camera_sn?.text = t
            }
        })

        StarLinkClientManager.getInstance().activationManager.getActiveTimeStamp(sysID, ActivationManager.ActiveModule.CAMERA, object : CompletionCallback.ICompletionCallbackWith<Long> {
            override fun onFailure(geoaiError: GEOAIError?) {
                tv_module_camera_active?.text = "获取失败"
            }

            override fun onResult(t: Long) {
                if (t > 0) {
                    //格式化时间戳
                    val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
                    tv_module_camera_active?.text = "${sdf.format(t)}"
                } else {
                    tv_module_camera_active?.text = "整机未激活"
                }
            }
        })
    }

    private fun getFcSn() {
        val sysID = getProduct()?.flyControllerManager?.getComponentConst()?.targetSysID ?: 1

        val connect = getProduct()?.flyControllerManager?.isProductConnected?:false

        tv_module_fc_connected?.text = if (connect) "飞控已连接" else "飞控未连接"

        StarLinkClientManager.getInstance().activationManager.getModuleSn(sysID, ActivationManager.ActiveModule.FC, object : CompletionCallback.ICompletionCallbackWith<String> {
            override fun onFailure(geoaiError: GEOAIError?) {
                tv_module_fc_sn?.text = "获取失败"
            }

            override fun onResult(t: String?) {
                tv_module_fc_sn?.text = t
            }
        })
    }

    private fun getBatSn() {
        val sysID = getProduct()?.flyControllerManager?.getComponentConst()?.targetSysID ?: 1

        val connect = getProduct()?.batteryManager?.isProductConnected?:false

        tv_module_bat_connected?.text = if (connect) "电池已连接" else "电池未连接"

        StarLinkClientManager.getInstance().activationManager.getModuleSn(sysID, ActivationManager.ActiveModule.BATTERY, object : CompletionCallback.ICompletionCallbackWith<String> {
            override fun onFailure(geoaiError: GEOAIError?) {
                tv_module_bat_sn?.text = "获取失败"
            }

            override fun onResult(t: String?) {
                tv_module_bat_sn?.text = t
            }
        })
    }

    private fun getCrestSn() {
        val sysID = getProduct()?.crestManager?.getComponentConst()?.targetSysID ?: 1

        val connect = getProduct()?.crestManager?.isProductConnected?:false

        tv_module_crest_connected?.text = if (connect) "机载电脑已连接" else "机载电脑未连接"

        StarLinkClientManager.getInstance().activationManager.getModuleSn(sysID, ActivationManager.ActiveModule.CREST, object : CompletionCallback.ICompletionCallbackWith<String> {
            override fun onFailure(geoaiError: GEOAIError?) {
                tv_module_crest_sn?.text = "获取失败"
            }

            override fun onResult(t: String?) {
                tv_module_crest_sn?.text = t
            }
        })
    }
}