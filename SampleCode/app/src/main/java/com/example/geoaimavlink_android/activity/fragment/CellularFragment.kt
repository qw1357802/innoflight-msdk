package com.example.geoaimavlink_android.activity.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.geoaimavlink_android.R
import com.geoai.mavlink.geoainet.airlink.AirlinkManager
import com.geoai.mavlink.geoainet.airlink.enums.CellularEnhancedTransmissionType
import com.geoai.mavlink.geoainet.airlink.module.dsp.P301DModule
import com.geoai.mavlink.geoainet.base.mavlinkcore.engine.GEOAIError
import com.geoai.mavlink.geoainet.base.mavlinkcore.interfaces.CompletionCallback
import com.geoai.video.YFV5Params
import com.geoai.widget.constant.AspectRatio
import com.geoai.widget.constant.CodecMode
import com.geoai.widget.constant.PlayNode
import com.geoai.widget.constant.RenderMode
import com.geoai.widget.setting.YFVParams
import kotlinx.android.synthetic.main.frag_cellular.btn_cellular_only
import kotlinx.android.synthetic.main.frag_cellular.btn_current_connection_mode
import kotlinx.android.synthetic.main.frag_cellular.btn_get_pair_code
import kotlinx.android.synthetic.main.frag_cellular.btn_reboot
import kotlinx.android.synthetic.main.frag_cellular.btn_set_max_power
import kotlinx.android.synthetic.main.frag_cellular.btn_set_min_power
import kotlinx.android.synthetic.main.frag_cellular.btn_set_pair_code
import kotlinx.android.synthetic.main.frag_cellular.btn_wireless_cellular
import kotlinx.android.synthetic.main.frag_cellular.btn_wireless_only
import kotlinx.android.synthetic.main.frag_cellular.edt_pair_code
import kotlinx.android.synthetic.main.frag_cellular.surface
import kotlinx.android.synthetic.main.frag_cellular.tv_product_connect_type
import kotlinx.android.synthetic.main.frag_cellular.tv_video_quality
import kotlinx.android.synthetic.main.frag_cellular.tv_wireless_signal

class CellularFragment : BaseFragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return View.inflate(context, R.layout.frag_cellular, null)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        initSDK()
    }

    private fun initPlayer() {
        surface.initPlayer(YFVParams.Builder()
            .setPlayNode(PlayNode.MAIN_CAM)
            .setVideoCodecMode(CodecMode.HARDWARE)
            .setAspectRatioMode(AspectRatio.AR_16_9_FIT_PARENT)
            .setSurfaceMode(RenderMode.RENDER_SURFACE_VIEW)
            .build())
        surface.startPlayer()
    }

    private fun initSDK() {
        AirlinkManager.getInstance().cellular?.setVideoChannelQualityListener { info ->
            requireActivity().runOnUiThread {
                tv_video_quality?.text = "丢包率：${info.packetLoss}\r\n" +
                        "接收码率：${info.receivedBitrate}\r\n" +
                        "帧率：${info.outputFrameRate}\r\n" +
                        "画面：h${info.remoteHeight};w${info.remoteWidth}"
            }
        }

        msdkInfoVm.msdkInfo.observe(viewLifecycleOwner) {
            if (it.isAircraftConnected) {
                AirlinkManager.getInstance().dsp?.setWirelessSignalQualityListener {wireless->
                    tv_wireless_signal?.text = "【上行：${wireless.uplinkSignalLevel.name}】" + "【下行：${wireless.downlinkSignalLevel.name}】"
                }
                getProduct()?.setProductConnectionListener {status ->
                    requireActivity().runOnUiThread {
                        tv_product_connect_type?.text = "【无线电：${status?.udpConnectionType?.name}】" + "【4G：${status?.cellularConnectionType?.name}】"
                    }
                }

                initPlayer()
            } else {
                AirlinkManager.getInstance().dsp?.setWirelessSignalQualityListener(null)
            }
        }
    }

    private fun initView() {
        btn_wireless_only.setOnClickListener {
            AirlinkManager.getInstance()?.cellular?.setCellularEnhancedTransmissionType(CellularEnhancedTransmissionType.WIRELESS_ONLY) {
                showToast(it?.description?:"success")
            }
        }
        btn_cellular_only.setOnClickListener {
            AirlinkManager.getInstance()?.cellular?.setCellularEnhancedTransmissionType(CellularEnhancedTransmissionType.CELLULAR_ONLY) {
                showToast(it?.description?:"success")
            }
        }
        btn_wireless_cellular.setOnClickListener {
            AirlinkManager.getInstance()?.cellular?.setCellularEnhancedTransmissionType(CellularEnhancedTransmissionType.WIRELESS_CELLULAR) {
                showToast(it?.description?:"success")
            }
        }
        btn_current_connection_mode.setOnClickListener {
            AirlinkManager.getInstance()?.cellular?.getCellularEnhancedTransmissionType(object : CompletionCallback.ICompletionCallbackWith<CellularEnhancedTransmissionType>{
                override fun onFailure(geoaiError: GEOAIError?) {
                    showToast("err: ${geoaiError?.description}")
                }

                override fun onResult(t: CellularEnhancedTransmissionType?) {
                    btn_current_connection_mode.text = "当前模式: " + t?.name
                }
            })
        }
        btn_get_pair_code.setOnClickListener {
//            AirlinkManager.getInstance().dsp.let { it as P301DModule
//                it.getPairCode(object : CompletionCallback.ICompletionCallbackWith<String>{
//                    override fun onFailure(geoaiError: GEOAIError?) {
//                    }
//
//                    override fun onResult(t: String) {
//                        edt_pair_code.setText(t)
//                        showToast(t)
//                    }
//                })
//            }
        }
        btn_set_pair_code.setOnClickListener {
//            AirlinkManager.getInstance()?.dsp?.let { it as P301DModule
//                val pairCode = edt_pair_code.text.toString()
//                it.setPairCode(pairCode) { err ->
//                    showToast(err?.description?:"success")
//                }
//            }
        }
        btn_set_pair_code.setOnLongClickListener {
//            AirlinkManager.getInstance()?.dsp?.let { it as P301DModule
//                val pairCode = "12345678:B4B5B6B7:A0A1A2A3:A4A5A6A7"
//                it.setPairCode(pairCode) { err ->
//                    showToast(err?.description?:"success")
//                }
//            }
            true
        }
        btn_set_min_power.setOnClickListener {
//            AirlinkManager.getInstance().dsp.let { it as P301DModule
//                it.setWirelessPower("fix:1,1") { err ->
//                    showToast(err?.description?:"success")
//                }
//            }
        }
        btn_set_max_power.setOnClickListener {
//            AirlinkManager.getInstance().dsp.let { it as P301DModule
//                it.setWirelessPower("fix:28,25") { err ->
//                    showToast(err?.description?:"success")
//                }
//            }
        }
        btn_reboot.setOnClickListener {
//            AirlinkManager.getInstance().dsp.let { it as P301DModule
//                it.rebootWireless { err ->
//                    showToast(err?.description?:"success")
//                }
//            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }
}