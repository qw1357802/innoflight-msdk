package com.example.geoaimavlink_android.activity.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.geoaimavlink_android.R
import com.geoai.module.common.DateHelperUtils
import com.geoai.widget.constant.AspectRatio
import com.geoai.widget.constant.CodecMode
import com.geoai.widget.constant.PlayNode
import com.geoai.widget.constant.PlayerMode
import com.geoai.widget.constant.RenderMode
import com.geoai.widget.setting.YFVParams
import kotlinx.android.synthetic.main.frag_yf_mediaplayer.btn_primary_init
import kotlinx.android.synthetic.main.frag_yf_mediaplayer.btn_primary_record_disable
import kotlinx.android.synthetic.main.frag_yf_mediaplayer.btn_primary_record_enable
import kotlinx.android.synthetic.main.frag_yf_mediaplayer.btn_primary_start
import kotlinx.android.synthetic.main.frag_yf_mediaplayer.btn_primary_stop
import kotlinx.android.synthetic.main.frag_yf_mediaplayer.btn_secondary_init
import kotlinx.android.synthetic.main.frag_yf_mediaplayer.btn_secondary_record_disable
import kotlinx.android.synthetic.main.frag_yf_mediaplayer.btn_secondary_record_enable
import kotlinx.android.synthetic.main.frag_yf_mediaplayer.btn_secondary_start
import kotlinx.android.synthetic.main.frag_yf_mediaplayer.btn_secondary_stop
import kotlinx.android.synthetic.main.frag_yf_mediaplayer.primary_video_channel_fragment
import kotlinx.android.synthetic.main.frag_yf_mediaplayer.secondary_video_channel_fragment
import kotlinx.android.synthetic.main.frag_yf_mediaplayer.tv_primary_stream
import kotlinx.android.synthetic.main.frag_yf_mediaplayer.tv_secondary_stream
import java.io.File
import java.lang.StringBuilder

/**
 * @Author      : Ronny
 * @Email       : Ronny_xie@hotmail.com
 * @Date        : on 2024-05-21 14:27.
 * @Description :
 */
class YfMediaPlayerFragment : BaseFragment() {

    //rtsp://admin:3333qqqq@192.168.5.3:554/Streaming/Channels/101

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.frag_yf_mediaplayer, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()
    }

    private fun initView() {
        btn_primary_init.setOnClickListener {
            primary_video_channel_fragment.initPlayer(YFVParams.Builder()
                .setPlayerMode(PlayerMode.IJKPLAYER)
                .setSurfaceMode(RenderMode.RENDER_TEXTURE_VIEW)
                .setVideoCodecMode(CodecMode.HARDWARE)
                .setAspectRatioMode(AspectRatio.AR_ASPECT_FIT_PARENT)
                .setPlayNode(PlayNode.OTHER)
                .setCustomPlayUrl("rtsp://192.168.144.20:557/stream_chn0.h264")
                .build())

            primary_video_channel_fragment.setMediaStatisticsInfoListener {
                val sb = StringBuilder()
                sb.append("fps: ${it.vfps}\n")
                sb.append("dps: ${it.vdps}\n")
                sb.append("frame_rate: ${it.frame_rate}\n")
                sb.append("idr: ${it.idr_frame}\n")
                sb.append("avdelay: ${it.avdelay}\n")
                sb.append("avdiff: ${it.avdiff}\n")
                tv_primary_stream?.text = sb.toString()
            }
        }

        btn_primary_start.setOnClickListener {
            primary_video_channel_fragment.startPlayer()
        }

        btn_primary_stop.setOnClickListener {
            primary_video_channel_fragment.release()
        }

        btn_secondary_init.setOnClickListener {
            secondary_video_channel_fragment.initPlayer(YFVParams.Builder()
                .setSurfaceMode(RenderMode.RENDER_TEXTURE_VIEW)
                .setVideoCodecMode(CodecMode.HARDWARE)
                .setAspectRatioMode(AspectRatio.AR_ASPECT_FIT_PARENT)
                .setPlayNode(PlayNode.OTHER)
                .setCustomPlayUrl("rtsp://192.168.144.20:558/stream_chn0.h264")
                .setLogOutputFile(context?.externalCacheDir!!.path + File.separator + "player-" + DateHelperUtils.getDateSeries() + ".log")
                .setRecordOutputFile(context?.externalCacheDir!!.path + File.separator + "record-" + DateHelperUtils.getDateSeries() + ".real")
                .build())

            secondary_video_channel_fragment.setMediaStatisticsInfoListener {
                val sb = StringBuilder()
                sb.append("fps: ${it.vfps}\n")
                sb.append("dps: ${it.vdps}\n")
                sb.append("frame_rate: ${it.frame_rate}\n")
                sb.append("idr: ${it.idr_frame}\n")
                sb.append("avdelay: ${it.avdelay}\n")
                sb.append("avdiff: ${it.avdiff}\n")
                tv_secondary_stream?.text = sb.toString()
            }
        }

        btn_secondary_record_enable.setOnClickListener {
            secondary_video_channel_fragment.startRecord()
        }

        btn_secondary_record_disable.setOnClickListener {
            secondary_video_channel_fragment.stopRecord()
        }

        btn_primary_record_enable.setOnClickListener {
            primary_video_channel_fragment.startRecord()
        }

        btn_primary_record_disable.setOnClickListener {
            primary_video_channel_fragment.stopRecord()
        }

        btn_secondary_start.setOnClickListener {
            secondary_video_channel_fragment.startPlayer()
        }

        btn_secondary_stop.setOnClickListener {
            secondary_video_channel_fragment.release()
        }
    }
}