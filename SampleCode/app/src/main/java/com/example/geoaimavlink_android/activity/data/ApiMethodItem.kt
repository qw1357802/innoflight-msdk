package com.example.geoaimavlink_android.activity.data

import java.lang.reflect.Method

data class ApiMethodItem(val name: String, val method: Method) {
    var isSelected: Boolean = false
}
