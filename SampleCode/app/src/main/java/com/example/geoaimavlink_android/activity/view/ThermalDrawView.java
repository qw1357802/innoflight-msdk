package com.example.geoaimavlink_android.activity.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.FloatRange;

public class ThermalDrawView extends View {
    private Paint redPaintSpot;
    private Paint bluePaintSpot;
    private Paint paintArea;

    public ThermalDrawView(Context context) {
        super(context);
        init();
    }

    public ThermalDrawView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ThermalDrawView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        redPaintSpot = new Paint();
        redPaintSpot.setColor(Color.RED);
        redPaintSpot.setStyle(Paint.Style.FILL);

        bluePaintSpot = new Paint();
        bluePaintSpot.setColor(Color.BLUE);
        bluePaintSpot.setStyle(Paint.Style.FILL);

        paintArea = new Paint();
        paintArea.setColor(Color.GREEN);
        paintArea.setStyle(Paint.Style.STROKE);
    }

    private float redPointX = -1;
    private float redPointY = -1;
    private float bluePointX = -1;
    private float bluePointY = -1;

    public float areaStartX = -1;
    public float areaStartY = -1;
    public float areaEndX = -1;
    public float areaEndY = -1;

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (redPointX != -1 && redPointY != -1) {
            canvas.drawCircle(getWidth() * redPointX, getHeight() * redPointY, 10, redPaintSpot);
        }
        if (bluePointX != -1 && bluePointY != -1) {
            canvas.drawCircle(getWidth() * bluePointX, getHeight() * bluePointY, 10, bluePaintSpot);
        }
        if (areaStartX != -1 && areaStartY != -1 && areaEndX != -1 && areaEndY != -1) {
            canvas.drawRect(areaStartX, areaStartY, areaEndX, areaEndY, paintArea);
        }
    }

    public void setRedPoint(@FloatRange(from = 0, to = 1) float x, @FloatRange(from = 0, to = 1) float y) {
        this.redPointX = x;
        this.redPointY = y;
        invalidate();
    }

    public void setBluePoint(@FloatRange(from = 0, to = 1) float x, @FloatRange(from = 0, to = 1) float y) {
        this.bluePointX = x;
        this.bluePointY = y;
        invalidate();
    }

    public void setStartX(float x, float y) {
        this.areaStartX = x;
        this.areaStartY = y;
    }
    public void setEndX(float x, float y) {
        this.areaEndX = x;
        this.areaEndY = y;
        invalidate();
    }

    public void clear() {
        this.redPointX = -1;
        this.redPointY = -1;

        this.areaStartX = -1;
        this.areaStartY = -1;
        this.areaEndX = -1;
        this.areaEndY = -1;
    }
}
