package com.example.geoaimavlink_android.activity.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.geoaimavlink_android.R
import com.geoai.mavlink.geoainet.base.mavlinkcore.controller.StarLinkClientManager
import com.geoai.mavlink.geoainet.base.mavlinkcore.engine.GEOAIError
import com.geoai.mavlink.geoainet.base.mavlinkcore.interfaces.CompletionCallback.ICompletionCallbackWith
import com.geoai.mavlink.geoainet.obstacle.enums.ObstacleAvoidanceType
import kotlinx.android.synthetic.main.frag_obstacle.btn_refresh_enable
import kotlinx.android.synthetic.main.frag_obstacle.btn_set_obstacle_distance
import kotlinx.android.synthetic.main.frag_obstacle.edt_obstacle_distance
import kotlinx.android.synthetic.main.frag_obstacle.sw_obstacle_enable
import kotlinx.android.synthetic.main.frag_obstacle.tv_obstacle_data

class ObstacleFragment: BaseFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return View.inflate(context, R.layout.frag_obstacle, null)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initSDK()
        initView()
    }

    private fun initView() {
        btn_refresh_enable.setOnClickListener {
            getProduct()?.obstacleManager?.getObstacleAvoidanceEnable(object :
                ICompletionCallbackWith<Boolean> {
                override fun onFailure(geoaiError: GEOAIError?) {
                    mainHandler.post { Toast.makeText(context, geoaiError?.description?:"error1.", Toast.LENGTH_SHORT).show() }
                }

                override fun onResult(t: Boolean) {
                    mainHandler.post { sw_obstacle_enable?.isChecked = t }
                }
            })
        }
        sw_obstacle_enable.setOnCheckedChangeListener { buttonView, isChecked ->
            getProduct()?.obstacleManager?.setObstacleAvoidanceEnable(isChecked) {
                showToast(it?.description?:"success")
            }
        }
        btn_set_obstacle_distance.setOnClickListener {
            val ret = edt_obstacle_distance?.text?.toString()?.toFloat()?:0.0f
            getProduct()?.obstacleManager?.setObstacleAvoidanceBrakingDistance(ret) {
                showToast(it?.description?:"success")
            }
        }
    }

    private fun initSDK() {
        msdkInfoVm.msdkInfo.observe(viewLifecycleOwner) {
            if (it.isAircraftConnected.not()) return@observe

            StarLinkClientManager.getInstance().getProduct(it.droneID)?.obstacleManager?.let { obstacleManager ->

                obstacleManager.getObstacleAvoidanceEnable(object :
                    ICompletionCallbackWith<Boolean> {
                    override fun onFailure(geoaiError: GEOAIError?) {
                        mainHandler.post { Toast.makeText(context, geoaiError?.description?:"error1.", Toast.LENGTH_SHORT).show() }
                    }

                    override fun onResult(t: Boolean) {
                        mainHandler.post { sw_obstacle_enable.isChecked = t }
                    }
                })

                obstacleManager.getObstacleAvoidanceBrakingDistance(object : ICompletionCallbackWith<Float>{
                    override fun onFailure(geoaiError: GEOAIError?) {
                        mainHandler.post { Toast.makeText(context, geoaiError?.description?:"error2.", Toast.LENGTH_SHORT).show() }
                    }

                    override fun onResult(t: Float) {
                        mainHandler.post { edt_obstacle_distance?.setText(t.toString()) }
                    }
                })

                obstacleManager.getObstacleAvoidanceType(object : ICompletionCallbackWith<ObstacleAvoidanceType>{
                    override fun onFailure(geoaiError: GEOAIError?) {
                        mainHandler.post { Toast.makeText(requireActivity(), geoaiError?.description?:"error3.", Toast.LENGTH_SHORT).show() }
                    }

                    override fun onResult(t: ObstacleAvoidanceType?) {
                    }
                })

                obstacleManager.setObstacleDataListener {info ->
                    tv_obstacle_data?.text = format(gson.toJson(info))
                }
            }
        }
    }

    override fun onDestroyView() {
        msdkInfoVm.msdkInfo.removeObservers(viewLifecycleOwner)
        super.onDestroyView()
    }
}