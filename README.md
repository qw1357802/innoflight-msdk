# InnoFlight-MSDK
![logo](http://ronny-2023-image.oss-cn-guangzhou.aliyuncs.com/img/logo.png)
[![license](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitee.com/qw1357802/innoflight-msdk/blob/master/LICENSE) ![Website](https://img.shields.io/website?url=http%3A%2F%2Fronnyxie.gitee.io%2Fgeoai_msdk_docs) ![Website](https://img.shields.io/website?url=http%3A%2F%2Fronnyxie.gitee.io%2Fgeoai_msdk_docs&label=maven)
 [![msdk-version](https://img.shields.io/nexus/geoai-release/com.geoai.mavlink.msdk-provided/mavsdk?server=http%3A%2F%2F121.37.203.145%3A8081%2F&label=MSDK
)]()  

| 名称   | 依赖源                                                                        | 版本信息 |
|------|----------------------------------------------------------------------------|------|
| 主工程  | implementation("com.geoai.mavlink.msdk-provided:mavsdk:{version}")         |  ![msdk-version](https://img.shields.io/nexus/geoai-release/com.geoai.mavlink.msdk-provided/mavsdk?server=http%3A%2F%2F121.37.203.145%3A8081%2F&label=MSDK)  ![Sonatype Nexus (Snapshots)](https://img.shields.io/nexus/s/com.geoai.mavlink.msdk-provided/mavsdk?server=http%3A%2F%2F121.37.203.145%3A8081%2F&label=MSDK-snapshot)  |
| 依赖库(*)  | implementation("com.geoai.mavlink.common:mavsdk:{version}")                |  ![Sonatype Nexus (Releases)](https://img.shields.io/nexus/r/com.geoai.mavlink.common/mavsdk?server=http%3A%2F%2F121.37.203.145%3A8081%2F&nexusVersion=3&label=common)|
| 协议层(*)  | implementation("com.geoai.mavlink.protocol:mavsdk:{version}")              |![Sonatype Nexus (Releases)](https://img.shields.io/nexus/r/com.geoai.mavlink.protocol/mavsdk?server=http%3A%2F%2F121.37.203.145%3A8081%2F&nexusVersion=3&label=protocol)|
| RTK(*)  | implementation("com.geoai.mavlink.rtk:mavsdk:{version}")                   |![Sonatype Nexus (Releases)](https://img.shields.io/nexus/r/com.geoai.mavlink.rtk/mavsdk?server=http%3A%2F%2F121.37.203.145%3A8081%2F&nexusVersion=3&label=rtk)|
| 增强图传(*) | implementation("com.geoai.mavlink.agora:mavsdk:{version}")                   |![Sonatype Nexus (Releases)](https://img.shields.io/nexus/r/com.geoai.mavlink.agora/mavsdk?server=http%3A%2F%2F121.37.203.145%3A8081%2F&nexusVersion=3&label=cellular)|
| 上云(*) | implementation("com.geoai.mavlink.clouds:mavsdk:{version}")                   |![Sonatype Nexus (Releases)](https://img.shields.io/nexus/r/com.geoai.mavlink.clouds/mavsdk?server=http%3A%2F%2F121.37.203.145%3A8081%2F&nexusVersion=3&label=clouds)|
| 双目校准 | implementation("com.geoai.mavlink.vision-calibration:mavsdk:{version}")    |![Sonatype Nexus (Releases)](https://img.shields.io/nexus/r/com.geoai.mavlink.vision-calibration/mavsdk?server=http%3A%2F%2F121.37.203.145%3A8081%2F&nexusVersion=3&label=vision-calibration)|
| GEOAI播放器  | implementation("com.geoai.mavlink.videoplayer-geoairtsp:mavsdk:{version}") |![Sonatype Nexus (Releases)](https://img.shields.io/nexus/r/com.geoai.mavlink.videoplayer-geoairtsp/mavsdk?server=http%3A%2F%2F121.37.203.145%3A8081%2F&nexusVersion=3&label=geoai-player)|
| EasyPlayer | implementation("com.geoai.mavlink.videoplayer-easyrtsp:mavsdk:{version}")                   |![Sonatype Nexus (Releases)](https://img.shields.io/nexus/r/com.geoai.mavlink.videoplayer-easyrtsp/mavsdk?server=http%3A%2F%2F121.37.203.145%3A8081%2F&nexusVersion=3&label=easy-player)|
| YF播放器 | implementation("com.geoai.mavlink.yfplayer:mavsdk:{version}")                   |![Sonatype Nexus (Releases)](https://img.shields.io/nexus/r/com.geoai.mavlink.yfplayer/mavsdk?server=http%3A%2F%2F121.37.203.145%3A8081%2F&nexusVersion=3&label=yfplayer)|
| APM  | implementation("com.geoai.mavlink.apm:mavsdk:{version}")                   |暂不开放|

> MSDK主工程已涵盖 * 所示依赖。

---

## · InnoFlight MSDK是什么？


InnoFlight Mobile Software Development Kit，也就是移动软件开发套件。旨在帮助开发者以最简单、最直观的方式获取和控制无人机的特性。MSDK可以大幅减少开发者的开发负担，让使用方能更专注于创新性的应用开发。

开发套件为开发者提供了访问无人机传感器、硬件控制等核心功能的API，允许开发者获取无人机的实时位置、飞行速度和高度等信息，同时控制无人机的飞行行为、相机设置，甚至设定自动飞行路径。也就是说，开发者可以对无人机进行全方位的操作、监控和数据采集。

MSDK相当于无人机与应用开发者之间的桥梁，暗藏无尽可能性。无论开发者是想创建一款无人机应用以进行固定路线巡查，还是用于扩展现有的软件以增加无人机操作的功能，MSDK的灵活性和广泛控制能力都能满足他们的需求。

---

## · 当前版本MSDK支持的设备：
* [InnoFlight C60](https://geoai.com/innoflight)
* [GEOAI SS125](https://geoai.com/Mount-SS125.html)
* [GEOAI iCrest](https://geoai.com/yunguan.html)
* [InnoFlight C60-V2(即将支持)](https://geoai.com/innoflight)
* [InnoFlight C70(即将支持)](https://geoai.com/innoflight)

---

## · 如何使用MSDK？

#### 1.使用推荐环境
* Android Studio：`Android Studio Giraffe 2022.3.1`
* Java Runtime：`17` or `11` or `8`
* Kotlin：`1.9.0`
* Gradle：`8.1.3`
* Android Gradle Plugin：`8.3.0`
* minSdkVersion：`24`
* targetSdkVersion：`34`
> 如需使用非推荐版本，请根据实际版本自行适配整个集成过程。Android Studio Giraffe 2022.3.1 默认集成 Java Runtime 17，一般可以直接使用即可，无需做任何配置。

#### 2.修改setting.gradle.kts(Project)文件
打开 <code>Gradle Scripts</code> 下的 <code>setting.gradle.kts (Project)</code>，按照如下代码修改内容，这段代码的作用是提供正确的引用库。
```kotlin
...
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        maven("http://msdkdoc.innoflyght.cn:8081/repository/geoai-sonatype/") {
            isAllowInsecureProtocol = true
        }
    }
}
...
```

#### 3.修改build.gradle.kts(Module)文件
```
...

dependencies {
 //远程依赖-核心库
 implementation("com.geoai.mavlink.msdk-provided:mavsdk:[VERSION]")
 //远程依赖-播放器
 implementation("com.geoai.mavlink.videoplayer-geoairtsp:mavsdk:[VERSION]")
}
...
```
---

## · 运行示例程序
> 当前基于MSDK程序提供了两套开源示例工程，可用于学习MSDK和进行相关功能验证。

· [Sample Code](https://gitee.com/qw1357802/innoflight-msdk/tree/master/SampleCode)：MSDKAPI接口能力调用示例程序，提供高效快捷的接口调用方式。可用于接口能力验证，方案测试等。  
· [InnoFlight GCS](https://gitee.com/qw1357802/innoflight-msdk/tree/master/GCS)：基于MSDK开源的GroundControlSystem程序，基于简单的UI示例用于快速实现接口功能。

---

## · MSDK接口定义
· [InnoFlight MSDK API Documentation](http://ronnyxie.gitee.io/geoai_msdk_docs)  
· [WIKI Document](https://gitee.com/qw1357802/innoflight-msdk/wikis)

---

## · 联系我们
· [Issus](https://gitee.com/qw1357802/innoflight-msdk/issues)  
· [Email](mailto:xiezhijie@geoai.com)  
· 地址:广州越秀区太和岗路20号黄花岗科技园1号楼7楼 
· 联系电话:400-0020910  
· 商务合作:market@geoai.com  
· 技术支持:support@geoai.com